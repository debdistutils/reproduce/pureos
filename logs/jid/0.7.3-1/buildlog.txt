+ date
Tue Apr 18 16:09:54 UTC 2023
+ apt-get source --only-source jid=0.7.3-1
Reading package lists...
NOTICE: 'jid' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/go-team/packages/jid.git
Please use:
git clone https://salsa.debian.org/go-team/packages/jid.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 21.2 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main jid 0.7.3-1 (dsc) [2158 B]
Get:2 http://repo.pureos.net/pureos byzantium/main jid 0.7.3-1 (tar) [15.8 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main jid 0.7.3-1 (diff) [3244 B]
dpkg-source: info: extracting jid in jid-0.7.3
dpkg-source: info: unpacking jid_0.7.3.orig.tar.gz
dpkg-source: info: unpacking jid_0.7.3-1.debian.tar.xz
Fetched 21.2 kB in 0s (134 kB/s)
W: Download is performed unsandboxed as root as file 'jid_0.7.3-1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source jid=0.7.3-1
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  asciidoc-base asciidoc-common dh-golang docbook-xsl golang-1.15-go
  golang-1.15-src golang-github-bitly-go-simplejson-dev
  golang-github-bmizerany-assert-dev golang-github-creack-pty-dev
  golang-github-davecgh-go-spew-dev golang-github-fatih-color-dev
  golang-github-kr-pretty-dev golang-github-kr-text-dev
  golang-github-mattn-go-colorable-dev golang-github-mattn-go-isatty-dev
  golang-github-mattn-go-runewidth-dev golang-github-nsf-termbox-go-dev
  golang-github-nwidger-jsoncolor-dev golang-github-pkg-errors-dev
  golang-github-pmezard-go-difflib-dev golang-github-stretchr-objx-dev
  golang-github-stretchr-testify-dev golang-go golang-golang-x-sys-dev
  golang-gopkg-yaml.v3-dev golang-src libxml2-utils xmlto xsltproc
0 upgraded, 29 newly installed, 0 to remove and 0 not upgraded.
Need to get 63.9 MB of archives.
After this operation, 384 MB of additional disk space will be used.
Get:1 http://repo.pureos.net/pureos byzantium/main amd64 asciidoc-common all 9.0.0~rc2-1 [286 kB]
Get:2 http://repo.pureos.net/pureos byzantium-security/main amd64 libxml2-utils amd64 2.9.10+dfsg-6.7+deb11u3 [109 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main amd64 xsltproc amd64 1.1.34-4+deb11u1 [124 kB]
Get:4 http://repo.pureos.net/pureos byzantium/main amd64 asciidoc-base all 9.0.0~rc2-1 [135 kB]
Get:5 http://repo.pureos.net/pureos byzantium/main amd64 dh-golang all 1.51 [24.6 kB]
Get:6 http://repo.pureos.net/pureos byzantium/main amd64 docbook-xsl all 1.79.2+dfsg-1 [1237 kB]
Get:7 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-src amd64 1.15.15-1~deb11u4 [13.9 MB]
Get:8 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-go amd64 1.15.15-1~deb11u4 [47.4 MB]
Get:9 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-creack-pty-dev all 1.1.11-1 [8380 B]
Get:10 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-kr-text-dev all 0.2.0-1 [11.1 kB]
Get:11 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-kr-pretty-dev all 0.2.1+git20200831.59b4212-1 [14.6 kB]
Get:12 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-bmizerany-assert-dev all 0.0~git20120716-4 [4068 B]
Get:13 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-bitly-go-simplejson-dev all 0.5.0-5 [7260 B]
Get:14 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-davecgh-go-spew-dev all 1.1.1-2 [29.7 kB]
Get:15 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-sys-dev all 0.0~git20210124.22da62e-1 [308 kB]
Get:16 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-mattn-go-isatty-dev all 0.0.12-1 [6472 B]
Get:17 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-mattn-go-colorable-dev all 0.1.7-1 [9936 B]
Get:18 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-fatih-color-dev all 1.7.0-1 [11.4 kB]
Get:19 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-mattn-go-runewidth-dev all 0.0.9-1 [14.1 kB]
Get:20 http://repo.pureos.net/pureos byzantium/main amd64 golang-src amd64 2:1.15~1 [4856 B]
Get:21 http://repo.pureos.net/pureos byzantium/main amd64 golang-go amd64 2:1.15~1 [23.9 kB]
Get:22 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-nsf-termbox-go-dev all 0.0~git20160914-3 [26.8 kB]
Get:23 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-nwidger-jsoncolor-dev all 20161209-2 [4732 B]
Get:24 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-pkg-errors-dev all 0.9.1-1 [13.0 kB]
Get:25 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-pmezard-go-difflib-dev all 1.0.0-3 [12.3 kB]
Get:26 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-stretchr-objx-dev all 0.3.0-1 [25.4 kB]
Get:27 http://repo.pureos.net/pureos byzantium/main amd64 golang-gopkg-yaml.v3-dev all 3.0.0~git20200121.a6ecf24-3 [70.5 kB]
Get:28 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-stretchr-testify-dev all 1.6.1-2 [60.4 kB]
Get:29 http://repo.pureos.net/pureos byzantium/main amd64 xmlto amd64 0.0.28-2.1 [33.6 kB]
Fetched 63.9 MB in 2s (30.2 MB/s)
Selecting previously unselected package asciidoc-common.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 102989 files and directories currently installed.)
Preparing to unpack .../00-asciidoc-common_9.0.0~rc2-1_all.deb ...
Unpacking asciidoc-common (9.0.0~rc2-1) ...
Selecting previously unselected package libxml2-utils.
Preparing to unpack .../01-libxml2-utils_2.9.10+dfsg-6.7+deb11u3_amd64.deb ...
Unpacking libxml2-utils (2.9.10+dfsg-6.7+deb11u3) ...
Selecting previously unselected package xsltproc.
Preparing to unpack .../02-xsltproc_1.1.34-4+deb11u1_amd64.deb ...
Unpacking xsltproc (1.1.34-4+deb11u1) ...
Selecting previously unselected package asciidoc-base.
Preparing to unpack .../03-asciidoc-base_9.0.0~rc2-1_all.deb ...
Unpacking asciidoc-base (9.0.0~rc2-1) ...
Selecting previously unselected package dh-golang.
Preparing to unpack .../04-dh-golang_1.51_all.deb ...
Unpacking dh-golang (1.51) ...
Selecting previously unselected package docbook-xsl.
Preparing to unpack .../05-docbook-xsl_1.79.2+dfsg-1_all.deb ...
Unpacking docbook-xsl (1.79.2+dfsg-1) ...
Selecting previously unselected package golang-1.15-src.
Preparing to unpack .../06-golang-1.15-src_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-src (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-1.15-go.
Preparing to unpack .../07-golang-1.15-go_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-go (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-github-creack-pty-dev.
Preparing to unpack .../08-golang-github-creack-pty-dev_1.1.11-1_all.deb ...
Unpacking golang-github-creack-pty-dev (1.1.11-1) ...
Selecting previously unselected package golang-github-kr-text-dev.
Preparing to unpack .../09-golang-github-kr-text-dev_0.2.0-1_all.deb ...
Unpacking golang-github-kr-text-dev (0.2.0-1) ...
Selecting previously unselected package golang-github-kr-pretty-dev.
Preparing to unpack .../10-golang-github-kr-pretty-dev_0.2.1+git20200831.59b4212-1_all.deb ...
Unpacking golang-github-kr-pretty-dev (0.2.1+git20200831.59b4212-1) ...
Selecting previously unselected package golang-github-bmizerany-assert-dev.
Preparing to unpack .../11-golang-github-bmizerany-assert-dev_0.0~git20120716-4_all.deb ...
Unpacking golang-github-bmizerany-assert-dev (0.0~git20120716-4) ...
Selecting previously unselected package golang-github-bitly-go-simplejson-dev.
Preparing to unpack .../12-golang-github-bitly-go-simplejson-dev_0.5.0-5_all.deb ...
Unpacking golang-github-bitly-go-simplejson-dev (0.5.0-5) ...
Selecting previously unselected package golang-github-davecgh-go-spew-dev.
Preparing to unpack .../13-golang-github-davecgh-go-spew-dev_1.1.1-2_all.deb ...
Unpacking golang-github-davecgh-go-spew-dev (1.1.1-2) ...
Selecting previously unselected package golang-golang-x-sys-dev.
Preparing to unpack .../14-golang-golang-x-sys-dev_0.0~git20210124.22da62e-1_all.deb ...
Unpacking golang-golang-x-sys-dev (0.0~git20210124.22da62e-1) ...
Selecting previously unselected package golang-github-mattn-go-isatty-dev.
Preparing to unpack .../15-golang-github-mattn-go-isatty-dev_0.0.12-1_all.deb ...
Unpacking golang-github-mattn-go-isatty-dev (0.0.12-1) ...
Selecting previously unselected package golang-github-mattn-go-colorable-dev.
Preparing to unpack .../16-golang-github-mattn-go-colorable-dev_0.1.7-1_all.deb ...
Unpacking golang-github-mattn-go-colorable-dev (0.1.7-1) ...
Selecting previously unselected package golang-github-fatih-color-dev.
Preparing to unpack .../17-golang-github-fatih-color-dev_1.7.0-1_all.deb ...
Unpacking golang-github-fatih-color-dev (1.7.0-1) ...
Selecting previously unselected package golang-github-mattn-go-runewidth-dev.
Preparing to unpack .../18-golang-github-mattn-go-runewidth-dev_0.0.9-1_all.deb ...
Unpacking golang-github-mattn-go-runewidth-dev (0.0.9-1) ...
Selecting previously unselected package golang-src:amd64.
Preparing to unpack .../19-golang-src_2%3a1.15~1_amd64.deb ...
Unpacking golang-src:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-go.
Preparing to unpack .../20-golang-go_2%3a1.15~1_amd64.deb ...
Unpacking golang-go (2:1.15~1) ...
Selecting previously unselected package golang-github-nsf-termbox-go-dev.
Preparing to unpack .../21-golang-github-nsf-termbox-go-dev_0.0~git20160914-3_all.deb ...
Unpacking golang-github-nsf-termbox-go-dev (0.0~git20160914-3) ...
Selecting previously unselected package golang-github-nwidger-jsoncolor-dev.
Preparing to unpack .../22-golang-github-nwidger-jsoncolor-dev_20161209-2_all.deb ...
Unpacking golang-github-nwidger-jsoncolor-dev (20161209-2) ...
Selecting previously unselected package golang-github-pkg-errors-dev.
Preparing to unpack .../23-golang-github-pkg-errors-dev_0.9.1-1_all.deb ...
Unpacking golang-github-pkg-errors-dev (0.9.1-1) ...
Selecting previously unselected package golang-github-pmezard-go-difflib-dev.
Preparing to unpack .../24-golang-github-pmezard-go-difflib-dev_1.0.0-3_all.deb ...
Unpacking golang-github-pmezard-go-difflib-dev (1.0.0-3) ...
Selecting previously unselected package golang-github-stretchr-objx-dev.
Preparing to unpack .../25-golang-github-stretchr-objx-dev_0.3.0-1_all.deb ...
Unpacking golang-github-stretchr-objx-dev (0.3.0-1) ...
Selecting previously unselected package golang-gopkg-yaml.v3-dev.
Preparing to unpack .../26-golang-gopkg-yaml.v3-dev_3.0.0~git20200121.a6ecf24-3_all.deb ...
Unpacking golang-gopkg-yaml.v3-dev (3.0.0~git20200121.a6ecf24-3) ...
Selecting previously unselected package golang-github-stretchr-testify-dev.
Preparing to unpack .../27-golang-github-stretchr-testify-dev_1.6.1-2_all.deb ...
Unpacking golang-github-stretchr-testify-dev (1.6.1-2) ...
Selecting previously unselected package xmlto.
Preparing to unpack .../28-xmlto_0.0.28-2.1_amd64.deb ...
Unpacking xmlto (0.0.28-2.1) ...
Setting up dh-golang (1.51) ...
Setting up golang-github-mattn-go-runewidth-dev (0.0.9-1) ...
Setting up golang-github-creack-pty-dev (1.1.11-1) ...
Setting up golang-github-stretchr-objx-dev (0.3.0-1) ...
Setting up golang-github-pkg-errors-dev (0.9.1-1) ...
Setting up golang-1.15-src (1.15.15-1~deb11u4) ...
Setting up golang-gopkg-yaml.v3-dev (3.0.0~git20200121.a6ecf24-3) ...
Setting up golang-golang-x-sys-dev (0.0~git20210124.22da62e-1) ...
Setting up xsltproc (1.1.34-4+deb11u1) ...
Setting up golang-github-pmezard-go-difflib-dev (1.0.0-3) ...
Setting up golang-github-nwidger-jsoncolor-dev (20161209-2) ...
Setting up asciidoc-common (9.0.0~rc2-1) ...
Setting up golang-github-davecgh-go-spew-dev (1.1.1-2) ...
Setting up golang-github-kr-text-dev (0.2.0-1) ...
Setting up golang-github-stretchr-testify-dev (1.6.1-2) ...
Setting up docbook-xsl (1.79.2+dfsg-1) ...
Setting up libxml2-utils (2.9.10+dfsg-6.7+deb11u3) ...
Setting up golang-src:amd64 (2:1.15~1) ...
Setting up golang-1.15-go (1.15.15-1~deb11u4) ...
Setting up golang-github-kr-pretty-dev (0.2.1+git20200831.59b4212-1) ...
Setting up golang-github-mattn-go-isatty-dev (0.0.12-1) ...
Setting up golang-go (2:1.15~1) ...
Setting up golang-github-mattn-go-colorable-dev (0.1.7-1) ...
Setting up asciidoc-base (9.0.0~rc2-1) ...
Setting up golang-github-nsf-termbox-go-dev (0.0~git20160914-3) ...
Setting up golang-github-bmizerany-assert-dev (0.0~git20120716-4) ...
Setting up golang-github-bitly-go-simplejson-dev (0.5.0-5) ...
Setting up golang-github-fatih-color-dev (1.7.0-1) ...
Processing triggers for sgml-base (1.30) ...
Processing triggers for man-db (2.9.4-2) ...
Setting up xmlto (0.0.28-2.1) ...
+ find . -maxdepth 1 -name jid* -type d
+ cd ./jid-0.7.3
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package jid
dpkg-buildpackage: info: source version 0.7.3-1
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by ChangZhuo Chen (陳昌倬) <czchen@debian.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --buildsystem=golang --with=golang
   dh_auto_clean -O--buildsystem=golang
   dh_autoreconf_clean -O--buildsystem=golang
   dh_clean -O--buildsystem=golang
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building jid using existing ./jid_0.7.3.orig.tar.gz
dpkg-source: info: building jid in jid_0.7.3-1.debian.tar.xz
dpkg-source: info: building jid in jid_0.7.3-1.dsc
 debian/rules build
dh build --buildsystem=golang --with=golang
   dh_update_autotools_config -O--buildsystem=golang
   dh_autoreconf -O--buildsystem=golang
   debian/rules override_dh_auto_configure
make[1]: Entering directory '/build/jid/jid-0.7.3'
cat debian/*manpages | sed 's/$/.txt/p' | xargs -n 1 a2x --doctype manpage --format manpage
dh_auto_configure
make[1]: Leaving directory '/build/jid/jid-0.7.3'
   dh_auto_build -O--buildsystem=golang
	cd obj-x86_64-linux-gnu && go install -trimpath -v -p 50 github.com/simeji/jid github.com/simeji/jid/cmd/jid
internal/unsafeheader
runtime/internal/sys
unicode/utf8
internal/race
encoding
unicode/utf16
math/bits
golang.org/x/sys/internal/unsafeheader
sync/atomic
runtime/internal/atomic
internal/cpu
unicode
runtime/internal/math
internal/testlog
internal/bytealg
math
runtime
internal/reflectlite
sync
errors
sort
internal/oserror
io
strconv
syscall
bytes
strings
reflect
path
regexp/syntax
internal/syscall/execenv
internal/syscall/unix
time
regexp
internal/poll
internal/fmtsort
encoding/binary
os
encoding/base64
golang.org/x/sys/unix
github.com/mattn/go-runewidth
os/signal
path/filepath
fmt
io/ioutil
log
encoding/hex
github.com/pkg/errors
flag
encoding/json
github.com/nsf/termbox-go
github.com/mattn/go-isatty
github.com/mattn/go-colorable
github.com/fatih/color
github.com/bitly/go-simplejson
github.com/nwidger/jsoncolor
github.com/simeji/jid
github.com/simeji/jid/cmd/jid
   dh_auto_test -O--buildsystem=golang
	cd obj-x86_64-linux-gnu && go test -vet=off -v -p 50 github.com/simeji/jid github.com/simeji/jid/cmd/jid
=== RUN   TestNewEngine
--- PASS: TestNewEngine (0.00s)
=== RUN   TestNewEngineWithQuery
--- PASS: TestNewEngineWithQuery (0.00s)
=== RUN   TestDeleteChar
--- PASS: TestDeleteChar (0.00s)
=== RUN   TestDeleteWordBackward
--- PASS: TestDeleteWordBackward (0.00s)
=== RUN   TestDeleteLineQuery
--- PASS: TestDeleteLineQuery (0.00s)
=== RUN   TestScrollToAbove
--- PASS: TestScrollToAbove (0.00s)
=== RUN   TestScrollToBelow
--- PASS: TestScrollToBelow (0.00s)
=== RUN   TestScrollToBottomAndTop
--- PASS: TestScrollToBottomAndTop (0.00s)
=== RUN   TestGetContents
--- PASS: TestGetContents (0.00s)
=== RUN   TestSetCandidateData
--- PASS: TestSetCandidateData (0.00s)
=== RUN   TestConfirmCandidate
--- PASS: TestConfirmCandidate (0.00s)
=== RUN   TestCtrllAction
--- PASS: TestCtrllAction (0.00s)
=== RUN   TestTabAction
--- PASS: TestTabAction (0.00s)
=== RUN   TestEscAction
--- PASS: TestEscAction (0.00s)
=== RUN   TestInputChar
--- PASS: TestInputChar (0.00s)
=== RUN   TestMoveCursorForwardAndBackward
--- PASS: TestMoveCursorForwardAndBackward (0.00s)
=== RUN   TestMoveCursorToTopAndEnd
--- PASS: TestMoveCursorToTopAndEnd (0.00s)
=== RUN   TestNewJson
--- PASS: TestNewJson (0.00s)
=== RUN   TestNewJsonWithError
--- PASS: TestNewJsonWithError (0.00s)
=== RUN   TestGet
--- PASS: TestGet (0.00s)
=== RUN   TestGetPretty
--- PASS: TestGetPretty (0.00s)
=== RUN   TestGetItem
--- PASS: TestGetItem (0.00s)
=== RUN   TestGetFilteredData
--- PASS: TestGetFilteredData (0.00s)
=== RUN   TestGetFilteredDataWithMatchQuery
--- PASS: TestGetFilteredDataWithMatchQuery (0.00s)
=== RUN   TestGetCandidateKeys
--- PASS: TestGetCandidateKeys (0.00s)
=== RUN   TestGetCurrentKeys
--- PASS: TestGetCurrentKeys (0.00s)
=== RUN   TestIsEmptyJson
--- PASS: TestIsEmptyJson (0.00s)
=== RUN   TestValidate
--- PASS: TestValidate (0.00s)
=== RUN   TestNewQuery
--- PASS: TestNewQuery (0.00s)
=== RUN   TestNewQueryWithInvalidQuery
--- PASS: TestNewQueryWithInvalidQuery (0.00s)
=== RUN   TestNewQueryWithString
--- PASS: TestNewQueryWithString (0.00s)
=== RUN   TestNewQueryWithStringWithInvalidQuery
--- PASS: TestNewQueryWithStringWithInvalidQuery (0.00s)
=== RUN   TestQueryGet
--- PASS: TestQueryGet (0.00s)
=== RUN   TestQueryLength
--- PASS: TestQueryLength (0.00s)
=== RUN   TestQueryIndexOffsetN
--- PASS: TestQueryIndexOffsetN (0.00s)
=== RUN   TestQueryGetChar
--- PASS: TestQueryGetChar (0.00s)
=== RUN   TestQuerySet
--- PASS: TestQuerySet (0.00s)
=== RUN   TestQuerySetWithInvalidQuery
--- PASS: TestQuerySetWithInvalidQuery (0.00s)
=== RUN   TestQueryAdd
--- PASS: TestQueryAdd (0.00s)
=== RUN   TestQueryInsert
--- PASS: TestQueryInsert (0.00s)
=== RUN   TestQueryStringInsert
--- PASS: TestQueryStringInsert (0.00s)
=== RUN   TestQueryClear
--- PASS: TestQueryClear (0.00s)
=== RUN   TestQueryDelete
--- PASS: TestQueryDelete (0.00s)
=== RUN   TestGetKeywords
--- PASS: TestGetKeywords (0.00s)
=== RUN   TestGetLastKeyword
--- PASS: TestGetLastKeyword (0.00s)
=== RUN   TestStringGetLastKeyword
--- PASS: TestStringGetLastKeyword (0.00s)
=== RUN   TestPopKeyword
--- PASS: TestPopKeyword (0.00s)
=== RUN   TestQueryStringGet
--- PASS: TestQueryStringGet (0.00s)
=== RUN   TestQueryStringSet
--- PASS: TestQueryStringSet (0.00s)
=== RUN   TestQueryStringAdd
--- PASS: TestQueryStringAdd (0.00s)
=== RUN   TestStringGetKeywords
--- PASS: TestStringGetKeywords (0.00s)
=== RUN   TestStringPopKeyword
--- PASS: TestStringPopKeyword (0.00s)
=== RUN   TestNewSuggestion
--- PASS: TestNewSuggestion (0.00s)
=== RUN   TestSuggestionGet
--- PASS: TestSuggestionGet (0.00s)
=== RUN   TestSuggestionGetCurrentType
--- PASS: TestSuggestionGetCurrentType (0.00s)
=== RUN   TestSuggestionGetCandidateKeys
--- PASS: TestSuggestionGetCandidateKeys (0.00s)
PASS
ok  	github.com/simeji/jid	0.031s
=== RUN   TestJidRun
{"test":"result"}.querystring{"test":"result"}--- PASS: TestJidRun (0.00s)
=== RUN   TestJidRunWithError
--- PASS: TestJidRunWithError (0.00s)
PASS
ok  	github.com/simeji/jid/cmd/jid	0.010s
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary --buildsystem=golang --with=golang
   dh_testroot -O--buildsystem=golang
   dh_prep -O--buildsystem=golang
   dh_auto_install -O--buildsystem=golang
	cd obj-x86_64-linux-gnu && mkdir -p /build/jid/jid-0.7.3/debian/jid/usr
	cd obj-x86_64-linux-gnu && cp -r bin /build/jid/jid-0.7.3/debian/jid/usr
   dh_installdocs -O--buildsystem=golang
   dh_installchangelogs -O--buildsystem=golang
   dh_installman -O--buildsystem=golang
   dh_installinit -O--buildsystem=golang
   dh_perl -O--buildsystem=golang
   dh_link -O--buildsystem=golang
   dh_strip_nondeterminism -O--buildsystem=golang
   dh_compress -O--buildsystem=golang
   dh_fixperms -O--buildsystem=golang
   dh_missing -O--buildsystem=golang
   dh_strip -O--buildsystem=golang
dh_strip: warning: Could not find the BuildID in debian/jid/usr/bin/jid
   dh_makeshlibs -O--buildsystem=golang
   dh_shlibdeps -O--buildsystem=golang
   dh_installdeb -O--buildsystem=golang
   dh_golang -O--buildsystem=golang
   dh_gencontrol -O--buildsystem=golang
dpkg-gencontrol: warning: Depends field of package jid: substitution variable ${shlibs:Depends} used, but is not defined
   dh_md5sums -O--buildsystem=golang
   dh_builddeb -O--buildsystem=golang
dpkg-deb: building package 'jid' in '../jid_0.7.3-1_amd64.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../jid_0.7.3-1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload (original source is included)
+ date
Tue Apr 18 16:10:45 UTC 2023
+ cd ..
+ ls -la
total 840
drwxr-xr-x 3 root root   4096 Apr 18 16:10 .
drwxr-xr-x 3 root root   4096 Apr 18 16:09 ..
-rw-r--r-- 1 root root  21517 Apr 18 16:10 buildlog.txt
drwxr-xr-x 7 root root   4096 Apr 18 16:10 jid-0.7.3
-rw-r--r-- 1 root root   3248 Apr 18 16:10 jid_0.7.3-1.debian.tar.xz
-rw-r--r-- 1 root root   1275 Apr 18 16:10 jid_0.7.3-1.dsc
-rw-r--r-- 1 root root   6973 Apr 18 16:10 jid_0.7.3-1_amd64.buildinfo
-rw-r--r-- 1 root root   1844 Apr 18 16:10 jid_0.7.3-1_amd64.changes
-rw-r--r-- 1 root root 785572 Apr 18 16:10 jid_0.7.3-1_amd64.deb
-rw-r--r-- 1 root root  15811 Feb 11  2019 jid_0.7.3.orig.tar.gz
+ find . -maxdepth 1 -type f
+ sha256sum ./jid_0.7.3-1_amd64.buildinfo ./jid_0.7.3.orig.tar.gz ./buildlog.txt ./jid_0.7.3-1_amd64.deb ./jid_0.7.3-1_amd64.changes ./jid_0.7.3-1.debian.tar.xz ./jid_0.7.3-1.dsc
8962b084d7feff3b88d2d396bc04797a33c575a621cee1b5ff6e9f667108301b  ./jid_0.7.3-1_amd64.buildinfo
4565030bdf22f831007ecc214fbd14205855b3dff98f7acc8dc6e1f7a2591820  ./jid_0.7.3.orig.tar.gz
a96ffcf1dba870252129d54c5ded5de993c0f5cc85593fc3c3b43d82260c6b0d  ./buildlog.txt
f1de0e07f419fd996b4536c577059c9fc67176f576cd58feca514afc725454c8  ./jid_0.7.3-1_amd64.deb
67dba3fb02b30e702b1833186f65e3d7885f863035ed7e0a0a12174ce95f2316  ./jid_0.7.3-1_amd64.changes
f94194b9b409ee047b6d3f8a1272433add598160422d9da4becbfe313cf59120  ./jid_0.7.3-1.debian.tar.xz
0ba5e553696ce547a72450a5c49d4cb517a9f28b02e4c78bcc923bf1b092f1af  ./jid_0.7.3-1.dsc
+ mkdir published
+ cd published
+ cd ../
+ ls jid_0.7.3-1_amd64.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/j/jid/jid_0.7.3-1_amd64.deb
--2023-04-18 16:10:45--  http://repo.pureos.net/pureos/pool/main/j/jid/jid_0.7.3-1_amd64.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 758488 (741K) [application/octet-stream]
Saving to: 'jid_0.7.3-1_amd64.deb'

     0K .......... .......... .......... .......... ..........  6%  950K 1s
    50K .......... .......... .......... .......... .......... 13% 1.85M 1s
   100K .......... .......... .......... .......... .......... 20% 17.1M 0s
   150K .......... .......... .......... .......... .......... 27% 23.5M 0s
   200K .......... .......... .......... .......... .......... 33% 2.05M 0s
   250K .......... .......... .......... .......... .......... 40% 15.8M 0s
   300K .......... .......... .......... .......... .......... 47% 16.2M 0s
   350K .......... .......... .......... .......... .......... 54% 14.1M 0s
   400K .......... .......... .......... .......... .......... 60% 3.21M 0s
   450K .......... .......... .......... .......... .......... 67% 15.9M 0s
   500K .......... .......... .......... .......... .......... 74% 15.7M 0s
   550K .......... .......... .......... .......... .......... 81% 13.5M 0s
   600K .......... .......... .......... .......... .......... 87% 16.2M 0s
   650K .......... .......... .......... .......... .......... 94% 11.7M 0s
   700K .......... .......... .......... ..........           100% 16.0M=0.2s

2023-04-18 16:10:45 (4.76 MB/s) - 'jid_0.7.3-1_amd64.deb' saved [758488/758488]

+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./jid_0.7.3-1_amd64.deb
560830ff1239b842a337a0669cbbd879f1862521236162cb9af9dac161818425  ./jid_0.7.3-1_amd64.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./jid_0.7.3-1_amd64.deb: FAILED
sha256sum: WARNING: 1 computed checksum did NOT match
