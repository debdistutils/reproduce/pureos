+ date
Sun May 21 08:59:46 UTC 2023
+ id
uid=0(root) gid=0(root) groups=0(root)
+ apt-get source --only-source shim-signed=1.38
Reading package lists...
NOTICE: 'shim-signed' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/efi-team/shim-signed.git
Please use:
git clone https://salsa.debian.org/efi-team/shim-signed.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 562 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main shim-signed 1.38 (dsc) [1808 B]
Get:2 http://repo.pureos.net/pureos byzantium/main shim-signed 1.38 (tar) [560 kB]
dpkg-source: info: extracting shim-signed in shim-signed-1.38
dpkg-source: info: unpacking shim-signed_1.38.tar.xz
Fetched 562 kB in 0s (1559 kB/s)
W: Download is performed unsandboxed as root as file 'shim-signed_1.38.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source shim-signed=1.38
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
Some packages could not be installed. This may mean that you have
requested an impossible situation or if you are using the unstable
distribution that some required packages have not yet been created
or been moved out of Incoming.
The following information may help to resolve the situation:

The following packages have unmet dependencies:
 builddeps:shim-signed : Depends: shim-unsigned (= 15.4-7) but it is not going to be installed
E: Unable to correct problems, you have held broken packages.
