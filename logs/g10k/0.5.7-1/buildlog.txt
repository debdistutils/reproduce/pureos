+ date
Tue Apr 18 06:05:58 UTC 2023
+ apt-get source --only-source g10k=0.5.7-1
Reading package lists...
NOTICE: 'g10k' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/go-team/packages/g10k.git
Please use:
git clone https://salsa.debian.org/go-team/packages/g10k.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 379 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main g10k 0.5.7-1 (dsc) [2369 B]
Get:2 http://repo.pureos.net/pureos byzantium/main g10k 0.5.7-1 (tar) [366 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main g10k 0.5.7-1 (diff) [10.7 kB]
dpkg-source: info: extracting g10k in g10k-0.5.7
dpkg-source: info: unpacking g10k_0.5.7.orig.tar.xz
dpkg-source: info: unpacking g10k_0.5.7-1.debian.tar.xz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: applying patch_disable-tests-with-internet.diff
Fetched 379 kB in 0s (1463 kB/s)
W: Download is performed unsandboxed as root as file 'g10k_0.5.7-1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source g10k=0.5.7-1
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  dh-golang golang-1.15-go golang-1.15-src golang-any
  golang-github-cespare-xxhash-dev golang-github-davecgh-go-spew-dev
  golang-github-fatih-color-dev golang-github-gosuri-uilive-dev
  golang-github-gosuri-uiprogress-dev golang-github-kballard-go-shellquote-dev
  golang-github-klauspost-compress-dev golang-github-klauspost-pgzip-dev
  golang-github-mattn-go-colorable-dev golang-github-mattn-go-isatty-dev
  golang-github-remeh-sizedwaitgroup-dev golang-github-tidwall-gjson-dev
  golang-github-tidwall-match-dev golang-github-tidwall-pretty-dev
  golang-github-xorpaul-uiprogress-dev golang-go golang-golang-x-crypto-dev
  golang-golang-x-net-dev golang-golang-x-sys-dev golang-golang-x-term-dev
  golang-golang-x-text-dev golang-gopkg-yaml.v2-dev golang-src
  libcmark-gfm-extensions0 libcmark-gfm0 pandoc pandoc-data
0 upgraded, 31 newly installed, 0 to remove and 0 not upgraded.
Need to get 101 MB of archives.
After this operation, 593 MB of additional disk space will be used.
Get:1 http://repo.pureos.net/pureos byzantium/main amd64 dh-golang all 1.51 [24.6 kB]
Get:2 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-src amd64 1.15.15-1~deb11u4 [13.9 MB]
Get:3 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-go amd64 1.15.15-1~deb11u4 [47.4 MB]
Get:4 http://repo.pureos.net/pureos byzantium/main amd64 golang-src amd64 2:1.15~1 [4856 B]
Get:5 http://repo.pureos.net/pureos byzantium/main amd64 golang-go amd64 2:1.15~1 [23.9 kB]
Get:6 http://repo.pureos.net/pureos byzantium/main amd64 golang-any amd64 2:1.15~1 [4976 B]
Get:7 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-davecgh-go-spew-dev all 1.1.1-2 [29.7 kB]
Get:8 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-sys-dev all 0.0~git20210124.22da62e-1 [308 kB]
Get:9 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-mattn-go-isatty-dev all 0.0.12-1 [6472 B]
Get:10 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-mattn-go-colorable-dev all 0.1.7-1 [9936 B]
Get:11 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-fatih-color-dev all 1.7.0-1 [11.4 kB]
Get:12 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-gosuri-uilive-dev all 0.0~git20170323.ac356e6-1.1 [5020 B]
Get:13 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-gosuri-uiprogress-dev all 0.0~git20170224.d0567a9-1.1 [7668 B]
Get:14 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-kballard-go-shellquote-dev all 0.0~git20180428.95032a8-1 [6472 B]
Get:15 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-cespare-xxhash-dev all 2.1.1-1 [8748 B]
Get:16 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-klauspost-compress-dev all 1.11.7-2 [14.2 MB]
Get:17 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-klauspost-pgzip-dev all 1.2.5-1 [118 kB]
Get:18 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-remeh-sizedwaitgroup-dev all 0.0~git20180822.5e7302b-1.1 [3836 B]
Get:19 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-tidwall-match-dev all 1.0.3-1 [5860 B]
Get:20 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-tidwall-pretty-dev all 1.0.5-1 [9552 B]
Get:21 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-tidwall-gjson-dev all 1.6.7-1 [27.7 kB]
Get:22 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-xorpaul-uiprogress-dev all 0.0~git20170224.d0567a9-1.1 [7700 B]
Get:23 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-text-dev all 0.3.6-1 [3857 kB]
Get:24 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-net-dev all 1:0.0+git20210119.5f4716e+dfsg-4 [659 kB]
Get:25 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-term-dev all 0.0~git20201210.2321bbc-1 [14.5 kB]
Get:26 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-crypto-dev all 1:0.0~git20201221.eec23a3-1 [1538 kB]
Get:27 http://repo.pureos.net/pureos byzantium/main amd64 golang-gopkg-yaml.v2-dev all 2.4.0-1 [61.3 kB]
Get:28 http://repo.pureos.net/pureos byzantium/main amd64 libcmark-gfm0 amd64 0.29.0.gfm.0-6 [117 kB]
Get:29 http://repo.pureos.net/pureos byzantium/main amd64 libcmark-gfm-extensions0 amd64 0.29.0.gfm.0-6 [46.1 kB]
Get:30 http://repo.pureos.net/pureos byzantium/main amd64 pandoc-data all 2.9.2.1-1 [376 kB]
Get:31 http://repo.pureos.net/pureos byzantium/main amd64 pandoc amd64 2.9.2.1-1+b1 [18.5 MB]
Fetched 101 MB in 4s (24.7 MB/s)
Selecting previously unselected package dh-golang.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 102989 files and directories currently installed.)
Preparing to unpack .../00-dh-golang_1.51_all.deb ...
Unpacking dh-golang (1.51) ...
Selecting previously unselected package golang-1.15-src.
Preparing to unpack .../01-golang-1.15-src_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-src (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-1.15-go.
Preparing to unpack .../02-golang-1.15-go_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-go (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-src:amd64.
Preparing to unpack .../03-golang-src_2%3a1.15~1_amd64.deb ...
Unpacking golang-src:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-go.
Preparing to unpack .../04-golang-go_2%3a1.15~1_amd64.deb ...
Unpacking golang-go (2:1.15~1) ...
Selecting previously unselected package golang-any:amd64.
Preparing to unpack .../05-golang-any_2%3a1.15~1_amd64.deb ...
Unpacking golang-any:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-github-davecgh-go-spew-dev.
Preparing to unpack .../06-golang-github-davecgh-go-spew-dev_1.1.1-2_all.deb ...
Unpacking golang-github-davecgh-go-spew-dev (1.1.1-2) ...
Selecting previously unselected package golang-golang-x-sys-dev.
Preparing to unpack .../07-golang-golang-x-sys-dev_0.0~git20210124.22da62e-1_all.deb ...
Unpacking golang-golang-x-sys-dev (0.0~git20210124.22da62e-1) ...
Selecting previously unselected package golang-github-mattn-go-isatty-dev.
Preparing to unpack .../08-golang-github-mattn-go-isatty-dev_0.0.12-1_all.deb ...
Unpacking golang-github-mattn-go-isatty-dev (0.0.12-1) ...
Selecting previously unselected package golang-github-mattn-go-colorable-dev.
Preparing to unpack .../09-golang-github-mattn-go-colorable-dev_0.1.7-1_all.deb ...
Unpacking golang-github-mattn-go-colorable-dev (0.1.7-1) ...
Selecting previously unselected package golang-github-fatih-color-dev.
Preparing to unpack .../10-golang-github-fatih-color-dev_1.7.0-1_all.deb ...
Unpacking golang-github-fatih-color-dev (1.7.0-1) ...
Selecting previously unselected package golang-github-gosuri-uilive-dev.
Preparing to unpack .../11-golang-github-gosuri-uilive-dev_0.0~git20170323.ac356e6-1.1_all.deb ...
Unpacking golang-github-gosuri-uilive-dev (0.0~git20170323.ac356e6-1.1) ...
Selecting previously unselected package golang-github-gosuri-uiprogress-dev.
Preparing to unpack .../12-golang-github-gosuri-uiprogress-dev_0.0~git20170224.d0567a9-1.1_all.deb ...
Unpacking golang-github-gosuri-uiprogress-dev (0.0~git20170224.d0567a9-1.1) ...
Selecting previously unselected package golang-github-kballard-go-shellquote-dev.
Preparing to unpack .../13-golang-github-kballard-go-shellquote-dev_0.0~git20180428.95032a8-1_all.deb ...
Unpacking golang-github-kballard-go-shellquote-dev (0.0~git20180428.95032a8-1) ...
Selecting previously unselected package golang-github-cespare-xxhash-dev.
Preparing to unpack .../14-golang-github-cespare-xxhash-dev_2.1.1-1_all.deb ...
Unpacking golang-github-cespare-xxhash-dev (2.1.1-1) ...
Selecting previously unselected package golang-github-klauspost-compress-dev.
Preparing to unpack .../15-golang-github-klauspost-compress-dev_1.11.7-2_all.deb ...
Unpacking golang-github-klauspost-compress-dev (1.11.7-2) ...
Selecting previously unselected package golang-github-klauspost-pgzip-dev.
Preparing to unpack .../16-golang-github-klauspost-pgzip-dev_1.2.5-1_all.deb ...
Unpacking golang-github-klauspost-pgzip-dev (1.2.5-1) ...
Selecting previously unselected package golang-github-remeh-sizedwaitgroup-dev.
Preparing to unpack .../17-golang-github-remeh-sizedwaitgroup-dev_0.0~git20180822.5e7302b-1.1_all.deb ...
Unpacking golang-github-remeh-sizedwaitgroup-dev (0.0~git20180822.5e7302b-1.1) ...
Selecting previously unselected package golang-github-tidwall-match-dev.
Preparing to unpack .../18-golang-github-tidwall-match-dev_1.0.3-1_all.deb ...
Unpacking golang-github-tidwall-match-dev (1.0.3-1) ...
Selecting previously unselected package golang-github-tidwall-pretty-dev.
Preparing to unpack .../19-golang-github-tidwall-pretty-dev_1.0.5-1_all.deb ...
Unpacking golang-github-tidwall-pretty-dev (1.0.5-1) ...
Selecting previously unselected package golang-github-tidwall-gjson-dev.
Preparing to unpack .../20-golang-github-tidwall-gjson-dev_1.6.7-1_all.deb ...
Unpacking golang-github-tidwall-gjson-dev (1.6.7-1) ...
Selecting previously unselected package golang-github-xorpaul-uiprogress-dev.
Preparing to unpack .../21-golang-github-xorpaul-uiprogress-dev_0.0~git20170224.d0567a9-1.1_all.deb ...
Unpacking golang-github-xorpaul-uiprogress-dev (0.0~git20170224.d0567a9-1.1) ...
Selecting previously unselected package golang-golang-x-text-dev.
Preparing to unpack .../22-golang-golang-x-text-dev_0.3.6-1_all.deb ...
Unpacking golang-golang-x-text-dev (0.3.6-1) ...
Selecting previously unselected package golang-golang-x-net-dev.
Preparing to unpack .../23-golang-golang-x-net-dev_1%3a0.0+git20210119.5f4716e+dfsg-4_all.deb ...
Unpacking golang-golang-x-net-dev (1:0.0+git20210119.5f4716e+dfsg-4) ...
Selecting previously unselected package golang-golang-x-term-dev.
Preparing to unpack .../24-golang-golang-x-term-dev_0.0~git20201210.2321bbc-1_all.deb ...
Unpacking golang-golang-x-term-dev (0.0~git20201210.2321bbc-1) ...
Selecting previously unselected package golang-golang-x-crypto-dev.
Preparing to unpack .../25-golang-golang-x-crypto-dev_1%3a0.0~git20201221.eec23a3-1_all.deb ...
Unpacking golang-golang-x-crypto-dev (1:0.0~git20201221.eec23a3-1) ...
Selecting previously unselected package golang-gopkg-yaml.v2-dev.
Preparing to unpack .../26-golang-gopkg-yaml.v2-dev_2.4.0-1_all.deb ...
Unpacking golang-gopkg-yaml.v2-dev (2.4.0-1) ...
Selecting previously unselected package libcmark-gfm0:amd64.
Preparing to unpack .../27-libcmark-gfm0_0.29.0.gfm.0-6_amd64.deb ...
Unpacking libcmark-gfm0:amd64 (0.29.0.gfm.0-6) ...
Selecting previously unselected package libcmark-gfm-extensions0:amd64.
Preparing to unpack .../28-libcmark-gfm-extensions0_0.29.0.gfm.0-6_amd64.deb ...
Unpacking libcmark-gfm-extensions0:amd64 (0.29.0.gfm.0-6) ...
Selecting previously unselected package pandoc-data.
Preparing to unpack .../29-pandoc-data_2.9.2.1-1_all.deb ...
Unpacking pandoc-data (2.9.2.1-1) ...
Selecting previously unselected package pandoc.
Preparing to unpack .../30-pandoc_2.9.2.1-1+b1_amd64.deb ...
Unpacking pandoc (2.9.2.1-1+b1) ...
Setting up dh-golang (1.51) ...
Setting up golang-gopkg-yaml.v2-dev (2.4.0-1) ...
Setting up golang-github-remeh-sizedwaitgroup-dev (0.0~git20180822.5e7302b-1.1) ...
Setting up golang-github-tidwall-pretty-dev (1.0.5-1) ...
Setting up golang-1.15-src (1.15.15-1~deb11u4) ...
Setting up golang-github-tidwall-match-dev (1.0.3-1) ...
Setting up golang-golang-x-sys-dev (0.0~git20210124.22da62e-1) ...
Setting up golang-golang-x-term-dev (0.0~git20201210.2321bbc-1) ...
Setting up golang-github-xorpaul-uiprogress-dev (0.0~git20170224.d0567a9-1.1) ...
Setting up golang-github-cespare-xxhash-dev (2.1.1-1) ...
Setting up golang-github-davecgh-go-spew-dev (1.1.1-2) ...
Setting up golang-github-gosuri-uiprogress-dev (0.0~git20170224.d0567a9-1.1) ...
Setting up libcmark-gfm0:amd64 (0.29.0.gfm.0-6) ...
Setting up golang-github-klauspost-compress-dev (1.11.7-2) ...
Setting up golang-golang-x-text-dev (0.3.6-1) ...
Setting up golang-github-klauspost-pgzip-dev (1.2.5-1) ...
Setting up pandoc-data (2.9.2.1-1) ...
Setting up golang-src:amd64 (2:1.15~1) ...
Setting up golang-1.15-go (1.15.15-1~deb11u4) ...
Setting up golang-github-tidwall-gjson-dev (1.6.7-1) ...
Setting up golang-github-gosuri-uilive-dev (0.0~git20170323.ac356e6-1.1) ...
Setting up golang-github-mattn-go-isatty-dev (0.0.12-1) ...
Setting up golang-go (2:1.15~1) ...
Setting up golang-github-mattn-go-colorable-dev (0.1.7-1) ...
Setting up libcmark-gfm-extensions0:amd64 (0.29.0.gfm.0-6) ...
Setting up golang-any:amd64 (2:1.15~1) ...
Setting up pandoc (2.9.2.1-1+b1) ...
Setting up golang-golang-x-net-dev (1:0.0+git20210119.5f4716e+dfsg-4) ...
Setting up golang-golang-x-crypto-dev (1:0.0~git20201221.eec23a3-1) ...
Setting up golang-github-kballard-go-shellquote-dev (0.0~git20180428.95032a8-1) ...
Setting up golang-github-fatih-color-dev (1.7.0-1) ...
Processing triggers for man-db (2.9.4-2) ...
Processing triggers for libc-bin (2.31-13+deb11u5) ...
+ find . -maxdepth 1 -name g10k* -type d
+ cd ./g10k-0.5.7
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package g10k
dpkg-buildpackage: info: source version 0.5.7-1
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Jack Henschel <jackdev@mailbox.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
debian/rules:19: warning: overriding recipe for target 'override_dh_auto_install'
debian/rules:8: warning: ignoring old recipe for target 'override_dh_auto_install'
dh clean --buildsystem=golang --with=golang
   dh_auto_clean -O--buildsystem=golang
   dh_autoreconf_clean -O--buildsystem=golang
   dh_clean -O--buildsystem=golang
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building g10k using existing ./g10k_0.5.7.orig.tar.xz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: building g10k in g10k_0.5.7-1.debian.tar.xz
dpkg-source: info: building g10k in g10k_0.5.7-1.dsc
 debian/rules build
debian/rules:19: warning: overriding recipe for target 'override_dh_auto_install'
debian/rules:8: warning: ignoring old recipe for target 'override_dh_auto_install'
dh build --buildsystem=golang --with=golang
   dh_update_autotools_config -O--buildsystem=golang
   dh_autoreconf -O--buildsystem=golang
   dh_auto_configure -O--buildsystem=golang
   debian/rules override_dh_auto_build
make[1]: Entering directory '/build/g10k/g10k-0.5.7'
debian/rules:19: warning: overriding recipe for target 'override_dh_auto_install'
debian/rules:8: warning: ignoring old recipe for target 'override_dh_auto_install'
dh_auto_build
	cd obj-x86_64-linux-gnu && go install -trimpath -v -p 50 github.com/xorpaul/g10k
internal/unsafeheader
unicode/utf16
vendor/golang.org/x/crypto/cryptobyte/asn1
container/list
runtime/internal/sys
crypto/internal/subtle
vendor/golang.org/x/crypto/internal/subtle
encoding
unicode/utf8
internal/nettrace
internal/race
crypto/subtle
sync/atomic
golang.org/x/sys/internal/unsafeheader
math/bits
runtime/internal/atomic
internal/cpu
runtime/cgo
unicode
runtime/internal/math
internal/bytealg
github.com/tidwall/match
internal/testlog
math
runtime
internal/reflectlite
sync
internal/singleflight
math/rand
errors
sort
io
internal/oserror
vendor/golang.org/x/net/dns/dnsmessage
strconv
syscall
github.com/tidwall/pretty
hash
crypto/internal/randutil
bytes
strings
crypto/hmac
hash/crc32
vendor/golang.org/x/crypto/hkdf
crypto/rc4
crypto
reflect
vendor/golang.org/x/text/transform
github.com/kballard/go-shellquote
path
bufio
regexp/syntax
internal/syscall/execenv
internal/syscall/unix
time
regexp
github.com/gosuri/uiprogress/util/strutil
context
internal/poll
github.com/remeh/sizedwaitgroup
internal/fmtsort
encoding/binary
os
crypto/md5
vendor/golang.org/x/crypto/poly1305
crypto/cipher
crypto/sha256
crypto/sha1
encoding/base64
crypto/sha512
crypto/ed25519/internal/edwards25519
golang.org/x/sys/unix
encoding/pem
vendor/golang.org/x/crypto/chacha20
crypto/des
crypto/aes
path/filepath
fmt
net
io/ioutil
os/exec
vendor/golang.org/x/sys/cpu
vendor/golang.org/x/crypto/chacha20poly1305
encoding/hex
log
flag
os/user
github.com/gosuri/uilive
encoding/json
net/url
compress/flate
net/http/internal
github.com/klauspost/compress/flate
mime/quotedprintable
mime
vendor/golang.org/x/net/http2/hpack
vendor/golang.org/x/crypto/curve25519
math/big
gopkg.in/yaml.v2
vendor/golang.org/x/text/unicode/norm
github.com/xorpaul/uiprogress
vendor/golang.org/x/text/unicode/bidi
github.com/mattn/go-isatty
golang.org/x/term
compress/gzip
github.com/mattn/go-colorable
github.com/fatih/color
vendor/golang.org/x/text/secure/bidirule
golang.org/x/crypto/ssh/terminal
vendor/golang.org/x/net/idna
github.com/tidwall/gjson
github.com/klauspost/pgzip
crypto/rand
crypto/dsa
encoding/asn1
crypto/elliptic
crypto/ed25519
crypto/rsa
crypto/x509/pkix
vendor/golang.org/x/crypto/cryptobyte
crypto/ecdsa
archive/tar
vendor/golang.org/x/net/http/httpproxy
net/textproto
crypto/x509
vendor/golang.org/x/net/http/httpguts
mime/multipart
crypto/tls
net/http/httptrace
net/http
github.com/xorpaul/g10k
# update version number in manpage
DEB_VERSION_UPSTREAM 0.5.7
sed -e "s/__VERSION__/0.5.7/g" < debian/g10k.1.md.in > debian/g10k.1.md
# build custom manpages from markdown
pandoc debian/g10k.1.md -s -f markdown -t man -o debian/g10k.1
make[1]: Leaving directory '/build/g10k/g10k-0.5.7'
   dh_auto_test -O--buildsystem=golang
	cd obj-x86_64-linux-gnu && go test -vet=off -v -p 50 github.com/xorpaul/g10k
=== RUN   TestPreparePuppetfile
--- PASS: TestPreparePuppetfile (0.00s)
=== RUN   TestCommentPuppetfile
--- PASS: TestCommentPuppetfile (0.00s)
=== RUN   TestReadPuppetfile
--- PASS: TestReadPuppetfile (0.00s)
=== RUN   TestFallbackPuppetfile
--- PASS: TestFallbackPuppetfile (0.00s)
=== RUN   TestForgeCacheTTLPuppetfile
--- PASS: TestForgeCacheTTLPuppetfile (0.00s)
=== RUN   TestForceForgeVersionsPuppetfile
--- PASS: TestForceForgeVersionsPuppetfile (0.01s)
=== RUN   TestForceForgeVersionsPuppetfileCorrect
--- PASS: TestForceForgeVersionsPuppetfileCorrect (0.01s)
=== RUN   TestReadPuppetfileDuplicateGitAttribute
--- PASS: TestReadPuppetfileDuplicateGitAttribute (0.01s)
=== RUN   TestReadPuppetfileTrailingComma
--- PASS: TestReadPuppetfileTrailingComma (0.01s)
=== RUN   TestReadPuppetfileInvalidForgeModuleName
--- PASS: TestReadPuppetfileInvalidForgeModuleName (0.01s)
=== RUN   TestReadPuppetfileDuplicateForgeModule
--- PASS: TestReadPuppetfileDuplicateForgeModule (0.01s)
=== RUN   TestReadPuppetfileMissingGitAttribute
--- PASS: TestReadPuppetfileMissingGitAttribute (0.01s)
=== RUN   TestReadPuppetfileTooManyGitAttributes
--- PASS: TestReadPuppetfileTooManyGitAttributes (0.01s)
=== RUN   TestReadPuppetfileConflictingGitAttributesTag
--- PASS: TestReadPuppetfileConflictingGitAttributesTag (0.01s)
=== RUN   TestReadPuppetfileConflictingGitAttributesBranch
--- PASS: TestReadPuppetfileConflictingGitAttributesBranch (0.01s)
=== RUN   TestReadPuppetfileConflictingGitAttributesCommit
--- PASS: TestReadPuppetfileConflictingGitAttributesCommit (0.01s)
=== RUN   TestReadPuppetfileConflictingGitAttributesRef
--- PASS: TestReadPuppetfileConflictingGitAttributesRef (0.01s)
=== RUN   TestReadPuppetfileIgnoreUnreachable
--- PASS: TestReadPuppetfileIgnoreUnreachable (0.01s)
=== RUN   TestReadPuppetfileForgeCacheTTL
--- PASS: TestReadPuppetfileForgeCacheTTL (0.01s)
=== RUN   TestReadPuppetfileLink
--- PASS: TestReadPuppetfileLink (0.01s)
=== RUN   TestReadPuppetfileDuplicateForgeGitModule
--- PASS: TestReadPuppetfileDuplicateForgeGitModule (0.01s)
=== RUN   TestReadPuppetfileChecksumAttribute
--- PASS: TestReadPuppetfileChecksumAttribute (0.00s)
=== RUN   TestReadPuppetfileForgeSlashNotation
--- PASS: TestReadPuppetfileForgeSlashNotation (0.00s)
=== RUN   TestReadPuppetfileForgeDash
--- PASS: TestReadPuppetfileForgeDash (0.00s)
=== RUN   TestReadPuppetfileInstallPath
--- PASS: TestReadPuppetfileInstallPath (0.00s)
=== RUN   TestReadPuppetfileLocalModule
--- PASS: TestReadPuppetfileLocalModule (0.00s)
=== RUN   TestReadPuppetfileMissingTrailingComma
--- PASS: TestReadPuppetfileMissingTrailingComma (0.01s)
=== RUN   TestReadPuppetfileMissingTrailingComma2
--- PASS: TestReadPuppetfileMissingTrailingComma2 (0.01s)
=== RUN   TestReadPuppetfileForgeNotationGitModule
--- PASS: TestReadPuppetfileForgeNotationGitModule (0.00s)
=== RUN   TestReadPuppetfileGitSlashNotation
--- PASS: TestReadPuppetfileGitSlashNotation (0.00s)
=== RUN   TestConfigPrefix
--- PASS: TestConfigPrefix (0.00s)
=== RUN   TestConfigForceForgeVersions
--- PASS: TestConfigForceForgeVersions (0.00s)
=== RUN   TestConfigAddWarning
--- PASS: TestConfigAddWarning (0.00s)
=== RUN   TestConfigSimplePostrunCommand
--- PASS: TestConfigSimplePostrunCommand (0.00s)
=== RUN   TestConfigPostrunCommand
--- PASS: TestConfigPostrunCommand (0.01s)
=== RUN   TestResolvStatic
    g10k_test.go:124: Skipping full Puppet environment resolv test, because package hashdeep is missing
--- SKIP: TestResolvStatic (0.00s)
=== RUN   TestInvalidFilesizeForgemodule
--- PASS: TestInvalidFilesizeForgemodule (0.04s)
=== RUN   TestInvalidMd5sumForgemodule
--- PASS: TestInvalidMd5sumForgemodule (0.03s)
=== RUN   TestInvalidSha256sumForgemodule
--- PASS: TestInvalidSha256sumForgemodule (0.03s)
=== RUN   TestModuleDirOverride
--- PASS: TestModuleDirOverride (0.00s)
=== RUN   TestSimplePostrunCommand
--- PASS: TestSimplePostrunCommand (1.97s)
PASS
ok  	github.com/xorpaul/g10k	2.301s
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
debian/rules:19: warning: overriding recipe for target 'override_dh_auto_install'
debian/rules:8: warning: ignoring old recipe for target 'override_dh_auto_install'
dh binary --buildsystem=golang --with=golang
   dh_testroot -O--buildsystem=golang
   dh_prep -O--buildsystem=golang
   debian/rules override_dh_auto_install
make[1]: Entering directory '/build/g10k/g10k-0.5.7'
debian/rules:19: warning: overriding recipe for target 'override_dh_auto_install'
debian/rules:8: warning: ignoring old recipe for target 'override_dh_auto_install'
dh_auto_install -- --no-source
	cd obj-x86_64-linux-gnu && mkdir -p /build/g10k/g10k-0.5.7/debian/g10k/usr
	cd obj-x86_64-linux-gnu && cp -r bin /build/g10k/g10k-0.5.7/debian/g10k/usr
make[1]: Leaving directory '/build/g10k/g10k-0.5.7'
   dh_installdocs -O--buildsystem=golang
   dh_installchangelogs -O--buildsystem=golang
   dh_installman -O--buildsystem=golang
   dh_installinit -O--buildsystem=golang
   dh_lintian -O--buildsystem=golang
   dh_perl -O--buildsystem=golang
   dh_link -O--buildsystem=golang
   dh_strip_nondeterminism -O--buildsystem=golang
   dh_compress -O--buildsystem=golang
   dh_fixperms -O--buildsystem=golang
   dh_missing -O--buildsystem=golang
   dh_strip -O--buildsystem=golang
dh_strip: warning: Could not find the BuildID in debian/g10k/usr/bin/g10k
   dh_makeshlibs -O--buildsystem=golang
   dh_shlibdeps -O--buildsystem=golang
   dh_installdeb -O--buildsystem=golang
   dh_golang -O--buildsystem=golang
   dh_gencontrol -O--buildsystem=golang
   dh_md5sums -O--buildsystem=golang
   dh_builddeb -O--buildsystem=golang
dpkg-deb: building package 'g10k' in '../g10k_0.5.7-1_amd64.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../g10k_0.5.7-1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload (original source is included)
+ date
Tue Apr 18 06:07:03 UTC 2023
+ cd ..
+ ls -la
total 2288
drwxr-xr-x 3 root root    4096 Apr 18 06:07 .
drwxr-xr-x 3 root root    4096 Apr 18 06:05 ..
-rw-r--r-- 1 root root   25527 Apr 18 06:07 buildlog.txt
drwxr-xr-x 6 root root    4096 Apr 18 06:06 g10k-0.5.7
-rw-r--r-- 1 root root   10680 Apr 18 06:06 g10k_0.5.7-1.debian.tar.xz
-rw-r--r-- 1 root root    1486 Apr 18 06:06 g10k_0.5.7-1.dsc
-rw-r--r-- 1 root root    6626 Apr 18 06:07 g10k_0.5.7-1_amd64.buildinfo
-rw-r--r-- 1 root root    1728 Apr 18 06:07 g10k_0.5.7-1_amd64.changes
-rw-r--r-- 1 root root 1900184 Apr 18 06:07 g10k_0.5.7-1_amd64.deb
-rw-r--r-- 1 root root  366396 Feb  8  2019 g10k_0.5.7.orig.tar.xz
+ find . -maxdepth 1 -type f
+ sha256sum ./buildlog.txt ./g10k_0.5.7-1_amd64.changes ./g10k_0.5.7-1_amd64.deb ./g10k_0.5.7.orig.tar.xz ./g10k_0.5.7-1.debian.tar.xz ./g10k_0.5.7-1_amd64.buildinfo ./g10k_0.5.7-1.dsc
4be8aade68bd525d004f1d415525034cb9b86ea7e863465413cedd4b39099fd3  ./buildlog.txt
e67ba0f37aded0411907f0717c81a875a16f5d7e8bc471546cee2092fc0d03bd  ./g10k_0.5.7-1_amd64.changes
cd0ca1b7effee7c795cff4465853d5230d3af5a586b60bd1da14eddb2e12107e  ./g10k_0.5.7-1_amd64.deb
420d45385d2b2814a87c4c7af97831213e3eecff67583630c9d164a2b3edf9ea  ./g10k_0.5.7.orig.tar.xz
507b3a9065e34547bb1b432d88838ad159004e42714c37fe1522531c7632d0cf  ./g10k_0.5.7-1.debian.tar.xz
7b1b29f17cb557b1bbd5d7696fe832523df5a17f67ad41f450f9e2f720e48f08  ./g10k_0.5.7-1_amd64.buildinfo
633c9d7477f12ca770346911629da97c2dffd5d3f7b9704e9e13c8812f28a152  ./g10k_0.5.7-1.dsc
+ mkdir published
+ cd published
+ cd ../
+ ls g10k_0.5.7-1_amd64.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://repo.pureos.net/pureos/pool/main/g/g10k/g10k_0.5.7-1_amd64.deb
+ find . -maxdepth 1 -type f
+ tee ../SHA256SUMS
+ sha256sum ./g10k_0.5.7-1_amd64.deb
c850dc8ff9e29db939b804a142cbbb8599a2789dc8c6a1096f88c07e41607e22  ./g10k_0.5.7-1_amd64.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./g10k_0.5.7-1_amd64.deb: FAILED
sha256sum: WARNING: 1 computed checksum did NOT match
