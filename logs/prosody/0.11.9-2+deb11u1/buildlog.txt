+ date
Wed Apr 19 16:17:00 UTC 2023
+ apt-get source --only-source prosody=0.11.9-2+deb11u1
Reading package lists...
NOTICE: 'prosody' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/xmpp-team/prosody.git
Please use:
git clone https://salsa.debian.org/xmpp-team/prosody.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 463 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium-security/main prosody 0.11.9-2+deb11u1 (dsc) [1905 B]
Get:2 http://repo.pureos.net/pureos byzantium-security/main prosody 0.11.9-2+deb11u1 (tar) [432 kB]
Get:3 http://repo.pureos.net/pureos byzantium-security/main prosody 0.11.9-2+deb11u1 (diff) [29.8 kB]
dpkg-source: info: extracting prosody in prosody-0.11.9
dpkg-source: info: unpacking prosody_0.11.9.orig.tar.gz
dpkg-source: info: unpacking prosody_0.11.9-2+deb11u1.debian.tar.xz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: applying 0001-conf.patch
dpkg-source: info: applying 0002-prosody-lua52.patch
dpkg-source: info: applying 0003-buildflags.patch
dpkg-source: info: applying 0004-fix-package.path-of-ejabberd2prosody.patch
dpkg-source: info: applying 0005-use-lua52.patch
dpkg-source: info: applying 0006-muc-fix-for-CWE-284.patch
dpkg-source: info: applying 0007-CVE-2022-0217.patch
Fetched 463 kB in 0s (1671 kB/s)
W: Download is performed unsandboxed as root as file 'prosody_0.11.9-2+deb11u1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source prosody=0.11.9-2+deb11u1
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
Some packages could not be installed. This may mean that you have
requested an impossible situation or if you are using the unstable
distribution that some required packages have not yet been created
or been moved out of Incoming.
The following information may help to resolve the situation:

The following packages have unmet dependencies:
 builddeps:prosody : Depends: libidn11-dev but it is not installable
                     Depends: liblua5.2-dev but it is not installable
                     Depends: lua-busted but it is not installable
                     Depends: lua5.2 but it is not installable
                     Depends: lua5.2-expat but it is not installable
                     Depends: lua5.2-socket but it is not installable
                     Depends: txt2man but it is not installable
E: Unable to correct problems, you have held broken packages.
