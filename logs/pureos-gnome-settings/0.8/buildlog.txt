+ date
Wed Apr 19 16:38:20 UTC 2023
+ apt-get source --only-source pureos-gnome-settings=0.8
Reading package lists...
NOTICE: 'pureos-gnome-settings' packaging is maintained in the 'Git' version control system at:
https://source.puri.sm/pureos/packages/pureos-gnome-settings.git
Please use:
git clone https://source.puri.sm/pureos/packages/pureos-gnome-settings.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 15.3 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main pureos-gnome-settings 0.8 (dsc) [1666 B]
Get:2 http://repo.pureos.net/pureos byzantium/main pureos-gnome-settings 0.8 (tar) [13.7 kB]
dpkg-source: info: extracting pureos-gnome-settings in pureos-gnome-settings-0.8
dpkg-source: info: unpacking pureos-gnome-settings_0.8.tar.xz
Fetched 15.3 kB in 0s (91.6 kB/s)
W: Download is performed unsandboxed as root as file 'pureos-gnome-settings_0.8.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source pureos-gnome-settings=0.8
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name pureos-gnome-settings* -type d
+ cd ./pureos-gnome-settings-0.8
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package pureos-gnome-settings
dpkg-buildpackage: info: source version 0.8
dpkg-buildpackage: info: source distribution byzantium
dpkg-buildpackage: info: source changed by Matthias Klumpp <matthias.klumpp@puri.sm>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
dpkg-source: info: using options from pureos-gnome-settings-0.8/debian/source/options: --compression=xz --compression-level=9
 debian/rules clean
dh clean
   dh_clean
 dpkg-source -b .
dpkg-source: info: using options from pureos-gnome-settings-0.8/debian/source/options: --compression=xz --compression-level=9
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building pureos-gnome-settings in pureos-gnome-settings_0.8.tar.xz
dpkg-source: info: building pureos-gnome-settings in pureos-gnome-settings_0.8.dsc
 debian/rules build
dh build
   dh_update_autotools_config
   dh_autoreconf
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary
   dh_testroot
   dh_prep
   dh_installdocs
   dh_installchangelogs
   debian/rules override_dh_installgsettings
make[1]: Entering directory '/build/pureos-gnome-settings/pureos-gnome-settings-0.8'
dh_installgsettings --priority=20
make[1]: Leaving directory '/build/pureos-gnome-settings/pureos-gnome-settings-0.8'
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'pureos-gnome-settings' in '../pureos-gnome-settings_0.8_all.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../pureos-gnome-settings_0.8_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-source: info: using options from pureos-gnome-settings-0.8/debian/source/options: --compression=xz --compression-level=9
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Wed Apr 19 16:38:25 UTC 2023
+ cd ..
+ ls -la
total 52
drwxr-xr-x 3 root root  4096 Apr 19 16:38 .
drwxr-xr-x 3 root root  4096 Apr 19 16:38 ..
-rw-r--r-- 1 root root  3447 Apr 19 16:38 buildlog.txt
drwxr-xr-x 3 root root  4096 May 27  2018 pureos-gnome-settings-0.8
-rw-r--r-- 1 root root   750 Apr 19 16:38 pureos-gnome-settings_0.8.dsc
-rw-r--r-- 1 root root 13652 Apr 19 16:38 pureos-gnome-settings_0.8.tar.xz
-rw-r--r-- 1 root root  3324 Apr 19 16:38 pureos-gnome-settings_0.8_all.deb
-rw-r--r-- 1 root root  5268 Apr 19 16:38 pureos-gnome-settings_0.8_amd64.buildinfo
-rw-r--r-- 1 root root  1657 Apr 19 16:38 pureos-gnome-settings_0.8_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./buildlog.txt ./pureos-gnome-settings_0.8.tar.xz ./pureos-gnome-settings_0.8_amd64.buildinfo ./pureos-gnome-settings_0.8_all.deb ./pureos-gnome-settings_0.8_amd64.changes ./pureos-gnome-settings_0.8.dsc
2097172b24426b5bfd3e74b394664d622383448895a923b188776b4ed647fcdd  ./buildlog.txt
e6f6ab62b3e9547c03315429ea2d2c8ba639b0c0c901c562d5bd8f031dbd7d77  ./pureos-gnome-settings_0.8.tar.xz
48d0b28caf17f4e8a904d7e29bb059a64a60cc19e5c8ba601c7ac62af3ec808e  ./pureos-gnome-settings_0.8_amd64.buildinfo
af58898d080154eeec520f6dd55419be81ff48c3f04ce35e46cc978087342767  ./pureos-gnome-settings_0.8_all.deb
34c7c0a5966c40856d4fd0bab3663155b7bf0cbfaf35ca425265d2c4b52158ef  ./pureos-gnome-settings_0.8_amd64.changes
d25ea458fa700939507d681e603af866bc20dc24e1cb56ca2f5d4a3db252e362  ./pureos-gnome-settings_0.8.dsc
+ mkdir published
+ cd published
+ cd ../
+ ls pureos-gnome-settings_0.8_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/p/pureos-gnome-settings/pureos-gnome-settings_0.8_all.deb
--2023-04-19 16:38:25--  http://repo.pureos.net/pureos/pool/main/p/pureos-gnome-settings/pureos-gnome-settings_0.8_all.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 3324 (3.2K) [application/octet-stream]
Saving to: 'pureos-gnome-settings_0.8_all.deb'

     0K ...                                                   100% 75.3M=0s

2023-04-19 16:38:25 (75.3 MB/s) - 'pureos-gnome-settings_0.8_all.deb' saved [3324/3324]

+ tee ../SHA256SUMS+ 
find . -maxdepth 1 -type f
+ sha256sum ./pureos-gnome-settings_0.8_all.deb
af58898d080154eeec520f6dd55419be81ff48c3f04ce35e46cc978087342767  ./pureos-gnome-settings_0.8_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./pureos-gnome-settings_0.8_all.deb: OK
+ echo Package pureos-gnome-settings version 0.8 is reproducible!
Package pureos-gnome-settings version 0.8 is reproducible!
