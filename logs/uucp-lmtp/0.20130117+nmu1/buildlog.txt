+ date
Thu Apr 20 01:58:30 UTC 2023
+ apt-get source --only-source uucp-lmtp=0.20130117+nmu1
Reading package lists...
Need to get 24.8 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main uucp-lmtp 0.20130117+nmu1 (dsc) [807 B]
Get:2 http://repo.pureos.net/pureos byzantium/main uucp-lmtp 0.20130117+nmu1 (tar) [23.9 kB]
dpkg-source: info: extracting uucp-lmtp in uucp-lmtp-0.20130117+nmu1
dpkg-source: info: unpacking uucp-lmtp_0.20130117+nmu1.tar.gz
Fetched 24.8 kB in 0s (147 kB/s)
W: Download is performed unsandboxed as root as file 'uucp-lmtp_0.20130117+nmu1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source uucp-lmtp=0.20130117+nmu1
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name uucp-lmtp* -type d
+ cd ./uucp-lmtp-0.20130117+nmu1
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package uucp-lmtp
dpkg-buildpackage: info: source version 0.20130117+nmu1
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by  Dmitry Shachnev <mitya57@gmail.com>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --with python3
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
dh: error: unable to load addon python3: Can't locate Debian/Debhelper/Sequence/python3.pm in @INC (you may need to install the Debian::Debhelper::Sequence::python3 module) (@INC contains: /etc/perl /usr/local/lib/x86_64-linux-gnu/perl/5.32.1 /usr/local/share/perl/5.32.1 /usr/lib/x86_64-linux-gnu/perl5/5.32 /usr/share/perl5 /usr/lib/x86_64-linux-gnu/perl-base /usr/lib/x86_64-linux-gnu/perl/5.32 /usr/share/perl/5.32 /usr/local/lib/site_perl) at (eval 10) line 1.
BEGIN failed--compilation aborted at (eval 10) line 1.

make: *** [debian/rules:8: clean] Error 25
dpkg-buildpackage: error: debian/rules clean subprocess returned exit status 2
