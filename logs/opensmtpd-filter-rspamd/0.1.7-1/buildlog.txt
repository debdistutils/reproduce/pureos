+ date
Wed Apr 19 12:47:00 UTC 2023
+ apt-get source --only-source opensmtpd-filter-rspamd=0.1.7-1
Reading package lists...
NOTICE: 'opensmtpd-filter-rspamd' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/go-team/packages/opensmtpd-filter-rspamd.git
Please use:
git clone https://salsa.debian.org/go-team/packages/opensmtpd-filter-rspamd.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 12.0 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main opensmtpd-filter-rspamd 0.1.7-1 (dsc) [2198 B]
Get:2 http://repo.pureos.net/pureos byzantium/main opensmtpd-filter-rspamd 0.1.7-1 (tar) [6902 B]
Get:3 http://repo.pureos.net/pureos byzantium/main opensmtpd-filter-rspamd 0.1.7-1 (diff) [2860 B]
dpkg-source: info: extracting opensmtpd-filter-rspamd in opensmtpd-filter-rspamd-0.1.7
dpkg-source: info: unpacking opensmtpd-filter-rspamd_0.1.7.orig.tar.gz
dpkg-source: info: unpacking opensmtpd-filter-rspamd_0.1.7-1.debian.tar.xz
Fetched 12.0 kB in 0s (33.8 kB/s)
W: Download is performed unsandboxed as root as file 'opensmtpd-filter-rspamd_0.1.7-1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source opensmtpd-filter-rspamd=0.1.7-1
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  dh-golang golang-1.15-go golang-1.15-src golang-any golang-go golang-src
0 upgraded, 6 newly installed, 0 to remove and 0 not upgraded.
Need to get 61.3 MB of archives.
After this operation, 359 MB of additional disk space will be used.
Get:1 http://repo.pureos.net/pureos byzantium/main amd64 dh-golang all 1.51 [24.6 kB]
Get:2 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-src amd64 1.15.15-1~deb11u4 [13.9 MB]
Get:3 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-go amd64 1.15.15-1~deb11u4 [47.4 MB]
Get:4 http://repo.pureos.net/pureos byzantium/main amd64 golang-src amd64 2:1.15~1 [4856 B]
Get:5 http://repo.pureos.net/pureos byzantium/main amd64 golang-go amd64 2:1.15~1 [23.9 kB]
Get:6 http://repo.pureos.net/pureos byzantium/main amd64 golang-any amd64 2:1.15~1 [4976 B]
Fetched 61.3 MB in 2s (30.8 MB/s)
Selecting previously unselected package dh-golang.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 102989 files and directories currently installed.)
Preparing to unpack .../0-dh-golang_1.51_all.deb ...
Unpacking dh-golang (1.51) ...
Selecting previously unselected package golang-1.15-src.
Preparing to unpack .../1-golang-1.15-src_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-src (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-1.15-go.
Preparing to unpack .../2-golang-1.15-go_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-go (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-src:amd64.
Preparing to unpack .../3-golang-src_2%3a1.15~1_amd64.deb ...
Unpacking golang-src:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-go.
Preparing to unpack .../4-golang-go_2%3a1.15~1_amd64.deb ...
Unpacking golang-go (2:1.15~1) ...
Selecting previously unselected package golang-any:amd64.
Preparing to unpack .../5-golang-any_2%3a1.15~1_amd64.deb ...
Unpacking golang-any:amd64 (2:1.15~1) ...
Setting up dh-golang (1.51) ...
Setting up golang-1.15-src (1.15.15-1~deb11u4) ...
Setting up golang-src:amd64 (2:1.15~1) ...
Setting up golang-1.15-go (1.15.15-1~deb11u4) ...
Setting up golang-go (2:1.15~1) ...
Setting up golang-any:amd64 (2:1.15~1) ...
Processing triggers for man-db (2.9.4-2) ...
+ find . -maxdepth 1 -name opensmtpd-filter-rspamd* -type d
+ cd ./opensmtpd-filter-rspamd-0.1.7
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package opensmtpd-filter-rspamd
dpkg-buildpackage: info: source version 0.1.7-1
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Ryan Kavanagh <rak@debian.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --builddirectory=_build --buildsystem=golang --with=golang
   dh_auto_clean -O--builddirectory=_build -O--buildsystem=golang
   dh_autoreconf_clean -O--builddirectory=_build -O--buildsystem=golang
   dh_clean -O--builddirectory=_build -O--buildsystem=golang
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building opensmtpd-filter-rspamd using existing ./opensmtpd-filter-rspamd_0.1.7.orig.tar.gz
dpkg-source: info: building opensmtpd-filter-rspamd in opensmtpd-filter-rspamd_0.1.7-1.debian.tar.xz
dpkg-source: info: building opensmtpd-filter-rspamd in opensmtpd-filter-rspamd_0.1.7-1.dsc
 debian/rules binary
dh binary --builddirectory=_build --buildsystem=golang --with=golang
   dh_update_autotools_config -O--builddirectory=_build -O--buildsystem=golang
   dh_autoreconf -O--builddirectory=_build -O--buildsystem=golang
   dh_auto_configure -O--builddirectory=_build -O--buildsystem=golang
   dh_auto_build -O--builddirectory=_build -O--buildsystem=golang
	cd _build && go install -trimpath -v -p 40 github.com/poolporg/filter-rspamd
internal/race
unicode/utf16
runtime/internal/sys
internal/unsafeheader
vendor/golang.org/x/crypto/cryptobyte/asn1
encoding
internal/cpu
container/list
math/bits
crypto/internal/subtle
runtime/internal/atomic
runtime/cgo
unicode
internal/nettrace
sync/atomic
vendor/golang.org/x/crypto/internal/subtle
crypto/subtle
unicode/utf8
runtime/internal/math
internal/bytealg
internal/testlog
math
runtime
internal/reflectlite
sync
internal/singleflight
math/rand
errors
sort
internal/oserror
io
vendor/golang.org/x/net/dns/dnsmessage
strconv
syscall
hash
crypto/internal/randutil
bytes
strings
crypto/hmac
hash/crc32
crypto/rc4
crypto
reflect
vendor/golang.org/x/crypto/hkdf
vendor/golang.org/x/text/transform
bufio
path
internal/syscall/execenv
internal/syscall/unix
time
context
internal/poll
encoding/binary
internal/fmtsort
os
encoding/base64
crypto/sha512
crypto/md5
crypto/cipher
vendor/golang.org/x/crypto/poly1305
crypto/ed25519/internal/edwards25519
crypto/sha1
crypto/sha256
encoding/pem
crypto/des
vendor/golang.org/x/crypto/chacha20
crypto/aes
path/filepath
fmt
net
io/ioutil
vendor/golang.org/x/sys/cpu
vendor/golang.org/x/crypto/chacha20poly1305
flag
log
encoding/hex
encoding/json
net/url
net/http/internal
mime/quotedprintable
math/big
vendor/golang.org/x/crypto/curve25519
vendor/golang.org/x/net/http2/hpack
compress/flate
mime
vendor/golang.org/x/text/unicode/norm
vendor/golang.org/x/text/unicode/bidi
compress/gzip
vendor/golang.org/x/text/secure/bidirule
vendor/golang.org/x/net/idna
crypto/dsa
crypto/rand
crypto/elliptic
encoding/asn1
crypto/ed25519
crypto/rsa
crypto/x509/pkix
vendor/golang.org/x/crypto/cryptobyte
crypto/ecdsa
vendor/golang.org/x/net/http/httpproxy
net/textproto
crypto/x509
mime/multipart
vendor/golang.org/x/net/http/httpguts
crypto/tls
net/http/httptrace
net/http
github.com/poolporg/filter-rspamd
   dh_auto_test -O--builddirectory=_build -O--buildsystem=golang
	cd _build && go test -vet=off -v -p 40 github.com/poolporg/filter-rspamd
?   	github.com/poolporg/filter-rspamd	[no test files]
   create-stamp debian/debhelper-build-stamp
   dh_testroot -O--builddirectory=_build -O--buildsystem=golang
   dh_prep -O--builddirectory=_build -O--buildsystem=golang
   debian/rules override_dh_auto_install
make[1]: Entering directory '/build/opensmtpd-filter-rspamd/opensmtpd-filter-rspamd-0.1.7'
dh_install _build/bin/filter-rspamd usr/libexec/opensmtpd/
make[1]: Leaving directory '/build/opensmtpd-filter-rspamd/opensmtpd-filter-rspamd-0.1.7'
   dh_installdocs -O--builddirectory=_build -O--buildsystem=golang
   dh_installchangelogs -O--builddirectory=_build -O--buildsystem=golang
   dh_installsystemduser -O--builddirectory=_build -O--buildsystem=golang
   dh_perl -O--builddirectory=_build -O--buildsystem=golang
   dh_link -O--builddirectory=_build -O--buildsystem=golang
   dh_strip_nondeterminism -O--builddirectory=_build -O--buildsystem=golang
   dh_compress -O--builddirectory=_build -O--buildsystem=golang
   dh_fixperms -O--builddirectory=_build -O--buildsystem=golang
   dh_missing -O--builddirectory=_build -O--buildsystem=golang
   dh_dwz -a -O--builddirectory=_build -O--buildsystem=golang
dwz: debian/opensmtpd-filter-rspamd/usr/libexec/opensmtpd/filter-rspamd: .debug_info section not present
   dh_strip -a -O--builddirectory=_build -O--buildsystem=golang
dh_strip: warning: Could not find the BuildID in debian/opensmtpd-filter-rspamd/usr/libexec/opensmtpd/filter-rspamd
   dh_makeshlibs -a -O--builddirectory=_build -O--buildsystem=golang
   dh_shlibdeps -a -O--builddirectory=_build -O--buildsystem=golang
   dh_installdeb -O--builddirectory=_build -O--buildsystem=golang
   dh_golang -O--builddirectory=_build -O--buildsystem=golang
   dh_gencontrol -O--builddirectory=_build -O--buildsystem=golang
   dh_md5sums -O--builddirectory=_build -O--buildsystem=golang
   dh_builddeb -O--builddirectory=_build -O--buildsystem=golang
dpkg-deb: building package 'opensmtpd-filter-rspamd' in '../opensmtpd-filter-rspamd_0.1.7-1_amd64.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../opensmtpd-filter-rspamd_0.1.7-1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload (original source is included)
+ date
Wed Apr 19 12:47:42 UTC 2023
+ cd ..
+ ls -la
total 1592
drwxr-xr-x 3 root root    4096 Apr 19 12:47 .
drwxr-xr-x 3 root root    4096 Apr 19 12:47 ..
-rw-r--r-- 1 root root   10124 Apr 19 12:47 buildlog.txt
drwxr-xr-x 6 root root    4096 Apr 19 12:47 opensmtpd-filter-rspamd-0.1.7
-rw-r--r-- 1 root root    2860 Apr 19 12:47 opensmtpd-filter-rspamd_0.1.7-1.debian.tar.xz
-rw-r--r-- 1 root root    1294 Apr 19 12:47 opensmtpd-filter-rspamd_0.1.7-1.dsc
-rw-r--r-- 1 root root    5515 Apr 19 12:47 opensmtpd-filter-rspamd_0.1.7-1_amd64.buildinfo
-rw-r--r-- 1 root root    2418 Apr 19 12:47 opensmtpd-filter-rspamd_0.1.7-1_amd64.changes
-rw-r--r-- 1 root root 1574280 Apr 19 12:47 opensmtpd-filter-rspamd_0.1.7-1_amd64.deb
-rw-r--r-- 1 root root    6902 Jan  5  2021 opensmtpd-filter-rspamd_0.1.7.orig.tar.gz
+ find . -maxdepth 1 -type f
+ sha256sum ./opensmtpd-filter-rspamd_0.1.7.orig.tar.gz ./buildlog.txt ./opensmtpd-filter-rspamd_0.1.7-1_amd64.changes ./opensmtpd-filter-rspamd_0.1.7-1_amd64.deb ./opensmtpd-filter-rspamd_0.1.7-1.debian.tar.xz ./opensmtpd-filter-rspamd_0.1.7-1.dsc ./opensmtpd-filter-rspamd_0.1.7-1_amd64.buildinfo
734733c3b672b660bcfe8f0008b09c84469f051f7381230d332265981af6f287  ./opensmtpd-filter-rspamd_0.1.7.orig.tar.gz
9b2f2c5e84f28f3a5b48c25fa62ffcf0d137a0d675cab1507d9ab5ef6c05747d  ./buildlog.txt
030e165239754135d8ce6251a95cc9338977b477447172513ce1a8b3ac564b73  ./opensmtpd-filter-rspamd_0.1.7-1_amd64.changes
39949103e3842614789366e8825a37ecafb9b1d6a7992b00ea82de193ad57626  ./opensmtpd-filter-rspamd_0.1.7-1_amd64.deb
08f6612b7e69c5242ab8bcb4c46bb54376fd55be48dd5226d221c6931c12e7c0  ./opensmtpd-filter-rspamd_0.1.7-1.debian.tar.xz
f60b18bc12ac78a5b9c2d7542358c25a7c23f45d48eacbf013ad647a628bbe3f  ./opensmtpd-filter-rspamd_0.1.7-1.dsc
a93fe1de2a065d7c6d32502941483d0a75ed0887b0d756c1f5d5409de81873b0  ./opensmtpd-filter-rspamd_0.1.7-1_amd64.buildinfo
+ mkdir published
+ cd published
+ cd ../
+ ls opensmtpd-filter-rspamd_0.1.7-1_amd64.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/o/opensmtpd-filter-rspamd/opensmtpd-filter-rspamd_0.1.7-1_amd64.deb
--2023-04-19 12:47:42--  http://repo.pureos.net/pureos/pool/main/o/opensmtpd-filter-rspamd/opensmtpd-filter-rspamd_0.1.7-1_amd64.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 404 Not Found
2023-04-19 12:47:42 ERROR 404: Not Found.

