+ date
Tue Apr 18 15:00:22 UTC 2023
+ apt-get source --only-source haskell-edison-api=1.3.1-5
Reading package lists...
NOTICE: 'haskell-edison-api' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-edison-api]
Please use:
git clone https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-edison-api]
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 39.0 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main haskell-edison-api 1.3.1-5 (dsc) [2350 B]
Get:2 http://repo.pureos.net/pureos byzantium/main haskell-edison-api 1.3.1-5 (tar) [30.5 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main haskell-edison-api 1.3.1-5 (diff) [6096 B]
dpkg-source: info: extracting haskell-edison-api in haskell-edison-api-1.3.1
dpkg-source: info: unpacking haskell-edison-api_1.3.1.orig.tar.gz
dpkg-source: info: unpacking haskell-edison-api_1.3.1-5.debian.tar.xz
Fetched 39.0 kB in 0s (221 kB/s)
W: Download is performed unsandboxed as root as file 'haskell-edison-api_1.3.1-5.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source haskell-edison-api=1.3.1-5
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  cdbs dh-buildinfo fonts-mathjax ghc-doc ghc-prof haskell-devscripts
  haskell-devscripts-minimal hscolour html-xml-utils libjs-mathjax
0 upgraded, 10 newly installed, 0 to remove and 0 not upgraded.
Need to get 86.4 MB of archives.
After this operation, 1115 MB of additional disk space will be used.
Get:1 http://repo.pureos.net/pureos byzantium/main amd64 fonts-mathjax all 2.7.9+dfsg-1 [2210 kB]
Get:2 http://repo.pureos.net/pureos byzantium/main amd64 libjs-mathjax all 2.7.9+dfsg-1 [5667 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main amd64 ghc-doc all 8.8.4-2 [15.9 MB]
Get:4 http://repo.pureos.net/pureos byzantium/main amd64 cdbs all 0.4.163 [82.6 kB]
Get:5 http://repo.pureos.net/pureos byzantium/main amd64 dh-buildinfo all 0.11+nmu2 [18.4 kB]
Get:6 http://repo.pureos.net/pureos byzantium/main amd64 ghc-prof amd64 8.8.4-2 [61.9 MB]
Get:7 http://repo.pureos.net/pureos byzantium/main amd64 html-xml-utils amd64 7.7-1.1 [317 kB]
Get:8 http://repo.pureos.net/pureos byzantium/main amd64 haskell-devscripts-minimal all 0.16.0 [40.8 kB]
Get:9 http://repo.pureos.net/pureos byzantium/main amd64 hscolour amd64 1.24.4-3+b1 [303 kB]
Get:10 http://repo.pureos.net/pureos byzantium/main amd64 haskell-devscripts all 0.16.0 [17.1 kB]
Fetched 86.4 MB in 7s (13.0 MB/s)
Selecting previously unselected package fonts-mathjax.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 102989 files and directories currently installed.)
Preparing to unpack .../0-fonts-mathjax_2.7.9+dfsg-1_all.deb ...
Unpacking fonts-mathjax (2.7.9+dfsg-1) ...
Selecting previously unselected package libjs-mathjax.
Preparing to unpack .../1-libjs-mathjax_2.7.9+dfsg-1_all.deb ...
Unpacking libjs-mathjax (2.7.9+dfsg-1) ...
Selecting previously unselected package ghc-doc.
Preparing to unpack .../2-ghc-doc_8.8.4-2_all.deb ...
Unpacking ghc-doc (8.8.4-2) ...
Selecting previously unselected package cdbs.
Preparing to unpack .../3-cdbs_0.4.163_all.deb ...
Unpacking cdbs (0.4.163) ...
Selecting previously unselected package dh-buildinfo.
Preparing to unpack .../4-dh-buildinfo_0.11+nmu2_all.deb ...
Unpacking dh-buildinfo (0.11+nmu2) ...
Selecting previously unselected package ghc-prof.
Preparing to unpack .../5-ghc-prof_8.8.4-2_amd64.deb ...
Unpacking ghc-prof (8.8.4-2) ...
Selecting previously unselected package html-xml-utils.
Preparing to unpack .../6-html-xml-utils_7.7-1.1_amd64.deb ...
Unpacking html-xml-utils (7.7-1.1) ...
Selecting previously unselected package haskell-devscripts-minimal.
Preparing to unpack .../7-haskell-devscripts-minimal_0.16.0_all.deb ...
Unpacking haskell-devscripts-minimal (0.16.0) ...
Selecting previously unselected package hscolour.
Preparing to unpack .../8-hscolour_1.24.4-3+b1_amd64.deb ...
Unpacking hscolour (1.24.4-3+b1) ...
Selecting previously unselected package haskell-devscripts.
Preparing to unpack .../9-haskell-devscripts_0.16.0_all.deb ...
Unpacking haskell-devscripts (0.16.0) ...
Setting up hscolour (1.24.4-3+b1) ...
Setting up fonts-mathjax (2.7.9+dfsg-1) ...
Setting up html-xml-utils (7.7-1.1) ...
Setting up libjs-mathjax (2.7.9+dfsg-1) ...
Setting up ghc-prof (8.8.4-2) ...
Setting up ghc-doc (8.8.4-2) ...
Setting up dh-buildinfo (0.11+nmu2) ...
Setting up cdbs (0.4.163) ...
Setting up haskell-devscripts-minimal (0.16.0) ...
Setting up haskell-devscripts (0.16.0) ...
Processing triggers for man-db (2.9.4-2) ...
Processing triggers for fontconfig (2.13.1-4.2) ...
+ find . -maxdepth 1 -name haskell-edison-api* -type d
+ cd ./haskell-edison-api-1.3.1
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package haskell-edison-api
dpkg-buildpackage: info: source version 1.3.1-5
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Ilias Tsitsimpis <iliastsi@debian.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
test -x debian/rules
dh_clean 
. /usr/share/haskell-devscripts/Dh_Haskell.sh && \
clean_recipe
Running rm -rf dist dist-ghc dist-ghcjs dist-hugs debian/hlibrary.setup Setup.hi Setup.ho Setup.o .\*config\*
Running rm -f configure-ghc-stamp configure-ghcjs-stamp build-ghc-stamp build-ghcjs-stamp build-hugs-stamp build-haddock-stamp
Running rm -rf debian/tmp-inst-ghc debian/tmp-inst-ghcjs
Running rm -f debian/extra-depends-ghc debian/extra-depends-ghcjs
Running rm -f debian/hlibrary.Makefile
Running rm -rf debian/dh_haskell_shlibdeps
Running rm -rf debian/tmp-db
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building haskell-edison-api using existing ./haskell-edison-api_1.3.1.orig.tar.gz
dpkg-source: info: building haskell-edison-api in haskell-edison-api_1.3.1-5.debian.tar.xz
dpkg-source: info: building haskell-edison-api in haskell-edison-api_1.3.1-5.dsc
 debian/rules binary
test -x debian/rules
dh_testroot
dh_prep 
dh_installdirs -A 
mkdir -p "."
CDBS WARNING:    DEB_DH_STRIP_ARGS is deprecated since 0.4.85
CDBS WARNING:    DEB_COMPRESS_EXCLUDE is deprecated since 0.4.85
. /usr/share/haskell-devscripts/Dh_Haskell.sh && \
make_setup_recipe
Running ghc --make Setup.hs -o debian/hlibrary.setup
[1 of 1] Compiling Main             ( Setup.hs, Setup.o )
Linking debian/hlibrary.setup ...
. /usr/share/haskell-devscripts/Dh_Haskell.sh && \
configure_recipe
Running debian/hlibrary.setup configure --ghc -v2 --package-db=/var/lib/ghc/package.conf.d --prefix=/usr --libdir=/usr/lib/haskell-packages/ghc/lib --libexecdir=/usr/lib --builddir=dist-ghc --ghc-option=-optl-Wl\,-z\,relro --haddockdir=/usr/lib/ghc-doc/haddock/edison-api-1.3.1/ --datasubdir=edison-api --htmldir=/usr/share/doc/libghc-edison-api-doc/html/ --enable-library-profiling
Using Parsec parser
Configuring EdisonAPI-1.3.1...
Dependency base ==4.*: using base-4.13.0.0
Dependency mtl >=1.0: using mtl-2.2.2
Source component graph: component lib
Configured component graph:
    component EdisonAPI-1.3.1-IyiCRf5zFeOKm3zrOQ0coy
        include base-4.13.0.0
        include mtl-2.2.2
Linked component graph:
    unit EdisonAPI-1.3.1-IyiCRf5zFeOKm3zrOQ0coy
        include base-4.13.0.0
        include mtl-2.2.2
        Data.Edison=EdisonAPI-1.3.1-IyiCRf5zFeOKm3zrOQ0coy:Data.Edison,Data.Edison.Assoc=EdisonAPI-1.3.1-IyiCRf5zFeOKm3zrOQ0coy:Data.Edison.Assoc,Data.Edison.Coll=EdisonAPI-1.3.1-IyiCRf5zFeOKm3zrOQ0coy:Data.Edison.Coll,Data.Edison.Coll.Utils=EdisonAPI-1.3.1-IyiCRf5zFeOKm3zrOQ0coy:Data.Edison.Coll.Utils,Data.Edison.Prelude=EdisonAPI-1.3.1-IyiCRf5zFeOKm3zrOQ0coy:Data.Edison.Prelude,Data.Edison.Seq=EdisonAPI-1.3.1-IyiCRf5zFeOKm3zrOQ0coy:Data.Edison.Seq,Data.Edison.Seq.ListSeq=EdisonAPI-1.3.1-IyiCRf5zFeOKm3zrOQ0coy:Data.Edison.Seq.ListSeq,Data.Edison.Sym=EdisonAPI-1.3.1-IyiCRf5zFeOKm3zrOQ0coy:Data.Edison.Sym
Ready component graph:
    definite EdisonAPI-1.3.1-IyiCRf5zFeOKm3zrOQ0coy
        depends base-4.13.0.0
        depends mtl-2.2.2
Using Cabal-3.0.1.0 compiled by ghc-8.8
Using compiler: ghc-8.8.4
Using install prefix: /usr
Executables installed in: /usr/bin
Libraries installed in:
/usr/lib/haskell-packages/ghc/lib/x86_64-linux-ghc-8.8.4/EdisonAPI-1.3.1-IyiCRf5zFeOKm3zrOQ0coy
Dynamic Libraries installed in:
/usr/lib/haskell-packages/ghc/lib/x86_64-linux-ghc-8.8.4
Private executables installed in:
/usr/lib/x86_64-linux-ghc-8.8.4/EdisonAPI-1.3.1
Data files installed in: /usr/share/edison-api
Documentation installed in:
/usr/share/doc/x86_64-linux-ghc-8.8.4/EdisonAPI-1.3.1
Configuration files installed in: /usr/etc
No alex found
Using ar found on system at: /usr/bin/x86_64-linux-gnu-ar
No c2hs found
No cpphs found
No doctest found
Using gcc version 10 found on system at: /usr/bin/x86_64-linux-gnu-gcc
Using ghc version 8.8.4 found on system at: /usr/bin/ghc
Using ghc-pkg version 8.8.4 found on system at: /usr/bin/ghc-pkg
No ghcjs found
No ghcjs-pkg found
No greencard found
Using haddock version 2.23.0 found on system at: /usr/bin/haddock
No happy found
Using haskell-suite found on system at: haskell-suite-dummy-location
Using haskell-suite-pkg found on system at: haskell-suite-pkg-dummy-location
No hmake found
Using hpc version 0.67 found on system at: /usr/bin/hpc
Using hsc2hs version 0.68.7 found on system at: /usr/bin/hsc2hs
Using hscolour version 1.24 found on system at: /usr/bin/HsColour
No jhc found
Using ld found on system at: /usr/bin/x86_64-linux-gnu-ld.gold
Using pkg-config version 0.29.2 found on system at: /usr/bin/pkg-config
Using runghc version 8.8.4 found on system at: /usr/bin/runghc
Using strip version 2.35 found on system at: /usr/bin/strip
Using tar found on system at: /usr/bin/tar
No uhc found
touch configure-ghc-stamp
. /usr/share/haskell-devscripts/Dh_Haskell.sh &&\
haddock_recipe
Running debian/hlibrary.setup haddock --builddir=dist-ghc --with-haddock=/usr/bin/haddock --with-ghc=ghc --verbose=2 --html --hoogle --haddock-options=\"--mathjax=file:///usr/share/javascript/mathjax/MathJax.js\" --hyperlink-source
/usr/bin/haddock --version
/usr/bin/ghc --numeric-version
/usr/bin/haddock --ghc-version '--mathjax=file:///usr/share/javascript/mathjax/MathJax.js'
/usr/bin/ghc --print-libdir
/usr/bin/ghc-pkg init dist-ghc/package.conf.inplace
creating dist-ghc/build
creating dist-ghc/build/autogen
creating dist-ghc/build/autogen
Preprocessing library for EdisonAPI-1.3.1..
Running Haddock on library for EdisonAPI-1.3.1..
creating dist-ghc/doc/html/EdisonAPI
creating dist-ghc/doc/html
creating dist-ghc/doc
creating dist-ghc/doc/html
creating dist-ghc/doc/html/EdisonAPI
/usr/bin/haddock '@dist-ghc/doc/html/EdisonAPI/haddock-response1486-3.txt' '--mathjax=file:///usr/share/javascript/mathjax/MathJax.js'
Warning: --source-* options are ignored when --hyperlinked-source is enabled.

src/Data/Edison/Seq/ListSeq.hs:134:12: error:
    * Could not deduce (MonadFail rm) arising from a use of `fail'
      from the context: Monad rm
        bound by the type signature for:
                   lview :: forall (rm :: * -> *) a. Monad rm => [a] -> rm (a, [a])
        at src/Data/Edison/Seq/ListSeq.hs:57:1-50
      Possible fix:
        add (MonadFail rm) to the context of
          the type signature for:
            lview :: forall (rm :: * -> *) a. Monad rm => [a] -> rm (a, [a])
    * In the expression: fail "ListSeq.lview: empty sequence"
      In an equation for `lview':
          lview [] = fail "ListSeq.lview: empty sequence"
    |
134 | lview [] = fail "ListSeq.lview: empty sequence"
    |            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

src/Data/Edison/Seq/ListSeq.hs:137:13: error:
    * Could not deduce (MonadFail rm) arising from a use of `fail'
      from the context: Monad rm
        bound by the type signature for:
                   lheadM :: forall (rm :: * -> *) a. Monad rm => [a] -> rm a
        at src/Data/Edison/Seq/ListSeq.hs:59:1-43
      Possible fix:
        add (MonadFail rm) to the context of
          the type signature for:
            lheadM :: forall (rm :: * -> *) a. Monad rm => [a] -> rm a
    * In the expression: fail "ListSeq.lheadM: empty sequence"
      In an equation for `lheadM':
          lheadM [] = fail "ListSeq.lheadM: empty sequence"
    |
137 | lheadM [] = fail "ListSeq.lheadM: empty sequence"
    |             ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

src/Data/Edison/Seq/ListSeq.hs:143:13: error:
    * Could not deduce (MonadFail rm) arising from a use of `fail'
      from the context: Monad rm
        bound by the type signature for:
                   ltailM :: forall (rm :: * -> *) a. Monad rm => [a] -> rm [a]
        at src/Data/Edison/Seq/ListSeq.hs:61:1-45
      Possible fix:
        add (MonadFail rm) to the context of
          the type signature for:
            ltailM :: forall (rm :: * -> *) a. Monad rm => [a] -> rm [a]
    * In the expression: fail "ListSeq.ltailM: empty sequence"
      In an equation for `ltailM':
          ltailM [] = fail "ListSeq.ltailM: empty sequence"
    |
143 | ltailM [] = fail "ListSeq.ltailM: empty sequence"
    |             ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

src/Data/Edison/Seq/ListSeq.hs:149:12: error:
    * Could not deduce (MonadFail rm) arising from a use of `fail'
      from the context: Monad rm
        bound by the type signature for:
                   rview :: forall (rm :: * -> *) a. Monad rm => [a] -> rm (a, [a])
        at src/Data/Edison/Seq/ListSeq.hs:62:1-50
      Possible fix:
        add (MonadFail rm) to the context of
          the type signature for:
            rview :: forall (rm :: * -> *) a. Monad rm => [a] -> rm (a, [a])
    * In the expression: fail "ListSeq.rview: empty sequence"
      In an equation for `rview':
          rview [] = fail "ListSeq.rview: empty sequence"
    |
149 | rview [] = fail "ListSeq.rview: empty sequence"
    |            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

src/Data/Edison/Seq/ListSeq.hs:152:13: error:
    * Could not deduce (MonadFail rm) arising from a use of `fail'
      from the context: Monad rm
        bound by the type signature for:
                   rheadM :: forall (rm :: * -> *) a. Monad rm => [a] -> rm a
        at src/Data/Edison/Seq/ListSeq.hs:64:1-43
      Possible fix:
        add (MonadFail rm) to the context of
          the type signature for:
            rheadM :: forall (rm :: * -> *) a. Monad rm => [a] -> rm a
    * In the expression: fail "ListSeq.rheadM: empty sequence"
      In an equation for `rheadM':
          rheadM [] = fail "ListSeq.rheadM: empty sequence"
    |
152 | rheadM [] = fail "ListSeq.rheadM: empty sequence"
    |             ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

src/Data/Edison/Seq/ListSeq.hs:162:13: error:
    * Could not deduce (MonadFail rm) arising from a use of `fail'
      from the context: Monad rm
        bound by the type signature for:
                   rtailM :: forall (rm :: * -> *) a. Monad rm => [a] -> rm [a]
        at src/Data/Edison/Seq/ListSeq.hs:66:1-45
      Possible fix:
        add (MonadFail rm) to the context of
          the type signature for:
            rtailM :: forall (rm :: * -> *) a. Monad rm => [a] -> rm [a]
    * In the expression: fail "ListSeq.rtailM: empty sequence"
      In an equation for `rtailM':
          rtailM [] = fail "ListSeq.rtailM: empty sequence"
    |
162 | rtailM [] = fail "ListSeq.rtailM: empty sequence"
    |             ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

src/Data/Edison/Seq/ListSeq.hs:258:13: error:
    * Could not deduce (MonadFail m) arising from a use of `fail'
      from the context: Monad m
        bound by the type signature for:
                   lookupM :: forall (m :: * -> *) a. Monad m => Int -> [a] -> m a
        at src/Data/Edison/Seq/ListSeq.hs:95:1-48
      Possible fix:
        add (MonadFail m) to the context of
          the type signature for:
            lookupM :: forall (m :: * -> *) a. Monad m => Int -> [a] -> m a
    * In the expression: fail "ListSeq.lookup: not found"
      In an equation for `lookupM':
          lookupM i xs
            | i < 0 = fail "ListSeq.lookup: not found"
            | otherwise
            = case drop i xs of
                [] -> fail "ListSeq.lookup: not found"
                (x : _) -> return x
    |
258 |   | i < 0 = fail "ListSeq.lookup: not found"
    |             ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Haddock coverage:
 100% (  6 /  6) in 'Data.Edison.Prelude'
 100% ( 12 / 12) in 'Data.Edison.Seq'
Haddock failed (no modules?), refusing to create empty documentation package.
make: *** [/usr/share/cdbs/1/class/hlibrary.mk:173: build-haddock-stamp] Error 1
dpkg-buildpackage: error: debian/rules binary subprocess returned exit status 2
