+ date
Wed Apr 19 20:37:19 UTC 2023
+ apt-get source --only-source rawdns=1.6~ds1-1
Reading package lists...
NOTICE: 'rawdns' packaging is maintained in the 'Git' version control system at:
https://anonscm.debian.org/git/pkg-go/packages/rawdns.git
Please use:
git clone https://anonscm.debian.org/git/pkg-go/packages/rawdns.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 38.9 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main rawdns 1.6~ds1-1 (dsc) [2067 B]
Get:2 http://repo.pureos.net/pureos byzantium/main rawdns 1.6~ds1-1 (tar) [33.5 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main rawdns 1.6~ds1-1 (diff) [3332 B]
dpkg-source: info: extracting rawdns in rawdns-1.6~ds1
dpkg-source: info: unpacking rawdns_1.6~ds1.orig.tar.gz
dpkg-source: info: unpacking rawdns_1.6~ds1-1.debian.tar.xz
Fetched 38.9 kB in 0s (233 kB/s)
W: Download is performed unsandboxed as root as file 'rawdns_1.6~ds1-1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source rawdns=1.6~ds1-1
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  dh-exec dh-golang golang-1.15-go golang-1.15-src golang-any
  golang-github-miekg-dns-dev golang-go golang-golang-x-crypto-dev
  golang-golang-x-net-dev golang-golang-x-sync-dev golang-golang-x-sys-dev
  golang-golang-x-term-dev golang-golang-x-text-dev golang-src
0 upgraded, 14 newly installed, 0 to remove and 0 not upgraded.
Need to get 67.9 MB of archives.
After this operation, 413 MB of additional disk space will be used.
Get:1 http://repo.pureos.net/pureos byzantium/main amd64 dh-exec amd64 0.23.2 [26.6 kB]
Get:2 http://repo.pureos.net/pureos byzantium/main amd64 dh-golang all 1.51 [24.6 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-src amd64 1.15.15-1~deb11u4 [13.9 MB]
Get:4 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-go amd64 1.15.15-1~deb11u4 [47.4 MB]
Get:5 http://repo.pureos.net/pureos byzantium/main amd64 golang-src amd64 2:1.15~1 [4856 B]
Get:6 http://repo.pureos.net/pureos byzantium/main amd64 golang-go amd64 2:1.15~1 [23.9 kB]
Get:7 http://repo.pureos.net/pureos byzantium/main amd64 golang-any amd64 2:1.15~1 [4976 B]
Get:8 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-sys-dev all 0.0~git20210124.22da62e-1 [308 kB]
Get:9 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-text-dev all 0.3.6-1 [3857 kB]
Get:10 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-net-dev all 1:0.0+git20210119.5f4716e+dfsg-4 [659 kB]
Get:11 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-term-dev all 0.0~git20201210.2321bbc-1 [14.5 kB]
Get:12 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-crypto-dev all 1:0.0~git20201221.eec23a3-1 [1538 kB]
Get:13 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-sync-dev all 0.0~git20210220.036812b-1 [19.1 kB]
Get:14 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-miekg-dns-dev all 1.1.35-1 [162 kB]
Fetched 67.9 MB in 3s (26.4 MB/s)
Selecting previously unselected package dh-exec.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 102989 files and directories currently installed.)
Preparing to unpack .../00-dh-exec_0.23.2_amd64.deb ...
Unpacking dh-exec (0.23.2) ...
Selecting previously unselected package dh-golang.
Preparing to unpack .../01-dh-golang_1.51_all.deb ...
Unpacking dh-golang (1.51) ...
Selecting previously unselected package golang-1.15-src.
Preparing to unpack .../02-golang-1.15-src_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-src (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-1.15-go.
Preparing to unpack .../03-golang-1.15-go_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-go (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-src:amd64.
Preparing to unpack .../04-golang-src_2%3a1.15~1_amd64.deb ...
Unpacking golang-src:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-go.
Preparing to unpack .../05-golang-go_2%3a1.15~1_amd64.deb ...
Unpacking golang-go (2:1.15~1) ...
Selecting previously unselected package golang-any:amd64.
Preparing to unpack .../06-golang-any_2%3a1.15~1_amd64.deb ...
Unpacking golang-any:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-golang-x-sys-dev.
Preparing to unpack .../07-golang-golang-x-sys-dev_0.0~git20210124.22da62e-1_all.deb ...
Unpacking golang-golang-x-sys-dev (0.0~git20210124.22da62e-1) ...
Selecting previously unselected package golang-golang-x-text-dev.
Preparing to unpack .../08-golang-golang-x-text-dev_0.3.6-1_all.deb ...
Unpacking golang-golang-x-text-dev (0.3.6-1) ...
Selecting previously unselected package golang-golang-x-net-dev.
Preparing to unpack .../09-golang-golang-x-net-dev_1%3a0.0+git20210119.5f4716e+dfsg-4_all.deb ...
Unpacking golang-golang-x-net-dev (1:0.0+git20210119.5f4716e+dfsg-4) ...
Selecting previously unselected package golang-golang-x-term-dev.
Preparing to unpack .../10-golang-golang-x-term-dev_0.0~git20201210.2321bbc-1_all.deb ...
Unpacking golang-golang-x-term-dev (0.0~git20201210.2321bbc-1) ...
Selecting previously unselected package golang-golang-x-crypto-dev.
Preparing to unpack .../11-golang-golang-x-crypto-dev_1%3a0.0~git20201221.eec23a3-1_all.deb ...
Unpacking golang-golang-x-crypto-dev (1:0.0~git20201221.eec23a3-1) ...
Selecting previously unselected package golang-golang-x-sync-dev.
Preparing to unpack .../12-golang-golang-x-sync-dev_0.0~git20210220.036812b-1_all.deb ...
Unpacking golang-golang-x-sync-dev (0.0~git20210220.036812b-1) ...
Selecting previously unselected package golang-github-miekg-dns-dev.
Preparing to unpack .../13-golang-github-miekg-dns-dev_1.1.35-1_all.deb ...
Unpacking golang-github-miekg-dns-dev (1.1.35-1) ...
Setting up dh-golang (1.51) ...
Setting up golang-1.15-src (1.15.15-1~deb11u4) ...
Setting up golang-golang-x-sys-dev (0.0~git20210124.22da62e-1) ...
Setting up dh-exec (0.23.2) ...
Setting up golang-golang-x-term-dev (0.0~git20201210.2321bbc-1) ...
Setting up golang-golang-x-sync-dev (0.0~git20210220.036812b-1) ...
Setting up golang-golang-x-text-dev (0.3.6-1) ...
Setting up golang-src:amd64 (2:1.15~1) ...
Setting up golang-1.15-go (1.15.15-1~deb11u4) ...
Setting up golang-go (2:1.15~1) ...
Setting up golang-any:amd64 (2:1.15~1) ...
Setting up golang-golang-x-net-dev (1:0.0+git20210119.5f4716e+dfsg-4) ...
Setting up golang-golang-x-crypto-dev (1:0.0~git20201221.eec23a3-1) ...
Setting up golang-github-miekg-dns-dev (1.1.35-1) ...
Processing triggers for man-db (2.9.4-2) ...
+ find . -maxdepth 1 -name rawdns* -type d
+ cd ./rawdns-1.6~ds1
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package rawdns
dpkg-buildpackage: info: source version 1.6~ds1-1
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Tianon Gravi <tianon@debian.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --buildsystem=golang --with=golang
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_auto_clean -O--buildsystem=golang
dh_auto_clean: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_clean -O--buildsystem=golang
dh_clean: warning: Compatibility levels before 10 are deprecated (level 9 in use)
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building rawdns using existing ./rawdns_1.6~ds1.orig.tar.gz
dpkg-source: info: building rawdns in rawdns_1.6~ds1-1.debian.tar.xz
dpkg-source: info: building rawdns in rawdns_1.6~ds1-1.dsc
 debian/rules build
dh build --buildsystem=golang --with=golang
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_update_autotools_config -O--buildsystem=golang
   dh_auto_configure -O--buildsystem=golang
dh_auto_configure: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_auto_build -O--buildsystem=golang
dh_auto_build: warning: Compatibility levels before 10 are deprecated (level 9 in use)
	cd obj-x86_64-linux-gnu && go install -trimpath -v -p 1 github.com/tianon/rawdns/src/cmd/rawdns
internal/unsafeheader
internal/cpu
internal/bytealg
runtime/internal/atomic
runtime/internal/sys
runtime/internal/math
runtime
internal/reflectlite
errors
internal/race
sync/atomic
sync
io
unicode
unicode/utf8
bytes
container/list
internal/oserror
syscall
time
context
hash
math/bits
math
strconv
crypto
crypto/internal/subtle
crypto/subtle
reflect
encoding/binary
crypto/cipher
crypto/aes
crypto/des
sort
internal/fmtsort
internal/syscall/unix
internal/poll
internal/syscall/execenv
internal/testlog
os
fmt
math/rand
strings
math/big
crypto/elliptic
crypto/internal/randutil
crypto/sha512
unicode/utf16
encoding/asn1
vendor/golang.org/x/crypto/cryptobyte/asn1
vendor/golang.org/x/crypto/cryptobyte
crypto/ecdsa
crypto/ed25519/internal/edwards25519
bufio
crypto/rand
crypto/ed25519
crypto/hmac
crypto/md5
crypto/rc4
crypto/rsa
crypto/sha1
crypto/sha256
crypto/dsa
encoding/hex
crypto/x509/pkix
encoding/base64
encoding/pem
path/filepath
io/ioutil
vendor/golang.org/x/net/dns/dnsmessage
internal/nettrace
internal/singleflight
runtime/cgo
net
net/url
crypto/x509
vendor/golang.org/x/crypto/internal/subtle
vendor/golang.org/x/crypto/chacha20
vendor/golang.org/x/crypto/poly1305
vendor/golang.org/x/sys/cpu
vendor/golang.org/x/crypto/chacha20poly1305
vendor/golang.org/x/crypto/curve25519
vendor/golang.org/x/crypto/hkdf
crypto/tls
encoding
encoding/json
encoding/base32
golang.org/x/crypto/ed25519
golang.org/x/net/bpf
golang.org/x/net/internal/iana
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
golang.org/x/net/internal/socket
golang.org/x/net/ipv4
golang.org/x/net/ipv6
github.com/miekg/dns
log
compress/flate
hash/crc32
compress/gzip
vendor/golang.org/x/text/transform
vendor/golang.org/x/text/unicode/bidi
vendor/golang.org/x/text/secure/bidirule
vendor/golang.org/x/text/unicode/norm
vendor/golang.org/x/net/idna
net/textproto
vendor/golang.org/x/net/http/httpguts
vendor/golang.org/x/net/http/httpproxy
vendor/golang.org/x/net/http2/hpack
mime
mime/quotedprintable
mime/multipart
net/http/httptrace
net/http/internal
path
net/http
os/signal
github.com/tianon/rawdns/src/cmd/rawdns
   dh_auto_test -O--buildsystem=golang
dh_auto_test: warning: Compatibility levels before 10 are deprecated (level 9 in use)
	cd obj-x86_64-linux-gnu && go test -vet=off -v -p 1 github.com/tianon/rawdns/src/cmd/rawdns
?   	github.com/tianon/rawdns/src/cmd/rawdns	[no test files]
 debian/rules binary
dh binary --buildsystem=golang --with=golang
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_testroot -O--buildsystem=golang
   dh_prep -O--buildsystem=golang
   debian/rules override_dh_auto_install
make[1]: Entering directory '/build/rawdns/rawdns-1.6~ds1'
dh_auto_install
dh_auto_install: warning: Compatibility levels before 10 are deprecated (level 9 in use)
	cd obj-x86_64-linux-gnu && mkdir -p /build/rawdns/rawdns-1.6\~ds1/debian/rawdns/usr
	cd obj-x86_64-linux-gnu && cp -r bin /build/rawdns/rawdns-1.6\~ds1/debian/rawdns/usr
# the source code of rawdns does not make a library
rm -r debian/rawdns/usr/share/gocode
make[1]: Leaving directory '/build/rawdns/rawdns-1.6~ds1'
   dh_install -O--buildsystem=golang
dh_install: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installdocs -O--buildsystem=golang
dh_installdocs: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installchangelogs -O--buildsystem=golang
dh_installchangelogs: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installinit -O--buildsystem=golang
dh_installinit: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_perl -O--buildsystem=golang
   dh_link -O--buildsystem=golang
   dh_strip_nondeterminism -O--buildsystem=golang
   dh_compress -O--buildsystem=golang
dh_compress: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_fixperms -O--buildsystem=golang
   dh_missing -O--buildsystem=golang
dh_missing: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_strip -O--buildsystem=golang
dh_strip: warning: Compatibility levels before 10 are deprecated (level 9 in use)
dh_strip: warning: Could not find the BuildID in debian/rawdns/usr/bin/rawdns
   dh_makeshlibs -O--buildsystem=golang
dh_makeshlibs: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_shlibdeps -O--buildsystem=golang
dh_shlibdeps: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installdeb -O--buildsystem=golang
dh_installdeb: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_golang -O--buildsystem=golang
dh_golang: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_gencontrol -O--buildsystem=golang
   dh_md5sums -O--buildsystem=golang
   dh_builddeb -O--buildsystem=golang
dpkg-deb: building package 'rawdns' in '../rawdns_1.6~ds1-1_amd64.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../rawdns_1.6~ds1-1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload (original source is included)
+ date
Wed Apr 19 20:38:43 UTC 2023
+ cd ..
+ ls -la
total 1876
drwxr-xr-x 3 root root    4096 Apr 19 20:38 .
drwxr-xr-x 3 root root    4096 Apr 19 20:37 ..
-rw-r--r-- 1 root root   14198 Apr 19 20:38 buildlog.txt
drwxr-xr-x 8 root root    4096 Apr 19 20:38 rawdns-1.6~ds1
-rw-r--r-- 1 root root    3288 Apr 19 20:38 rawdns_1.6~ds1-1.debian.tar.xz
-rw-r--r-- 1 root root    1159 Apr 19 20:38 rawdns_1.6~ds1-1.dsc
-rw-r--r-- 1 root root    5758 Apr 19 20:38 rawdns_1.6~ds1-1_amd64.buildinfo
-rw-r--r-- 1 root root    1882 Apr 19 20:38 rawdns_1.6~ds1-1_amd64.changes
-rw-r--r-- 1 root root 1833692 Apr 19 20:38 rawdns_1.6~ds1-1_amd64.deb
-rw-r--r-- 1 root root   33509 Feb  3  2017 rawdns_1.6~ds1.orig.tar.gz
+ find . -maxdepth 1 -type f
+ sha256sum ./rawdns_1.6~ds1-1.debian.tar.xz ./buildlog.txt ./rawdns_1.6~ds1.orig.tar.gz ./rawdns_1.6~ds1-1.dsc ./rawdns_1.6~ds1-1_amd64.changes ./rawdns_1.6~ds1-1_amd64.buildinfo ./rawdns_1.6~ds1-1_amd64.deb
9899e4e41cc99537a6db42460a4845ee34436bf014fae9055b1c5de698229e65  ./rawdns_1.6~ds1-1.debian.tar.xz
58a0607d213bf4223b570ef2fff914a4676ea5e574f5550593229f2cbce3ee6e  ./buildlog.txt
4bccc85c1849a319813892b513da692f41b7d9a8f90b243c54ee6d5855505a62  ./rawdns_1.6~ds1.orig.tar.gz
1828c5b1f32aee1dde4f31ce874cbd218f72b166afdd93243b40a9e85c649901  ./rawdns_1.6~ds1-1.dsc
985304723d6476c96864fb68e4d1fbc5aec7c7e73dbd77d079830ddecdea0a98  ./rawdns_1.6~ds1-1_amd64.changes
24c35fb6e1111bf1e3c288db85fbad35ba1470451d5ba0100bcda977d948e905  ./rawdns_1.6~ds1-1_amd64.buildinfo
75f82c2c7b1894a0961203b017dbac68dd091914f7017f590c654547eeb589f4  ./rawdns_1.6~ds1-1_amd64.deb
+ mkdir published
+ cd published
+ cd ../
+ ls rawdns_1.6~ds1-1_amd64.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/r/rawdns/rawdns_1.6~ds1-1_amd64.deb
--2023-04-19 20:38:43--  http://repo.pureos.net/pureos/pool/main/r/rawdns/rawdns_1.6~ds1-1_amd64.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 404 Not Found
2023-04-19 20:38:43 ERROR 404: Not Found.

