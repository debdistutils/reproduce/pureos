+ date
Tue Apr 18 10:12:34 UTC 2023
+ apt-get source --only-source golang-github-fernet-fernet-go=0.0~git20180830.9eac43b-2
Reading package lists...
NOTICE: 'golang-github-fernet-fernet-go' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/go-team/packages/fernet-go.git
Please use:
git clone https://salsa.debian.org/go-team/packages/fernet-go.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 10.9 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main golang-github-fernet-fernet-go 0.0~git20180830.9eac43b-2 (dsc) [2391 B]
Get:2 http://repo.pureos.net/pureos byzantium/main golang-github-fernet-fernet-go 0.0~git20180830.9eac43b-2 (tar) [6180 B]
Get:3 http://repo.pureos.net/pureos byzantium/main golang-github-fernet-fernet-go 0.0~git20180830.9eac43b-2 (diff) [2320 B]
dpkg-source: info: extracting golang-github-fernet-fernet-go in golang-github-fernet-fernet-go-0.0~git20180830.9eac43b
dpkg-source: info: unpacking golang-github-fernet-fernet-go_0.0~git20180830.9eac43b.orig.tar.xz
dpkg-source: info: unpacking golang-github-fernet-fernet-go_0.0~git20180830.9eac43b-2.debian.tar.xz
Fetched 10.9 kB in 0s (74.9 kB/s)
W: Download is performed unsandboxed as root as file 'golang-github-fernet-fernet-go_0.0~git20180830.9eac43b-2.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source golang-github-fernet-fernet-go=0.0~git20180830.9eac43b-2
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  dh-golang golang-1.15-go golang-1.15-src golang-any golang-go golang-src
0 upgraded, 6 newly installed, 0 to remove and 0 not upgraded.
Need to get 61.3 MB of archives.
After this operation, 359 MB of additional disk space will be used.
Get:1 http://repo.pureos.net/pureos byzantium/main amd64 dh-golang all 1.51 [24.6 kB]
Get:2 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-src amd64 1.15.15-1~deb11u4 [13.9 MB]
Get:3 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-go amd64 1.15.15-1~deb11u4 [47.4 MB]
Get:4 http://repo.pureos.net/pureos byzantium/main amd64 golang-src amd64 2:1.15~1 [4856 B]
Get:5 http://repo.pureos.net/pureos byzantium/main amd64 golang-go amd64 2:1.15~1 [23.9 kB]
Get:6 http://repo.pureos.net/pureos byzantium/main amd64 golang-any amd64 2:1.15~1 [4976 B]
Fetched 61.3 MB in 2s (31.2 MB/s)
Selecting previously unselected package dh-golang.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 102989 files and directories currently installed.)
Preparing to unpack .../0-dh-golang_1.51_all.deb ...
Unpacking dh-golang (1.51) ...
Selecting previously unselected package golang-1.15-src.
Preparing to unpack .../1-golang-1.15-src_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-src (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-1.15-go.
Preparing to unpack .../2-golang-1.15-go_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-go (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-src:amd64.
Preparing to unpack .../3-golang-src_2%3a1.15~1_amd64.deb ...
Unpacking golang-src:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-go.
Preparing to unpack .../4-golang-go_2%3a1.15~1_amd64.deb ...
Unpacking golang-go (2:1.15~1) ...
Selecting previously unselected package golang-any:amd64.
Preparing to unpack .../5-golang-any_2%3a1.15~1_amd64.deb ...
Unpacking golang-any:amd64 (2:1.15~1) ...
Setting up dh-golang (1.51) ...
Setting up golang-1.15-src (1.15.15-1~deb11u4) ...
Setting up golang-src:amd64 (2:1.15~1) ...
Setting up golang-1.15-go (1.15.15-1~deb11u4) ...
Setting up golang-go (2:1.15~1) ...
Setting up golang-any:amd64 (2:1.15~1) ...
Processing triggers for man-db (2.9.4-2) ...
+ find . -maxdepth 1 -name golang-github-fernet-fernet-go* -type d
+ cd ./golang-github-fernet-fernet-go-0.0~git20180830.9eac43b
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package golang-github-fernet-fernet-go
dpkg-buildpackage: info: source version 0.0~git20180830.9eac43b-2
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Dmitry Smirnov <onlyjob@debian.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --buildsystem=golang --with=golang
   dh_auto_clean -O--buildsystem=golang
   dh_autoreconf_clean -O--buildsystem=golang
   dh_clean -O--buildsystem=golang
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building golang-github-fernet-fernet-go using existing ./golang-github-fernet-fernet-go_0.0~git20180830.9eac43b.orig.tar.xz
dpkg-source: info: building golang-github-fernet-fernet-go in golang-github-fernet-fernet-go_0.0~git20180830.9eac43b-2.debian.tar.xz
dpkg-source: info: building golang-github-fernet-fernet-go in golang-github-fernet-fernet-go_0.0~git20180830.9eac43b-2.dsc
 debian/rules build
dh build --buildsystem=golang --with=golang
   dh_update_autotools_config -O--buildsystem=golang
   dh_autoreconf -O--buildsystem=golang
   dh_auto_configure -O--buildsystem=golang
   dh_auto_build -O--buildsystem=golang
	cd obj-x86_64-linux-gnu && go install -trimpath -v -p 50 github.com/fernet/fernet-go github.com/fernet/fernet-go/cmd/fernet-keygen github.com/fernet/fernet-go/cmd/fernet-sign
crypto/internal/subtle
runtime/internal/sys
math/bits
unicode
crypto/subtle
internal/race
internal/unsafeheader
sync/atomic
unicode/utf8
runtime/internal/atomic
internal/cpu
runtime/internal/math
internal/testlog
internal/bytealg
math
runtime
internal/reflectlite
sync
math/rand
errors
sort
io
internal/oserror
strconv
syscall
hash
strings
bytes
crypto/hmac
crypto
reflect
bufio
internal/syscall/execenv
internal/syscall/unix
time
internal/poll
encoding/binary
internal/fmtsort
os
encoding/base64
crypto/cipher
crypto/sha256
path/filepath
fmt
crypto/aes
io/ioutil
encoding/hex
log
math/big
crypto/rand
github.com/fernet/fernet-go
github.com/fernet/fernet-go/cmd/fernet-keygen
github.com/fernet/fernet-go/cmd/fernet-sign
   dh_auto_test -O--buildsystem=golang
	cd obj-x86_64-linux-gnu && go test -vet=off -v -p 50 github.com/fernet/fernet-go github.com/fernet/fernet-go/cmd/fernet-keygen github.com/fernet/fernet-go/cmd/fernet-sign
=== RUN   TestGenerate
--- PASS: TestGenerate (0.00s)
=== RUN   TestVerifyOk
    fernet_test.go:55: test 0 
    fernet_test.go:57: tok
    fernet_test.go:150: [128]
    fernet_test.go:150: [0 0 0 0 29 192 158 176]
    fernet_test.go:150: [0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15]
    fernet_test.go:150: [45 54 213 202 70 85 98 153 253 225 48 8 99 56 4 178 197 255 144 149 245 211 143 154 184 110 85 67 224 38 134 240 59 62 201 113 185 171 71 174 35 86 106]
    fernet_test.go:150: [84 224 140 42 12]
    fernet_test.go:55: test 1 
    fernet_test.go:57: tok
    fernet_test.go:150: [128]
    fernet_test.go:150: [0 0 0 0 29 192 158 176]
    fernet_test.go:150: [0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15]
    fernet_test.go:150: [45 54 213 202 70 85 98 153 253 225 48 8 99 56 4 178 197 255 144 149 245 211 143 154 184 110 85 67 224 38 134 240 59 62 201 113 185 171 71 174 35 86 106]
    fernet_test.go:150: [84 224 140 42 12]
--- PASS: TestVerifyOk (0.00s)
=== RUN   TestVerifyBad
    fernet_test.go:73: test 0 incorrect mac
    fernet_test.go:74: gAAAAAAdwJ6xAAECAwQFBgcICQoLDA0OD3HkMATM5lFqGaerZ-fWPAl1-szkFVzXTuGb4hR8AKtwcaX1YdykQUFBQUFBQUFBQQ==
    fernet_test.go:73: test 1 too short
    fernet_test.go:74: gAAAAAAdwJ6xAAECAwQFBgcICQoLDA0OD3HkMATM5lFqGaerZ-fWPA==
    fernet_test.go:73: test 3 payload size not multiple of block size
    fernet_test.go:74: gAAAAAAdwJ6xAAECAwQFBgcICQoLDA0OD3HkMATM5lFqGaerZ-fWPOm73QeoCk9uGib28Xe5vz6oxq5nmxbx_v7mrfyudzUm
    fernet_test.go:73: test 4 payload padding error
    fernet_test.go:74: gAAAAAAdwJ6xAAECAwQFBgcICQoLDA0ODz4LEpdELGQAad7aNEHbf-JkLPIpuiYRLQ3RtXatOYREu2FWke6CnJNYIbkuKNqOhw==
    fernet_test.go:73: test 5 far-future TS (unacceptable clock skew)
    fernet_test.go:74: gAAAAAAdwStRAAECAwQFBgcICQoLDA0OD3HkMATM5lFqGaerZ-fWPAnja1xKYyhd-Y6mSkTOyTGJmw2Xc2a6kBd-iX9b_qXQcw==
    fernet_test.go:73: test 6 expired TTL
    fernet_test.go:74: gAAAAAAdwJ6xAAECAwQFBgcICQoLDA0OD3HkMATM5lFqGaerZ-fWPAl1-szkFVzXTuGb4hR8AKtwcaX1YdykRtfsH-p1YsUD2Q==
    fernet_test.go:73: test 7 incorrect IV (causes padding error)
    fernet_test.go:74: gAAAAAAdwJ6xBQECAwQFBgcICQoLDA0OD3HkMATM5lFqGaerZ-fWPAkLhFLHpGtDBRLRTZeUfWgHSv49TF2AUEZ1TIvcZjK1zQ==
--- PASS: TestVerifyBad (0.00s)
=== RUN   TestVerifyBadBase64
    fernet_test.go:92: test 2 invalid base64
    fernet_test.go:93: %%%%%%%%%%%%%AECAwQFBgcICQoLDA0OD3HkMATM5lFqGaerZ-fWPAl1-szkFVzXTuGb4hR8AKtwcaX1YdykRtfsH-p1YsUD2Q==
--- PASS: TestVerifyBadBase64 (0.00s)
=== RUN   TestDecodeKey
--- PASS: TestDecodeKey (0.00s)
=== RUN   TestDecodeKeys
--- PASS: TestDecodeKeys (0.00s)
=== RUN   TestEncode
--- PASS: TestEncode (0.00s)
=== RUN   Example
--- PASS: Example (0.00s)
PASS
ok  	github.com/fernet/fernet-go	0.006s
?   	github.com/fernet/fernet-go/cmd/fernet-keygen	[no test files]
?   	github.com/fernet/fernet-go/cmd/fernet-sign	[no test files]
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary --buildsystem=golang --with=golang
   dh_testroot -O--buildsystem=golang
   dh_prep -O--buildsystem=golang
   debian/rules override_dh_auto_install
make[1]: Entering directory '/build/golang-github-fernet-fernet-go/golang-github-fernet-fernet-go-0.0~git20180830.9eac43b'
dh_auto_install
	cd obj-x86_64-linux-gnu && mkdir -p /build/golang-github-fernet-fernet-go/golang-github-fernet-fernet-go-0.0\~git20180830.9eac43b/debian/tmp/usr
	cd obj-x86_64-linux-gnu && cp -r bin /build/golang-github-fernet-fernet-go/golang-github-fernet-fernet-go-0.0\~git20180830.9eac43b/debian/tmp/usr
rm -rf debian/tmp/usr/share/gocode/src/github.com/fernet/fernet-go/Readme
rm -rf debian/tmp/usr/share/gocode/src/github.com/fernet/fernet-go/License
rm -rf debian/tmp/usr/share/gocode/src/github.com/fernet/fernet-go/.gitignore
make[1]: Leaving directory '/build/golang-github-fernet-fernet-go/golang-github-fernet-fernet-go-0.0~git20180830.9eac43b'
   dh_install -O--buildsystem=golang
   dh_installdocs -O--buildsystem=golang
   dh_installchangelogs -O--buildsystem=golang
   dh_installinit -O--buildsystem=golang
   dh_perl -O--buildsystem=golang
   dh_link -O--buildsystem=golang
   dh_strip_nondeterminism -O--buildsystem=golang
   dh_compress -O--buildsystem=golang
   dh_fixperms -O--buildsystem=golang
   dh_missing -O--buildsystem=golang
   dh_strip -O--buildsystem=golang
dh_strip: warning: Could not find the BuildID in debian/fernet-go/usr/bin/fernet-sign
dh_strip: warning: Could not find the BuildID in debian/fernet-go/usr/bin/fernet-keygen
   dh_makeshlibs -O--buildsystem=golang
   dh_shlibdeps -O--buildsystem=golang
   dh_installdeb -O--buildsystem=golang
   dh_golang -O--buildsystem=golang
   dh_gencontrol -O--buildsystem=golang
dpkg-gencontrol: warning: Depends field of package fernet-go: substitution variable ${shlibs:Depends} used, but is not defined
dpkg-gencontrol: warning: Depends field of package golang-github-fernet-fernet-go-dev: substitution variable ${shlibs:Depends} used, but is not defined
   dh_md5sums -O--buildsystem=golang
   dh_builddeb -O--buildsystem=golang
dpkg-deb: building package 'fernet-go' in '../fernet-go_0.0~git20180830.9eac43b-2_amd64.deb'.
dpkg-deb: building package 'golang-github-fernet-fernet-go-dev' in '../golang-github-fernet-fernet-go-dev_0.0~git20180830.9eac43b-2_all.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../golang-github-fernet-fernet-go_0.0~git20180830.9eac43b-2_amd64.changes
dpkg-genchanges: info: not including original source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: binary and diff upload (original source NOT included)
+ date
Tue Apr 18 10:13:24 UTC 2023
+ cd ..
+ ls -la
total 724
drwxr-xr-x 3 root root   4096 Apr 18 10:13 .
drwxr-xr-x 3 root root   4096 Apr 18 10:12 ..
-rw-r--r-- 1 root root  12575 Apr 18 10:13 buildlog.txt
-rw-r--r-- 1 root root 672576 Apr 18 10:13 fernet-go_0.0~git20180830.9eac43b-2_amd64.deb
drwxr-xr-x 6 root root   4096 Apr 18 10:13 golang-github-fernet-fernet-go-0.0~git20180830.9eac43b
-rw-r--r-- 1 root root   7784 Apr 18 10:13 golang-github-fernet-fernet-go-dev_0.0~git20180830.9eac43b-2_all.deb
-rw-r--r-- 1 root root   2320 Apr 18 10:13 golang-github-fernet-fernet-go_0.0~git20180830.9eac43b-2.debian.tar.xz
-rw-r--r-- 1 root root   1508 Apr 18 10:13 golang-github-fernet-fernet-go_0.0~git20180830.9eac43b-2.dsc
-rw-r--r-- 1 root root   6045 Apr 18 10:13 golang-github-fernet-fernet-go_0.0~git20180830.9eac43b-2_amd64.buildinfo
-rw-r--r-- 1 root root   2569 Apr 18 10:13 golang-github-fernet-fernet-go_0.0~git20180830.9eac43b-2_amd64.changes
-rw-r--r-- 1 root root   6180 Nov  8  2019 golang-github-fernet-fernet-go_0.0~git20180830.9eac43b.orig.tar.xz
+ find . -maxdepth 1 -type f
+ sha256sum ./golang-github-fernet-fernet-go_0.0~git20180830.9eac43b-2.debian.tar.xz ./buildlog.txt ./fernet-go_0.0~git20180830.9eac43b-2_amd64.deb ./golang-github-fernet-fernet-go_0.0~git20180830.9eac43b.orig.tar.xz ./golang-github-fernet-fernet-go_0.0~git20180830.9eac43b-2.dsc ./golang-github-fernet-fernet-go_0.0~git20180830.9eac43b-2_amd64.changes ./golang-github-fernet-fernet-go_0.0~git20180830.9eac43b-2_amd64.buildinfo ./golang-github-fernet-fernet-go-dev_0.0~git20180830.9eac43b-2_all.deb
215c559b76248b78842eed630b8cdd32508317457cfb07739e038401ca449812  ./golang-github-fernet-fernet-go_0.0~git20180830.9eac43b-2.debian.tar.xz
f037b317693783a0f779ac075674186b88368892e6057f91f6ef36f2aaa9d6b9  ./buildlog.txt
033207e8c24d20b205b32bb2c40b666b4bb13d62605b82cbd45de1e86493fcb9  ./fernet-go_0.0~git20180830.9eac43b-2_amd64.deb
3883a7b08847f7b885420211194caaa5fe1951734e552639a3a5b406f0dc6ee2  ./golang-github-fernet-fernet-go_0.0~git20180830.9eac43b.orig.tar.xz
87cee4c2822d102883bd76cbbe92b22c26b31c937fad2d088f001d478c64ea67  ./golang-github-fernet-fernet-go_0.0~git20180830.9eac43b-2.dsc
aeaed9b0313328c4586cff22247aec1725dc2f5cb6af393ad49f47fab8f14f13  ./golang-github-fernet-fernet-go_0.0~git20180830.9eac43b-2_amd64.changes
aab9c495ea171198710bf920b0adb4387d8325cd67833ea7b2cb599f6b89a55b  ./golang-github-fernet-fernet-go_0.0~git20180830.9eac43b-2_amd64.buildinfo
81be56cb7af10aded86d781d0ce2c931ddbd3db52fa1aa5ea8d8ddc4b10f1f96  ./golang-github-fernet-fernet-go-dev_0.0~git20180830.9eac43b-2_all.deb
+ mkdir published
+ cd published
+ cd ../
+ ls fernet-go_0.0~git20180830.9eac43b-2_amd64.deb golang-github-fernet-fernet-go-dev_0.0~git20180830.9eac43b-2_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/g/golang-github-fernet-fernet-go/fernet-go_0.0~git20180830.9eac43b-2_amd64.deb
--2023-04-18 10:13:24--  http://repo.pureos.net/pureos/pool/main/g/golang-github-fernet-fernet-go/fernet-go_0.0~git20180830.9eac43b-2_amd64.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 404 Not Found
2023-04-18 10:13:24 ERROR 404: Not Found.

+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/g/golang-github-fernet-fernet-go/golang-github-fernet-fernet-go-dev_0.0~git20180830.9eac43b-2_all.deb
--2023-04-18 10:13:24--  http://repo.pureos.net/pureos/pool/main/g/golang-github-fernet-fernet-go/golang-github-fernet-fernet-go-dev_0.0~git20180830.9eac43b-2_all.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 7784 (7.6K) [application/octet-stream]
Saving to: 'golang-github-fernet-fernet-go-dev_0.0~git20180830.9eac43b-2_all.deb'

     0K .......                                               100%  394M=0s

2023-04-18 10:13:24 (394 MB/s) - 'golang-github-fernet-fernet-go-dev_0.0~git20180830.9eac43b-2_all.deb' saved [7784/7784]

+ + tee ../SHA256SUMS
find . -maxdepth 1 -type f
+ sha256sum ./golang-github-fernet-fernet-go-dev_0.0~git20180830.9eac43b-2_all.deb
81be56cb7af10aded86d781d0ce2c931ddbd3db52fa1aa5ea8d8ddc4b10f1f96  ./golang-github-fernet-fernet-go-dev_0.0~git20180830.9eac43b-2_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./golang-github-fernet-fernet-go-dev_0.0~git20180830.9eac43b-2_all.deb: OK
+ echo Package golang-github-fernet-fernet-go version 0.0~git20180830.9eac43b-2 is reproducible!
Package golang-github-fernet-fernet-go version 0.0~git20180830.9eac43b-2 is reproducible!
