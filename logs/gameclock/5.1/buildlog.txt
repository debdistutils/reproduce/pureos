+ date
Tue Apr 18 06:24:49 UTC 2023
+ apt-get source --only-source gameclock=5.1
Reading package lists...
NOTICE: 'gameclock' packaging is maintained in the 'Git' version control system at:
https://0xacab.org/anarcat/gameclock.git -b 5.x
Please use:
git clone https://0xacab.org/anarcat/gameclock.git -b 5.x
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 48.1 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main gameclock 5.1 (dsc) [1607 B]
Get:2 http://repo.pureos.net/pureos byzantium/main gameclock 5.1 (tar) [46.5 kB]
dpkg-source: info: extracting gameclock in gameclock-5.1
dpkg-source: info: unpacking gameclock_5.1.tar.xz
Fetched 48.1 kB in 0s (249 kB/s)
W: Download is performed unsandboxed as root as file 'gameclock_5.1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source gameclock=5.1
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
Some packages could not be installed. This may mean that you have
requested an impossible situation or if you are using the unstable
distribution that some required packages have not yet been created
or been moved out of Incoming.
The following information may help to resolve the situation:

The following packages have unmet dependencies:
 dh-python : Breaks: python
E: Unable to correct problems, you have held broken packages.
