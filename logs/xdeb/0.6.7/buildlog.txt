+ date
Thu Apr 20 04:28:38 UTC 2023
+ apt-get source --only-source xdeb=0.6.7
Reading package lists...
Need to get 45.9 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main xdeb 0.6.7 (dsc) [1450 B]
Get:2 http://repo.pureos.net/pureos byzantium/main xdeb 0.6.7 (tar) [44.4 kB]
dpkg-source: info: extracting xdeb in xdeb-0.6.7
dpkg-source: info: unpacking xdeb_0.6.7.tar.xz
Fetched 45.9 kB in 0s (237 kB/s)
W: Download is performed unsandboxed as root as file 'xdeb_0.6.7.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source xdeb=0.6.7
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
Some packages could not be installed. This may mean that you have
requested an impossible situation or if you are using the unstable
distribution that some required packages have not yet been created
or been moved out of Incoming.
The following information may help to resolve the situation:

The following packages have unmet dependencies:
 builddeps:xdeb : Depends: python (>= 2.7)
                  Depends: python-debian (>= 0.1.15~) but it is not installable
                  Depends: python-apt (>= 0.7.91) but it is not installable
E: Unable to correct problems, you have held broken packages.
