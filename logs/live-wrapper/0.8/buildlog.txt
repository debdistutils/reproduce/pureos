+ date
Wed Apr 19 06:47:17 UTC 2023
+ apt-get source --only-source live-wrapper=0.8
Reading package lists...
NOTICE: 'live-wrapper' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/live-team/live-wrapper.git
Please use:
git clone https://salsa.debian.org/live-team/live-wrapper.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 31.8 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main live-wrapper 0.8 (dsc) [1948 B]
Get:2 http://repo.pureos.net/pureos byzantium/main live-wrapper 0.8 (tar) [29.9 kB]
dpkg-source: info: extracting live-wrapper in live-wrapper-0.8
dpkg-source: info: unpacking live-wrapper_0.8.tar.xz
Fetched 31.8 kB in 0s (192 kB/s)
W: Download is performed unsandboxed as root as file 'live-wrapper_0.8.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source live-wrapper=0.8
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
Some packages could not be installed. This may mean that you have
requested an impossible situation or if you are using the unstable
distribution that some required packages have not yet been created
or been moved out of Incoming.
The following information may help to resolve the situation:

The following packages have unmet dependencies:
 builddeps:live-wrapper : Depends: python-apt but it is not installable
                          Depends: python-cliapp but it is not installable
                          Depends: python-requests but it is not installable
                          Depends: vmdebootstrap (>= 1.7-1+nmu1) but it is not installable
E: Unable to correct problems, you have held broken packages.
