+ date
Sun May 21 08:52:03 UTC 2023
+ id
uid=0(root) gid=0(root) groups=0(root)
+ apt-get source --only-source librem5-base=60pureos1
Reading package lists...
Need to get 262 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main librem5-base 60pureos1 (dsc) [2166 B]
Get:2 http://repo.pureos.net/pureos byzantium/main librem5-base 60pureos1 (tar) [259 kB]
dpkg-source: info: extracting librem5-base in librem5-base-60pureos1
dpkg-source: info: unpacking librem5-base_60pureos1.tar.xz
Fetched 262 kB in 0s (540 kB/s)
W: Download is performed unsandboxed as root as file 'librem5-base_60pureos1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source librem5-base=60pureos1
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  desktop-file-utils germinate python3-germinate shellcheck
0 upgraded, 4 newly installed, 0 to remove and 77 not upgraded.
Need to get 2228 kB of archives.
After this operation, 16.0 MB of additional disk space will be used.
Get:1 http://repo.pureos.net/pureos byzantium/main amd64 desktop-file-utils amd64 0.26-1 [91.2 kB]
Get:2 http://repo.pureos.net/pureos byzantium/main amd64 python3-germinate all 2.36 [52.8 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main amd64 germinate all 2.36 [35.5 kB]
Get:4 http://repo.pureos.net/pureos byzantium/main amd64 shellcheck amd64 0.7.1-1+deb11u1 [2049 kB]
Fetched 2228 kB in 2s (1018 kB/s)
Selecting previously unselected package desktop-file-utils.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 102989 files and directories currently installed.)
Preparing to unpack .../desktop-file-utils_0.26-1_amd64.deb ...
Unpacking desktop-file-utils (0.26-1) ...
Selecting previously unselected package python3-germinate.
Preparing to unpack .../python3-germinate_2.36_all.deb ...
Unpacking python3-germinate (2.36) ...
Selecting previously unselected package germinate.
Preparing to unpack .../germinate_2.36_all.deb ...
Unpacking germinate (2.36) ...
Selecting previously unselected package shellcheck.
Preparing to unpack .../shellcheck_0.7.1-1+deb11u1_amd64.deb ...
Unpacking shellcheck (0.7.1-1+deb11u1) ...
Setting up desktop-file-utils (0.26-1) ...
Setting up shellcheck (0.7.1-1+deb11u1) ...
Setting up python3-germinate (2.36) ...
Setting up germinate (2.36) ...
Processing triggers for mailcap (3.69) ...
Processing triggers for man-db (2.9.4-2) ...
+ find . -maxdepth 1 -name librem5-base* -type d
+ cd ./librem5-base-60pureos1
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package librem5-base
dpkg-buildpackage: info: source version 60pureos1
dpkg-buildpackage: info: source distribution byzantium
dpkg-buildpackage: info: source changed by Sebastian Krzyszkowiak <sebastian.krzyszkowiak@puri.sm>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --with germinate
   dh_auto_clean
   dh_germinate_clean
   dh_clean
 dpkg-source -b .
dpkg-source: warning: Version number suggests PureOS changes, but there is no XSBC-Original-Maintainer field
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building librem5-base in librem5-base_60pureos1.tar.xz
dpkg-source: info: building librem5-base in librem5-base_60pureos1.dsc
 debian/rules build
dh build --with germinate
   dh_update_autotools_config
   dh_autoreconf
   dh_auto_configure
   dh_auto_build
	make -j40 "INSTALL=install --strip-program=true"
make[1]: Entering directory '/build/librem5-base/librem5-base-60pureos1'
shellcheck default/gadget/usb_gadget_start
shellcheck default/gadget/usb_gadget_stop
make[1]: Leaving directory '/build/librem5-base/librem5-base-60pureos1'
   debian/rules override_dh_auto_test
make[1]: Entering directory '/build/librem5-base/librem5-base-60pureos1'
desktop-file-validate default/hidden-apps/*.desktop
make[1]: Leaving directory '/build/librem5-base/librem5-base-60pureos1'
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary --with germinate
   dh_testroot
   dh_prep
   dh_auto_install
   dh_install
   dh_installdocs
   dh_installchangelogs
   debian/rules override_dh_installsystemd
make[1]: Entering directory '/build/librem5-base/librem5-base-60pureos1'
dh_installsystemd --no-start --name=usb_gadget
dh_installsystemd --no-start --name=bluetooth-brcmfmac
dh_installsystemd --no-start --name=librem5-lockdown-support
make[1]: Leaving directory '/build/librem5-base/librem5-base-60pureos1'
   debian/rules override_dh_installudev
make[1]: Entering directory '/build/librem5-base/librem5-base-60pureos1'
dh_installudev --name=librem5-devkit-sgtl5000 --priority=85
dh_installudev --name=librem5-pm --priority=85
dh_installudev --name=librem5-modem --priority=85
dh_installudev --name=librem5-cameras --priority=85
dh_installudev --name=librem5-hdmi-audio --priority=85
dh_installudev --name=librem5-storage --priority=85
dh_installudev --name=librem5-lockdown-support --priority=85
dh_installudev --name=librem5-brcmfmac --priority=85
dh_installudev --name=librem5-power-key --priority=85
make[1]: Leaving directory '/build/librem5-base/librem5-base-60pureos1'
   dh_installgsettings
   dh_installalternatives
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   debian/rules execute_after_dh_fixperms
make[1]: Entering directory '/build/librem5-base/librem5-base-60pureos1'
chmod 600 /build/librem5-base/librem5-base-60pureos1/debian/librem5-base-defaults/etc/usbguard/rules.d/librem5.conf
make[1]: Leaving directory '/build/librem5-base/librem5-base-60pureos1'
   dh_missing
   dh_germinate_metapackage
   dh_installdeb
   dh_gencontrol
dpkg-gencontrol: warning: Depends field of package librem5-dev-tools: substitution variable ${germinate:Depends} used, but is not defined
dpkg-gencontrol: warning: Depends field of package librem5-base-defaults: substitution variable ${shlibs:Depends} used, but is not defined
dpkg-gencontrol: warning: Recommends field of package librem5-gnome-phone: substitution variable ${germinate:Recommends} used, but is not defined
dpkg-gencontrol: warning: Recommends field of package librem5-gnome-dev: substitution variable ${germinate:Recommends} used, but is not defined
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'librem5-base' in '../librem5-base_60pureos1_all.deb'.
dpkg-deb: building package 'librem5-gnome-base' in '../librem5-gnome-base_60pureos1_all.deb'.
dpkg-deb: building package 'librem5-gnome-phone' in '../librem5-gnome-phone_60pureos1_all.deb'.
dpkg-deb: building package 'librem5-gnome-notfit' in '../librem5-gnome-notfit_60pureos1_all.deb'.
dpkg-deb: building package 'librem5-gnome' in '../librem5-gnome_60pureos1_all.deb'.
dpkg-deb: building package 'librem5-non-gnome' in '../librem5-non-gnome_60pureos1_all.deb'.
dpkg-deb: building package 'librem5-dev-tools' in '../librem5-dev-tools_60pureos1_all.deb'.
dpkg-deb: building package 'librem5-gnome-dev' in '../librem5-gnome-dev_60pureos1_all.deb'.
dpkg-deb: building package 'librem5-base-defaults' in '../librem5-base-defaults_60pureos1_all.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../librem5-base_60pureos1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Sun May 21 08:52:18 UTC 2023
+ cd ..
+ ls -la
total 716
drwxr-xr-x 3 root root   4096 May 21 08:52 .
drwxr-xr-x 3 root root   4096 May 21 08:52 ..
-rw-r--r-- 1 root root   8094 May 21 08:52 buildlog.txt
drwxr-xr-x 4 root root   4096 May  3 17:10 librem5-base-60pureos1
-rw-r--r-- 1 root root 261676 May 21 08:52 librem5-base-defaults_60pureos1_all.deb
-rw-r--r-- 1 root root   1457 May 21 08:52 librem5-base_60pureos1.dsc
-rw-r--r-- 1 root root 259444 May 21 08:52 librem5-base_60pureos1.tar.xz
-rw-r--r-- 1 root root  18028 May 21 08:52 librem5-base_60pureos1_all.deb
-rw-r--r-- 1 root root   8360 May 21 08:52 librem5-base_60pureos1_amd64.buildinfo
-rw-r--r-- 1 root root   5128 May 21 08:52 librem5-base_60pureos1_amd64.changes
-rw-r--r-- 1 root root  17988 May 21 08:52 librem5-dev-tools_60pureos1_all.deb
-rw-r--r-- 1 root root  17976 May 21 08:52 librem5-gnome-base_60pureos1_all.deb
-rw-r--r-- 1 root root  17944 May 21 08:52 librem5-gnome-dev_60pureos1_all.deb
-rw-r--r-- 1 root root  17852 May 21 08:52 librem5-gnome-notfit_60pureos1_all.deb
-rw-r--r-- 1 root root  17864 May 21 08:52 librem5-gnome-phone_60pureos1_all.deb
-rw-r--r-- 1 root root  17940 May 21 08:52 librem5-gnome_60pureos1_all.deb
-rw-r--r-- 1 root root  17860 May 21 08:52 librem5-non-gnome_60pureos1_all.deb
+ find . -maxdepth 1 -type f
+ sha256sum ./librem5-gnome-phone_60pureos1_all.deb ./librem5-base_60pureos1_amd64.buildinfo ./librem5-gnome-base_60pureos1_all.deb ./librem5-base_60pureos1.tar.xz ./buildlog.txt ./librem5-gnome_60pureos1_all.deb ./librem5-gnome-notfit_60pureos1_all.deb ./librem5-base_60pureos1_amd64.changes ./librem5-gnome-dev_60pureos1_all.deb ./librem5-base_60pureos1.dsc ./librem5-non-gnome_60pureos1_all.deb ./librem5-dev-tools_60pureos1_all.deb ./librem5-base_60pureos1_all.deb ./librem5-base-defaults_60pureos1_all.deb
277d7978d7abaa582ff7797020ac605cc4cb41ce78a552df780bac64eadb216b  ./librem5-gnome-phone_60pureos1_all.deb
805304265cf91a071bcbaf43c7411b9d1fd3f9f02be924b3b6bf70802c936e1f  ./librem5-base_60pureos1_amd64.buildinfo
e7234921708b66c4951bf2ba9bf49160bc36eaf2e19a93970140984135bfdaea  ./librem5-gnome-base_60pureos1_all.deb
c6f01532ecf692aae6f6dae552efda07bf1d0601402cc60b02685ca73008b906  ./librem5-base_60pureos1.tar.xz
07d94eb6c5254632a878da01f1a69e558329205effd8e8494408795bc7c2729e  ./buildlog.txt
2f8ac2f8b2d3f740488a687f76329a9528c6ba83d73766388f02eef983824f73  ./librem5-gnome_60pureos1_all.deb
69ac066a7c3fc7bf2170ca865ebd4d1a0cacadfa5402bd146cbd84e9b8dcd696  ./librem5-gnome-notfit_60pureos1_all.deb
d329ed1d7a86df181befcf7c8e07f4a23c2f26e8d0859860ffebb4503b33a4e2  ./librem5-base_60pureos1_amd64.changes
381852c2e8d50cc806f2811c6ac731ae63a25dd0a5a9036979c00e9b62ef05bd  ./librem5-gnome-dev_60pureos1_all.deb
c61be3af009ebcf7a6cd573553e45db9ed07d902603a33189146dd9e0b9e296b  ./librem5-base_60pureos1.dsc
5672c39325672891e4bf0474f454c05dd16b396938e037279b569a6346fbb847  ./librem5-non-gnome_60pureos1_all.deb
aa51b711f62c39aa1b0948a8c5abe81e45d2f35946a74bb83f36584ea2a89a55  ./librem5-dev-tools_60pureos1_all.deb
00b78ee311c33e68fefaf28c2a64d9fcaf2d5bf403aca89681db573eb4cc1b7e  ./librem5-base_60pureos1_all.deb
032aa81f973fe726eb1e82db4d147fedfef0cf217ef9f1af6490f8a35a99d907  ./librem5-base-defaults_60pureos1_all.deb
+ mkdir published
+ cd published
+ cd ../
+ ls librem5-base-defaults_60pureos1_all.deb librem5-base_60pureos1_all.deb librem5-dev-tools_60pureos1_all.deb librem5-gnome-base_60pureos1_all.deb librem5-gnome-dev_60pureos1_all.deb librem5-gnome-notfit_60pureos1_all.deb librem5-gnome-phone_60pureos1_all.deb librem5-gnome_60pureos1_all.deb librem5-non-gnome_60pureos1_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/libr/librem5-base/librem5-base-defaults_60pureos1_all.deb
--2023-05-21 08:52:18--  http://repo.pureos.net/pureos/pool/main/libr/librem5-base/librem5-base-defaults_60pureos1_all.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 261676 (256K) [application/octet-stream]
Saving to: 'librem5-base-defaults_60pureos1_all.deb'

     0K .......... .......... .......... .......... .......... 19%  507K 0s
    50K .......... .......... .......... .......... .......... 39%  972K 0s
   100K .......... .......... .......... .......... .......... 58%  809K 0s
   150K .......... .......... .......... .......... .......... 78%  551K 0s
   200K .......... .......... .......... .......... .......... 97% 1.48M 0s
   250K .....                                                 100% 1.44M=0.3s

2023-05-21 08:52:19 (754 KB/s) - 'librem5-base-defaults_60pureos1_all.deb' saved [261676/261676]

+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/libr/librem5-base/librem5-base_60pureos1_all.deb
--2023-05-21 08:52:19--  http://repo.pureos.net/pureos/pool/main/libr/librem5-base/librem5-base_60pureos1_all.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 18028 (18K) [application/octet-stream]
Saving to: 'librem5-base_60pureos1_all.deb'

     0K .......... .......                                    100%  391K=0.04s

2023-05-21 08:52:19 (391 KB/s) - 'librem5-base_60pureos1_all.deb' saved [18028/18028]

+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/libr/librem5-base/librem5-dev-tools_60pureos1_all.deb
--2023-05-21 08:52:19--  http://repo.pureos.net/pureos/pool/main/libr/librem5-base/librem5-dev-tools_60pureos1_all.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 17988 (18K) [application/octet-stream]
Saving to: 'librem5-dev-tools_60pureos1_all.deb'

     0K .......... .......                                    100%  394K=0.04s

2023-05-21 08:52:19 (394 KB/s) - 'librem5-dev-tools_60pureos1_all.deb' saved [17988/17988]

+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/libr/librem5-base/librem5-gnome-base_60pureos1_all.deb
--2023-05-21 08:52:19--  http://repo.pureos.net/pureos/pool/main/libr/librem5-base/librem5-gnome-base_60pureos1_all.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 17976 (18K) [application/octet-stream]
Saving to: 'librem5-gnome-base_60pureos1_all.deb'

     0K .......... .......                                    100%  428K=0.04s

2023-05-21 08:52:19 (428 KB/s) - 'librem5-gnome-base_60pureos1_all.deb' saved [17976/17976]

+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/libr/librem5-base/librem5-gnome-dev_60pureos1_all.deb
--2023-05-21 08:52:19--  http://repo.pureos.net/pureos/pool/main/libr/librem5-base/librem5-gnome-dev_60pureos1_all.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 17944 (18K) [application/octet-stream]
Saving to: 'librem5-gnome-dev_60pureos1_all.deb'

     0K .......... .......                                    100%  312K=0.06s

2023-05-21 08:52:19 (312 KB/s) - 'librem5-gnome-dev_60pureos1_all.deb' saved [17944/17944]

+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/libr/librem5-base/librem5-gnome-notfit_60pureos1_all.deb
--2023-05-21 08:52:19--  http://repo.pureos.net/pureos/pool/main/libr/librem5-base/librem5-gnome-notfit_60pureos1_all.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 17852 (17K) [application/octet-stream]
Saving to: 'librem5-gnome-notfit_60pureos1_all.deb'

     0K .......... .......                                    100%  291K=0.06s

2023-05-21 08:52:19 (291 KB/s) - 'librem5-gnome-notfit_60pureos1_all.deb' saved [17852/17852]

+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/libr/librem5-base/librem5-gnome-phone_60pureos1_all.deb
--2023-05-21 08:52:19--  http://repo.pureos.net/pureos/pool/main/libr/librem5-base/librem5-gnome-phone_60pureos1_all.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 17864 (17K) [application/octet-stream]
Saving to: 'librem5-gnome-phone_60pureos1_all.deb'

     0K .......... .......                                    100%  636K=0.03s

2023-05-21 08:52:20 (636 KB/s) - 'librem5-gnome-phone_60pureos1_all.deb' saved [17864/17864]

+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/libr/librem5-base/librem5-gnome_60pureos1_all.deb
--2023-05-21 08:52:20--  http://repo.pureos.net/pureos/pool/main/libr/librem5-base/librem5-gnome_60pureos1_all.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 17940 (18K) [application/octet-stream]
Saving to: 'librem5-gnome_60pureos1_all.deb'

     0K .......... .......                                    100%  650K=0.03s

2023-05-21 08:52:20 (650 KB/s) - 'librem5-gnome_60pureos1_all.deb' saved [17940/17940]

+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/libr/librem5-base/librem5-non-gnome_60pureos1_all.deb
--2023-05-21 08:52:20--  http://repo.pureos.net/pureos/pool/main/libr/librem5-base/librem5-non-gnome_60pureos1_all.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 17860 (17K) [application/octet-stream]
Saving to: 'librem5-non-gnome_60pureos1_all.deb'

     0K .......... .......                                    100%  645K=0.03s

2023-05-21 08:52:20 (645 KB/s) - 'librem5-non-gnome_60pureos1_all.deb' saved [17860/17860]

+ + tee ../SHA256SUMSfind
 . -maxdepth 1 -type f
+ sha256sum ./librem5-gnome-phone_60pureos1_all.deb ./librem5-gnome-base_60pureos1_all.deb ./librem5-gnome_60pureos1_all.deb ./librem5-gnome-notfit_60pureos1_all.deb ./librem5-gnome-dev_60pureos1_all.deb ./librem5-non-gnome_60pureos1_all.deb ./librem5-dev-tools_60pureos1_all.deb ./librem5-base_60pureos1_all.deb ./librem5-base-defaults_60pureos1_all.deb
277d7978d7abaa582ff7797020ac605cc4cb41ce78a552df780bac64eadb216b  ./librem5-gnome-phone_60pureos1_all.deb
e7234921708b66c4951bf2ba9bf49160bc36eaf2e19a93970140984135bfdaea  ./librem5-gnome-base_60pureos1_all.deb
2f8ac2f8b2d3f740488a687f76329a9528c6ba83d73766388f02eef983824f73  ./librem5-gnome_60pureos1_all.deb
69ac066a7c3fc7bf2170ca865ebd4d1a0cacadfa5402bd146cbd84e9b8dcd696  ./librem5-gnome-notfit_60pureos1_all.deb
381852c2e8d50cc806f2811c6ac731ae63a25dd0a5a9036979c00e9b62ef05bd  ./librem5-gnome-dev_60pureos1_all.deb
5672c39325672891e4bf0474f454c05dd16b396938e037279b569a6346fbb847  ./librem5-non-gnome_60pureos1_all.deb
aa51b711f62c39aa1b0948a8c5abe81e45d2f35946a74bb83f36584ea2a89a55  ./librem5-dev-tools_60pureos1_all.deb
00b78ee311c33e68fefaf28c2a64d9fcaf2d5bf403aca89681db573eb4cc1b7e  ./librem5-base_60pureos1_all.deb
032aa81f973fe726eb1e82db4d147fedfef0cf217ef9f1af6490f8a35a99d907  ./librem5-base-defaults_60pureos1_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./librem5-gnome-phone_60pureos1_all.deb: OK
./librem5-gnome-base_60pureos1_all.deb: OK
./librem5-gnome_60pureos1_all.deb: OK
./librem5-gnome-notfit_60pureos1_all.deb: OK
./librem5-gnome-dev_60pureos1_all.deb: OK
./librem5-non-gnome_60pureos1_all.deb: OK
./librem5-dev-tools_60pureos1_all.deb: OK
./librem5-base_60pureos1_all.deb: OK
./librem5-base-defaults_60pureos1_all.deb: OK
+ echo Package librem5-base version 60pureos1 is reproducible!
Package librem5-base version 60pureos1 is reproducible!
