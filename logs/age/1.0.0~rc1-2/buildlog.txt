+ date
Mon Apr 17 11:52:30 UTC 2023
+ apt-get source --only-source age=1.0.0~rc1-2
Reading package lists...
NOTICE: 'age' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/go-team/packages/age.git
Please use:
git clone https://salsa.debian.org/go-team/packages/age.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 53.3 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main age 1.0.0~rc1-2 (dsc) [1412 B]
Get:2 http://repo.pureos.net/pureos byzantium/main age 1.0.0~rc1-2 (tar) [47.0 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main age 1.0.0~rc1-2 (diff) [4928 B]
dpkg-source: info: extracting age in age-1.0.0~rc1
dpkg-source: info: unpacking age_1.0.0~rc1.orig.tar.gz
dpkg-source: info: unpacking age_1.0.0~rc1-2.debian.tar.xz
Fetched 53.3 kB in 0s (266 kB/s)
W: Download is performed unsandboxed as root as file 'age_1.0.0~rc1-2.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source age=1.0.0~rc1-2
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  dh-golang golang-1.15-go golang-1.15-src golang-any golang-go
  golang-golang-x-crypto-dev golang-golang-x-net-dev golang-golang-x-sys-dev
  golang-golang-x-term-dev golang-golang-x-text-dev golang-src
0 upgraded, 11 newly installed, 0 to remove and 0 not upgraded.
Need to get 67.7 MB of archives.
After this operation, 412 MB of additional disk space will be used.
Get:1 http://repo.pureos.net/pureos byzantium/main amd64 dh-golang all 1.51 [24.6 kB]
Get:2 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-src amd64 1.15.15-1~deb11u4 [13.9 MB]
Get:3 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-go amd64 1.15.15-1~deb11u4 [47.4 MB]
Get:4 http://repo.pureos.net/pureos byzantium/main amd64 golang-src amd64 2:1.15~1 [4856 B]
Get:5 http://repo.pureos.net/pureos byzantium/main amd64 golang-go amd64 2:1.15~1 [23.9 kB]
Get:6 http://repo.pureos.net/pureos byzantium/main amd64 golang-any amd64 2:1.15~1 [4976 B]
Get:7 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-sys-dev all 0.0~git20210124.22da62e-1 [308 kB]
Get:8 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-text-dev all 0.3.6-1 [3857 kB]
Get:9 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-net-dev all 1:0.0+git20210119.5f4716e+dfsg-4 [659 kB]
Get:10 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-term-dev all 0.0~git20201210.2321bbc-1 [14.5 kB]
Get:11 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-crypto-dev all 1:0.0~git20201221.eec23a3-1 [1538 kB]
Fetched 67.7 MB in 3s (24.3 MB/s)
Selecting previously unselected package dh-golang.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 102989 files and directories currently installed.)
Preparing to unpack .../00-dh-golang_1.51_all.deb ...
Unpacking dh-golang (1.51) ...
Selecting previously unselected package golang-1.15-src.
Preparing to unpack .../01-golang-1.15-src_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-src (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-1.15-go.
Preparing to unpack .../02-golang-1.15-go_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-go (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-src:amd64.
Preparing to unpack .../03-golang-src_2%3a1.15~1_amd64.deb ...
Unpacking golang-src:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-go.
Preparing to unpack .../04-golang-go_2%3a1.15~1_amd64.deb ...
Unpacking golang-go (2:1.15~1) ...
Selecting previously unselected package golang-any:amd64.
Preparing to unpack .../05-golang-any_2%3a1.15~1_amd64.deb ...
Unpacking golang-any:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-golang-x-sys-dev.
Preparing to unpack .../06-golang-golang-x-sys-dev_0.0~git20210124.22da62e-1_all.deb ...
Unpacking golang-golang-x-sys-dev (0.0~git20210124.22da62e-1) ...
Selecting previously unselected package golang-golang-x-text-dev.
Preparing to unpack .../07-golang-golang-x-text-dev_0.3.6-1_all.deb ...
Unpacking golang-golang-x-text-dev (0.3.6-1) ...
Selecting previously unselected package golang-golang-x-net-dev.
Preparing to unpack .../08-golang-golang-x-net-dev_1%3a0.0+git20210119.5f4716e+dfsg-4_all.deb ...
Unpacking golang-golang-x-net-dev (1:0.0+git20210119.5f4716e+dfsg-4) ...
Selecting previously unselected package golang-golang-x-term-dev.
Preparing to unpack .../09-golang-golang-x-term-dev_0.0~git20201210.2321bbc-1_all.deb ...
Unpacking golang-golang-x-term-dev (0.0~git20201210.2321bbc-1) ...
Selecting previously unselected package golang-golang-x-crypto-dev.
Preparing to unpack .../10-golang-golang-x-crypto-dev_1%3a0.0~git20201221.eec23a3-1_all.deb ...
Unpacking golang-golang-x-crypto-dev (1:0.0~git20201221.eec23a3-1) ...
Setting up dh-golang (1.51) ...
Setting up golang-1.15-src (1.15.15-1~deb11u4) ...
Setting up golang-golang-x-sys-dev (0.0~git20210124.22da62e-1) ...
Setting up golang-golang-x-term-dev (0.0~git20201210.2321bbc-1) ...
Setting up golang-golang-x-text-dev (0.3.6-1) ...
Setting up golang-src:amd64 (2:1.15~1) ...
Setting up golang-1.15-go (1.15.15-1~deb11u4) ...
Setting up golang-go (2:1.15~1) ...
Setting up golang-any:amd64 (2:1.15~1) ...
Setting up golang-golang-x-net-dev (1:0.0+git20210119.5f4716e+dfsg-4) ...
Setting up golang-golang-x-crypto-dev (1:0.0~git20201221.eec23a3-1) ...
Processing triggers for man-db (2.9.4-2) ...
+ find . -maxdepth 1 -name age* -type d
+ cd ./age-1.0.0~rc1
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package age
dpkg-buildpackage: info: source version 1.0.0~rc1-2
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Johan Fleury <jfleury@arcaik.net>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --builddirectory=_build --buildsystem=golang --with=golang
   dh_auto_clean -O--builddirectory=_build -O--buildsystem=golang
   dh_autoreconf_clean -O--builddirectory=_build -O--buildsystem=golang
   dh_clean -O--builddirectory=_build -O--buildsystem=golang
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building age using existing ./age_1.0.0~rc1.orig.tar.gz
dpkg-source: info: building age in age_1.0.0~rc1-2.debian.tar.xz
dpkg-source: info: building age in age_1.0.0~rc1-2.dsc
 debian/rules binary
dh binary --builddirectory=_build --buildsystem=golang --with=golang
   dh_update_autotools_config -O--builddirectory=_build -O--buildsystem=golang
   dh_autoreconf -O--builddirectory=_build -O--buildsystem=golang
   dh_auto_configure -O--builddirectory=_build -O--buildsystem=golang
   debian/rules override_dh_auto_build
make[1]: Entering directory '/build/age/age-1.0.0~rc1'
dh_auto_build -- -buildmode=pie -ldflags "-X main.Version=1.0.0~rc1"
	cd _build && go install -trimpath -v -p 50 -buildmode=pie -ldflags "-X main.Version=1.0.0~rc1" filippo.io/age filippo.io/age/agessh filippo.io/age/armor filippo.io/age/cmd/age filippo.io/age/cmd/age-keygen filippo.io/age/internal/bech32 filippo.io/age/internal/format filippo.io/age/internal/stream
internal/unsafeheader
crypto/subtle
golang.org/x/crypto/internal/subtle
internal/race
unicode/utf16
runtime/internal/atomic
sync/atomic
crypto/internal/subtle
unicode
runtime/cgo
golang.org/x/crypto/cryptobyte/asn1
internal/cpu
unicode/utf8
internal/nettrace
vendor/golang.org/x/crypto/cryptobyte/asn1
golang.org/x/sys/internal/unsafeheader
runtime/internal/sys
math/bits
internal/bytealg
runtime/internal/math
internal/testlog
math
runtime
internal/reflectlite
sync
internal/singleflight
math/rand
errors
sort
internal/oserror
io
vendor/golang.org/x/net/dns/dnsmessage
strconv
syscall
crypto/internal/randutil
hash
bytes
strings
crypto/hmac
golang.org/x/crypto/pbkdf2
golang.org/x/crypto/hkdf
crypto/rc4
crypto
golang.org/x/crypto/blowfish
reflect
bufio
internal/syscall/execenv
internal/syscall/unix
time
context
internal/poll
os
encoding/binary
internal/fmtsort
path/filepath
fmt
runtime/debug
net
encoding/base64
golang.org/x/crypto/poly1305
crypto/ed25519/internal/edwards25519
crypto/sha256
crypto/cipher
crypto/md5
crypto/sha512
crypto/sha1
golang.org/x/sys/unix
encoding/pem
io/ioutil
golang.org/x/crypto/scrypt
golang.org/x/crypto/ssh/internal/bcrypt_pbkdf
crypto/des
golang.org/x/crypto/chacha20
crypto/aes
golang.org/x/sys/cpu
golang.org/x/crypto/chacha20poly1305
filippo.io/age/internal/bech32
filippo.io/age/internal/format
golang.org/x/crypto/curve25519
encoding/hex
log
net/url
flag
math/big
filippo.io/age/internal/stream
filippo.io/age/armor
golang.org/x/term
golang.org/x/crypto/ssh/terminal
crypto/rand
crypto/dsa
crypto/elliptic
encoding/asn1
crypto/ed25519
filippo.io/age
crypto/rsa
golang.org/x/crypto/ed25519
filippo.io/age/cmd/age-keygen
crypto/x509/pkix
golang.org/x/crypto/cryptobyte
vendor/golang.org/x/crypto/cryptobyte
crypto/ecdsa
crypto/x509
golang.org/x/crypto/ssh
filippo.io/age/agessh
filippo.io/age/cmd/age
make[1]: Leaving directory '/build/age/age-1.0.0~rc1'
   dh_auto_test -O--builddirectory=_build -O--buildsystem=golang
	cd _build && go test -vet=off -v -p 50 filippo.io/age filippo.io/age/agessh filippo.io/age/armor filippo.io/age/cmd/age filippo.io/age/cmd/age-keygen filippo.io/age/internal/bech32 filippo.io/age/internal/format filippo.io/age/internal/stream
=== RUN   TestEncryptDecryptX25519
--- PASS: TestEncryptDecryptX25519 (0.00s)
=== RUN   TestEncryptDecryptScrypt
--- PASS: TestEncryptDecryptScrypt (0.33s)
=== RUN   TestParseIdentities
=== RUN   TestParseIdentities/valid
=== RUN   TestParseIdentities/invalid
--- PASS: TestParseIdentities (0.00s)
    --- PASS: TestParseIdentities/valid (0.00s)
    --- PASS: TestParseIdentities/invalid (0.00s)
=== RUN   TestX25519RoundTrip
--- PASS: TestX25519RoundTrip (0.00s)
=== RUN   TestScryptRoundTrip
--- PASS: TestScryptRoundTrip (0.31s)
=== RUN   ExampleEncrypt
--- PASS: ExampleEncrypt (0.00s)
=== RUN   ExampleDecrypt
--- PASS: ExampleDecrypt (0.00s)
=== RUN   ExampleParseIdentities
--- PASS: ExampleParseIdentities (0.00s)
=== RUN   ExampleGenerateX25519Identity
--- PASS: ExampleGenerateX25519Identity (0.00s)
PASS
ok  	filippo.io/age	0.648s
=== RUN   TestSSHRSARoundTrip
--- PASS: TestSSHRSARoundTrip (0.02s)
=== RUN   TestSSHEd25519RoundTrip
--- PASS: TestSSHEd25519RoundTrip (0.00s)
PASS
ok  	filippo.io/age/agessh	0.029s
=== RUN   TestArmor
--- PASS: TestArmor (0.00s)
=== RUN   ExampleNewWriter
--- PASS: ExampleNewWriter (0.00s)
=== RUN   ExampleNewReader
--- PASS: ExampleNewReader (0.00s)
PASS
ok  	filippo.io/age/armor	0.006s
=== RUN   TestVectors
=== RUN   TestVectors/ed25519
    age_test.go:84: hi
=== RUN   TestVectors/empty_recipient_body
    age_test.go:84: 2 test 2 string
=== RUN   TestVectors/fail_large_filekey_scrypt
=== RUN   TestVectors/fail_large_filekey_x25519
=== RUN   TestVectors/fail_scrypt_and_x25519
=== RUN   TestVectors/fail_scrypt_work_factor_23
=== RUN   TestVectors/nomatch_scrypt
=== RUN   TestVectors/nomatch_x25519
=== RUN   TestVectors/rsa
    age_test.go:84: hi
=== RUN   TestVectors/scrypt_work_factor_10
    age_test.go:84: hi
--- PASS: TestVectors (0.05s)
    --- PASS: TestVectors/ed25519 (0.00s)
    --- PASS: TestVectors/empty_recipient_body (0.00s)
    --- PASS: TestVectors/fail_large_filekey_scrypt (0.01s)
    --- PASS: TestVectors/fail_large_filekey_x25519 (0.00s)
    --- PASS: TestVectors/fail_scrypt_and_x25519 (0.00s)
    --- PASS: TestVectors/fail_scrypt_work_factor_23 (0.00s)
    --- PASS: TestVectors/nomatch_scrypt (0.01s)
    --- PASS: TestVectors/nomatch_x25519 (0.00s)
    --- PASS: TestVectors/rsa (0.01s)
    --- PASS: TestVectors/scrypt_work_factor_10 (0.01s)
PASS
ok  	filippo.io/age/cmd/age	0.063s
?   	filippo.io/age/cmd/age-keygen	[no test files]
=== RUN   TestBech32
--- PASS: TestBech32 (0.00s)
PASS
ok  	filippo.io/age/internal/bech32	0.007s
=== RUN   TestStanzaMarshal
--- PASS: TestStanzaMarshal (0.00s)
PASS
ok  	filippo.io/age/internal/format	0.004s
=== RUN   TestRoundTrip
=== RUN   TestRoundTrip/len=0,step=512
    stream_test.go:74: buffer size: 16
=== RUN   TestRoundTrip/len=1000,step=512
    stream_test.go:74: buffer size: 1016
=== RUN   TestRoundTrip/len=65536,step=512
    stream_test.go:74: buffer size: 65552
=== RUN   TestRoundTrip/len=65636,step=512
    stream_test.go:74: buffer size: 65668
=== RUN   TestRoundTrip/len=0,step=600
    stream_test.go:74: buffer size: 16
=== RUN   TestRoundTrip/len=1000,step=600
    stream_test.go:74: buffer size: 1016
=== RUN   TestRoundTrip/len=65536,step=600
    stream_test.go:74: buffer size: 65552
=== RUN   TestRoundTrip/len=65636,step=600
    stream_test.go:74: buffer size: 65668
=== RUN   TestRoundTrip/len=0,step=1000
    stream_test.go:74: buffer size: 16
=== RUN   TestRoundTrip/len=1000,step=1000
    stream_test.go:74: buffer size: 1016
=== RUN   TestRoundTrip/len=65536,step=1000
    stream_test.go:74: buffer size: 65552
=== RUN   TestRoundTrip/len=65636,step=1000
    stream_test.go:74: buffer size: 65668
=== RUN   TestRoundTrip/len=0,step=65536
    stream_test.go:74: buffer size: 16
=== RUN   TestRoundTrip/len=1000,step=65536
    stream_test.go:74: buffer size: 1016
=== RUN   TestRoundTrip/len=65536,step=65536
    stream_test.go:74: buffer size: 65552
=== RUN   TestRoundTrip/len=65636,step=65536
    stream_test.go:74: buffer size: 65668
--- PASS: TestRoundTrip (0.02s)
    --- PASS: TestRoundTrip/len=0,step=512 (0.00s)
    --- PASS: TestRoundTrip/len=1000,step=512 (0.00s)
    --- PASS: TestRoundTrip/len=65536,step=512 (0.00s)
    --- PASS: TestRoundTrip/len=65636,step=512 (0.00s)
    --- PASS: TestRoundTrip/len=0,step=600 (0.00s)
    --- PASS: TestRoundTrip/len=1000,step=600 (0.00s)
    --- PASS: TestRoundTrip/len=65536,step=600 (0.00s)
    --- PASS: TestRoundTrip/len=65636,step=600 (0.00s)
    --- PASS: TestRoundTrip/len=0,step=1000 (0.00s)
    --- PASS: TestRoundTrip/len=1000,step=1000 (0.00s)
    --- PASS: TestRoundTrip/len=65536,step=1000 (0.01s)
    --- PASS: TestRoundTrip/len=65636,step=1000 (0.00s)
    --- PASS: TestRoundTrip/len=0,step=65536 (0.00s)
    --- PASS: TestRoundTrip/len=1000,step=65536 (0.00s)
    --- PASS: TestRoundTrip/len=65536,step=65536 (0.00s)
    --- PASS: TestRoundTrip/len=65636,step=65536 (0.00s)
PASS
ok  	filippo.io/age/internal/stream	0.024s
   create-stamp debian/debhelper-build-stamp
   dh_testroot -O--builddirectory=_build -O--buildsystem=golang
   dh_prep -O--builddirectory=_build -O--buildsystem=golang
   debian/rules override_dh_auto_install
make[1]: Entering directory '/build/age/age-1.0.0~rc1'
dh_auto_install -- --no-source
	cd _build && mkdir -p /build/age/age-1.0.0\~rc1/debian/age/usr
	cd _build && cp -r bin /build/age/age-1.0.0\~rc1/debian/age/usr
make[1]: Leaving directory '/build/age/age-1.0.0~rc1'
   dh_installdocs -O--builddirectory=_build -O--buildsystem=golang
   dh_installchangelogs -O--builddirectory=_build -O--buildsystem=golang
   dh_installman -O--builddirectory=_build -O--buildsystem=golang
   dh_installsystemduser -O--builddirectory=_build -O--buildsystem=golang
   dh_perl -O--builddirectory=_build -O--buildsystem=golang
   dh_link -O--builddirectory=_build -O--buildsystem=golang
   dh_strip_nondeterminism -O--builddirectory=_build -O--buildsystem=golang
   dh_compress -O--builddirectory=_build -O--buildsystem=golang
   dh_fixperms -O--builddirectory=_build -O--buildsystem=golang
   dh_missing -O--builddirectory=_build -O--buildsystem=golang
   dh_dwz -a -O--builddirectory=_build -O--buildsystem=golang
dwz: debian/age/usr/bin/age: .debug_info section not present
dwz: debian/age/usr/bin/age-keygen: .debug_info section not present
dwz: Too few files for multifile optimization
dh_dwz: warning: No dwz multifile created, but not explicitly requested either so ignoring it.
dh_dwz: warning: Common issues include no debug information at all (missing -g) and
dh_dwz: warning: compressed debug information (#931891).
   dh_strip -a -O--builddirectory=_build -O--buildsystem=golang
dh_strip: warning: Could not find the BuildID in debian/age/usr/bin/age-keygen
dh_strip: warning: Could not find the BuildID in debian/age/usr/bin/age
   dh_makeshlibs -a -O--builddirectory=_build -O--buildsystem=golang
objdump: debian/age/usr/bin/age-keygen: .gnu.version_r invalid entry
objdump: warning: private headers incomplete: bad value
   dh_shlibdeps -a -O--builddirectory=_build -O--buildsystem=golang
objdump: debian/age/usr/bin/age-keygen: .gnu.version_r invalid entry
objdump: warning: private headers incomplete: bad value
objdump: debian/age/usr/bin/age-keygen: .gnu.version_r invalid entry
objdump: debian/age/usr/bin/age-keygen: bad value
   dh_installdeb -O--builddirectory=_build -O--buildsystem=golang
   dh_golang -O--builddirectory=_build -O--buildsystem=golang
   dh_gencontrol -O--builddirectory=_build -O--buildsystem=golang
   dh_md5sums -O--builddirectory=_build -O--buildsystem=golang
   dh_builddeb -O--builddirectory=_build -O--buildsystem=golang
dpkg-deb: building package 'age' in '../age_1.0.0~rc1-2_amd64.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../age_1.0.0~rc1-2_amd64.changes
dpkg-genchanges: info: not including original source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: binary and diff upload (original source NOT included)
+ date
Mon Apr 17 11:53:27 UTC 2023
+ cd ..
+ ls -la
total 1392
drwxr-xr-x  3 root root    4096 Apr 17 11:53 .
drwxr-xr-x  3 root root    4096 Apr 17 11:52 ..
drwxr-xr-x 12 root root    4096 Apr 17 11:53 age-1.0.0~rc1
-rw-r--r--  1 root root    4928 Apr 17 11:53 age_1.0.0~rc1-2.debian.tar.xz
-rw-r--r--  1 root root    1110 Apr 17 11:53 age_1.0.0~rc1-2.dsc
-rw-r--r--  1 root root    5623 Apr 17 11:53 age_1.0.0~rc1-2_amd64.buildinfo
-rw-r--r--  1 root root    1551 Apr 17 11:53 age_1.0.0~rc1-2_amd64.changes
-rw-r--r--  1 root root 1317812 Apr 17 11:53 age_1.0.0~rc1-2_amd64.deb
-rw-r--r--  1 root root   46998 Mar 28  2021 age_1.0.0~rc1.orig.tar.gz
-rw-r--r--  1 root root   18138 Apr 17 11:53 buildlog.txt
+ find . -maxdepth 1 -type f
+ sha256sum ./age_1.0.0~rc1-2_amd64.buildinfo ./age_1.0.0~rc1-2_amd64.changes ./buildlog.txt ./age_1.0.0~rc1.orig.tar.gz ./age_1.0.0~rc1-2.dsc ./age_1.0.0~rc1-2.debian.tar.xz ./age_1.0.0~rc1-2_amd64.deb
f406ad6403047b93e5c9f16b28350c28f83eabeb3dcff3ff782eb6fc9a38dae4  ./age_1.0.0~rc1-2_amd64.buildinfo
677ddb769efc4e86e3e45183ae00287387fd6474056aac39da66cef7efd19834  ./age_1.0.0~rc1-2_amd64.changes
0b8c6e2b8a4bfe3fa2cecf83919920b9c9056f9da42f2ea8df46006f65702a01  ./buildlog.txt
2ef0839b0e2e79435c037662c67df675b4ebc50d7bf001079d24b0bdf1d0c098  ./age_1.0.0~rc1.orig.tar.gz
19d5c3143bf608e6e3a285fc65b9ab076170f46af8bc51323873dace74edca63  ./age_1.0.0~rc1-2.dsc
4ca27488935df932340172fef897f1a6777d398a1c7e45639fa180160cd0c1e1  ./age_1.0.0~rc1-2.debian.tar.xz
fa0ea252c0de64f82bedb4979958e734633556f7912d68763d9e88fd93d6317a  ./age_1.0.0~rc1-2_amd64.deb
+ mkdir published
+ cd published
+ cd ../
+ ls age_1.0.0~rc1-2_amd64.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://repo.pureos.net/pureos/pool/main/a/age/age_1.0.0~rc1-2_amd64.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./age_1.0.0~rc1-2_amd64.deb
91bea49a398815679a6e9aad92f924a6097c0b415a038614191b91c7447e9f15  ./age_1.0.0~rc1-2_amd64.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./age_1.0.0~rc1-2_amd64.deb: FAILED
sha256sum: WARNING: 1 computed checksum did NOT match
