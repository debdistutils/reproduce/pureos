+ date
Mon Apr 17 12:12:23 UTC 2023
+ apt-get source --only-source apt-mirror=0.5.4-1pureos2
Reading package lists...
NOTICE: 'apt-mirror' packaging is maintained in the 'Git' version control system at:
https://source.puri.sm/pureos/core/apt-mirror.git
Please use:
git clone https://source.puri.sm/pureos/core/apt-mirror.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 24.9 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main apt-mirror 0.5.4-1pureos2 (dsc) [1665 B]
Get:2 http://repo.pureos.net/pureos byzantium/main apt-mirror 0.5.4-1pureos2 (tar) [17.2 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main apt-mirror 0.5.4-1pureos2 (diff) [6008 B]
dpkg-source: info: extracting apt-mirror in apt-mirror-0.5.4
dpkg-source: info: unpacking apt-mirror_0.5.4.orig.tar.gz
dpkg-source: info: unpacking apt-mirror_0.5.4-1pureos2.debian.tar.xz
Fetched 24.9 kB in 0s (164 kB/s)
W: Download is performed unsandboxed as root as file 'apt-mirror_0.5.4-1pureos2.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source apt-mirror=0.5.4-1pureos2
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name apt-mirror* -type d
+ cd ./apt-mirror-0.5.4
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package apt-mirror
dpkg-buildpackage: info: source version 0.5.4-1pureos2
dpkg-buildpackage: info: source distribution byzantium
dpkg-buildpackage: info: source changed by Jonas Smedegaard <jonas.smedegaard@puri.sm>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_auto_clean
dh_auto_clean: warning: Compatibility levels before 10 are deprecated (level 9 in use)
	make -j1 clean
make[1]: Entering directory '/build/apt-mirror/apt-mirror-0.5.4'
rm -f *.tar.*
make[1]: Leaving directory '/build/apt-mirror/apt-mirror-0.5.4'
   dh_clean
dh_clean: warning: Compatibility levels before 10 are deprecated (level 9 in use)
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building apt-mirror using existing ./apt-mirror_0.5.4.orig.tar.gz
dpkg-source: info: building apt-mirror in apt-mirror_0.5.4-1pureos2.debian.tar.xz
dpkg-source: info: building apt-mirror in apt-mirror_0.5.4-1pureos2.dsc
 debian/rules build
dh build
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_update_autotools_config
   dh_auto_configure
dh_auto_configure: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_auto_build
dh_auto_build: warning: Compatibility levels before 10 are deprecated (level 9 in use)
	make -j1
make[1]: Entering directory '/build/apt-mirror/apt-mirror-0.5.4'
make[1]: Nothing to be done for 'all'.
make[1]: Leaving directory '/build/apt-mirror/apt-mirror-0.5.4'
   dh_auto_test
dh_auto_test: warning: Compatibility levels before 10 are deprecated (level 9 in use)
 debian/rules binary
dh binary
dh: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_testroot
   dh_prep
   debian/rules override_dh_auto_install
make[1]: Entering directory '/build/apt-mirror/apt-mirror-0.5.4'
dh_auto_install -- PREFIX=/usr
dh_auto_install: warning: Compatibility levels before 10 are deprecated (level 9 in use)
	make -j1 install DESTDIR=/build/apt-mirror/apt-mirror-0.5.4/debian/apt-mirror AM_UPDATE_INFO_DIR=no PREFIX=/usr
make[2]: Entering directory '/build/apt-mirror/apt-mirror-0.5.4'
install -m 755 -D apt-mirror /build/apt-mirror/apt-mirror-0.5.4/debian/apt-mirror/usr/bin/apt-mirror
mkdir -p /build/apt-mirror/apt-mirror-0.5.4/debian/apt-mirror/usr/share/man/man1/
pod2man apt-mirror > /build/apt-mirror/apt-mirror-0.5.4/debian/apt-mirror/usr/share/man/man1/apt-mirror.1
if test ! -f /build/apt-mirror/apt-mirror-0.5.4/debian/apt-mirror/etc/apt/mirror.list; then install -m 644 -D mirror.list /build/apt-mirror/apt-mirror-0.5.4/debian/apt-mirror/etc/apt/mirror.list; fi
mkdir -p /build/apt-mirror/apt-mirror-0.5.4/debian/apt-mirror/var/spool/apt-mirror/mirror
mkdir -p /build/apt-mirror/apt-mirror-0.5.4/debian/apt-mirror/var/spool/apt-mirror/skel
mkdir -p /build/apt-mirror/apt-mirror-0.5.4/debian/apt-mirror/var/spool/apt-mirror/var
make[2]: Leaving directory '/build/apt-mirror/apt-mirror-0.5.4'
sed "s#@CODENAME@#byzantium#" debian/PureOS-mirror.list > debian/apt-mirror/etc/apt/mirror.list
make[1]: Leaving directory '/build/apt-mirror/apt-mirror-0.5.4'
   dh_installdocs
dh_installdocs: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installchangelogs
dh_installchangelogs: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installman
dh_installman: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installcron
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
dh_compress: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_fixperms
   dh_missing
dh_missing: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_installdeb
dh_installdeb: warning: Compatibility levels before 10 are deprecated (level 9 in use)
   dh_gencontrol
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'apt-mirror' in '../apt-mirror_0.5.4-1pureos2_all.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../apt-mirror_0.5.4-1pureos2_amd64.changes
dpkg-genchanges: info: not including original source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: binary and diff upload (original source NOT included)
+ date
Mon Apr 17 12:12:29 UTC 2023
+ cd ..
+ ls -la
total 84
drwxr-xr-x 3 root root  4096 Apr 17 12:12 .
drwxr-xr-x 3 root root  4096 Apr 17 12:12 ..
drwxr-xr-x 4 root root  4096 Apr 17 12:12 apt-mirror-0.5.4
-rw-r--r-- 1 root root  6008 Apr 17 12:12 apt-mirror_0.5.4-1pureos2.debian.tar.xz
-rw-r--r-- 1 root root  1127 Apr 17 12:12 apt-mirror_0.5.4-1pureos2.dsc
-rw-r--r-- 1 root root 18836 Apr 17 12:12 apt-mirror_0.5.4-1pureos2_all.deb
-rw-r--r-- 1 root root  5755 Apr 17 12:12 apt-mirror_0.5.4-1pureos2_amd64.buildinfo
-rw-r--r-- 1 root root  1882 Apr 17 12:12 apt-mirror_0.5.4-1pureos2_amd64.changes
-rw-r--r-- 1 root root 17206 Nov 15  2017 apt-mirror_0.5.4.orig.tar.gz
-rw-r--r-- 1 root root  5906 Apr 17 12:12 buildlog.txt
+ find . -maxdepth 1 -type f
+ sha256sum ./apt-mirror_0.5.4-1pureos2_amd64.changes ./apt-mirror_0.5.4-1pureos2.debian.tar.xz ./buildlog.txt ./apt-mirror_0.5.4-1pureos2_all.deb ./apt-mirror_0.5.4-1pureos2.dsc ./apt-mirror_0.5.4.orig.tar.gz ./apt-mirror_0.5.4-1pureos2_amd64.buildinfo
b2e7673b4b3b790d8d1915dc63a9c9869faab7ee9bc0b99faff6a86cf3e5e1c9  ./apt-mirror_0.5.4-1pureos2_amd64.changes
07d45b314fd0bc773b7b2437c00363931b0f584a23222a69aa8c8e5db7f9555e  ./apt-mirror_0.5.4-1pureos2.debian.tar.xz
90f4d1393d9829276ac90140f00731f8464c055b98ebde270bb3128d19445d92  ./buildlog.txt
28c1d9f6da4f3d49b65c2d8ca642a53d83e8fba2d837f53b523046601e99d55f  ./apt-mirror_0.5.4-1pureos2_all.deb
e01945ed73e9e64e76f431dcd22163d78d8ab259e35dcef94c4ace919cacedff  ./apt-mirror_0.5.4-1pureos2.dsc
d251ea3369f4a059711812c22d1d806e64c483482cbd61a3545d5434a14f6652  ./apt-mirror_0.5.4.orig.tar.gz
da9d25f82817f18da62f742c77238dbc12659077253adf5695abaad53eaa6d39  ./apt-mirror_0.5.4-1pureos2_amd64.buildinfo
+ mkdir published
+ cd published
+ cd ../
+ ls apt-mirror_0.5.4-1pureos2_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://repo.pureos.net/pureos/pool/main/a/apt-mirror/apt-mirror_0.5.4-1pureos2_all.deb
+ find . -maxdepth 1 -type f
+ tee ../SHA256SUMS
+ sha256sum ./apt-mirror_0.5.4-1pureos2_all.deb
28c1d9f6da4f3d49b65c2d8ca642a53d83e8fba2d837f53b523046601e99d55f  ./apt-mirror_0.5.4-1pureos2_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./apt-mirror_0.5.4-1pureos2_all.deb: OK
+ echo Package apt-mirror version 0.5.4-1pureos2 is reproducible!
Package apt-mirror version 0.5.4-1pureos2 is reproducible!
