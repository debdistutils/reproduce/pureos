+ date
Wed Apr 19 21:03:59 UTC 2023
+ apt-get source --only-source reflex=0.3.0-1
Reading package lists...
NOTICE: 'reflex' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/debian/reflex.git
Please use:
git clone https://salsa.debian.org/debian/reflex.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 23.2 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main reflex 0.3.0-1 (dsc) [2083 B]
Get:2 http://repo.pureos.net/pureos byzantium/main reflex 0.3.0-1 (tar) [17.9 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main reflex 0.3.0-1 (diff) [3220 B]
dpkg-source: info: extracting reflex in reflex-0.3.0
dpkg-source: info: unpacking reflex_0.3.0.orig.tar.gz
dpkg-source: info: unpacking reflex_0.3.0-1.debian.tar.xz
Fetched 23.2 kB in 0s (135 kB/s)
W: Download is performed unsandboxed as root as file 'reflex_0.3.0-1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source reflex=0.3.0-1
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  dh-golang golang-1.15-go golang-1.15-src golang-any
  golang-github-creack-pty-dev golang-github-fsnotify-fsnotify-dev
  golang-github-kballard-go-shellquote-dev golang-github-kr-pretty-dev
  golang-github-kr-pty-dev golang-github-kr-text-dev
  golang-github-ogier-pflag-dev golang-go golang-golang-x-sys-dev golang-src
0 upgraded, 14 newly installed, 0 to remove and 0 not upgraded.
Need to get 61.7 MB of archives.
After this operation, 366 MB of additional disk space will be used.
Get:1 http://repo.pureos.net/pureos byzantium/main amd64 dh-golang all 1.51 [24.6 kB]
Get:2 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-src amd64 1.15.15-1~deb11u4 [13.9 MB]
Get:3 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-go amd64 1.15.15-1~deb11u4 [47.4 MB]
Get:4 http://repo.pureos.net/pureos byzantium/main amd64 golang-src amd64 2:1.15~1 [4856 B]
Get:5 http://repo.pureos.net/pureos byzantium/main amd64 golang-go amd64 2:1.15~1 [23.9 kB]
Get:6 http://repo.pureos.net/pureos byzantium/main amd64 golang-any amd64 2:1.15~1 [4976 B]
Get:7 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-creack-pty-dev all 1.1.11-1 [8380 B]
Get:8 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-sys-dev all 0.0~git20210124.22da62e-1 [308 kB]
Get:9 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-fsnotify-fsnotify-dev all 1.4.9-2 [27.6 kB]
Get:10 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-kballard-go-shellquote-dev all 0.0~git20180428.95032a8-1 [6472 B]
Get:11 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-kr-text-dev all 0.2.0-1 [11.1 kB]
Get:12 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-kr-pretty-dev all 0.2.1+git20200831.59b4212-1 [14.6 kB]
Get:13 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-kr-pty-dev all 1.1.6-1 [10.6 kB]
Get:14 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-ogier-pflag-dev all 0.0~git20160129.0.45c278a-4 [17.9 kB]
Fetched 61.7 MB in 2s (31.1 MB/s)
Selecting previously unselected package dh-golang.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 102989 files and directories currently installed.)
Preparing to unpack .../00-dh-golang_1.51_all.deb ...
Unpacking dh-golang (1.51) ...
Selecting previously unselected package golang-1.15-src.
Preparing to unpack .../01-golang-1.15-src_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-src (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-1.15-go.
Preparing to unpack .../02-golang-1.15-go_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-go (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-src:amd64.
Preparing to unpack .../03-golang-src_2%3a1.15~1_amd64.deb ...
Unpacking golang-src:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-go.
Preparing to unpack .../04-golang-go_2%3a1.15~1_amd64.deb ...
Unpacking golang-go (2:1.15~1) ...
Selecting previously unselected package golang-any:amd64.
Preparing to unpack .../05-golang-any_2%3a1.15~1_amd64.deb ...
Unpacking golang-any:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-github-creack-pty-dev.
Preparing to unpack .../06-golang-github-creack-pty-dev_1.1.11-1_all.deb ...
Unpacking golang-github-creack-pty-dev (1.1.11-1) ...
Selecting previously unselected package golang-golang-x-sys-dev.
Preparing to unpack .../07-golang-golang-x-sys-dev_0.0~git20210124.22da62e-1_all.deb ...
Unpacking golang-golang-x-sys-dev (0.0~git20210124.22da62e-1) ...
Selecting previously unselected package golang-github-fsnotify-fsnotify-dev.
Preparing to unpack .../08-golang-github-fsnotify-fsnotify-dev_1.4.9-2_all.deb ...
Unpacking golang-github-fsnotify-fsnotify-dev (1.4.9-2) ...
Selecting previously unselected package golang-github-kballard-go-shellquote-dev.
Preparing to unpack .../09-golang-github-kballard-go-shellquote-dev_0.0~git20180428.95032a8-1_all.deb ...
Unpacking golang-github-kballard-go-shellquote-dev (0.0~git20180428.95032a8-1) ...
Selecting previously unselected package golang-github-kr-text-dev.
Preparing to unpack .../10-golang-github-kr-text-dev_0.2.0-1_all.deb ...
Unpacking golang-github-kr-text-dev (0.2.0-1) ...
Selecting previously unselected package golang-github-kr-pretty-dev.
Preparing to unpack .../11-golang-github-kr-pretty-dev_0.2.1+git20200831.59b4212-1_all.deb ...
Unpacking golang-github-kr-pretty-dev (0.2.1+git20200831.59b4212-1) ...
Selecting previously unselected package golang-github-kr-pty-dev.
Preparing to unpack .../12-golang-github-kr-pty-dev_1.1.6-1_all.deb ...
Unpacking golang-github-kr-pty-dev (1.1.6-1) ...
Selecting previously unselected package golang-github-ogier-pflag-dev.
Preparing to unpack .../13-golang-github-ogier-pflag-dev_0.0~git20160129.0.45c278a-4_all.deb ...
Unpacking golang-github-ogier-pflag-dev (0.0~git20160129.0.45c278a-4) ...
Setting up dh-golang (1.51) ...
Setting up golang-github-creack-pty-dev (1.1.11-1) ...
Setting up golang-1.15-src (1.15.15-1~deb11u4) ...
Setting up golang-golang-x-sys-dev (0.0~git20210124.22da62e-1) ...
Setting up golang-github-kr-text-dev (0.2.0-1) ...
Setting up golang-github-kr-pty-dev (1.1.6-1) ...
Setting up golang-github-fsnotify-fsnotify-dev (1.4.9-2) ...
Setting up golang-github-ogier-pflag-dev (0.0~git20160129.0.45c278a-4) ...
Setting up golang-src:amd64 (2:1.15~1) ...
Setting up golang-1.15-go (1.15.15-1~deb11u4) ...
Setting up golang-github-kr-pretty-dev (0.2.1+git20200831.59b4212-1) ...
Setting up golang-go (2:1.15~1) ...
Setting up golang-any:amd64 (2:1.15~1) ...
Setting up golang-github-kballard-go-shellquote-dev (0.0~git20180428.95032a8-1) ...
Processing triggers for man-db (2.9.4-2) ...
+ find . -maxdepth 1 -name reflex* -type d
+ cd ./reflex-0.3.0
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package reflex
dpkg-buildpackage: info: source version 0.3.0-1
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Kyle Robbertze <paddatrapper@debian.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --buildsystem=golang --with=golang
   dh_auto_clean -O--buildsystem=golang
   dh_autoreconf_clean -O--buildsystem=golang
   dh_clean -O--buildsystem=golang
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building reflex using existing ./reflex_0.3.0.orig.tar.gz
dpkg-source: info: building reflex in reflex_0.3.0-1.debian.tar.xz
dpkg-source: info: building reflex in reflex_0.3.0-1.dsc
 debian/rules binary
dh binary --buildsystem=golang --with=golang
   dh_update_autotools_config -O--buildsystem=golang
   dh_autoreconf -O--buildsystem=golang
   dh_auto_configure -O--buildsystem=golang
   dh_auto_build -O--buildsystem=golang
	cd obj-x86_64-linux-gnu && go install -trimpath -v -p 40 github.com/cespare/reflex
math/bits
internal/race
internal/unsafeheader
unicode
golang.org/x/sys/internal/unsafeheader
unicode/utf8
internal/nettrace
runtime/internal/atomic
runtime/internal/sys
runtime/cgo
internal/cpu
sync/atomic
runtime/internal/math
internal/testlog
internal/bytealg
math
runtime
internal/reflectlite
sync
internal/singleflight
math/rand
errors
sort
internal/oserror
io
vendor/golang.org/x/net/dns/dnsmessage
strconv
syscall
bytes
strings
reflect
bufio
github.com/kballard/go-shellquote
regexp/syntax
internal/syscall/execenv
internal/syscall/unix
time
regexp
context
internal/poll
internal/fmtsort
encoding/binary
os
golang.org/x/sys/unix
path/filepath
os/signal
fmt
net
os/exec
io/ioutil
github.com/creack/pty
log
github.com/fsnotify/fsnotify
github.com/ogier/pflag
github.com/cespare/reflex
   dh_auto_test -O--buildsystem=golang
	cd obj-x86_64-linux-gnu && go test -vet=off -v -p 40 github.com/cespare/reflex
=== RUN   TestUnifiedBacklog
--- PASS: TestUnifiedBacklog (0.00s)
=== RUN   TestUniqueFilesBacklog
--- PASS: TestUniqueFilesBacklog (0.00s)
=== RUN   TestReadConfigs
--- PASS: TestReadConfigs (0.00s)
=== RUN   TestReadConfigsBad
--- PASS: TestReadConfigsBad (0.00s)
=== RUN   TestMatchers
--- PASS: TestMatchers (0.00s)
=== RUN   TestExcludePrefix
--- PASS: TestExcludePrefix (0.00s)
=== RUN   TestDefaultExcludes
--- PASS: TestDefaultExcludes (0.00s)
PASS
ok  	github.com/cespare/reflex	0.008s
   create-stamp debian/debhelper-build-stamp
   dh_testroot -O--buildsystem=golang
   dh_prep -O--buildsystem=golang
   debian/rules override_dh_auto_install
make[1]: Entering directory '/build/reflex/reflex-0.3.0'
dh_auto_install -- --no-source
	cd obj-x86_64-linux-gnu && mkdir -p /build/reflex/reflex-0.3.0/debian/reflex/usr
	cd obj-x86_64-linux-gnu && cp -r bin /build/reflex/reflex-0.3.0/debian/reflex/usr
make[1]: Leaving directory '/build/reflex/reflex-0.3.0'
   dh_installdocs -O--buildsystem=golang
   dh_installchangelogs -O--buildsystem=golang
   dh_installman -O--buildsystem=golang
   dh_installsystemduser -O--buildsystem=golang
   dh_perl -O--buildsystem=golang
   dh_link -O--buildsystem=golang
   dh_strip_nondeterminism -O--buildsystem=golang
   dh_compress -O--buildsystem=golang
   dh_fixperms -O--buildsystem=golang
   dh_missing -O--buildsystem=golang
   dh_dwz -a -O--buildsystem=golang
dwz: debian/reflex/usr/bin/reflex: .debug_info section not present
   dh_strip -a -O--buildsystem=golang
dh_strip: warning: Could not find the BuildID in debian/reflex/usr/bin/reflex
   dh_makeshlibs -a -O--buildsystem=golang
   dh_shlibdeps -a -O--buildsystem=golang
   dh_installdeb -O--buildsystem=golang
   dh_golang -O--buildsystem=golang
   dh_gencontrol -O--buildsystem=golang
   dh_md5sums -O--buildsystem=golang
   dh_builddeb -O--buildsystem=golang
dpkg-deb: building package 'reflex' in '../reflex_0.3.0-1_amd64.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../reflex_0.3.0-1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload (original source is included)
+ date
Wed Apr 19 21:05:06 UTC 2023
+ cd ..
+ ls -la
total 784
drwxr-xr-x 3 root root   4096 Apr 19 21:05 .
drwxr-xr-x 3 root root   4096 Apr 19 21:03 ..
-rw-r--r-- 1 root root  11870 Apr 19 21:05 buildlog.txt
drwxr-xr-x 5 root root   4096 Apr 19 21:04 reflex-0.3.0
-rw-r--r-- 1 root root   3220 Apr 19 21:04 reflex_0.3.0-1.debian.tar.xz
-rw-r--r-- 1 root root   1200 Apr 19 21:04 reflex_0.3.0-1.dsc
-rw-r--r-- 1 root root   5770 Apr 19 21:05 reflex_0.3.0-1_amd64.buildinfo
-rw-r--r-- 1 root root   1873 Apr 19 21:05 reflex_0.3.0-1_amd64.changes
-rw-r--r-- 1 root root 734256 Apr 19 21:05 reflex_0.3.0-1_amd64.deb
-rw-r--r-- 1 root root  17909 Aug 24  2020 reflex_0.3.0.orig.tar.gz
+ find . -maxdepth 1 -type f
+ sha256sum ./reflex_0.3.0-1_amd64.changes ./buildlog.txt ./reflex_0.3.0.orig.tar.gz ./reflex_0.3.0-1_amd64.buildinfo ./reflex_0.3.0-1.dsc ./reflex_0.3.0-1_amd64.deb ./reflex_0.3.0-1.debian.tar.xz
e779c5fc9ed5a4728147a6c84846f186503c6a632e869e6c47e3be81c991a96a  ./reflex_0.3.0-1_amd64.changes
2af410c8f2e010de869991e4a12de35f2dc7f9df0b596471a2878222a8a8bfe3  ./buildlog.txt
cadb2d439bedc81df0f14e33ef14e3dfc088b95965bf7a6b2e880eb5220a39d5  ./reflex_0.3.0.orig.tar.gz
aaa67563f9893d4cbc56d28779d1ca587c9b819a6d4c71c7f7b9a892f273d8fe  ./reflex_0.3.0-1_amd64.buildinfo
fba2534c422d4cedd9917e5e2c7898c4614fce63870143defb82141024646c35  ./reflex_0.3.0-1.dsc
6218f866c4b1b81e51691e099ede331e94dd0223a61eff64011090e3796ead1e  ./reflex_0.3.0-1_amd64.deb
52082be1ec1f3609e5d6b1f595f5d7a5de937fd999863669a767c0ed605ef938  ./reflex_0.3.0-1.debian.tar.xz
+ mkdir published
+ cd published
+ cd ../
+ ls reflex_0.3.0-1_amd64.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/r/reflex/reflex_0.3.0-1_amd64.deb
--2023-04-19 21:05:06--  http://repo.pureos.net/pureos/pool/main/r/reflex/reflex_0.3.0-1_amd64.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 404 Not Found
2023-04-19 21:05:06 ERROR 404: Not Found.

