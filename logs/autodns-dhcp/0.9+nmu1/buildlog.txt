+ date
Mon Apr 17 12:52:05 UTC 2023
+ apt-get source --only-source autodns-dhcp=0.9+nmu1
Reading package lists...
Need to get 12.4 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main autodns-dhcp 0.9+nmu1 (dsc) [1429 B]
Get:2 http://repo.pureos.net/pureos byzantium/main autodns-dhcp 0.9+nmu1 (tar) [11.0 kB]
dpkg-source: info: extracting autodns-dhcp in autodns-dhcp-0.9+nmu1
dpkg-source: info: unpacking autodns-dhcp_0.9+nmu1.tar.gz
Fetched 12.4 kB in 0s (68.2 kB/s)
W: Download is performed unsandboxed as root as file 'autodns-dhcp_0.9+nmu1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source autodns-dhcp=0.9+nmu1
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name autodns-dhcp* -type d
+ cd ./autodns-dhcp-0.9+nmu1
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package autodns-dhcp
dpkg-buildpackage: info: source version 0.9+nmu1
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Holger Levsen <holger@debian.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh_testdir
dh_testroot
rm -f build-stamp install-stamp
# Add here commands to clean up after the build process.
dh_clean
	rm -f debian/debhelper-build-stamp
	rm -rf debian/.debhelper/
dh_clean: warning: Compatibility levels before 10 are deprecated (level 5 in use)
	rm -f -- debian/autodns-dhcp.substvars debian/files
	rm -fr -- debian/autodns-dhcp/ debian/tmp/
	find .  \( \( \
		\( -path .\*/.git -o -path .\*/.svn -o -path .\*/.bzr -o -path .\*/.hg -o -path .\*/CVS -o -path .\*/.pc -o -path .\*/_darcs \) -prune -o -type f -a \
	        \( -name '#*#' -o -name '.*~' -o -name '*~' -o -name DEADJOE \
		 -o -name '*.orig' -o -name '*.rej' -o -name '*.bak' \
		 -o -name '.*.orig' -o -name .*.rej -o -name '.SUMS' \
		 -o -name TAGS -o \( -path '*/.deps/*' -a -name '*.P' \) \
		\) -exec rm -f {} + \) -o \
		\( -type d -a -name autom4te.cache -prune -exec rm -rf {} + \) \)
 dpkg-source -b .
dpkg-source: warning: no source format specified in debian/source/format, see dpkg-source(1)
dpkg-source: info: using source format '1.0'
dpkg-source: info: building autodns-dhcp in autodns-dhcp_0.9+nmu1.tar.gz
dpkg-source: info: building autodns-dhcp in autodns-dhcp_0.9+nmu1.dsc
 debian/rules build
dh_testdir
# Add here commands to compile the package.
/usr/bin/make 
make[1]: Entering directory '/build/autodns-dhcp/autodns-dhcp-0.9+nmu1'
make[1]: Nothing to be done for 'all'.
make[1]: Leaving directory '/build/autodns-dhcp/autodns-dhcp-0.9+nmu1'
touch build-stamp
 debian/rules binary
dh_testdir
dh_testroot
dh_clean -k
dh_clean: warning: Compatibility levels before 10 are deprecated (level 5 in use)
dh_clean: warning: dh_clean -k is deprecated; use dh_prep instead
dh_clean: warning: This feature will be removed in compat 12.
	rm -f debian/debhelper-build-stamp
	rm -rf debian/.debhelper/
	rm -f -- debian/autodns-dhcp.substvars
	rm -fr -- debian/autodns-dhcp/ debian/tmp/
	find .  \( \( \
		\( -path .\*/.git -o -path .\*/.svn -o -path .\*/.bzr -o -path .\*/.hg -o -path .\*/CVS -o -path .\*/.pc -o -path .\*/_darcs \) -prune -o -type f -a \
	        \( -name '#*#' -o -name '.*~' -o -name '*~' -o -name DEADJOE \
		 -o -name '*.orig' -o -name '*.rej' -o -name '*.bak' \
		 -o -name '.*.orig' -o -name .*.rej -o -name '.SUMS' \
		 -o -name TAGS -o \( -path '*/.deps/*' -a -name '*.P' \) \
		\) -exec rm -f {} + \) -o \
		\( -type d -a -name autom4te.cache -prune -exec rm -rf {} + \) \)
dh_installdirs
dh_installdirs: warning: Compatibility levels before 10 are deprecated (level 5 in use)
	install -d debian/autodns-dhcp
	install -d debian/autodns-dhcp/var/lib/autodns-dhcp debian/autodns-dhcp/usr/share/man/man1 debian/autodns-dhcp/usr/sbin debian/autodns-dhcp/etc
/usr/bin/make install DESTDIR=`pwd`/debian/autodns-dhcp
make[1]: Entering directory '/build/autodns-dhcp/autodns-dhcp-0.9+nmu1'
install autodns-dhcp_cron autodns-dhcp_ddns /build/autodns-dhcp/autodns-dhcp-0.9+nmu1/debian/autodns-dhcp/usr/sbin/
cp autodns-dhcp.conf /build/autodns-dhcp/autodns-dhcp-0.9+nmu1/debian/autodns-dhcp/etc/
cp autodns-dhcp_cron.1 autodns-dhcp_ddns.1 /build/autodns-dhcp/autodns-dhcp-0.9+nmu1/debian/autodns-dhcp/usr/share/man/man1/
make[1]: Leaving directory '/build/autodns-dhcp/autodns-dhcp-0.9+nmu1'
touch install-stamp
dh_testdir
dh_testroot
dh_installdocs
dh_installdocs: warning: Compatibility levels before 10 are deprecated (level 5 in use)
	install -d debian/autodns-dhcp/usr/share/doc/autodns-dhcp
	install -d debian/.debhelper/generated/autodns-dhcp
	cp --reflink=auto -a ./README debian/autodns-dhcp/usr/share/doc/autodns-dhcp
	chown -R 0:0 debian/autodns-dhcp/usr/share/doc
	chmod -R u\+rw,go=rX debian/autodns-dhcp/usr/share/doc
	install -p -m0644 debian/copyright debian/autodns-dhcp/usr/share/doc/autodns-dhcp/copyright
dh_installcron
	install -d debian/autodns-dhcp/etc/cron.d
	install -p -m0644 debian/cron.d debian/autodns-dhcp/etc/cron.d/autodns-dhcp
dh_installchangelogs
	install -p -m0644 debian/changelog debian/autodns-dhcp/usr/share/doc/autodns-dhcp/changelog
dh_compress
dh_compress: warning: Compatibility levels before 10 are deprecated (level 5 in use)
	cd debian/autodns-dhcp
	chmod a-x usr/share/doc/autodns-dhcp/README usr/share/doc/autodns-dhcp/changelog usr/share/man/man1/autodns-dhcp_cron.1 usr/share/man/man1/autodns-dhcp_ddns.1
	gzip -9nf usr/share/doc/autodns-dhcp/README usr/share/doc/autodns-dhcp/changelog usr/share/man/man1/autodns-dhcp_cron.1 usr/share/man/man1/autodns-dhcp_ddns.1
	cd '/build/autodns-dhcp/autodns-dhcp-0.9+nmu1'
dh_fixperms
	find debian/autodns-dhcp -true -print0 2>/dev/null | xargs -0r chown --no-dereference 0:0
	find debian/autodns-dhcp ! -type l -a -true -a -true -print0 2>/dev/null | xargs -0r chmod go=rX,u+rw,a-s
	find debian/autodns-dhcp/usr/share/doc -type f -a -true -a ! -regex 'debian/autodns-dhcp/usr/share/doc/[^/]*/examples/.*' -print0 2>/dev/null | xargs -0r chmod 0644
	find debian/autodns-dhcp/usr/share/doc -type d -a -true -a -true -print0 2>/dev/null | xargs -0r chmod 0755
	find debian/autodns-dhcp/usr/share/man -type f -a -true -a -true -print0 2>/dev/null | xargs -0r chmod 0644
	find debian/autodns-dhcp -type f \( -name '*.so.*' -o -name '*.so' -o -name '*.la' -o -name '*.a' -o -name '*.js' -o -name '*.css' -o -name '*.scss' -o -name '*.sass' -o -name '*.jpeg' -o -name '*.jpg' -o -name '*.png' -o -name '*.gif' -o -name '*.cmxs' -o -name '*.node' \) -a -true -a -true -print0 2>/dev/null | xargs -0r chmod 0644
	find debian/autodns-dhcp/usr/sbin -type f -a -true -a -true -print0 2>/dev/null | xargs -0r chmod a+x
dh_installdeb
dh_installdeb: warning: Compatibility levels before 10 are deprecated (level 5 in use)
	install -d debian/autodns-dhcp/DEBIAN
	cp -f debian/postinst debian/autodns-dhcp/DEBIAN/postinst
	[META] Replace #TOKEN#s in "debian/autodns-dhcp/DEBIAN/postinst"
	chmod 0755 -- debian/autodns-dhcp/DEBIAN/postinst
	chown 0:0 -- debian/autodns-dhcp/DEBIAN/postinst
	cp -f debian/postrm debian/autodns-dhcp/DEBIAN/postrm
	[META] Replace #TOKEN#s in "debian/autodns-dhcp/DEBIAN/postrm"
	chmod 0755 -- debian/autodns-dhcp/DEBIAN/postrm
	chown 0:0 -- debian/autodns-dhcp/DEBIAN/postrm
	find debian/autodns-dhcp/etc -type f -printf '/etc/%P
' | LC_ALL=C sort >> debian/autodns-dhcp/DEBIAN/conffiles
	chmod 0644 -- debian/autodns-dhcp/DEBIAN/conffiles
	chown 0:0 -- debian/autodns-dhcp/DEBIAN/conffiles
dh_shlibdeps
dh_shlibdeps: warning: Compatibility levels before 10 are deprecated (level 5 in use)
dh_gencontrol
	echo misc:Depends= >> debian/autodns-dhcp.substvars
	echo misc:Pre-Depends= >> debian/autodns-dhcp.substvars
	dpkg-gencontrol -pautodns-dhcp -ldebian/changelog -Tdebian/autodns-dhcp.substvars -Pdebian/autodns-dhcp
	chmod 0644 -- debian/autodns-dhcp/DEBIAN/control
	chown 0:0 -- debian/autodns-dhcp/DEBIAN/control
dh_md5sums
	cd debian/autodns-dhcp >/dev/null && xargs -r0 md5sum | perl -pe 'if (s@^\\@@) { s/\\\\/\\/g; }' > DEBIAN/md5sums
	chmod 0644 -- debian/autodns-dhcp/DEBIAN/md5sums
	chown 0:0 -- debian/autodns-dhcp/DEBIAN/md5sums
dh_builddeb
	dpkg-deb --build debian/autodns-dhcp ..
dpkg-deb: building package 'autodns-dhcp' in '../autodns-dhcp_0.9+nmu1_all.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../autodns-dhcp_0.9+nmu1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Mon Apr 17 12:52:09 UTC 2023
+ cd ..
+ ls -la
total 64
drwxr-xr-x 3 root root  4096 Apr 17 12:52 .
drwxr-xr-x 3 root root  4096 Apr 17 12:52 ..
drwxr-xr-x 3 root root  4096 Apr 17 12:52 autodns-dhcp-0.9+nmu1
-rw-r--r-- 1 root root   546 Apr 17 12:52 autodns-dhcp_0.9+nmu1.dsc
-rw-r--r-- 1 root root 10985 Apr 17 12:52 autodns-dhcp_0.9+nmu1.tar.gz
-rw-r--r-- 1 root root 11800 Apr 17 12:52 autodns-dhcp_0.9+nmu1_all.deb
-rw-r--r-- 1 root root  5221 Apr 17 12:52 autodns-dhcp_0.9+nmu1_amd64.buildinfo
-rw-r--r-- 1 root root  1612 Apr 17 12:52 autodns-dhcp_0.9+nmu1_amd64.changes
-rw-r--r-- 1 root root  8797 Apr 17 12:52 buildlog.txt
+ find . -maxdepth 1 -type f
+ sha256sum ./autodns-dhcp_0.9+nmu1_amd64.changes ./buildlog.txt ./autodns-dhcp_0.9+nmu1.tar.gz ./autodns-dhcp_0.9+nmu1_all.deb ./autodns-dhcp_0.9+nmu1.dsc ./autodns-dhcp_0.9+nmu1_amd64.buildinfo
c5b6ddb480070628bb849a09d9bd93be1e045b65e827a8bcb8fbabcec3d7cbf1  ./autodns-dhcp_0.9+nmu1_amd64.changes
5a10fa639b1d7ebd5af4ec9b67afc8788d8714a93382ef851f92a284a1bb31b0  ./buildlog.txt
7314ab185cfd2c7ff88abf430f1fc76dbf00378696d51ff3089a9afada33ca80  ./autodns-dhcp_0.9+nmu1.tar.gz
bfc68296080ccab499dab8540892544f28822a170d6c7c9c560f95d01a2fdd58  ./autodns-dhcp_0.9+nmu1_all.deb
f8796c6caad532a140d8a868bda67f8ace5d22e03f36d3f28514601ddce34d09  ./autodns-dhcp_0.9+nmu1.dsc
34a142e1b998c6d9b5f48cfe8bc7446d528c24dbbe16f0ef1a3af65ec44810a4  ./autodns-dhcp_0.9+nmu1_amd64.buildinfo
+ mkdir published
+ cd published
+ cd ../
+ ls autodns-dhcp_0.9+nmu1_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://repo.pureos.net/pureos/pool/main/a/autodns-dhcp/autodns-dhcp_0.9+nmu1_all.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./autodns-dhcp_0.9+nmu1_all.deb
bfc68296080ccab499dab8540892544f28822a170d6c7c9c560f95d01a2fdd58  ./autodns-dhcp_0.9+nmu1_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./autodns-dhcp_0.9+nmu1_all.deb: OK
+ echo Package autodns-dhcp version 0.9+nmu1 is reproducible!
Package autodns-dhcp version 0.9+nmu1 is reproducible!
