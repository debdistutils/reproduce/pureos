+ date
Tue Apr 18 17:36:45 UTC 2023
+ apt-get source --only-source libadwaita-1=1.2.0-1pureos1
Reading package lists...
NOTICE: 'libadwaita-1' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/gnome-team/libadwaita.git
Please use:
git clone https://salsa.debian.org/gnome-team/libadwaita.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 1343 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main libadwaita-1 1.2.0-1pureos1 (dsc) [2541 B]
Get:2 http://repo.pureos.net/pureos byzantium/main libadwaita-1 1.2.0-1pureos1 (tar) [1325 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main libadwaita-1 1.2.0-1pureos1 (diff) [15.0 kB]
dpkg-source: info: extracting libadwaita-1 in libadwaita-1-1.2.0
dpkg-source: info: unpacking libadwaita-1_1.2.0.orig.tar.xz
dpkg-source: info: unpacking libadwaita-1_1.2.0-1pureos1.debian.tar.xz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: applying pureos/Lower-meson-version-requirement-to-what-is-in-Byzantium.patch
Fetched 1343 kB in 0s (4387 kB/s)
W: Download is performed unsandboxed as root as file 'libadwaita-1_1.2.0-1pureos1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source libadwaita-1=1.2.0-1pureos1
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  gi-docgen gir1.2-atk-1.0 gir1.2-atspi-2.0 gir1.2-freedesktop
  gir1.2-gdesktopenums-3.0 gir1.2-gdkpixbuf-2.0 gir1.2-glib-2.0
  gir1.2-gnomedesktop-3.0 gir1.2-graphene-1.0 gir1.2-gtk-3.0 gir1.2-gtk-4.0
  gir1.2-harfbuzz-0.0 gir1.2-pango-1.0 gnome-pkg-tools gobject-introspection
  gsettings-desktop-schemas-dev libatk-bridge2.0-dev libatk1.0-dev
  libatspi2.0-dev libblkid-dev libbrotli-dev libcairo-script-interpreter2
  libcairo2-dev libdatrie-dev libdbus-1-dev libegl-dev libegl1-mesa-dev
  libepoxy-dev libexpat1-dev libfontconfig-dev libfontconfig1-dev
  libfreetype-dev libfreetype6-dev libfribidi-dev libgdk-pixbuf-2.0-dev
  libgirepository-1.0-1 libgirepository1.0-dev libgl-dev libgles-dev libgles1
  libgles2 libglib2.0-bin libglib2.0-dev libglib2.0-dev-bin libglvnd-core-dev
  libglvnd-dev libglx-dev libgnome-desktop-3-dev libgraphene-1.0-dev
  libgraphite2-dev libgtk-3-dev libgtk-4-1 libgtk-4-common libgtk-4-dev
  libharfbuzz-dev libharfbuzz-gobject0 libice-dev libmount-dev libopengl-dev
  libpango1.0-dev libpangoxft-1.0-0 libpixman-1-dev libpthread-stubs0-dev
  libsass1 libseccomp-dev libselinux1-dev libsepol1-dev libsm-dev
  libsystemd-dev libthai-dev libudev-dev libvala-0.48-0 libvalacodegen-0.48-0
  libvulkan-dev libwayland-bin libwayland-dev libx11-dev libxau-dev
  libxcb-render0-dev libxcb-shm0-dev libxcb1-dev libxcomposite-dev
  libxcursor-dev libxdamage-dev libxdmcp-dev libxext-dev libxfixes-dev
  libxfont2 libxft-dev libxi-dev libxinerama-dev libxkbcommon-dev
  libxkbregistry-dev libxml2-dev libxml2-utils libxrandr-dev libxrender-dev
  libxtst-dev meson ninja-build pango1.0-tools python3-gi python3-jinja2
  python3-mako python3-markdown python3-markupsafe python3-smartypants
  python3-toml python3-typogrify sassc uuid-dev valac valac-0.48-vapi
  valac-bin wayland-protocols x11-xkb-utils x11proto-dev x11proto-input-dev
  x11proto-randr-dev x11proto-record-dev x11proto-xext-dev
  x11proto-xinerama-dev xorg-sgml-doctools xserver-common xtrans-dev xvfb
0 upgraded, 126 newly installed, 0 to remove and 0 not upgraded.
Need to get 38.1 MB of archives.
After this operation, 174 MB of additional disk space will be used.
Err:1 http://repo.pureos.net/pureos byzantium/main amd64 python3-markupsafe amd64 1.1.1-1+b3
  Temporary failure resolving 'repo.pureos.net'
Get:2 http://repo.pureos.net/pureos byzantium/main amd64 python3-jinja2 all 2.11.3-1 [114 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main amd64 python3-markdown all 3.3.4-1 [71.4 kB]
Get:4 http://repo.pureos.net/pureos byzantium/main amd64 python3-toml all 0.10.1-1 [15.9 kB]
Get:5 http://repo.pureos.net/pureos byzantium/main amd64 python3-smartypants all 2.0.0-2 [13.7 kB]
Get:6 http://repo.pureos.net/pureos byzantium/main amd64 python3-typogrify all 1:2.0.7-2 [12.9 kB]
Get:7 http://repo.pureos.net/pureos byzantium/main amd64 gi-docgen all 2022.1+ds-1pureos1 [82.6 kB]
Get:8 http://repo.pureos.net/pureos byzantium/main amd64 libgirepository-1.0-1 amd64 1.66.1-1+b1 [96.7 kB]
Get:9 http://repo.pureos.net/pureos byzantium/main amd64 gir1.2-glib-2.0 amd64 1.66.1-1+b1 [151 kB]
Get:10 http://repo.pureos.net/pureos byzantium/main amd64 gir1.2-atk-1.0 amd64 2.36.0-2 [26.0 kB]
Get:11 http://repo.pureos.net/pureos byzantium/main amd64 gir1.2-freedesktop amd64 1.66.1-1+b1 [33.4 kB]
Get:12 http://repo.pureos.net/pureos byzantium/main amd64 gir1.2-atspi-2.0 amd64 2.38.0-4 [22.7 kB]
Get:13 http://repo.pureos.net/pureos byzantium/main amd64 gir1.2-gdesktopenums-3.0 amd64 3.38.0-2pureos1 [10.4 kB]
Get:14 http://repo.pureos.net/pureos byzantium/main amd64 gir1.2-gdkpixbuf-2.0 amd64 2.42.2+dfsg-1+deb11u1 [19.9 kB]
Get:15 http://repo.pureos.net/pureos byzantium/main amd64 gir1.2-harfbuzz-0.0 amd64 2.7.4-1 [1154 kB]
Get:16 http://repo.pureos.net/pureos byzantium/main amd64 libpangoxft-1.0-0 amd64 1.50.7+ds-1~pureos1 [56.7 kB]
Get:17 http://repo.pureos.net/pureos byzantium/main amd64 gir1.2-pango-1.0 amd64 1.50.7+ds-1~pureos1 [67.3 kB]
Get:18 http://repo.pureos.net/pureos byzantium/main amd64 gir1.2-gtk-3.0 amd64 3.24.27-1pureos4 [258 kB]
Get:19 http://repo.pureos.net/pureos byzantium/main amd64 gir1.2-gnomedesktop-3.0 amd64 3.38.5-3pureos1 [33.9 kB]
Get:20 http://repo.pureos.net/pureos byzantium/main amd64 gir1.2-graphene-1.0 amd64 1.10.4+dfsg1-1 [12.3 kB]
Get:21 http://repo.pureos.net/pureos byzantium/main amd64 libcairo-script-interpreter2 amd64 1.16.0-5 [163 kB]
Get:22 http://repo.pureos.net/pureos byzantium/main amd64 libgtk-4-common all 4.6.5+ds-1pureos4 [3857 kB]
Get:23 http://repo.pureos.net/pureos byzantium/main amd64 libgtk-4-1 amd64 4.6.5+ds-1pureos4 [2767 kB]
Get:24 http://repo.pureos.net/pureos byzantium/main amd64 gir1.2-gtk-4.0 amd64 4.6.5+ds-1pureos4 [234 kB]
Get:25 http://repo.pureos.net/pureos byzantium/main amd64 gnome-pkg-tools all 0.21.2 [22.9 kB]
Get:26 http://repo.pureos.net/pureos byzantium/main amd64 python3-mako all 1.1.3+ds1-2 [80.2 kB]
Get:27 http://repo.pureos.net/pureos byzantium/main amd64 gobject-introspection amd64 1.66.1-1+b1 [301 kB]
Get:28 http://repo.pureos.net/pureos byzantium/main amd64 gsettings-desktop-schemas-dev amd64 3.38.0-2pureos1 [11.2 kB]
Get:29 http://repo.pureos.net/pureos byzantium/main amd64 libglib2.0-bin amd64 2.66.8-1pureos3 [141 kB]
Get:30 http://repo.pureos.net/pureos byzantium/main amd64 libglib2.0-dev-bin amd64 2.66.8-1pureos3 [179 kB]
Get:31 http://repo.pureos.net/pureos byzantium/main amd64 uuid-dev amd64 2.36.1-8pureos3 [99.8 kB]
Get:32 http://repo.pureos.net/pureos byzantium/main amd64 libblkid-dev amd64 2.36.1-8pureos3 [225 kB]
Get:33 http://repo.pureos.net/pureos byzantium/main amd64 libsepol1-dev amd64 3.1-1 [338 kB]
Get:34 http://repo.pureos.net/pureos byzantium/main amd64 libselinux1-dev amd64 3.1-3 [168 kB]
Get:35 http://repo.pureos.net/pureos byzantium/main amd64 libmount-dev amd64 2.36.1-8pureos3 [78.5 kB]
Get:36 http://repo.pureos.net/pureos byzantium/main amd64 libglib2.0-dev amd64 2.66.8-1pureos3 [1578 kB]
Get:37 http://repo.pureos.net/pureos byzantium/main amd64 libatk1.0-dev amd64 2.36.0-2 [104 kB]
Get:38 http://repo.pureos.net/pureos byzantium/main amd64 libdbus-1-dev amd64 1.12.24-0+deb11u1 [260 kB]
Get:39 http://repo.pureos.net/pureos byzantium/main amd64 xorg-sgml-doctools all 1:1.11-1.1 [22.1 kB]
Get:40 http://repo.pureos.net/pureos byzantium/main amd64 x11proto-dev all 2020.1-1 [594 kB]
Get:41 http://repo.pureos.net/pureos byzantium/main amd64 libxau-dev amd64 1:1.0.9-1 [22.9 kB]
Get:42 http://repo.pureos.net/pureos byzantium/main amd64 libxdmcp-dev amd64 1:1.1.2-3 [42.2 kB]
Get:43 http://repo.pureos.net/pureos byzantium/main amd64 xtrans-dev all 1.4.0-1 [98.7 kB]
Get:44 http://repo.pureos.net/pureos byzantium/main amd64 libpthread-stubs0-dev amd64 0.4-1 [5344 B]
Get:45 http://repo.pureos.net/pureos byzantium/main amd64 libxcb1-dev amd64 1.14-3 [176 kB]
Get:46 http://repo.pureos.net/pureos byzantium/main amd64 libx11-dev amd64 2:1.7.2-1 [841 kB]
Get:47 http://repo.pureos.net/pureos byzantium/main amd64 x11proto-xext-dev all 2020.1-1 [3404 B]
Get:48 http://repo.pureos.net/pureos byzantium/main amd64 libxext-dev amd64 2:1.3.3-1.1 [107 kB]
Get:49 http://repo.pureos.net/pureos byzantium/main amd64 libxfixes-dev amd64 1:5.0.3-2 [24.1 kB]
Get:50 http://repo.pureos.net/pureos byzantium/main amd64 x11proto-input-dev all 2020.1-1 [3412 B]
Get:51 http://repo.pureos.net/pureos byzantium/main amd64 libxi-dev amd64 2:1.7.10-1 [242 kB]
Get:52 http://repo.pureos.net/pureos byzantium/main amd64 x11proto-record-dev all 2020.1-1 [3408 B]
Get:53 http://repo.pureos.net/pureos byzantium/main amd64 libxtst-dev amd64 2:1.2.3-1 [31.9 kB]
Get:54 http://repo.pureos.net/pureos byzantium/main amd64 libatspi2.0-dev amd64 2.38.0-4 [74.6 kB]
Get:55 http://repo.pureos.net/pureos byzantium/main amd64 libatk-bridge2.0-dev amd64 2.38.0-1 [9888 B]
Get:56 http://repo.pureos.net/pureos byzantium/main amd64 libbrotli-dev amd64 1.0.9-2+b2 [288 kB]
Get:57 http://repo.pureos.net/pureos byzantium/main amd64 libexpat1-dev amd64 2.2.10-2+deb11u5 [141 kB]
Get:58 http://repo.pureos.net/pureos byzantium/main amd64 libfreetype-dev amd64 2.10.4+dfsg-1+deb11u1 [571 kB]
Get:59 http://repo.pureos.net/pureos byzantium/main amd64 libfreetype6-dev amd64 2.10.4+dfsg-1+deb11u1 [82.6 kB]
Get:60 http://repo.pureos.net/pureos byzantium/main amd64 libfontconfig-dev amd64 2.13.1-4.2 [368 kB]
Get:61 http://repo.pureos.net/pureos byzantium/main amd64 libfontconfig1-dev amd64 2.13.1-4.2 [238 kB]
Get:62 http://repo.pureos.net/pureos byzantium/main amd64 libxrender-dev amd64 1:0.9.10-1 [40.8 kB]
Get:63 http://repo.pureos.net/pureos byzantium/main amd64 libice-dev amd64 2:1.0.10-1 [67.1 kB]
Get:64 http://repo.pureos.net/pureos byzantium/main amd64 libsm-dev amd64 2:1.2.3-1 [38.0 kB]
Get:65 http://repo.pureos.net/pureos byzantium/main amd64 libpixman-1-dev amd64 0.40.0-1.1pureos1 [561 kB]
Get:66 http://repo.pureos.net/pureos byzantium/main amd64 libxcb-render0-dev amd64 1.14-3 [114 kB]
Get:67 http://repo.pureos.net/pureos byzantium/main amd64 libxcb-shm0-dev amd64 1.14-3 [102 kB]
Get:68 http://repo.pureos.net/pureos byzantium/main amd64 libcairo2-dev amd64 1.16.0-5 [739 kB]
Get:69 http://repo.pureos.net/pureos byzantium/main amd64 libdatrie-dev amd64 0.2.13-1 [18.4 kB]
Get:70 http://repo.pureos.net/pureos byzantium/main amd64 libglx-dev amd64 1.3.4-2pureos2 [16.9 kB]
Get:71 http://repo.pureos.net/pureos byzantium/main amd64 libgl-dev amd64 1.3.4-2pureos2 [101 kB]
Get:72 http://repo.pureos.net/pureos byzantium/main amd64 libegl-dev amd64 1.3.4-2pureos2 [20.5 kB]
Get:73 http://repo.pureos.net/pureos byzantium/main amd64 libglvnd-core-dev amd64 1.3.4-2pureos2 [14.5 kB]
Get:74 http://repo.pureos.net/pureos byzantium/main amd64 libgles1 amd64 1.3.4-2pureos2 [13.2 kB]
Get:75 http://repo.pureos.net/pureos byzantium/main amd64 libgles2 amd64 1.3.4-2pureos2 [18.5 kB]
Get:76 http://repo.pureos.net/pureos byzantium/main amd64 libgles-dev amd64 1.3.4-2pureos2 [50.9 kB]
Get:77 http://repo.pureos.net/pureos byzantium/main amd64 libopengl-dev amd64 1.3.4-2pureos2 [6492 B]
Get:78 http://repo.pureos.net/pureos byzantium/main amd64 libglvnd-dev amd64 1.3.4-2pureos2 [6256 B]
Get:79 http://repo.pureos.net/pureos byzantium/main amd64 libegl1-mesa-dev amd64 21.2.6-1pureos4 [55.9 kB]
Get:80 http://repo.pureos.net/pureos byzantium/main amd64 libepoxy-dev amd64 1.5.5-1 [127 kB]
Get:81 http://repo.pureos.net/pureos byzantium/main amd64 libfribidi-dev amd64 1.0.8-2+deb11u1 [105 kB]
Get:82 http://repo.pureos.net/pureos byzantium/main amd64 libgdk-pixbuf-2.0-dev amd64 2.42.2+dfsg-1+deb11u1 [56.9 kB]
Get:83 http://repo.pureos.net/pureos byzantium/main amd64 libgirepository1.0-dev amd64 1.66.1-1+b1 [834 kB]
Get:84 http://repo.pureos.net/pureos byzantium/main amd64 libharfbuzz-gobject0 amd64 2.7.4-1 [1147 kB]
Get:85 http://repo.pureos.net/pureos byzantium/main amd64 libgraphite2-dev amd64 1.3.14-1 [24.0 kB]
Get:86 http://repo.pureos.net/pureos byzantium/main amd64 libharfbuzz-dev amd64 2.7.4-1 [1630 kB]
Get:87 http://repo.pureos.net/pureos byzantium/main amd64 libthai-dev amd64 0.1.28-3 [24.3 kB]
Get:88 http://repo.pureos.net/pureos byzantium/main amd64 libxft-dev amd64 2.3.2-2 [68.7 kB]
Get:89 http://repo.pureos.net/pureos byzantium/main amd64 pango1.0-tools amd64 1.50.7+ds-1~pureos1 [69.8 kB]
Get:90 http://repo.pureos.net/pureos byzantium/main amd64 libpango1.0-dev amd64 1.50.7+ds-1~pureos1 [180 kB]
Get:91 http://repo.pureos.net/pureos byzantium/main amd64 libwayland-bin amd64 1.21.0-1pureos2 [23.8 kB]
Get:92 http://repo.pureos.net/pureos byzantium/main amd64 libwayland-dev amd64 1.21.0-1pureos2 [72.8 kB]
Get:93 http://repo.pureos.net/pureos byzantium/main amd64 libxcomposite-dev amd64 1:0.4.5-1 [20.1 kB]
Get:94 http://repo.pureos.net/pureos byzantium/main amd64 libxcursor-dev amd64 1:1.2.0-2 [44.8 kB]
Get:95 http://repo.pureos.net/pureos byzantium/main amd64 libxdamage-dev amd64 1:1.1.5-2 [15.6 kB]
Get:96 http://repo.pureos.net/pureos byzantium/main amd64 x11proto-xinerama-dev all 2020.1-1 [3408 B]
Get:97 http://repo.pureos.net/pureos byzantium/main amd64 libxinerama-dev amd64 2:1.1.4-2 [20.1 kB]
Get:98 http://repo.pureos.net/pureos byzantium/main amd64 libxkbcommon-dev amd64 1.0.3-2 [49.4 kB]
Get:99 http://repo.pureos.net/pureos byzantium/main amd64 x11proto-randr-dev all 2020.1-1 [3412 B]
Get:100 http://repo.pureos.net/pureos byzantium/main amd64 libxrandr-dev amd64 2:1.5.1-1 [45.0 kB]
Get:101 http://repo.pureos.net/pureos byzantium/main amd64 wayland-protocols all 1.31-1~pureos1 [75.4 kB]
Get:102 http://repo.pureos.net/pureos byzantium/main amd64 libgtk-3-dev amd64 3.24.27-1pureos4 [1145 kB]
Get:103 http://repo.pureos.net/pureos byzantium/main amd64 libseccomp-dev amd64 2.5.1-1+deb11u1 [92.4 kB]
Get:104 http://repo.pureos.net/pureos byzantium/main amd64 libsystemd-dev amd64 247.3-7+deb11u1 [401 kB]
Get:105 http://repo.pureos.net/pureos byzantium/main amd64 libudev-dev amd64 247.3-7+deb11u1 [123 kB]
Get:106 http://repo.pureos.net/pureos byzantium-security/main amd64 libxml2-dev amd64 2.9.10+dfsg-6.7+deb11u3 [790 kB]
Get:107 http://repo.pureos.net/pureos byzantium/main amd64 libxkbregistry-dev amd64 1.0.3-2 [11.1 kB]
Get:108 http://repo.pureos.net/pureos byzantium/main amd64 libgnome-desktop-3-dev amd64 3.38.5-3pureos1 [85.4 kB]
Get:109 http://repo.pureos.net/pureos byzantium/main amd64 libgraphene-1.0-dev amd64 1.10.4+dfsg1-1 [60.8 kB]
Get:110 http://repo.pureos.net/pureos byzantium/main amd64 libvulkan-dev amd64 1.2.162.0-1 [586 kB]
Get:111 http://repo.pureos.net/pureos byzantium/main amd64 libgtk-4-dev amd64 4.6.5+ds-1pureos4 [920 kB]
Get:112 http://repo.pureos.net/pureos byzantium/main amd64 libsass1 amd64 3.6.4+20201122-1 [705 kB]
Get:113 http://repo.pureos.net/pureos byzantium/main amd64 libvala-0.48-0 amd64 0.48.17-1 [817 kB]
Get:114 http://repo.pureos.net/pureos byzantium/main amd64 libvalacodegen-0.48-0 amd64 0.48.17-1 [675 kB]
Get:115 http://repo.pureos.net/pureos byzantium/main amd64 libxfont2 amd64 1:2.0.4-1 [136 kB]
Get:116 http://repo.pureos.net/pureos byzantium-security/main amd64 libxml2-utils amd64 2.9.10+dfsg-6.7+deb11u3 [109 kB]
Get:117 http://repo.pureos.net/pureos byzantium/main amd64 ninja-build amd64 1.10.1-1 [112 kB]
Get:118 http://repo.pureos.net/pureos byzantium/main amd64 meson all 0.56.2-1 [437 kB]
Get:119 http://repo.pureos.net/pureos byzantium/main amd64 python3-gi amd64 3.40.1-1~pureos1 [233 kB]
Get:120 http://repo.pureos.net/pureos byzantium/main amd64 sassc amd64 3.6.1+20201027-1 [10.5 kB]
Get:121 http://repo.pureos.net/pureos byzantium/main amd64 valac-0.48-vapi all 0.48.17-1 [1003 kB]
Get:122 http://repo.pureos.net/pureos byzantium/main amd64 valac-bin amd64 0.48.17-1 [304 kB]
Get:123 http://repo.pureos.net/pureos byzantium/main amd64 valac amd64 0.48.17-1 [395 kB]
Get:124 http://repo.pureos.net/pureos byzantium/main amd64 x11-xkb-utils amd64 7.7+5 [163 kB]
Get:125 http://repo.pureos.net/pureos byzantium-security/main amd64 xserver-common all 2:1.20.11-1+deb11u6 [2284 kB]
Get:126 http://repo.pureos.net/pureos byzantium-security/main amd64 xvfb amd64 2:1.20.11-1+deb11u6 [3044 kB]
Fetched 38.1 MB in 7s (5319 kB/s)
E: Failed to fetch http://repo.pureos.net/pureos/pool/main/m/markupsafe/python3-markupsafe_1.1.1-1%2bb3_amd64.deb  Temporary failure resolving 'repo.pureos.net'
E: Unable to fetch some archives, maybe run apt-get update or try with --fix-missing?
E: Failed to process build dependencies
