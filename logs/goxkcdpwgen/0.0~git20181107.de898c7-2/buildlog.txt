+ date
Tue Apr 18 13:00:23 UTC 2023
+ apt-get source --only-source goxkcdpwgen=0.0~git20181107.de898c7-2
Reading package lists...
NOTICE: 'goxkcdpwgen' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/go-team/packages/goxkcdpwgen.git
Please use:
git clone https://salsa.debian.org/go-team/packages/goxkcdpwgen.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 176 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main goxkcdpwgen 0.0~git20181107.de898c7-2 (dsc) [2282 B]
Get:2 http://repo.pureos.net/pureos byzantium/main goxkcdpwgen 0.0~git20181107.de898c7-2 (tar) [172 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main goxkcdpwgen 0.0~git20181107.de898c7-2 (diff) [2116 B]
dpkg-source: info: extracting goxkcdpwgen in goxkcdpwgen-0.0~git20181107.de898c7
dpkg-source: info: unpacking goxkcdpwgen_0.0~git20181107.de898c7.orig.tar.xz
dpkg-source: info: unpacking goxkcdpwgen_0.0~git20181107.de898c7-2.debian.tar.xz
Fetched 176 kB in 0s (793 kB/s)
W: Download is performed unsandboxed as root as file 'goxkcdpwgen_0.0~git20181107.de898c7-2.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source goxkcdpwgen=0.0~git20181107.de898c7-2
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  dh-golang golang-1.15-go golang-1.15-src golang-any
  golang-github-davecgh-go-spew-dev golang-github-pmezard-go-difflib-dev
  golang-github-stretchr-objx-dev golang-github-stretchr-testify-dev golang-go
  golang-gopkg-yaml.v3-dev golang-src
0 upgraded, 11 newly installed, 0 to remove and 0 not upgraded.
Need to get 61.5 MB of archives.
After this operation, 361 MB of additional disk space will be used.
Get:1 http://repo.pureos.net/pureos byzantium/main amd64 dh-golang all 1.51 [24.6 kB]
Get:2 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-src amd64 1.15.15-1~deb11u4 [13.9 MB]
Get:3 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-go amd64 1.15.15-1~deb11u4 [47.4 MB]
Get:4 http://repo.pureos.net/pureos byzantium/main amd64 golang-src amd64 2:1.15~1 [4856 B]
Get:5 http://repo.pureos.net/pureos byzantium/main amd64 golang-go amd64 2:1.15~1 [23.9 kB]
Get:6 http://repo.pureos.net/pureos byzantium/main amd64 golang-any amd64 2:1.15~1 [4976 B]
Get:7 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-davecgh-go-spew-dev all 1.1.1-2 [29.7 kB]
Get:8 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-pmezard-go-difflib-dev all 1.0.0-3 [12.3 kB]
Get:9 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-stretchr-objx-dev all 0.3.0-1 [25.4 kB]
Get:10 http://repo.pureos.net/pureos byzantium/main amd64 golang-gopkg-yaml.v3-dev all 3.0.0~git20200121.a6ecf24-3 [70.5 kB]
Get:11 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-stretchr-testify-dev all 1.6.1-2 [60.4 kB]
Fetched 61.5 MB in 2s (31.8 MB/s)
Selecting previously unselected package dh-golang.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 102989 files and directories currently installed.)
Preparing to unpack .../00-dh-golang_1.51_all.deb ...
Unpacking dh-golang (1.51) ...
Selecting previously unselected package golang-1.15-src.
Preparing to unpack .../01-golang-1.15-src_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-src (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-1.15-go.
Preparing to unpack .../02-golang-1.15-go_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-go (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-src:amd64.
Preparing to unpack .../03-golang-src_2%3a1.15~1_amd64.deb ...
Unpacking golang-src:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-go.
Preparing to unpack .../04-golang-go_2%3a1.15~1_amd64.deb ...
Unpacking golang-go (2:1.15~1) ...
Selecting previously unselected package golang-any:amd64.
Preparing to unpack .../05-golang-any_2%3a1.15~1_amd64.deb ...
Unpacking golang-any:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-github-davecgh-go-spew-dev.
Preparing to unpack .../06-golang-github-davecgh-go-spew-dev_1.1.1-2_all.deb ...
Unpacking golang-github-davecgh-go-spew-dev (1.1.1-2) ...
Selecting previously unselected package golang-github-pmezard-go-difflib-dev.
Preparing to unpack .../07-golang-github-pmezard-go-difflib-dev_1.0.0-3_all.deb ...
Unpacking golang-github-pmezard-go-difflib-dev (1.0.0-3) ...
Selecting previously unselected package golang-github-stretchr-objx-dev.
Preparing to unpack .../08-golang-github-stretchr-objx-dev_0.3.0-1_all.deb ...
Unpacking golang-github-stretchr-objx-dev (0.3.0-1) ...
Selecting previously unselected package golang-gopkg-yaml.v3-dev.
Preparing to unpack .../09-golang-gopkg-yaml.v3-dev_3.0.0~git20200121.a6ecf24-3_all.deb ...
Unpacking golang-gopkg-yaml.v3-dev (3.0.0~git20200121.a6ecf24-3) ...
Selecting previously unselected package golang-github-stretchr-testify-dev.
Preparing to unpack .../10-golang-github-stretchr-testify-dev_1.6.1-2_all.deb ...
Unpacking golang-github-stretchr-testify-dev (1.6.1-2) ...
Setting up dh-golang (1.51) ...
Setting up golang-github-stretchr-objx-dev (0.3.0-1) ...
Setting up golang-1.15-src (1.15.15-1~deb11u4) ...
Setting up golang-gopkg-yaml.v3-dev (3.0.0~git20200121.a6ecf24-3) ...
Setting up golang-github-pmezard-go-difflib-dev (1.0.0-3) ...
Setting up golang-github-davecgh-go-spew-dev (1.1.1-2) ...
Setting up golang-github-stretchr-testify-dev (1.6.1-2) ...
Setting up golang-src:amd64 (2:1.15~1) ...
Setting up golang-1.15-go (1.15.15-1~deb11u4) ...
Setting up golang-go (2:1.15~1) ...
Setting up golang-any:amd64 (2:1.15~1) ...
Processing triggers for man-db (2.9.4-2) ...
+ find . -maxdepth 1 -name goxkcdpwgen* -type d
+ cd ./goxkcdpwgen-0.0~git20181107.de898c7
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package goxkcdpwgen
dpkg-buildpackage: info: source version 0.0~git20181107.de898c7-2
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Balasankar C <balasankarc@debian.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --buildsystem=golang --with=golang
   dh_auto_clean -O--buildsystem=golang
   dh_autoreconf_clean -O--buildsystem=golang
   dh_clean -O--buildsystem=golang
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building goxkcdpwgen using existing ./goxkcdpwgen_0.0~git20181107.de898c7.orig.tar.xz
dpkg-source: info: building goxkcdpwgen in goxkcdpwgen_0.0~git20181107.de898c7-2.debian.tar.xz
dpkg-source: info: building goxkcdpwgen in goxkcdpwgen_0.0~git20181107.de898c7-2.dsc
 debian/rules binary
dh binary --buildsystem=golang --with=golang
   dh_update_autotools_config -O--buildsystem=golang
   dh_autoreconf -O--buildsystem=golang
   dh_auto_configure -O--buildsystem=golang
   dh_auto_build -O--buildsystem=golang
	cd obj-x86_64-linux-gnu && go install -trimpath -v -p 50 github.com/martinhoefling/goxkcdpwgen github.com/martinhoefling/goxkcdpwgen/xkcdpwgen
internal/unsafeheader
unicode/utf8
crypto/subtle
runtime/internal/sys
internal/race
sync/atomic
math/bits
runtime/internal/atomic
crypto/internal/subtle
internal/cpu
unicode
runtime/internal/math
internal/testlog
internal/bytealg
math
runtime
internal/reflectlite
sync
math/rand
errors
sort
internal/oserror
io
strconv
syscall
strings
bytes
reflect
bufio
internal/syscall/unix
internal/syscall/execenv
time
internal/poll
internal/fmtsort
encoding/binary
os
crypto/cipher
crypto/aes
fmt
flag
math/big
crypto/rand
github.com/martinhoefling/goxkcdpwgen/xkcdpwgen
github.com/martinhoefling/goxkcdpwgen
   dh_auto_test -O--buildsystem=golang
	cd obj-x86_64-linux-gnu && go test -vet=off -v -p 50 github.com/martinhoefling/goxkcdpwgen github.com/martinhoefling/goxkcdpwgen/xkcdpwgen
?   	github.com/martinhoefling/goxkcdpwgen	[no test files]
=== RUN   TestGenerator_GeneratePassword
--- PASS: TestGenerator_GeneratePassword (0.00s)
=== RUN   TestGenerator_GeneratePasswordWithCustomDelimiter
--- PASS: TestGenerator_GeneratePasswordWithCustomDelimiter (0.00s)
=== RUN   TestGenerator_GeneratePasswordWithCustomWordcount
--- PASS: TestGenerator_GeneratePasswordWithCustomWordcount (0.00s)
=== RUN   TestGenerator_GeneratePasswordWithCustomWordlist
--- PASS: TestGenerator_GeneratePasswordWithCustomWordlist (0.00s)
=== RUN   TestGenerator_GeneratePasswordWithCapitalization
--- PASS: TestGenerator_GeneratePasswordWithCapitalization (0.00s)
=== RUN   TestGenerator_SetLanguage
--- PASS: TestGenerator_SetLanguage (0.00s)
=== RUN   TestGenerator_SetInvalidLanguageRaisesError
--- PASS: TestGenerator_SetInvalidLanguageRaisesError (0.00s)
PASS
ok  	github.com/martinhoefling/goxkcdpwgen/xkcdpwgen	0.013s
   create-stamp debian/debhelper-build-stamp
   dh_testroot -O--buildsystem=golang
   dh_prep -O--buildsystem=golang
   dh_auto_install -O--buildsystem=golang
	cd obj-x86_64-linux-gnu && mkdir -p /build/goxkcdpwgen/goxkcdpwgen-0.0\~git20181107.de898c7/debian/goxkcdpwgen/usr
	cd obj-x86_64-linux-gnu && cp -r bin /build/goxkcdpwgen/goxkcdpwgen-0.0\~git20181107.de898c7/debian/goxkcdpwgen/usr
   dh_installdocs -O--buildsystem=golang
   dh_installchangelogs -O--buildsystem=golang
   dh_installinit -O--buildsystem=golang
   dh_installsystemduser -O--buildsystem=golang
   dh_perl -O--buildsystem=golang
   dh_link -O--buildsystem=golang
   dh_strip_nondeterminism -O--buildsystem=golang
   dh_compress -O--buildsystem=golang
   dh_fixperms -O--buildsystem=golang
   dh_missing -O--buildsystem=golang
   dh_dwz -O--buildsystem=golang
dwz: debian/goxkcdpwgen/usr/bin/goxkcdpwgen: .debug_info section not present
   dh_strip -O--buildsystem=golang
dh_strip: warning: Could not find the BuildID in debian/goxkcdpwgen/usr/bin/goxkcdpwgen
   dh_makeshlibs -O--buildsystem=golang
   dh_shlibdeps -O--buildsystem=golang
   dh_installdeb -O--buildsystem=golang
   dh_golang -O--buildsystem=golang
   dh_gencontrol -O--buildsystem=golang
dpkg-gencontrol: warning: Depends field of package goxkcdpwgen: substitution variable ${shlibs:Depends} used, but is not defined
   dh_md5sums -O--buildsystem=golang
   dh_builddeb -O--buildsystem=golang
dpkg-deb: building package 'goxkcdpwgen' in '../goxkcdpwgen_0.0~git20181107.de898c7-2_amd64.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../goxkcdpwgen_0.0~git20181107.de898c7-2_amd64.changes
dpkg-genchanges: info: not including original source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: binary and diff upload (original source NOT included)
+ date
Tue Apr 18 13:01:20 UTC 2023
+ cd ..
+ ls -la
total 972
drwxr-xr-x 3 root root   4096 Apr 18 13:01 .
drwxr-xr-x 3 root root   4096 Apr 18 13:00 ..
-rw-r--r-- 1 root root  11386 Apr 18 13:01 buildlog.txt
drwxr-xr-x 8 root root   4096 Apr 18 13:01 goxkcdpwgen-0.0~git20181107.de898c7
-rw-r--r-- 1 root root   2116 Apr 18 13:00 goxkcdpwgen_0.0~git20181107.de898c7-2.debian.tar.xz
-rw-r--r-- 1 root root   1399 Apr 18 13:00 goxkcdpwgen_0.0~git20181107.de898c7-2.dsc
-rw-r--r-- 1 root root   5789 Apr 18 13:01 goxkcdpwgen_0.0~git20181107.de898c7-2_amd64.buildinfo
-rw-r--r-- 1 root root   1954 Apr 18 13:01 goxkcdpwgen_0.0~git20181107.de898c7-2_amd64.changes
-rw-r--r-- 1 root root 774988 Apr 18 13:01 goxkcdpwgen_0.0~git20181107.de898c7-2_amd64.deb
-rw-r--r-- 1 root root 171832 Jan 26  2020 goxkcdpwgen_0.0~git20181107.de898c7.orig.tar.xz
+ find . -maxdepth 1 -type f
+ sha256sum ./buildlog.txt ./goxkcdpwgen_0.0~git20181107.de898c7-2.debian.tar.xz ./goxkcdpwgen_0.0~git20181107.de898c7-2_amd64.changes ./goxkcdpwgen_0.0~git20181107.de898c7-2.dsc ./goxkcdpwgen_0.0~git20181107.de898c7-2_amd64.deb ./goxkcdpwgen_0.0~git20181107.de898c7-2_amd64.buildinfo ./goxkcdpwgen_0.0~git20181107.de898c7.orig.tar.xz
069d223992f8f1d5c017daa4d32ece80db30988ad6f166d422b33f3f15d9f06a  ./buildlog.txt
0db4c64aabd85a1feda2364527f4fe86c6fe709c5beb208aa51704327d0a9713  ./goxkcdpwgen_0.0~git20181107.de898c7-2.debian.tar.xz
b0878c7515ec5e2290a6ce950cfb715e286b9bb90108cac88177d65d7d3500a1  ./goxkcdpwgen_0.0~git20181107.de898c7-2_amd64.changes
efae89027b5bb8e9fccf82af9d0f3412c5adab2b17ff5ab6f8c5543670d3cc37  ./goxkcdpwgen_0.0~git20181107.de898c7-2.dsc
0600fb800b8462aa5a4c6d8b39ca87e3150a33689a223c66b90d0d6fccb8534a  ./goxkcdpwgen_0.0~git20181107.de898c7-2_amd64.deb
63f14a3e842d94f27aaa99885ff4905305f2b1d50d60f33ccac97239589f130e  ./goxkcdpwgen_0.0~git20181107.de898c7-2_amd64.buildinfo
eb6ebeff2ee1eb848291244be43f406db61380ab460b9303688b3d4a0467b390  ./goxkcdpwgen_0.0~git20181107.de898c7.orig.tar.xz
+ mkdir published
+ cd published
+ cd ../
+ ls goxkcdpwgen_0.0~git20181107.de898c7-2_amd64.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/g/goxkcdpwgen/goxkcdpwgen_0.0~git20181107.de898c7-2_amd64.deb
--2023-04-18 13:01:20--  http://repo.pureos.net/pureos/pool/main/g/goxkcdpwgen/goxkcdpwgen_0.0~git20181107.de898c7-2_amd64.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 404 Not Found
2023-04-18 13:01:20 ERROR 404: Not Found.

