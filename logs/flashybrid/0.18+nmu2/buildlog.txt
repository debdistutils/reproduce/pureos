+ date
Tue Apr 18 05:37:38 UTC 2023
+ apt-get source --only-source flashybrid=0.18+nmu2
Reading package lists...
Need to get 28.1 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main flashybrid 0.18+nmu2 (dsc) [1434 B]
Get:2 http://repo.pureos.net/pureos byzantium/main flashybrid 0.18+nmu2 (tar) [26.7 kB]
dpkg-source: info: extracting flashybrid in flashybrid-0.18+nmu2
dpkg-source: info: unpacking flashybrid_0.18+nmu2.tar.xz
Fetched 28.1 kB in 0s (176 kB/s)
W: Download is performed unsandboxed as root as file 'flashybrid_0.18+nmu2.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source flashybrid=0.18+nmu2
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
Some packages could not be installed. This may mean that you have
requested an impossible situation or if you are using the unstable
distribution that some required packages have not yet been created
or been moved out of Incoming.
The following information may help to resolve the situation:

The following packages have unmet dependencies:
 builddeps:flashybrid : Depends: dh-systemd but it is not installable
E: Unable to correct problems, you have held broken packages.
