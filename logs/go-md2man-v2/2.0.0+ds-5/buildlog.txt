+ date
Tue Apr 18 09:01:47 UTC 2023
+ apt-get source --only-source go-md2man-v2=2.0.0+ds-5
Reading package lists...
NOTICE: 'go-md2man-v2' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/go-team/packages/go-md2man-v2.git
Please use:
git clone https://salsa.debian.org/go-team/packages/go-md2man-v2.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 13.8 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main go-md2man-v2 2.0.0+ds-5 (dsc) [1708 B]
Get:2 http://repo.pureos.net/pureos byzantium/main go-md2man-v2 2.0.0+ds-5 (tar) [7600 B]
Get:3 http://repo.pureos.net/pureos byzantium/main go-md2man-v2 2.0.0+ds-5 (diff) [4468 B]
dpkg-source: info: extracting go-md2man-v2 in go-md2man-v2-2.0.0+ds
dpkg-source: info: unpacking go-md2man-v2_2.0.0+ds.orig.tar.xz
dpkg-source: info: unpacking go-md2man-v2_2.0.0+ds-5.debian.tar.xz
Fetched 13.8 kB in 0s (79.6 kB/s)
W: Download is performed unsandboxed as root as file 'go-md2man-v2_2.0.0+ds-5.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source go-md2man-v2=2.0.0+ds-5
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  dh-golang golang-1.15-go golang-1.15-src golang-any
  golang-github-pmezard-go-difflib-dev
  golang-github-russross-blackfriday-v2-dev
  golang-github-shurcool-sanitized-anchor-name-dev golang-go golang-src
0 upgraded, 9 newly installed, 0 to remove and 0 not upgraded.
Need to get 61.4 MB of archives.
After this operation, 360 MB of additional disk space will be used.
Get:1 http://repo.pureos.net/pureos byzantium/main amd64 dh-golang all 1.51 [24.6 kB]
Get:2 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-src amd64 1.15.15-1~deb11u4 [13.9 MB]
Get:3 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-go amd64 1.15.15-1~deb11u4 [47.4 MB]
Get:4 http://repo.pureos.net/pureos byzantium/main amd64 golang-src amd64 2:1.15~1 [4856 B]
Get:5 http://repo.pureos.net/pureos byzantium/main amd64 golang-go amd64 2:1.15~1 [23.9 kB]
Get:6 http://repo.pureos.net/pureos byzantium/main amd64 golang-any amd64 2:1.15~1 [4976 B]
Get:7 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-pmezard-go-difflib-dev all 1.0.0-3 [12.3 kB]
Get:8 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-shurcool-sanitized-anchor-name-dev all 1.0.0-1 [3872 B]
Get:9 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-russross-blackfriday-v2-dev all 2.0.1-3 [67.8 kB]
Fetched 61.4 MB in 2s (28.6 MB/s)
Selecting previously unselected package dh-golang.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 102989 files and directories currently installed.)
Preparing to unpack .../0-dh-golang_1.51_all.deb ...
Unpacking dh-golang (1.51) ...
Selecting previously unselected package golang-1.15-src.
Preparing to unpack .../1-golang-1.15-src_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-src (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-1.15-go.
Preparing to unpack .../2-golang-1.15-go_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-go (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-src:amd64.
Preparing to unpack .../3-golang-src_2%3a1.15~1_amd64.deb ...
Unpacking golang-src:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-go.
Preparing to unpack .../4-golang-go_2%3a1.15~1_amd64.deb ...
Unpacking golang-go (2:1.15~1) ...
Selecting previously unselected package golang-any:amd64.
Preparing to unpack .../5-golang-any_2%3a1.15~1_amd64.deb ...
Unpacking golang-any:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-github-pmezard-go-difflib-dev.
Preparing to unpack .../6-golang-github-pmezard-go-difflib-dev_1.0.0-3_all.deb ...
Unpacking golang-github-pmezard-go-difflib-dev (1.0.0-3) ...
Selecting previously unselected package golang-github-shurcool-sanitized-anchor-name-dev.
Preparing to unpack .../7-golang-github-shurcool-sanitized-anchor-name-dev_1.0.0-1_all.deb ...
Unpacking golang-github-shurcool-sanitized-anchor-name-dev (1.0.0-1) ...
Selecting previously unselected package golang-github-russross-blackfriday-v2-dev.
Preparing to unpack .../8-golang-github-russross-blackfriday-v2-dev_2.0.1-3_all.deb ...
Unpacking golang-github-russross-blackfriday-v2-dev (2.0.1-3) ...
Setting up dh-golang (1.51) ...
Setting up golang-github-shurcool-sanitized-anchor-name-dev (1.0.0-1) ...
Setting up golang-1.15-src (1.15.15-1~deb11u4) ...
Setting up golang-github-pmezard-go-difflib-dev (1.0.0-3) ...
Setting up golang-src:amd64 (2:1.15~1) ...
Setting up golang-1.15-go (1.15.15-1~deb11u4) ...
Setting up golang-github-russross-blackfriday-v2-dev (2.0.1-3) ...
Setting up golang-go (2:1.15~1) ...
Setting up golang-any:amd64 (2:1.15~1) ...
Processing triggers for man-db (2.9.4-2) ...
+ find . -maxdepth 1 -name go-md2man-v2* -type d
+ cd ./go-md2man-v2-2.0.0+ds
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package go-md2man-v2
dpkg-buildpackage: info: source version 2.0.0+ds-5
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Shengjing Zhu <zhsj@debian.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --builddirectory=_build --buildsystem=golang --with=golang
   dh_auto_clean -O--builddirectory=_build -O--buildsystem=golang
   dh_autoreconf_clean -O--builddirectory=_build -O--buildsystem=golang
   dh_clean -O--builddirectory=_build -O--buildsystem=golang
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building go-md2man-v2 using existing ./go-md2man-v2_2.0.0+ds.orig.tar.xz
dpkg-source: info: building go-md2man-v2 in go-md2man-v2_2.0.0+ds-5.debian.tar.xz
dpkg-source: info: building go-md2man-v2 in go-md2man-v2_2.0.0+ds-5.dsc
 debian/rules binary
dh binary --builddirectory=_build --buildsystem=golang --with=golang
   dh_update_autotools_config -O--builddirectory=_build -O--buildsystem=golang
   dh_autoreconf -O--builddirectory=_build -O--buildsystem=golang
   dh_auto_configure -O--builddirectory=_build -O--buildsystem=golang
   dh_auto_build -O--builddirectory=_build -O--buildsystem=golang
	cd _build && go install -trimpath -v -p 50 github.com/cpuguy83/go-md2man/v2 github.com/cpuguy83/go-md2man/v2/md2man
internal/unsafeheader
math/bits
runtime/internal/sys
unicode/utf8
internal/race
unicode
runtime/internal/atomic
sync/atomic
internal/cpu
runtime/internal/math
internal/testlog
internal/bytealg
math
runtime
github.com/shurcooL/sanitized_anchor_name
internal/reflectlite
sync
sort
errors
io
internal/oserror
strconv
syscall
strings
bytes
reflect
html
regexp/syntax
internal/syscall/execenv
internal/syscall/unix
time
regexp
internal/poll
internal/fmtsort
os
path/filepath
fmt
io/ioutil
flag
github.com/russross/blackfriday/v2
github.com/cpuguy83/go-md2man/v2/md2man
github.com/cpuguy83/go-md2man/v2
   debian/rules execute_after_dh_auto_build
make[1]: Entering directory '/build/go-md2man-v2/go-md2man-v2-2.0.0+ds'
mv _build/bin/v2 _build/bin/go-md2man
_build/bin/go-md2man -in go-md2man.1.md -out go-md2man.1
make[1]: Leaving directory '/build/go-md2man-v2/go-md2man-v2-2.0.0+ds'
   dh_auto_test -O--builddirectory=_build -O--buildsystem=golang
	cd _build && go test -vet=off -v -p 50 github.com/cpuguy83/go-md2man/v2 github.com/cpuguy83/go-md2man/v2/md2man
?   	github.com/cpuguy83/go-md2man/v2	[no test files]
=== RUN   TestEmphasis
--- PASS: TestEmphasis (0.06s)
=== RUN   TestStrong
--- PASS: TestStrong (0.08s)
=== RUN   TestEmphasisMix
--- PASS: TestEmphasisMix (0.03s)
=== RUN   TestCodeSpan
--- PASS: TestCodeSpan (0.07s)
=== RUN   TestListLists
--- PASS: TestListLists (0.30s)
=== RUN   TestLineBreak
--- PASS: TestLineBreak (0.03s)
=== RUN   TestTable
--- PASS: TestTable (0.51s)
=== RUN   TestLinks
--- PASS: TestLinks (0.01s)
PASS
ok  	github.com/cpuguy83/go-md2man/v2/md2man	1.103s
   create-stamp debian/debhelper-build-stamp
   dh_testroot -O--builddirectory=_build -O--buildsystem=golang
   dh_prep -O--builddirectory=_build -O--buildsystem=golang
   dh_auto_install -O--builddirectory=_build -O--buildsystem=golang
	cd _build && mkdir -p /build/go-md2man-v2/go-md2man-v2-2.0.0\+ds/debian/tmp/usr
	cd _build && cp -r bin /build/go-md2man-v2/go-md2man-v2-2.0.0\+ds/debian/tmp/usr
   dh_install -O--builddirectory=_build -O--buildsystem=golang
   dh_installdocs -O--builddirectory=_build -O--buildsystem=golang
   dh_installchangelogs -O--builddirectory=_build -O--buildsystem=golang
   dh_installman -O--builddirectory=_build -O--buildsystem=golang
   dh_installsystemduser -O--builddirectory=_build -O--buildsystem=golang
   dh_perl -O--builddirectory=_build -O--buildsystem=golang
   dh_link -O--builddirectory=_build -O--buildsystem=golang
   dh_strip_nondeterminism -O--builddirectory=_build -O--buildsystem=golang
   dh_compress -O--builddirectory=_build -O--buildsystem=golang
   dh_fixperms -O--builddirectory=_build -O--buildsystem=golang
   dh_missing -O--builddirectory=_build -O--buildsystem=golang
   dh_dwz -a -O--builddirectory=_build -O--buildsystem=golang
dwz: debian/go-md2man/usr/bin/go-md2man: .debug_info section not present
   dh_strip -a -O--builddirectory=_build -O--buildsystem=golang
dh_strip: warning: Could not find the BuildID in debian/go-md2man/usr/bin/go-md2man
   dh_makeshlibs -a -O--builddirectory=_build -O--buildsystem=golang
   dh_shlibdeps -a -O--builddirectory=_build -O--buildsystem=golang
   dh_installdeb -O--builddirectory=_build -O--buildsystem=golang
   dh_golang -O--builddirectory=_build -O--buildsystem=golang
   dh_gencontrol -O--builddirectory=_build -O--buildsystem=golang
dpkg-gencontrol: warning: Depends field of package go-md2man: substitution variable ${shlibs:Depends} used, but is not defined
dpkg-gencontrol: warning: Depends field of package golang-github-cpuguy83-go-md2man-v2-dev: substitution variable ${shlibs:Depends} used, but is not defined
   dh_md5sums -O--builddirectory=_build -O--buildsystem=golang
   dh_builddeb -O--builddirectory=_build -O--buildsystem=golang
dpkg-deb: building package 'golang-github-cpuguy83-go-md2man-v2-dev' in '../golang-github-cpuguy83-go-md2man-v2-dev_2.0.0+ds-5_all.deb'.
dpkg-deb: building package 'go-md2man' in '../go-md2man_2.0.0+ds-5_amd64.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../go-md2man-v2_2.0.0+ds-5_amd64.changes
dpkg-genchanges: info: not including original source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: binary and diff upload (original source NOT included)
+ date
Tue Apr 18 09:02:44 UTC 2023
+ cd ..
+ ls -la
total 744
drwxr-xr-x 3 root root   4096 Apr 18 09:02 .
drwxr-xr-x 3 root root   4096 Apr 18 09:01 ..
-rw-r--r-- 1 root root  11381 Apr 18 09:02 buildlog.txt
drwxr-xr-x 7 root root   4096 Apr 18 09:02 go-md2man-v2-2.0.0+ds
-rw-r--r-- 1 root root   4468 Apr 18 09:02 go-md2man-v2_2.0.0+ds-5.debian.tar.xz
-rw-r--r-- 1 root root   1406 Apr 18 09:02 go-md2man-v2_2.0.0+ds-5.dsc
-rw-r--r-- 1 root root   5962 Apr 18 09:02 go-md2man-v2_2.0.0+ds-5_amd64.buildinfo
-rw-r--r-- 1 root root   2195 Apr 18 09:02 go-md2man-v2_2.0.0+ds-5_amd64.changes
-rw-r--r-- 1 root root   7600 Oct  4  2020 go-md2man-v2_2.0.0+ds.orig.tar.xz
-rw-r--r-- 1 root root 689864 Apr 18 09:02 go-md2man_2.0.0+ds-5_amd64.deb
-rw-r--r-- 1 root root   9932 Apr 18 09:02 golang-github-cpuguy83-go-md2man-v2-dev_2.0.0+ds-5_all.deb
+ find . -maxdepth 1 -type f
+ sha256sum ./go-md2man-v2_2.0.0+ds-5_amd64.buildinfo ./buildlog.txt ./go-md2man_2.0.0+ds-5_amd64.deb ./go-md2man-v2_2.0.0+ds-5.dsc ./go-md2man-v2_2.0.0+ds.orig.tar.xz ./go-md2man-v2_2.0.0+ds-5.debian.tar.xz ./go-md2man-v2_2.0.0+ds-5_amd64.changes ./golang-github-cpuguy83-go-md2man-v2-dev_2.0.0+ds-5_all.deb
297f855bb812b74540688c03f7fef2a80a1ffa8142411891de8f502479646082  ./go-md2man-v2_2.0.0+ds-5_amd64.buildinfo
6ee6dd1c86a1ab7b4cbd8f479edbb3e64b3e2ce8c1dfc4505163296a502ab45a  ./buildlog.txt
a073660a85f7aac8560cd70f2fc2ef97376e3aea7de195e99310f61dab7fe0c7  ./go-md2man_2.0.0+ds-5_amd64.deb
d7b7ed4b88300cbd9a3c61613bac1dc590443f4d4300b6e8286497b692b8b236  ./go-md2man-v2_2.0.0+ds-5.dsc
889c0e9d1474a988951506d472cfe6ef05b18cfc41b84f09196a3bba997b0df0  ./go-md2man-v2_2.0.0+ds.orig.tar.xz
279f92822f7931d1fc7aa836b5089f9c424caef3eaa778765cd49cf23555fae4  ./go-md2man-v2_2.0.0+ds-5.debian.tar.xz
dc337fc02394ed7c085df507503771548752b6a39004100b2995ce0708258413  ./go-md2man-v2_2.0.0+ds-5_amd64.changes
e703a567c931fe1f974fd019c82a41d0ff0f552100126f98ca81405803df6204  ./golang-github-cpuguy83-go-md2man-v2-dev_2.0.0+ds-5_all.deb
+ mkdir published
+ cd published
+ cd ../
+ ls go-md2man_2.0.0+ds-5_amd64.deb golang-github-cpuguy83-go-md2man-v2-dev_2.0.0+ds-5_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/g/go-md2man-v2/go-md2man_2.0.0+ds-5_amd64.deb
--2023-04-18 09:02:44--  http://repo.pureos.net/pureos/pool/main/g/go-md2man-v2/go-md2man_2.0.0+ds-5_amd64.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 404 Not Found
2023-04-18 09:02:44 ERROR 404: Not Found.

+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/g/go-md2man-v2/golang-github-cpuguy83-go-md2man-v2-dev_2.0.0+ds-5_all.deb
--2023-04-18 09:02:44--  http://repo.pureos.net/pureos/pool/main/g/go-md2man-v2/golang-github-cpuguy83-go-md2man-v2-dev_2.0.0+ds-5_all.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 9932 (9.7K) [application/octet-stream]
Saving to: 'golang-github-cpuguy83-go-md2man-v2-dev_2.0.0+ds-5_all.deb'

     0K .........                                             100% 17.1M=0.001s

2023-04-18 09:02:44 (17.1 MB/s) - 'golang-github-cpuguy83-go-md2man-v2-dev_2.0.0+ds-5_all.deb' saved [9932/9932]

+ tee ../SHA256SUMS+ 
find . -maxdepth 1 -type f
+ sha256sum ./golang-github-cpuguy83-go-md2man-v2-dev_2.0.0+ds-5_all.deb
e703a567c931fe1f974fd019c82a41d0ff0f552100126f98ca81405803df6204  ./golang-github-cpuguy83-go-md2man-v2-dev_2.0.0+ds-5_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./golang-github-cpuguy83-go-md2man-v2-dev_2.0.0+ds-5_all.deb: OK
+ echo Package go-md2man-v2 version 2.0.0+ds-5 is reproducible!
Package go-md2man-v2 version 2.0.0+ds-5 is reproducible!
