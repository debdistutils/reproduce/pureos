+ date
Wed Apr 19 07:46:15 UTC 2023
+ apt-get source --only-source mini-buildd=1.0.36
Reading package lists...
NOTICE: 'mini-buildd' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/debian/mini-buildd.git
Please use:
git clone https://salsa.debian.org/debian/mini-buildd.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 929 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main mini-buildd 1.0.36 (dsc) [2174 B]
Get:2 http://repo.pureos.net/pureos byzantium/main mini-buildd 1.0.36 (tar) [927 kB]
dpkg-source: info: extracting mini-buildd in mini-buildd-1.0.36
dpkg-source: info: unpacking mini-buildd_1.0.36.tar.xz
Fetched 929 kB in 0s (3034 kB/s)
W: Download is performed unsandboxed as root as file 'mini-buildd_1.0.36.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source mini-buildd=1.0.36
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
Some packages could not be installed. This may mean that you have
requested an impossible situation or if you are using the unstable
distribution that some required packages have not yet been created
or been moved out of Incoming.
The following information may help to resolve the situation:

The following packages have unmet dependencies:
 builddeps:mini-buildd : Depends: python (>= 2.7)
                         Depends: python-sphinx (>= 1.1.3) but it is not installable
                         Depends: python-pygraphviz but it is not installable
                         Depends: python-argcomplete (>= 0.5.4) but it is not installable
                         Depends: python-keyring (>= 1.6) but it is not installable
                         Depends: python-daemon (>= 2.0.5) but it is not installable
                         Depends: python-debian (>= 0.1.18~) but it is not installable
                         Depends: python-cherrypy3 but it is not installable
                         Depends: python-pyftpdlib (>= 1.2.0) but it is not installable
                         Depends: python-django (>= 1.7) but it is not installable
                         Depends: python-django (< 1:1.12) but it is not installable
                         Depends: python-django-registration (>= 2.0.4) but it is not installable
                         Depends: python-tz but it is not installable
                         Depends: python-tzlocal but it is not installable
E: Unable to correct problems, you have held broken packages.
