+ date
Wed Apr 19 08:09:03 UTC 2023
+ apt-get source --only-source mockery=0.0~git20181123.e78b021-3
Reading package lists...
NOTICE: 'mockery' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/go-team/packages/mockery.git
Please use:
git clone https://salsa.debian.org/go-team/packages/mockery.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 25.0 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main mockery 0.0~git20181123.e78b021-3 (dsc) [2228 B]
Get:2 http://repo.pureos.net/pureos byzantium/main mockery 0.0~git20181123.e78b021-3 (tar) [20.5 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main mockery 0.0~git20181123.e78b021-3 (diff) [2356 B]
dpkg-source: info: extracting mockery in mockery-0.0~git20181123.e78b021
dpkg-source: info: unpacking mockery_0.0~git20181123.e78b021.orig.tar.xz
dpkg-source: info: unpacking mockery_0.0~git20181123.e78b021-3.debian.tar.xz
Fetched 25.0 kB in 0s (135 kB/s)
W: Download is performed unsandboxed as root as file 'mockery_0.0~git20181123.e78b021-3.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source mockery=0.0~git20181123.e78b021-3
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  dh-golang golang-1.15-go golang-1.15-src golang-any
  golang-github-davecgh-go-spew-dev golang-github-pmezard-go-difflib-dev
  golang-github-stretchr-objx-dev golang-github-stretchr-testify-dev
  golang-github-yuin-goldmark-dev golang-go golang-golang-x-crypto-dev
  golang-golang-x-mod-dev golang-golang-x-net-dev golang-golang-x-sync-dev
  golang-golang-x-sys-dev golang-golang-x-term-dev golang-golang-x-text-dev
  golang-golang-x-tools-dev golang-golang-x-xerrors-dev
  golang-gopkg-yaml.v3-dev golang-src
0 upgraded, 21 newly installed, 0 to remove and 0 not upgraded.
Need to get 69.8 MB of archives.
After this operation, 424 MB of additional disk space will be used.
Get:1 http://repo.pureos.net/pureos byzantium/main amd64 dh-golang all 1.51 [24.6 kB]
Get:2 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-src amd64 1.15.15-1~deb11u4 [13.9 MB]
Get:3 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-go amd64 1.15.15-1~deb11u4 [47.4 MB]
Get:4 http://repo.pureos.net/pureos byzantium/main amd64 golang-src amd64 2:1.15~1 [4856 B]
Get:5 http://repo.pureos.net/pureos byzantium/main amd64 golang-go amd64 2:1.15~1 [23.9 kB]
Get:6 http://repo.pureos.net/pureos byzantium/main amd64 golang-any amd64 2:1.15~1 [4976 B]
Get:7 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-davecgh-go-spew-dev all 1.1.1-2 [29.7 kB]
Get:8 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-pmezard-go-difflib-dev all 1.0.0-3 [12.3 kB]
Get:9 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-stretchr-objx-dev all 0.3.0-1 [25.4 kB]
Get:10 http://repo.pureos.net/pureos byzantium/main amd64 golang-gopkg-yaml.v3-dev all 3.0.0~git20200121.a6ecf24-3 [70.5 kB]
Get:11 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-stretchr-testify-dev all 1.6.1-2 [60.4 kB]
Get:12 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-yuin-goldmark-dev all 1.3.2-1 [139 kB]
Get:13 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-sys-dev all 0.0~git20210124.22da62e-1 [308 kB]
Get:14 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-text-dev all 0.3.6-1 [3857 kB]
Get:15 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-net-dev all 1:0.0+git20210119.5f4716e+dfsg-4 [659 kB]
Get:16 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-term-dev all 0.0~git20201210.2321bbc-1 [14.5 kB]
Get:17 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-crypto-dev all 1:0.0~git20201221.eec23a3-1 [1538 kB]
Get:18 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-xerrors-dev all 0.0~git20191204.9bdfabe-1 [14.2 kB]
Get:19 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-mod-dev all 0.4.1-1 [87.9 kB]
Get:20 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-sync-dev all 0.0~git20210220.036812b-1 [19.1 kB]
Get:21 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-tools-dev all 1:0.1.0+ds-1 [1626 kB]
Fetched 69.8 MB in 4s (16.2 MB/s)
Selecting previously unselected package dh-golang.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 102989 files and directories currently installed.)
Preparing to unpack .../00-dh-golang_1.51_all.deb ...
Unpacking dh-golang (1.51) ...
Selecting previously unselected package golang-1.15-src.
Preparing to unpack .../01-golang-1.15-src_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-src (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-1.15-go.
Preparing to unpack .../02-golang-1.15-go_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-go (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-src:amd64.
Preparing to unpack .../03-golang-src_2%3a1.15~1_amd64.deb ...
Unpacking golang-src:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-go.
Preparing to unpack .../04-golang-go_2%3a1.15~1_amd64.deb ...
Unpacking golang-go (2:1.15~1) ...
Selecting previously unselected package golang-any:amd64.
Preparing to unpack .../05-golang-any_2%3a1.15~1_amd64.deb ...
Unpacking golang-any:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-github-davecgh-go-spew-dev.
Preparing to unpack .../06-golang-github-davecgh-go-spew-dev_1.1.1-2_all.deb ...
Unpacking golang-github-davecgh-go-spew-dev (1.1.1-2) ...
Selecting previously unselected package golang-github-pmezard-go-difflib-dev.
Preparing to unpack .../07-golang-github-pmezard-go-difflib-dev_1.0.0-3_all.deb ...
Unpacking golang-github-pmezard-go-difflib-dev (1.0.0-3) ...
Selecting previously unselected package golang-github-stretchr-objx-dev.
Preparing to unpack .../08-golang-github-stretchr-objx-dev_0.3.0-1_all.deb ...
Unpacking golang-github-stretchr-objx-dev (0.3.0-1) ...
Selecting previously unselected package golang-gopkg-yaml.v3-dev.
Preparing to unpack .../09-golang-gopkg-yaml.v3-dev_3.0.0~git20200121.a6ecf24-3_all.deb ...
Unpacking golang-gopkg-yaml.v3-dev (3.0.0~git20200121.a6ecf24-3) ...
Selecting previously unselected package golang-github-stretchr-testify-dev.
Preparing to unpack .../10-golang-github-stretchr-testify-dev_1.6.1-2_all.deb ...
Unpacking golang-github-stretchr-testify-dev (1.6.1-2) ...
Selecting previously unselected package golang-github-yuin-goldmark-dev.
Preparing to unpack .../11-golang-github-yuin-goldmark-dev_1.3.2-1_all.deb ...
Unpacking golang-github-yuin-goldmark-dev (1.3.2-1) ...
Selecting previously unselected package golang-golang-x-sys-dev.
Preparing to unpack .../12-golang-golang-x-sys-dev_0.0~git20210124.22da62e-1_all.deb ...
Unpacking golang-golang-x-sys-dev (0.0~git20210124.22da62e-1) ...
Selecting previously unselected package golang-golang-x-text-dev.
Preparing to unpack .../13-golang-golang-x-text-dev_0.3.6-1_all.deb ...
Unpacking golang-golang-x-text-dev (0.3.6-1) ...
Selecting previously unselected package golang-golang-x-net-dev.
Preparing to unpack .../14-golang-golang-x-net-dev_1%3a0.0+git20210119.5f4716e+dfsg-4_all.deb ...
Unpacking golang-golang-x-net-dev (1:0.0+git20210119.5f4716e+dfsg-4) ...
Selecting previously unselected package golang-golang-x-term-dev.
Preparing to unpack .../15-golang-golang-x-term-dev_0.0~git20201210.2321bbc-1_all.deb ...
Unpacking golang-golang-x-term-dev (0.0~git20201210.2321bbc-1) ...
Selecting previously unselected package golang-golang-x-crypto-dev.
Preparing to unpack .../16-golang-golang-x-crypto-dev_1%3a0.0~git20201221.eec23a3-1_all.deb ...
Unpacking golang-golang-x-crypto-dev (1:0.0~git20201221.eec23a3-1) ...
Selecting previously unselected package golang-golang-x-xerrors-dev.
Preparing to unpack .../17-golang-golang-x-xerrors-dev_0.0~git20191204.9bdfabe-1_all.deb ...
Unpacking golang-golang-x-xerrors-dev (0.0~git20191204.9bdfabe-1) ...
Selecting previously unselected package golang-golang-x-mod-dev.
Preparing to unpack .../18-golang-golang-x-mod-dev_0.4.1-1_all.deb ...
Unpacking golang-golang-x-mod-dev (0.4.1-1) ...
Selecting previously unselected package golang-golang-x-sync-dev.
Preparing to unpack .../19-golang-golang-x-sync-dev_0.0~git20210220.036812b-1_all.deb ...
Unpacking golang-golang-x-sync-dev (0.0~git20210220.036812b-1) ...
Selecting previously unselected package golang-golang-x-tools-dev.
Preparing to unpack .../20-golang-golang-x-tools-dev_1%3a0.1.0+ds-1_all.deb ...
Unpacking golang-golang-x-tools-dev (1:0.1.0+ds-1) ...
Setting up dh-golang (1.51) ...
Setting up golang-github-stretchr-objx-dev (0.3.0-1) ...
Setting up golang-1.15-src (1.15.15-1~deb11u4) ...
Setting up golang-gopkg-yaml.v3-dev (3.0.0~git20200121.a6ecf24-3) ...
Setting up golang-golang-x-sys-dev (0.0~git20210124.22da62e-1) ...
Setting up golang-github-pmezard-go-difflib-dev (1.0.0-3) ...
Setting up golang-golang-x-term-dev (0.0~git20201210.2321bbc-1) ...
Setting up golang-github-davecgh-go-spew-dev (1.1.1-2) ...
Setting up golang-github-stretchr-testify-dev (1.6.1-2) ...
Setting up golang-github-yuin-goldmark-dev (1.3.2-1) ...
Setting up golang-golang-x-sync-dev (0.0~git20210220.036812b-1) ...
Setting up golang-golang-x-text-dev (0.3.6-1) ...
Setting up golang-golang-x-xerrors-dev (0.0~git20191204.9bdfabe-1) ...
Setting up golang-src:amd64 (2:1.15~1) ...
Setting up golang-1.15-go (1.15.15-1~deb11u4) ...
Setting up golang-go (2:1.15~1) ...
Setting up golang-any:amd64 (2:1.15~1) ...
Setting up golang-golang-x-net-dev (1:0.0+git20210119.5f4716e+dfsg-4) ...
Setting up golang-golang-x-crypto-dev (1:0.0~git20201221.eec23a3-1) ...
Setting up golang-golang-x-mod-dev (0.4.1-1) ...
Setting up golang-golang-x-tools-dev (1:0.1.0+ds-1) ...
Processing triggers for man-db (2.9.4-2) ...
+ find . -maxdepth 1 -name mockery* -type d
+ cd ./mockery-0.0~git20181123.e78b021
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package mockery
dpkg-buildpackage: info: source version 0.0~git20181123.e78b021-3
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Dmitry Smirnov <onlyjob@debian.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --builddirectory=_build --buildsystem=golang --with=golang
   dh_auto_clean -O--builddirectory=_build -O--buildsystem=golang
   dh_autoreconf_clean -O--builddirectory=_build -O--buildsystem=golang
   dh_clean -O--builddirectory=_build -O--buildsystem=golang
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building mockery using existing ./mockery_0.0~git20181123.e78b021.orig.tar.xz
dpkg-source: info: building mockery in mockery_0.0~git20181123.e78b021-3.debian.tar.xz
dpkg-source: info: building mockery in mockery_0.0~git20181123.e78b021-3.dsc
 debian/rules binary
dh binary --builddirectory=_build --buildsystem=golang --with=golang
   dh_update_autotools_config -O--builddirectory=_build -O--buildsystem=golang
   dh_autoreconf -O--builddirectory=_build -O--buildsystem=golang
   dh_auto_configure -O--builddirectory=_build -O--buildsystem=golang
   dh_auto_build -O--builddirectory=_build -O--buildsystem=golang
	cd _build && go install -trimpath -v -p 40 github.com/vektra/mockery github.com/vektra/mockery/cmd/mockery github.com/vektra/mockery/mockery github.com/vektra/mockery/mockery/fixtures github.com/vektra/mockery/mockery/fixtures/buildtag/comment github.com/vektra/mockery/mockery/fixtures/buildtag/filename github.com/vektra/mockery/mockery/fixtures/http github.com/vektra/mockery/mockery/fixtures/mocks github.com/vektra/mockery/mockery/fixtures/test
unicode/utf8
golang.org/x/xerrors/internal
math/bits
github.com/vektra/mockery/mockery/fixtures/http
crypto/internal/subtle
github.com/vektra/mockery
golang.org/x/mod/semver
vendor/golang.org/x/crypto/cryptobyte/asn1
encoding
unicode/utf16
internal/goversion
internal/race
internal/nettrace
crypto/subtle
unicode
github.com/vektra/mockery/mockery/fixtures/buildtag/filename
github.com/vektra/mockery/mockery/fixtures/test
internal/unsafeheader
container/list
runtime/internal/sys
github.com/vektra/mockery/mockery/fixtures/buildtag/comment
sync/atomic
vendor/golang.org/x/crypto/internal/subtle
runtime/internal/atomic
internal/cpu
runtime/cgo
runtime/internal/math
internal/bytealg
internal/testlog
math
runtime
internal/reflectlite
sync
internal/singleflight
math/rand
errors
sort
internal/oserror
io
vendor/golang.org/x/net/dns/dnsmessage
strconv
syscall
container/heap
hash
bytes
text/tabwriter
strings
crypto/internal/randutil
crypto/hmac
hash/crc32
vendor/golang.org/x/crypto/hkdf
crypto/rc4
crypto
reflect
vendor/golang.org/x/text/transform
bufio
path
regexp/syntax
internal/syscall/execenv
internal/syscall/unix
time
regexp
context
internal/poll
internal/fmtsort
encoding/binary
os
encoding/base64
crypto/md5
crypto/sha512
crypto/ed25519/internal/edwards25519
crypto/sha1
crypto/cipher
vendor/golang.org/x/crypto/poly1305
crypto/sha256
encoding/pem
crypto/des
vendor/golang.org/x/crypto/chacha20
crypto/aes
internal/lazyregexp
runtime/debug
path/filepath
fmt
net
io/ioutil
os/exec
vendor/golang.org/x/sys/cpu
vendor/golang.org/x/crypto/chacha20poly1305
text/scanner
go/token
net/url
text/template/parse
flag
golang.org/x/xerrors
golang.org/x/tools/internal/event/label
log
encoding/json
golang.org/x/tools/internal/fastwalk
encoding/hex
vendor/golang.org/x/crypto/curve25519
vendor/golang.org/x/net/http2/hpack
compress/flate
internal/execabs
net/http/internal
github.com/pmezard/go-difflib/difflib
mime
mime/quotedprintable
golang.org/x/sys/execabs
vendor/golang.org/x/text/unicode/norm
gopkg.in/yaml.v3
math/big
internal/goroot
github.com/davecgh/go-spew/spew
golang.org/x/tools/internal/event/keys
golang.org/x/tools/internal/gopathwalk
vendor/golang.org/x/text/unicode/bidi
golang.org/x/mod/module
go/scanner
golang.org/x/tools/internal/event/core
go/ast
compress/gzip
golang.org/x/tools/internal/event
vendor/golang.org/x/text/secure/bidirule
text/template
golang.org/x/tools/internal/gocommand
runtime/pprof
vendor/golang.org/x/net/idna
github.com/stretchr/objx
golang.org/x/tools/internal/packagesinternal
go/parser
golang.org/x/tools/go/ast/astutil
go/printer
crypto/dsa
go/constant
crypto/rand
encoding/asn1
crypto/elliptic
go/doc
crypto/ed25519
crypto/rsa
crypto/x509/pkix
vendor/golang.org/x/crypto/cryptobyte
go/types
go/format
go/build
crypto/ecdsa
golang.org/x/tools/internal/imports
vendor/golang.org/x/net/http/httpproxy
net/textproto
crypto/x509
golang.org/x/tools/go/internal/packagesdriver
golang.org/x/tools/internal/typesinternal
golang.org/x/tools/go/internal/gcimporter
vendor/golang.org/x/net/http/httpguts
mime/multipart
golang.org/x/tools/imports
crypto/tls
golang.org/x/tools/go/gcexportdata
golang.org/x/tools/go/packages
github.com/vektra/mockery/mockery
github.com/vektra/mockery/cmd/mockery
net/http/httptrace
net/http
github.com/vektra/mockery/mockery/fixtures
net/http/httptest
github.com/stretchr/testify/assert
github.com/stretchr/testify/mock
github.com/vektra/mockery/mockery/fixtures/mocks
   debian/rules override_dh_auto_test
make[1]: Entering directory '/build/mockery/mockery-0.0~git20181123.e78b021'
dh_auto_test    
	cd _build && go test -vet=off -v -p 40 github.com/vektra/mockery github.com/vektra/mockery/cmd/mockery github.com/vektra/mockery/mockery github.com/vektra/mockery/mockery/fixtures github.com/vektra/mockery/mockery/fixtures/buildtag/comment github.com/vektra/mockery/mockery/fixtures/buildtag/filename github.com/vektra/mockery/mockery/fixtures/http github.com/vektra/mockery/mockery/fixtures/mocks github.com/vektra/mockery/mockery/fixtures/test
?   	github.com/vektra/mockery	[no test files]
=== RUN   TestParseConfigDefaults
--- PASS: TestParseConfigDefaults (0.00s)
=== RUN   TestParseConfigFlippingValues
--- PASS: TestParseConfigFlippingValues (0.00s)
PASS
ok  	github.com/vektra/mockery/cmd/mockery	0.008s
=== RUN   TestCompatSuite
=== RUN   TestCompatSuite/TestOnAnythingOfTypeVariadicArgs
    compat_test.go:34: PASS:	Sprintf(string,mock.AnythingOfTypeArgument,mock.AnythingOfTypeArgument)
=== RUN   TestCompatSuite/TestOnVariadicArgs
    compat_test.go:23: PASS:	Sprintf(string,int,string)
--- PASS: TestCompatSuite (0.00s)
    --- PASS: TestCompatSuite/TestOnAnythingOfTypeVariadicArgs (0.00s)
    --- PASS: TestCompatSuite/TestOnVariadicArgs (0.00s)
=== RUN   TestGeneratorSuite
=== RUN   TestGeneratorSuite/TestCalculateImport
=== RUN   TestGeneratorSuite/TestGenerator
=== RUN   TestGeneratorSuite/TestGeneratorArrayLiteralLen
=== RUN   TestGeneratorSuite/TestGeneratorChanType
=== RUN   TestGeneratorSuite/TestGeneratorChecksInterfacesForNilable
=== RUN   TestGeneratorSuite/TestGeneratorComplexChanFromConsul
=== RUN   TestGeneratorSuite/TestGeneratorElidedType
=== RUN   TestGeneratorSuite/TestGeneratorForEmptyInterface
=== RUN   TestGeneratorSuite/TestGeneratorForMapFunc
=== RUN   TestGeneratorSuite/TestGeneratorForMethodUsingInterface
=== RUN   TestGeneratorSuite/TestGeneratorForMethodUsingInterfaceInPackage
=== RUN   TestGeneratorSuite/TestGeneratorForStructValueReturn
=== RUN   TestGeneratorSuite/TestGeneratorFromImport
=== RUN   TestGeneratorSuite/TestGeneratorFuncType
=== RUN   TestGeneratorSuite/TestGeneratorHavingNoNamesOnArguments
=== RUN   TestGeneratorSuite/TestGeneratorNamespacedTypes
=== RUN   TestGeneratorSuite/TestGeneratorNoArguments
=== RUN   TestGeneratorSuite/TestGeneratorNoNothing
=== RUN   TestGeneratorSuite/TestGeneratorPointers
=== RUN   TestGeneratorSuite/TestGeneratorPrologue
=== RUN   TestGeneratorSuite/TestGeneratorPrologueNote
=== RUN   TestGeneratorSuite/TestGeneratorPrologueWithImports
=== RUN   TestGeneratorSuite/TestGeneratorPrologueWithMultipleImportsSameName
=== RUN   TestGeneratorSuite/TestGeneratorReturnElidedType
=== RUN   TestGeneratorSuite/TestGeneratorSingleReturn
=== RUN   TestGeneratorSuite/TestGeneratorSlice
=== RUN   TestGeneratorSuite/TestGeneratorUnexported
=== RUN   TestGeneratorSuite/TestGeneratorVariadicArgs
=== RUN   TestGeneratorSuite/TestGeneratorWhereArgumentNameConflictsWithImport
=== RUN   TestGeneratorSuite/TestGeneratorWhereArgumentNameConflictsWithNamedImport
=== RUN   TestGeneratorSuite/TestGeneratorWhereArgumentNameConflictsWithPackage
=== RUN   TestGeneratorSuite/TestGeneratorWithAliasing
=== RUN   TestGeneratorSuite/TestGeneratorWithImportSameAsLocalPackage
=== RUN   TestGeneratorSuite/TestGeneratorWithImportSameAsLocalPackageInpkgNoCycle
=== RUN   TestGeneratorSuite/TestPrologueWithImportFromNestedInterface
=== RUN   TestGeneratorSuite/TestPrologueWithImportSameAsLocalPackage
=== RUN   TestGeneratorSuite/TestVersionOnCorrectLine
--- PASS: TestGeneratorSuite (18.31s)
    --- PASS: TestGeneratorSuite/TestCalculateImport (0.00s)
    --- PASS: TestGeneratorSuite/TestGenerator (0.54s)
    --- PASS: TestGeneratorSuite/TestGeneratorArrayLiteralLen (0.50s)
    --- PASS: TestGeneratorSuite/TestGeneratorChanType (0.55s)
    --- PASS: TestGeneratorSuite/TestGeneratorChecksInterfacesForNilable (0.50s)
    --- PASS: TestGeneratorSuite/TestGeneratorComplexChanFromConsul (0.48s)
    --- PASS: TestGeneratorSuite/TestGeneratorElidedType (0.47s)
    --- PASS: TestGeneratorSuite/TestGeneratorForEmptyInterface (0.50s)
    --- PASS: TestGeneratorSuite/TestGeneratorForMapFunc (0.49s)
    --- PASS: TestGeneratorSuite/TestGeneratorForMethodUsingInterface (0.52s)
    --- PASS: TestGeneratorSuite/TestGeneratorForMethodUsingInterfaceInPackage (0.48s)
    --- PASS: TestGeneratorSuite/TestGeneratorForStructValueReturn (0.55s)
    --- PASS: TestGeneratorSuite/TestGeneratorFromImport (0.53s)
    --- PASS: TestGeneratorSuite/TestGeneratorFuncType (0.50s)
    --- PASS: TestGeneratorSuite/TestGeneratorHavingNoNamesOnArguments (0.50s)
    --- PASS: TestGeneratorSuite/TestGeneratorNamespacedTypes (0.50s)
    --- PASS: TestGeneratorSuite/TestGeneratorNoArguments (0.46s)
    --- PASS: TestGeneratorSuite/TestGeneratorNoNothing (0.50s)
    --- PASS: TestGeneratorSuite/TestGeneratorPointers (0.50s)
    --- PASS: TestGeneratorSuite/TestGeneratorPrologue (0.55s)
    --- PASS: TestGeneratorSuite/TestGeneratorPrologueNote (0.48s)
    --- PASS: TestGeneratorSuite/TestGeneratorPrologueWithImports (0.48s)
    --- PASS: TestGeneratorSuite/TestGeneratorPrologueWithMultipleImportsSameName (0.52s)
    --- PASS: TestGeneratorSuite/TestGeneratorReturnElidedType (0.50s)
    --- PASS: TestGeneratorSuite/TestGeneratorSingleReturn (0.51s)
    --- PASS: TestGeneratorSuite/TestGeneratorSlice (0.52s)
    --- PASS: TestGeneratorSuite/TestGeneratorUnexported (0.50s)
    --- PASS: TestGeneratorSuite/TestGeneratorVariadicArgs (0.52s)
    --- PASS: TestGeneratorSuite/TestGeneratorWhereArgumentNameConflictsWithImport (0.51s)
    --- PASS: TestGeneratorSuite/TestGeneratorWhereArgumentNameConflictsWithNamedImport (0.50s)
    --- PASS: TestGeneratorSuite/TestGeneratorWhereArgumentNameConflictsWithPackage (0.54s)
    --- PASS: TestGeneratorSuite/TestGeneratorWithAliasing (0.51s)
    --- PASS: TestGeneratorSuite/TestGeneratorWithImportSameAsLocalPackage (0.49s)
    --- PASS: TestGeneratorSuite/TestGeneratorWithImportSameAsLocalPackageInpkgNoCycle (0.52s)
    --- PASS: TestGeneratorSuite/TestPrologueWithImportFromNestedInterface (0.51s)
    --- PASS: TestGeneratorSuite/TestPrologueWithImportSameAsLocalPackage (0.52s)
    --- PASS: TestGeneratorSuite/TestVersionOnCorrectLine (0.58s)
=== RUN   TestFilenameBare
--- PASS: TestFilenameBare (0.00s)
=== RUN   TestFilenameMockOnly
--- PASS: TestFilenameMockOnly (0.00s)
=== RUN   TestFilenameMockTest
--- PASS: TestFilenameMockTest (0.00s)
=== RUN   TestFilenameTest
--- PASS: TestFilenameTest (0.00s)
=== RUN   TestUnderscoreCaseName
--- PASS: TestUnderscoreCaseName (0.00s)
=== RUN   TestFileParse
--- PASS: TestFileParse (0.51s)
=== RUN   TestBuildTagInFilename
--- PASS: TestBuildTagInFilename (1.91s)
=== RUN   TestBuildTagInComment
--- PASS: TestBuildTagInComment (3.12s)
=== RUN   TestCustomBuildTag
--- PASS: TestCustomBuildTag (1.37s)
=== RUN   TestWalkerHere
--- PASS: TestWalkerHere (8.22s)
=== RUN   TestWalkerRegexp
--- PASS: TestWalkerRegexp (7.87s)
PASS
ok  	github.com/vektra/mockery/mockery	41.325s
?   	github.com/vektra/mockery/mockery/fixtures	[no test files]
?   	github.com/vektra/mockery/mockery/fixtures/buildtag/comment	[no test files]
?   	github.com/vektra/mockery/mockery/fixtures/buildtag/filename	[no test files]
?   	github.com/vektra/mockery/mockery/fixtures/http	[no test files]
?   	github.com/vektra/mockery/mockery/fixtures/mocks	[no test files]
?   	github.com/vektra/mockery/mockery/fixtures/test	[no test files]
make[1]: Leaving directory '/build/mockery/mockery-0.0~git20181123.e78b021'
   create-stamp debian/debhelper-build-stamp
   dh_testroot -O--builddirectory=_build -O--buildsystem=golang
   dh_prep -O--builddirectory=_build -O--buildsystem=golang
   debian/rules override_dh_auto_install
make[1]: Entering directory '/build/mockery/mockery-0.0~git20181123.e78b021'
dh_auto_install -- --no-source
	cd _build && mkdir -p /build/mockery/mockery-0.0\~git20181123.e78b021/debian/mockery/usr
	cd _build && cp -r bin /build/mockery/mockery-0.0\~git20181123.e78b021/debian/mockery/usr
make[1]: Leaving directory '/build/mockery/mockery-0.0~git20181123.e78b021'
   dh_installdocs -O--builddirectory=_build -O--buildsystem=golang
   dh_installchangelogs -O--builddirectory=_build -O--buildsystem=golang
   dh_installinit -O--builddirectory=_build -O--buildsystem=golang
   dh_installsystemduser -O--builddirectory=_build -O--buildsystem=golang
   dh_perl -O--builddirectory=_build -O--buildsystem=golang
   dh_link -O--builddirectory=_build -O--buildsystem=golang
   dh_strip_nondeterminism -O--builddirectory=_build -O--buildsystem=golang
   dh_compress -O--builddirectory=_build -O--buildsystem=golang
   dh_fixperms -O--builddirectory=_build -O--buildsystem=golang
   dh_missing -O--builddirectory=_build -O--buildsystem=golang
   dh_dwz -O--builddirectory=_build -O--buildsystem=golang
dwz: debian/mockery/usr/bin/mockery: .debug_info section not present
   dh_strip -O--builddirectory=_build -O--buildsystem=golang
dh_strip: warning: Could not find the BuildID in debian/mockery/usr/bin/mockery
   dh_makeshlibs -O--builddirectory=_build -O--buildsystem=golang
   dh_shlibdeps -O--builddirectory=_build -O--buildsystem=golang
   dh_installdeb -O--builddirectory=_build -O--buildsystem=golang
   dh_golang -O--builddirectory=_build -O--buildsystem=golang
   dh_gencontrol -O--builddirectory=_build -O--buildsystem=golang
dpkg-gencontrol: warning: Depends field of package mockery: substitution variable ${shlibs:Depends} used, but is not defined
   dh_md5sums -O--builddirectory=_build -O--buildsystem=golang
   dh_builddeb -O--builddirectory=_build -O--buildsystem=golang
dpkg-deb: building package 'mockery' in '../mockery_0.0~git20181123.e78b021-3_amd64.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../mockery_0.0~git20181123.e78b021-3_amd64.changes
dpkg-genchanges: info: not including original source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: binary and diff upload (original source NOT included)
+ date
Wed Apr 19 08:11:01 UTC 2023
+ cd ..
+ ls -la
total 1636
drwxr-xr-x 3 root root    4096 Apr 19 08:11 .
drwxr-xr-x 3 root root    4096 Apr 19 08:09 ..
-rw-r--r-- 1 root root   26091 Apr 19 08:11 buildlog.txt
drwxr-xr-x 7 root root    4096 Apr 19 08:09 mockery-0.0~git20181123.e78b021
-rw-r--r-- 1 root root    2356 Apr 19 08:09 mockery_0.0~git20181123.e78b021-3.debian.tar.xz
-rw-r--r-- 1 root root    1345 Apr 19 08:09 mockery_0.0~git20181123.e78b021-3.dsc
-rw-r--r-- 1 root root    6274 Apr 19 08:11 mockery_0.0~git20181123.e78b021-3_amd64.buildinfo
-rw-r--r-- 1 root root    1795 Apr 19 08:11 mockery_0.0~git20181123.e78b021-3_amd64.changes
-rw-r--r-- 1 root root 1589928 Apr 19 08:11 mockery_0.0~git20181123.e78b021-3_amd64.deb
-rw-r--r-- 1 root root   20452 Nov 17  2019 mockery_0.0~git20181123.e78b021.orig.tar.xz
+ find . -maxdepth 1 -type f
+ sha256sum ./buildlog.txt ./mockery_0.0~git20181123.e78b021-3_amd64.changes ./mockery_0.0~git20181123.e78b021.orig.tar.xz ./mockery_0.0~git20181123.e78b021-3.debian.tar.xz ./mockery_0.0~git20181123.e78b021-3.dsc ./mockery_0.0~git20181123.e78b021-3_amd64.buildinfo ./mockery_0.0~git20181123.e78b021-3_amd64.deb
a23d8e798cdb7fed45a976bb38dfbb9a93fd5fdd77ca3f98307435352b065293  ./buildlog.txt
59430a8365c6d4b7578a0515ec6c2044d31725d675b29b8e7ab02e32d11fb079  ./mockery_0.0~git20181123.e78b021-3_amd64.changes
c6f534aa8ec75371bc9125af83ab24d95bb1bc7914bf8b1c276b78da89caf7d3  ./mockery_0.0~git20181123.e78b021.orig.tar.xz
7881f00e174ef67dbb441a9307dd0cc3258db4295e0f96ff8b291cd58e6be1cd  ./mockery_0.0~git20181123.e78b021-3.debian.tar.xz
75438097f0d144e7942e725f58a19ffa822c0d56fea2d8cda46e250d3f34a2c7  ./mockery_0.0~git20181123.e78b021-3.dsc
71b6490b0ddcbaf461e5d92a1dafeef1e4f382ca4ab74c3edd3f24d21f1451d1  ./mockery_0.0~git20181123.e78b021-3_amd64.buildinfo
346ddb4e361db50710c6045e11850e9a0fc6842ccb8593ae7f5e65fca62c0690  ./mockery_0.0~git20181123.e78b021-3_amd64.deb
+ mkdir published
+ cd published
+ cd ../
+ ls mockery_0.0~git20181123.e78b021-3_amd64.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/m/mockery/mockery_0.0~git20181123.e78b021-3_amd64.deb
--2023-04-19 08:11:01--  http://repo.pureos.net/pureos/pool/main/m/mockery/mockery_0.0~git20181123.e78b021-3_amd64.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 1836012 (1.8M) [application/octet-stream]
Saving to: 'mockery_0.0~git20181123.e78b021-3_amd64.deb'

     0K .......... .......... .......... .......... ..........  2%  951K 2s
    50K .......... .......... .......... .......... ..........  5% 1.81M 1s
   100K .......... .......... .......... .......... ..........  8% 45.0M 1s
   150K .......... .......... .......... .......... .......... 11% 42.1M 1s
   200K .......... .......... .......... .......... .......... 13% 1.88M 1s
   250K .......... .......... .......... .......... .......... 16% 40.2M 1s
   300K .......... .......... .......... .......... .......... 19% 37.0M 0s
   350K .......... .......... .......... .......... .......... 22% 44.2M 0s
   400K .......... .......... .......... .......... .......... 25% 2.30M 0s
   450K .......... .......... .......... .......... .......... 27% 26.4M 0s
   500K .......... .......... .......... .......... .......... 30% 23.8M 0s
   550K .......... .......... .......... .......... .......... 33% 30.1M 0s
   600K .......... .......... .......... .......... .......... 36% 44.1M 0s
   650K .......... .......... .......... .......... .......... 39% 38.3M 0s
   700K .......... .......... .......... .......... .......... 41% 41.5M 0s
   750K .......... .......... .......... .......... .......... 44% 2.68M 0s
   800K .......... .......... .......... .......... .......... 47% 28.6M 0s
   850K .......... .......... .......... .......... .......... 50% 28.7M 0s
   900K .......... .......... .......... .......... .......... 52% 17.5M 0s
   950K .......... .......... .......... .......... .......... 55% 20.7M 0s
  1000K .......... .......... .......... .......... .......... 58% 45.2M 0s
  1050K .......... .......... .......... .......... .......... 61% 39.7M 0s
  1100K .......... .......... .......... .......... .......... 64% 3.39M 0s
  1150K .......... .......... .......... .......... .......... 66% 18.0M 0s
  1200K .......... .......... .......... .......... .......... 69% 28.6M 0s
  1250K .......... .......... .......... .......... .......... 72% 17.9M 0s
  1300K .......... .......... .......... .......... .......... 75% 20.9M 0s
  1350K .......... .......... .......... .......... .......... 78% 16.9M 0s
  1400K .......... .......... .......... .......... .......... 80% 44.2M 0s
  1450K .......... .......... .......... .......... .......... 83% 38.6M 0s
  1500K .......... .......... .......... .......... .......... 86% 3.68M 0s
  1550K .......... .......... .......... .......... .......... 89% 23.2M 0s
  1600K .......... .......... .......... .......... .......... 92% 37.5M 0s
  1650K .......... .......... .......... .......... .......... 94% 19.4M 0s
  1700K .......... .......... .......... .......... .......... 97% 11.4M 0s
  1750K .......... .......... .......... .......... ..        100% 18.8M=0.2s

2023-04-19 08:11:01 (7.76 MB/s) - 'mockery_0.0~git20181123.e78b021-3_amd64.deb' saved [1836012/1836012]

+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./mockery_0.0~git20181123.e78b021-3_amd64.deb
286bf2d8b77e5ae414ec534a0b8f349488e908c9f2c488bdd20408e7466015c4  ./mockery_0.0~git20181123.e78b021-3_amd64.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./mockery_0.0~git20181123.e78b021-3_amd64.deb: FAILED
sha256sum: WARNING: 1 computed checksum did NOT match
