+ date
Tue Apr 18 07:55:16 UTC 2023
+ apt-get source --only-source globus-core=9.1
Reading package lists...
NOTICE: 'globus-core' packaging is maintained in the 'Svn' version control system at:
http://svn.nordugrid.org/repos/packaging/debian/globus-core
Need to get 4436 B of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main globus-core 9.1 (dsc) [1576 B]
Get:2 http://repo.pureos.net/pureos byzantium/main globus-core 9.1 (tar) [2860 B]
dpkg-source: info: extracting globus-core in globus-core-9.1
dpkg-source: info: unpacking globus-core_9.1.tar.xz
Fetched 4436 B in 0s (31.3 kB/s)
W: Download is performed unsandboxed as root as file 'globus-core_9.1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source globus-core=9.1
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name globus-core* -type d
+ cd ./globus-core-9.1
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package globus-core
dpkg-buildpackage: info: source version 9.1
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Mattias Ellert <mattias.ellert@fysast.uu.se>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh_testdir
dh_testroot
rm -f build-stamp configure-stamp
dh_clean
dh_clean: warning: Compatibility levels before 10 are deprecated (level 5 in use)
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building globus-core in globus-core_9.1.tar.xz
dpkg-source: info: building globus-core in globus-core_9.1.dsc
 debian/rules build
dh_testdir
touch configure-stamp
dh_testdir
touch build-stamp
 debian/rules binary
dh_testdir
dh_testroot
dh_prep
mkdir -p /build/globus-core/globus-core-9.1/debian/tmp/usr/lib/x86_64-linux-gnu/pkgconfig
install -p -m 644 debian/globus-core.pc /build/globus-core/globus-core-9.1/debian/tmp/usr/lib/x86_64-linux-gnu/pkgconfig
dh_testdir
dh_testroot
dh_installdocs
dh_installdocs: warning: Compatibility levels before 10 are deprecated (level 5 in use)
dh_installchangelogs
dh_install --fail-missing
dh_install: warning: Compatibility levels before 10 are deprecated (level 5 in use)
dh_install: warning: Please use dh_missing --list-missing/--fail-missing instead
dh_install: warning: This feature will be removed in compat 12.
dh_installman
dh_installman: warning: Compatibility levels before 10 are deprecated (level 5 in use)
dh_lintian
dh_link
dh_compress
dh_compress: warning: Compatibility levels before 10 are deprecated (level 5 in use)
dh_fixperms
dh_perl
dh_installdeb
dh_installdeb: warning: Compatibility levels before 10 are deprecated (level 5 in use)
dh_gencontrol
dh_md5sums
dh_builddeb
dpkg-deb: building package 'globus-core' in '../globus-core_9.1_amd64.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../globus-core_9.1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Tue Apr 18 07:55:21 UTC 2023
+ cd ..
+ ls -la
total 40
drwxr-xr-x 3 root root 4096 Apr 18 07:55 .
drwxr-xr-x 3 root root 4096 Apr 18 07:55 ..
-rw-r--r-- 1 root root 3344 Apr 18 07:55 buildlog.txt
drwxr-xr-x 3 root root 4096 Apr 18 07:55 globus-core-9.1
-rw-r--r-- 1 root root  709 Apr 18 07:55 globus-core_9.1.dsc
-rw-r--r-- 1 root root 2864 Apr 18 07:55 globus-core_9.1.tar.xz
-rw-r--r-- 1 root root 5176 Apr 18 07:55 globus-core_9.1_amd64.buildinfo
-rw-r--r-- 1 root root 1493 Apr 18 07:55 globus-core_9.1_amd64.changes
-rw-r--r-- 1 root root 3252 Apr 18 07:55 globus-core_9.1_amd64.deb
+ find . -maxdepth 1 -type f
+ sha256sum ./globus-core_9.1.dsc ./globus-core_9.1_amd64.deb ./buildlog.txt ./globus-core_9.1_amd64.changes ./globus-core_9.1.tar.xz ./globus-core_9.1_amd64.buildinfo
efaa26aa111ef5329bf5b770c3515b793d639125e5dd6b1b7fabe0692042617a  ./globus-core_9.1.dsc
76a3b42137038707621ffc78e08d29adb10434e79a810e07ad4527de6a4b1ede  ./globus-core_9.1_amd64.deb
d9304a13dc26f347b010d76c4b56e10a26d427820affd0d39371fec8da42a13a  ./buildlog.txt
2904a7e75d70e32bbd5cd79cb7b67fb9f7408eb6aa576f1ef4b6d05279764713  ./globus-core_9.1_amd64.changes
767ecf12c12f3d4636eeb3042950e8a1e8330b2bd90341d21bc4e13c57cce6b6  ./globus-core_9.1.tar.xz
c1aee6bb15b6ae28be06ddb2f831b6bb9998a801ab43a82980df2679ac12bca9  ./globus-core_9.1_amd64.buildinfo
+ mkdir published
+ cd published
+ cd ../
+ ls globus-core_9.1_amd64.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://repo.pureos.net/pureos/pool/main/g/globus-core/globus-core_9.1_amd64.deb
+ + tee ../SHA256SUMS
find . -maxdepth 1 -type f
+ sha256sum ./globus-core_9.1_amd64.deb
f1df73e1719c45ef02ee936a1a8d13c938985e3782612f0a032a4d937d3d28a8  ./globus-core_9.1_amd64.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./globus-core_9.1_amd64.deb: FAILED
sha256sum: WARNING: 1 computed checksum did NOT match
