+ date
Tue Apr 18 18:06:39 UTC 2023
+ apt-get source --only-source libhttp-tiny-perl=0.076-1
Reading package lists...
NOTICE: 'libhttp-tiny-perl' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/perl-team/modules/packages/libhttp-tiny-perl.git
Please use:
git clone https://salsa.debian.org/perl-team/modules/packages/libhttp-tiny-perl.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 76.5 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main libhttp-tiny-perl 0.076-1 (dsc) [2287 B]
Get:2 http://repo.pureos.net/pureos byzantium/main libhttp-tiny-perl 0.076-1 (tar) [70.1 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main libhttp-tiny-perl 0.076-1 (diff) [4136 B]
dpkg-source: info: extracting libhttp-tiny-perl in libhttp-tiny-perl-0.076
dpkg-source: info: unpacking libhttp-tiny-perl_0.076.orig.tar.gz
dpkg-source: info: unpacking libhttp-tiny-perl_0.076-1.debian.tar.xz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: applying tests-internet.patch
Fetched 76.5 kB in 0s (333 kB/s)
W: Download is performed unsandboxed as root as file 'libhttp-tiny-perl_0.076-1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source libhttp-tiny-perl=0.076-1
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  libhttp-cookiejar-perl
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 16.5 kB of archives.
After this operation, 48.1 kB of additional disk space will be used.
Get:1 http://repo.pureos.net/pureos byzantium/main amd64 libhttp-cookiejar-perl all 0.010-2 [16.5 kB]
Fetched 16.5 kB in 0s (95.8 kB/s)
Selecting previously unselected package libhttp-cookiejar-perl.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 102989 files and directories currently installed.)
Preparing to unpack .../libhttp-cookiejar-perl_0.010-2_all.deb ...
Unpacking libhttp-cookiejar-perl (0.010-2) ...
Setting up libhttp-cookiejar-perl (0.010-2) ...
Processing triggers for man-db (2.9.4-2) ...
+ find . -maxdepth 1 -name libhttp-tiny-perl* -type d
+ cd ./libhttp-tiny-perl-0.076
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package libhttp-tiny-perl
dpkg-buildpackage: info: source version 0.076-1
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by gregor herrmann <gregoa@debian.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
   dh_clean
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building libhttp-tiny-perl using existing ./libhttp-tiny-perl_0.076.orig.tar.gz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: building libhttp-tiny-perl in libhttp-tiny-perl_0.076-1.debian.tar.xz
dpkg-source: info: building libhttp-tiny-perl in libhttp-tiny-perl_0.076-1.dsc
 debian/rules build
dh build
   dh_update_autotools_config
   dh_autoreconf
   dh_auto_configure
	perl -I. Makefile.PL INSTALLDIRS=vendor "OPTIMIZE=-g -O2 -ffile-prefix-map=/build/libhttp-tiny-perl/libhttp-tiny-perl-0.076=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2" "LD=x86_64-linux-gnu-gcc -g -O2 -ffile-prefix-map=/build/libhttp-tiny-perl/libhttp-tiny-perl-0.076=. -fstack-protector-strong -Wformat -Werror=format-security -Wl,-z,relro"
Checking if your kit is complete...
Looks good
Generating a Unix-style Makefile
Writing Makefile for HTTP::Tiny
Writing MYMETA.yml and MYMETA.json
   dh_auto_build
	make -j50
make[1]: Entering directory '/build/libhttp-tiny-perl/libhttp-tiny-perl-0.076'
cp lib/HTTP/Tiny.pm blib/lib/HTTP/Tiny.pm
Manifying 1 pod document
make[1]: Leaving directory '/build/libhttp-tiny-perl/libhttp-tiny-perl-0.076'
   debian/rules override_dh_auto_test
make[1]: Entering directory '/build/libhttp-tiny-perl/libhttp-tiny-perl-0.076'
NO_NETWORK=1 dh_auto_test -- TEST_FILES="t/00-report-prereqs.t t/000_load.t t/001_api.t t/002_croakage.t t/003_agent.t t/004_timeout.t t/010_url.t t/020_headers.t t/030_response.t t/040_content.t t/050_chunked_body.t t/060_http_date.t t/070_cookie_jar.t t/100_get.t t/101_head.t t/102_put.t t/103_delete.t t/104_post.t t/110_mirror.t t/130_redirect.t t/140_proxy.t t/141_no_proxy.t t/150_post_form.t t/160_cookies.t t/161_basic_auth.t t/162_proxy_auth.t t/170_keepalive.t"
	make -j50 test TEST_VERBOSE=1 "TEST_FILES=t/00-report-prereqs.t t/000_load.t t/001_api.t t/002_croakage.t t/003_agent.t t/004_timeout.t t/010_url.t t/020_headers.t t/030_response.t t/040_content.t t/050_chunked_body.t t/060_http_date.t t/070_cookie_jar.t t/100_get.t t/101_head.t t/102_put.t t/103_delete.t t/104_post.t t/110_mirror.t t/130_redirect.t t/140_proxy.t t/141_no_proxy.t t/150_post_form.t t/160_cookies.t t/161_basic_auth.t t/162_proxy_auth.t t/170_keepalive.t"
make[2]: Entering directory '/build/libhttp-tiny-perl/libhttp-tiny-perl-0.076'
PERL_DL_NONLAZY=1 "/usr/bin/perl" "-MExtUtils::Command::MM" "-MTest::Harness" "-e" "undef *Test::Harness::Switches; test_harness(1, 'blib/lib', 'blib/arch')" t/00-report-prereqs.t t/000_load.t t/001_api.t t/002_croakage.t t/003_agent.t t/004_timeout.t t/010_url.t t/020_headers.t t/030_response.t t/040_content.t t/050_chunked_body.t t/060_http_date.t t/070_cookie_jar.t t/100_get.t t/101_head.t t/102_put.t t/103_delete.t t/104_post.t t/110_mirror.t t/130_redirect.t t/140_proxy.t t/141_no_proxy.t t/150_post_form.t t/160_cookies.t t/161_basic_auth.t t/162_proxy_auth.t t/170_keepalive.t
# 
# Versions for all modules listed in MYMETA.json (including optional ones):
# 
# === Configure Requires ===
# 
#     Module              Want Have
#     ------------------- ---- ----
#     ExtUtils::MakeMaker 6.17 7.44
# 
# === Configure Suggests ===
# 
#     Module      Want Have
#     -------- ------- ----
#     JSON::PP 2.27300 4.04
# 
# === Build Requires ===
# 
#     Module              Want Have
#     ------------------- ---- ----
#     ExtUtils::MakeMaker  any 7.44
# 
# === Test Requires ===
# 
#     Module              Want     Have
#     ------------------- ---- --------
#     Data::Dumper         any 2.174_01
#     Exporter             any     5.74
#     ExtUtils::MakeMaker  any     7.44
#     File::Basename       any     2.85
#     File::Spec           any     3.78
#     File::Temp           any   0.2309
#     IO::Dir              any     1.41
#     IO::File             any     1.41
#     IO::Socket::INET     any     1.41
#     IPC::Cmd             any     1.04
#     Test::More          0.96 1.302175
#     lib                  any     0.65
#     open                 any     1.12
# 
# === Test Recommends ===
# 
#     Module         Want     Have
#     ---------- -------- --------
#     CPAN::Meta 2.120900 2.150010
# 
# === Runtime Requires ===
# 
#     Module       Want  Have
#     ------------ ---- -----
#     Carp          any  1.50
#     Fcntl         any  1.13
#     IO::Socket    any  1.43
#     MIME::Base64  any  3.15
#     Socket        any 2.029
#     Time::Local   any  1.28
#     bytes         any  1.07
#     strict        any  1.11
#     warnings      any  1.47
# 
# === Runtime Recommends ===
# 
#     Module              Want    Have
#     --------------- -------- -------
#     HTTP::CookieJar    0.001   0.010
#     IO::Socket::IP      0.32    0.39
#     IO::Socket::SSL     1.42   2.069
#     Mozilla::CA     20160104 missing
#     Net::SSLeay         1.49    1.88
# 
# === Runtime Suggests ===
# 
#     Module          Want  Have
#     --------------- ---- -----
#     IO::Socket::SSL 1.56 2.069
# 
t/00-report-prereqs.t .. 
1..1
ok 1
ok
t/000_load.t ........... 
1..1
ok 1 - require HTTP::Tiny;
# HTTP::Tiny 0.076, Perl 5.032001, /usr/bin/perl
ok
t/001_api.t ............ 
1..2
ok 1 - HTTP::Tiny->can(...)
ok 2 - No unexpected subroutines defined
ok
t/002_croakage.t ....... 
ok 1 - get
ok 2 - get|http://www.example.com/|extra
ok 3 - get|http://www.example.com/|extra|extra
ok 4 - mirror
ok 5 - mirror|http://www.example.com/
ok 6 - mirror|http://www.example.com/|extra|extra
ok 7 - mirror|http://www.example.com/|extra|extra|extra
ok 8 - request
ok 9 - request|GET
ok 10 - request|GET|http://www.example.com/|extra
ok 11 - request|GET|http://www.example.com/|extra|extra
1..11
ok
t/003_agent.t .......... 
1..8
ok 1 - default agent string is as expected
ok 2 - agent string is as expected
ok 3 - agent string is as properly appended to
ok 4 - check _agent on class
ok 5 - check _agent on object
ok 6 - agent string is empty
ok 7 - agent string is as expected
ok 8 - agent string is as properly appended to
ok
t/004_timeout.t ........ 
1..5
ok 1 - default timeout is as expected
ok 2 - timeout is handled as a constructor param
ok 3 - constructor arg of timeout=0 is passed through
ok 4 - constructor arg of timeout=undef is ignored
ok 5 - timeout works as expected as a r/w attribute
ok
t/010_url.t ............ 
1..14
ok 1 - ->split_url('HtTp://Example.COM/')
ok 2 - ->split_url('HtTp://Example.com:1024/')
ok 3 - ->split_url('http://example.com')
ok 4 - ->split_url('http://example.com:')
ok 5 - ->split_url('http://foo@example.com:')
ok 6 - ->split_url('http://foo:pass@example.com:')
ok 7 - ->split_url('http://@example.com:')
ok 8 - ->split_url('http://example.com?foo=bar')
ok 9 - ->split_url('http://example.com?foo=bar\#fragment')
ok 10 - ->split_url('http://example.com/path?foo=bar')
ok 11 - ->split_url('http:///path?foo=bar')
ok 12 - ->split_url('HTTPS://example.com/')
ok 13 - ->split_url('http://[::]:1024')
ok 14 - ->split_url('xxx://foo/')
ok
t/020_headers.t ........ 
ok 1 - ->read_header_lines() CRLF
ok 2 - ->read_header_lines() LF
ok 3 - ->read_header_lines() insane continuations
ok 4 - roundtrip header lines
ok 5 - roundtrip header lines
1..5
ok
t/030_response.t ....... 
ok 1 - ->read_response_header CRLF
ok 2 - ->read_response_header LF
1..2
ok
t/040_content.t ........ 
ok 1 - written 131072 octets
ok 2 - read 131072 octets
1..2
ok
t/050_chunked_body.t ... 
ok 1 - roundtrip chunked chunks w/o trailers
ok 2 - roundtrip chunked trailers
ok 3 - roundtrip chunked chunks (with trailers)
1..3
ok
t/060_http_date.t ...... 
1..7
ok 1 - epoch -> RFC822/RFC1123
ok 2 - RFC822+RFC1123 -> epoch
ok 3 - broken RFC822+RFC1123 -> epoch
ok 4 - old rfc850 HTTP format -> epoch
ok 5 - broken rfc850 HTTP format -> epoch
ok 6 - ctime format -> epoch
ok 7 - same as ctime, except no TZ -> epoch
ok
t/070_cookie_jar.t ..... 
1..4
ok 1 - default cookie jar is as expected
ok 2 - cookie_jar is as expected
ok 3 - invalid jar does not support add method
ok 4 - invalid jar does not support cookie_header method
ok
t/100_get.t ............ 
ok 1 - get-01.txt host example.com
ok 2 - get-01.txt port 80
ok 3 - get-01.txt request data
ok 4 - get-01.txt response code 200
ok 5 - get-01.txt success flag true
ok 6 - get-01.txt response URL
ok 7 - get-01.txt content
ok 8 - get-01.txt redirects array doesn't exist
ok 9 - get-02.txt host example.com
ok 10 - get-02.txt port 80
ok 11 - get-02.txt request data
ok 12 - get-02.txt response code 200
ok 13 - get-02.txt success flag true
ok 14 - get-02.txt response URL
ok 15 - get-02.txt content
ok 16 - get-02.txt redirects array doesn't exist
ok 17 - get-03.txt host example.com
ok 18 - get-03.txt port 80
ok 19 - get-03.txt request data
ok 20 - get-03.txt response code 404
ok 21 - get-03.txt success flag false
ok 22 - get-03.txt response URL
ok 23 - get-03.txt content
ok 24 - get-03.txt redirects array doesn't exist
ok 25 - get-04.txt host example.com
ok 26 - get-04.txt port 9000
ok 27 - get-04.txt request data
ok 28 - get-04.txt response code 200
ok 29 - get-04.txt success flag true
ok 30 - get-04.txt response URL
ok 31 - get-04.txt content
ok 32 - get-04.txt redirects array doesn't exist
ok 33 - get-05.txt host example.com
ok 34 - get-05.txt port 80
ok 35 - get-05.txt request data
ok 36 - get-05.txt response code 200
ok 37 - get-05.txt success flag true
ok 38 - get-05.txt response URL
ok 39 - get-05.txt content
ok 40 - get-05.txt redirects array doesn't exist
ok 41 - get-06.txt host example.com
ok 42 - get-06.txt port 80
ok 43 - get-06.txt request data
ok 44 - get-06.txt response code 200
ok 45 - get-06.txt success flag true
ok 46 - get-06.txt response URL
ok 47 - get-06.txt cb got content
ok 48 - get-06.txt resp content empty
ok 49 - get-06.txt redirects array doesn't exist
ok 50 - get-07.txt host proxy.example.com
ok 51 - get-07.txt port 8080
ok 52 - get-07.txt request data
ok 53 - get-07.txt response code 200
ok 54 - get-07.txt success flag true
ok 55 - get-07.txt response URL
ok 56 - get-07.txt content
ok 57 - get-07.txt redirects array doesn't exist
ok 58 - get-08.txt host example.com
ok 59 - get-08.txt port 80
ok 60 - get-08.txt request data
ok 61 - get-08.txt response code 599
ok 62 - get-08.txt success flag false
ok 63 - get-08.txt response URL
ok 64 - get-08.txt content
ok 65 - get-08.txt redirects array doesn't exist
ok 66 - get-09.txt host example.com
ok 67 - get-09.txt port 80
ok 68 - get-09.txt request data
ok 69 - get-09.txt response code 200
ok 70 - get-09.txt success flag true
ok 71 - get-09.txt response URL
ok 72 - get-09.txt content
ok 73 - get-09.txt redirects array doesn't exist
ok 74 - get-10.txt host example.com
ok 75 - get-10.txt port 80
ok 76 - get-10.txt request data
ok 77 - get-10.txt response code 200
ok 78 - get-10.txt success flag true
ok 79 - get-10.txt response URL
ok 80 - get-10.txt content
ok 81 - get-10.txt redirects array doesn't exist
ok 82 - get-11.txt host example.com
ok 83 - get-11.txt port 80
ok 84 - get-11.txt request data
ok 85 - get-11.txt response code 200
ok 86 - get-11.txt success flag true
ok 87 - get-11.txt response URL
ok 88 - get-11.txt content
ok 89 - get-11.txt redirects array doesn't exist
ok 90 - get-12.txt host localhost
ok 91 - get-12.txt port 80
ok 92 - get-12.txt request data
ok 93 - get-12.txt response code 200
ok 94 - get-12.txt success flag true
ok 95 - get-12.txt response URL
ok 96 - get-12.txt content
ok 97 - get-12.txt redirects array doesn't exist
ok 98 - get-13.txt host example.com
ok 99 - get-13.txt port 80
ok 100 - get-13.txt request data
ok 101 - get-13.txt response code 200
ok 102 - get-13.txt success flag true
ok 103 - get-13.txt response URL
ok 104 - get-13.txt content
ok 105 - get-13.txt redirects array doesn't exist
ok 106 - get-14.txt host example.com
ok 107 - get-14.txt port 80
ok 108 - get-14.txt request data
ok 109 - get-14.txt response code 200
ok 110 - get-14.txt success flag true
ok 111 - get-14.txt response URL
ok 112 - get-14.txt content
ok 113 - get-14.txt redirects array doesn't exist
ok 114 - get-15.txt host example.com
ok 115 - get-15.txt port 80
ok 116 - get-15.txt request data
ok 117 - get-15.txt response code 200
ok 118 - get-15.txt success flag true
ok 119 - get-15.txt response URL
ok 120 - get-15.txt content
ok 121 - get-15.txt redirects array doesn't exist
ok 122 - get-16.txt host example.com
ok 123 - get-16.txt port 80
ok 124 - get-16.txt request data
ok 125 - get-16.txt response code 599
ok 126 - get-16.txt success flag false
ok 127 - get-16.txt response URL
ok 128 - get-16.txt content
ok 129 - get-16.txt redirects array doesn't exist
ok 130 - get-17.txt host example.com
ok 131 - get-17.txt port 80
ok 132 - get-17.txt request data
ok 133 - get-17.txt response code 599
ok 134 - get-17.txt success flag false
ok 135 - get-17.txt response URL
ok 136 - get-17.txt content
ok 137 - get-17.txt redirects array doesn't exist
ok 138 - get-18.txt host example.com
ok 139 - get-18.txt port 80
ok 140 - get-18.txt request data
ok 141 - get-18.txt response code 200
ok 142 - get-18.txt success flag true
ok 143 - get-18.txt response URL
ok 144 - get-18.txt expected headers
ok 145 - get-18.txt content
ok 146 - get-18.txt redirects array doesn't exist
ok 147 - get-19.txt host example.com
ok 148 - get-19.txt port 80
ok 149 - get-19.txt request data
ok 150 - get-19.txt response code 200
ok 151 - get-19.txt success flag true
ok 152 - get-19.txt response URL
ok 153 - get-19.txt content
ok 154 - get-19.txt redirects array doesn't exist
ok 155 - get-20.txt host example.com
ok 156 - get-20.txt port 80
ok 157 - get-20.txt request data
ok 158 - get-20.txt response code 200
ok 159 - get-20.txt success flag true
ok 160 - get-20.txt response URL
ok 161 - get-20.txt content
ok 162 - get-20.txt redirects array doesn't exist
ok 163 - get-21.txt host example.com
ok 164 - get-21.txt port 80
ok 165 - get-21.txt request data
ok 166 - get-21.txt response code 200
ok 167 - get-21.txt success flag true
ok 168 - get-21.txt response URL
ok 169 - get-21.txt content
ok 170 - get-21.txt redirects array doesn't exist
ok 171 - get-22.txt host example.com
ok 172 - get-22.txt port 80
ok 173 - get-22.txt request data
ok 174 - get-22.txt response code 599
ok 175 - get-22.txt success flag false
ok 176 - get-22.txt response URL
ok 177 - get-22.txt content
ok 178 - get-22.txt redirects array doesn't exist
1..178
ok
t/101_head.t ........... 
ok 1 - head-01.txt request
ok 2 - head-01.txt response code 200
ok 3 - head-01.txt success flag true
1..3
ok
t/102_put.t ............ 
ok 1 - put-01.txt request
ok 2 - put-01.txt response code 201
ok 3 - put-01.txt success flag true
ok 4 - put-02.txt request
ok 5 - put-02.txt response code 201
ok 6 - put-02.txt success flag true
ok 7 - put-03.txt request
ok 8 - put-03.txt response code 201
ok 9 - put-03.txt success flag true
ok 10 - put-04.txt request
ok 11 - put-04.txt response code 201
ok 12 - put-04.txt success flag true
ok 13 - put-05.txt request
ok 14 - put-05.txt response code 201
ok 15 - put-05.txt success flag true
1..15
ok
t/103_delete.t ......... 
ok 1 - delete-01.txt request
ok 2 - delete-01.txt response code 200
ok 3 - delete-01.txt success flag true
1..3
ok
t/104_post.t ........... 
ok 1 - post-01.txt request
ok 2 - post-01.txt response code 200
ok 3 - post-01.txt success flag true
1..3
ok
t/110_mirror.t ......... 
ok 1 - mirror-01.txt request
ok 2 - mirror-01.txt response code 200
ok 3 - mirror-01.txt success flag true
ok 4 - mirror-01.txt file created
ok 5 - mirror-02.txt request
ok 6 - mirror-02.txt response code 304
ok 7 - mirror-02.txt success flag true
ok 8 - mirror-02.txt file not overwritten
ok 9 - mirror-03.txt request
ok 10 - mirror-03.txt response code 200
ok 11 - mirror-03.txt success flag true
ok 12 - mirror-03.txt file created
ok 13 - mirror-04.txt request
ok 14 - mirror-04.txt response code 404
ok 15 - mirror-04.txt success flag false
ok 16 - mirror-04.txt file not created
ok 17 - mirror-05.txt request
ok 18 - mirror-05.txt response code 200
ok 19 - mirror-05.txt success flag true
ok 20 - mirror-05.txt file created
1..20
ok
t/130_redirect.t ....... 
ok 1 - redirect-01.txt request (0)
ok 2 - redirect-01.txt request (1)
ok 3 - redirect-01.txt content
ok 4 - redirect-01.txt redirects array size
ok 5 - redirect-01.txt response URL
ok 6 - redirect-02.txt request (0)
ok 7 - redirect-02.txt content
ok 8 - redirect-02.txt redirects array size
ok 9 - redirect-02.txt response URL
ok 10 - redirect-03.txt request (0)
ok 11 - redirect-03.txt request (1)
ok 12 - redirect-03.txt content
ok 13 - redirect-03.txt redirects array size
ok 14 - redirect-03.txt response URL
ok 15 - redirect-04.txt request (0)
ok 16 - redirect-04.txt request (1)
ok 17 - redirect-04.txt request (2)
ok 18 - redirect-04.txt content
ok 19 - redirect-04.txt redirects array size
ok 20 - redirect-04.txt response URL
ok 21 - redirect-05.txt request (0)
ok 22 - redirect-05.txt request (1)
ok 23 - redirect-05.txt request (2)
ok 24 - redirect-05.txt content
ok 25 - redirect-05.txt redirects array size
ok 26 - redirect-05.txt response URL
ok 27 - redirect-06.txt request (0)
ok 28 - redirect-06.txt request (1)
ok 29 - redirect-06.txt content
ok 30 - redirect-06.txt redirects array size
ok 31 - redirect-06.txt response URL
ok 32 - redirect-07.txt request (0)
ok 33 - redirect-07.txt request (1)
ok 34 - redirect-07.txt content
ok 35 - redirect-07.txt redirects array size
ok 36 - redirect-07.txt response URL
ok 37 - redirect-08.txt request (0)
ok 38 - redirect-08.txt content
ok 39 - redirect-08.txt redirects array size
ok 40 - redirect-08.txt response URL
ok 41 - redirect-09.txt request (0)
ok 42 - redirect-09.txt request (1)
ok 43 - redirect-09.txt content
ok 44 - redirect-09.txt redirects array size
ok 45 - redirect-09.txt response URL
ok 46 - redirect-10.txt request (0)
ok 47 - redirect-10.txt request (1)
ok 48 - redirect-10.txt content
ok 49 - redirect-10.txt redirects array size
ok 50 - redirect-10.txt response URL
1..50
ok
t/140_proxy.t .......... 
ok 1
ok 2
ok 3
ok 4
ok 5
ok 6
ok 7 - proxy => undef disables ENV proxy
ok 8 - http_proxy => undef disables ENV proxy
ok 9 - https_proxy => undef disables ENV proxy
ok 10 - set http_proxy from HTTP_PROXY
ok 11 - set http_proxy from http_proxy
ok 12 - set https_proxy from HTTPS_PROXY
ok 13 - set https_proxy from https_proxy
ok 14 - set proxy from ALL_PROXY
ok 15 - set proxy from all_proxy
ok 16 - http_proxy not set from HTTP_PROXY if REQUEST_METHOD set
1..16
ok
t/141_no_proxy.t ....... 
ok 1 - no no_proxy given
ok 2 - $ENV{no_proxy} = undef
ok 3 - new(no_proxy => undef) versus other $ENV{no_proxy}
ok 4 - new(no_proxy => [qw//])
ok 5 - $ENV{no_proxy} = 'localhost'
ok 6 - new(no_proxy => 'localhost') versus other $ENV{no_proxy}
ok 7 - new(no_proxy => [qw/localhost/])
ok 8 - $ENV{no_proxy} = 'localhost,example.com'
ok 9 - new(no_proxy => 'localhost,example.com') versus other $ENV{no_proxy}
ok 10 - $ENV{no_proxy} = 'localhost, example.com'
ok 11 - new(no_proxy => 'localhost, example.com') versus other $ENV{no_proxy}
ok 12 - new(no_proxy => [qw/localhost example.com/])
1..12
ok
t/150_post_form.t ...... 
ok 1 - form-01.txt request
ok 2 - form-01.txt response code 201
ok 3 - form-01.txt success flag true
ok 4 - form-02.txt request
ok 5 - form-02.txt response code 201
ok 6 - form-02.txt success flag true
ok 7 - form-03.txt request
ok 8 - form-03.txt response code 201
ok 9 - form-03.txt success flag true
ok 10 - form-04.txt request
ok 11 - form-04.txt response code 201
ok 12 - form-04.txt success flag true
ok 13 - form-05.txt request
ok 14 - form-05.txt response code 201
ok 15 - form-05.txt success flag true
1..15
ok
t/160_cookies.t ........ 
# Subtest: SimpleCookieJar
    ok 1 - cookies-01.txt request data
    ok 2 - cookies-01.txt request data
    ok 3 - cookies-02.txt request data
    ok 4 - cookies-02.txt request data
    ok 5 - cookies-02.txt request data
    ok 6 - cookies-03.txt request data
    ok 7 - cookies-03.txt request data
    ok 8 - cookies-03.txt request data
    ok 9 - cookies-03.txt request data
    ok 10 - cookies-03.txt request data
    ok 11 - cookies-04.txt request data
    ok 12 - cookies-04.txt request data
    ok 13 - cookies-05.txt request data
    ok 14 - cookies-05.txt request data
    ok 15 - cookies-06.txt request data
    ok 16 - cookies-06.txt request data
    ok 17 - cookies-06.txt request data
    ok 18 - cookies-07.txt request data
    ok 19 - cookies-07.txt request data
    1..19
ok 1 - SimpleCookieJar
# Subtest: HTTP::CookieJar
    ok 1 - cookies-01.txt request data
    ok 2 - cookies-01.txt request data
    ok 3 - cookies-02.txt request data
    ok 4 - cookies-02.txt request data
    ok 5 - cookies-02.txt request data
    ok 6 - cookies-03.txt request data
    ok 7 - cookies-03.txt request data
    ok 8 - cookies-03.txt request data
    ok 9 - cookies-03.txt request data
    ok 10 - cookies-03.txt request data
    ok 11 - cookies-04.txt request data
    ok 12 - cookies-04.txt request data
    ok 13 - cookies-05.txt request data
    ok 14 - cookies-05.txt request data
    ok 15 - cookies-06.txt request data
    ok 16 - cookies-06.txt request data
    ok 17 - cookies-06.txt request data
    ok 18 - cookies-07.txt request data
    ok 19 - cookies-07.txt request data
    1..19
ok 2 - HTTP::CookieJar
1..2
ok
t/161_basic_auth.t ..... 
ok 1 - auth-01.txt request (0)
ok 2 - auth-01.txt content
ok 3 - auth-02.txt request (0)
ok 4 - auth-02.txt content
ok 5 - auth-03.txt request (0)
ok 6 - auth-03.txt request (1)
ok 7 - auth-03.txt content
ok 8 - auth-03.txt response URL
ok 9 - auth-04.txt request (0)
ok 10 - auth-04.txt content
ok 11 - auth-05.txt request (0)
ok 12 - auth-05.txt content
1..12
ok
t/162_proxy_auth.t ..... 
ok 1 - proxy-auth-01.txt request (0)
ok 2 - proxy-auth-01.txt content
1..2
ok
t/170_keepalive.t ...... 
ok 1 - Keep-alive
ok 2 - Different scheme
ok 3 - Different host
ok 4 - Different port
ok 5 - Different timeout
ok 6 - Same timeout
ok 7 - Default headers change
ok 8 - Socket closed
ok 9 - keepalive-01.txt - No content length
ok 10 - keepalive-02.txt - Wrong content length
ok 11 - keepalive-03.txt - Connection close
ok 12 - keepalive-04.txt - Not HTTP/1.1
ok 13 - keepalive-05.txt - Not HTTP/1.1 with keep-alive
1..13
ok
All tests successful.
Files=27, Tests=409,  4 wallclock secs ( 0.17 usr  0.07 sys +  3.24 cusr  0.70 csys =  4.18 CPU)
Result: PASS
make[2]: Leaving directory '/build/libhttp-tiny-perl/libhttp-tiny-perl-0.076'
make[1]: Leaving directory '/build/libhttp-tiny-perl/libhttp-tiny-perl-0.076'
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary
   dh_testroot
   dh_prep
   dh_auto_install
	make -j50 install DESTDIR=/build/libhttp-tiny-perl/libhttp-tiny-perl-0.076/debian/libhttp-tiny-perl AM_UPDATE_INFO_DIR=no PREFIX=/usr
make[1]: Entering directory '/build/libhttp-tiny-perl/libhttp-tiny-perl-0.076'
Manifying 1 pod document
Installing /build/libhttp-tiny-perl/libhttp-tiny-perl-0.076/debian/libhttp-tiny-perl/usr/share/perl5/HTTP/Tiny.pm
Installing /build/libhttp-tiny-perl/libhttp-tiny-perl-0.076/debian/libhttp-tiny-perl/usr/share/man/man3/HTTP::Tiny.3pm
make[1]: Leaving directory '/build/libhttp-tiny-perl/libhttp-tiny-perl-0.076'
   dh_installdocs
   dh_installchangelogs
   dh_installexamples
   dh_installman
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'libhttp-tiny-perl' in '../libhttp-tiny-perl_0.076-1_all.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../libhttp-tiny-perl_0.076-1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload (original source is included)
+ date
Tue Apr 18 18:06:53 UTC 2023
+ cd ..
+ ls -la
total 180
drwxr-xr-x  3 root root  4096 Apr 18 18:06 .
drwxr-xr-x  3 root root  4096 Apr 18 18:06 ..
-rw-r--r--  1 root root 27095 Apr 18 18:06 buildlog.txt
drwxr-xr-x 10 root root  4096 Apr 18 18:06 libhttp-tiny-perl-0.076
-rw-r--r--  1 root root  4136 Apr 18 18:06 libhttp-tiny-perl_0.076-1.debian.tar.xz
-rw-r--r--  1 root root  1274 Apr 18 18:06 libhttp-tiny-perl_0.076-1.dsc
-rw-r--r--  1 root root 41512 Apr 18 18:06 libhttp-tiny-perl_0.076-1_all.deb
-rw-r--r--  1 root root  5364 Apr 18 18:06 libhttp-tiny-perl_0.076-1_amd64.buildinfo
-rw-r--r--  1 root root  1958 Apr 18 18:06 libhttp-tiny-perl_0.076-1_amd64.changes
-rw-r--r--  1 root root 70121 Aug 11  2018 libhttp-tiny-perl_0.076.orig.tar.gz
+ find . -maxdepth 1 -type f
+ sha256sum ./libhttp-tiny-perl_0.076.orig.tar.gz ./buildlog.txt ./libhttp-tiny-perl_0.076-1.dsc ./libhttp-tiny-perl_0.076-1_all.deb ./libhttp-tiny-perl_0.076-1_amd64.buildinfo ./libhttp-tiny-perl_0.076-1_amd64.changes ./libhttp-tiny-perl_0.076-1.debian.tar.xz
ddbdaa2fb511339fa621a80021bf1b9733fddafc4fe0245f26c8b92171ef9387  ./libhttp-tiny-perl_0.076.orig.tar.gz
0fd98960c8753dc7317600da112ed9bad5e16f720b3d261b3bc45f9c6136f5d6  ./buildlog.txt
f658f19ca8752acf77bc6e5edf3916e7adae621b2a9d7f24ab5e746bfe06cb28  ./libhttp-tiny-perl_0.076-1.dsc
3e6abe0b5523bbf5cb594b5e0767168622b65c65dcf33389c357d788a530c3ab  ./libhttp-tiny-perl_0.076-1_all.deb
0d746ff0310a0de51706d7b9f792fa8385957f960702b086a29c888fd6796ec5  ./libhttp-tiny-perl_0.076-1_amd64.buildinfo
60f6ec3bda8c86e4f1390054b7dcc60235f9c4b301352ee1ef78a1db37514b54  ./libhttp-tiny-perl_0.076-1_amd64.changes
b90a5b87f981e6b1f88142eb6677d58a54a30f022f777f34a5de34a71a491691  ./libhttp-tiny-perl_0.076-1.debian.tar.xz
+ mkdir published
+ cd published
+ cd ../
+ ls libhttp-tiny-perl_0.076-1_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/libh/libhttp-tiny-perl/libhttp-tiny-perl_0.076-1_all.deb
--2023-04-18 18:06:53--  http://repo.pureos.net/pureos/pool/main/libh/libhttp-tiny-perl/libhttp-tiny-perl_0.076-1_all.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 41488 (41K) [application/octet-stream]
Saving to: 'libhttp-tiny-perl_0.076-1_all.deb'

     0K .......... .......... .......... ..........           100%  775K=0.05s

2023-04-18 18:06:53 (775 KB/s) - 'libhttp-tiny-perl_0.076-1_all.deb' saved [41488/41488]

+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./libhttp-tiny-perl_0.076-1_all.deb
921ab501b12438e565386badc30fb7712c6b1c8697c305ac799376df7e6839d0  ./libhttp-tiny-perl_0.076-1_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./libhttp-tiny-perl_0.076-1_all.deb: FAILED
sha256sum: WARNING: 1 computed checksum did NOT match
