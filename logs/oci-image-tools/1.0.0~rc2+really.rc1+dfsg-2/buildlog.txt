+ date
Wed Apr 19 11:34:46 UTC 2023
+ apt-get source --only-source oci-image-tools=1.0.0~rc2+really.rc1+dfsg-2
Reading package lists...
NOTICE: 'oci-image-tools' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/go-team/packages/oci-image-tools.git
Please use:
git clone https://salsa.debian.org/go-team/packages/oci-image-tools.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 36.9 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main oci-image-tools 1.0.0~rc2+really.rc1+dfsg-2 (dsc) [2677 B]
Get:2 http://repo.pureos.net/pureos byzantium/main oci-image-tools 1.0.0~rc2+really.rc1+dfsg-2 (tar) [30.8 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main oci-image-tools 1.0.0~rc2+really.rc1+dfsg-2 (diff) [3368 B]
dpkg-source: info: extracting oci-image-tools in oci-image-tools-1.0.0~rc2+really.rc1+dfsg
dpkg-source: info: unpacking oci-image-tools_1.0.0~rc2+really.rc1+dfsg.orig.tar.xz
dpkg-source: info: unpacking oci-image-tools_1.0.0~rc2+really.rc1+dfsg-2.debian.tar.xz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: applying test-dep-fix.patch
dpkg-source: info: applying auto-gitignore
Fetched 36.9 kB in 0s (149 kB/s)
W: Download is performed unsandboxed as root as file 'oci-image-tools_1.0.0~rc2+really.rc1+dfsg-2.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source oci-image-tools=1.0.0~rc2+really.rc1+dfsg-2
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  bash-completion dh-golang go-md2man golang-1.15-go golang-1.15-src
  golang-any golang-github-cpuguy83-go-md2man-v2-dev
  golang-github-opencontainers-go-digest-dev
  golang-github-opencontainers-image-spec-dev
  golang-github-opencontainers-specs-dev golang-github-pkg-errors-dev
  golang-github-pmezard-go-difflib-dev
  golang-github-russross-blackfriday-v2-dev
  golang-github-shurcool-sanitized-anchor-name-dev
  golang-github-sirupsen-logrus-dev golang-github-urfave-cli-dev
  golang-github-xeipuuv-gojsonpointer-dev
  golang-github-xeipuuv-gojsonreference-dev
  golang-github-xeipuuv-gojsonschema-dev golang-go golang-golang-x-sys-dev
  golang-src
0 upgraded, 22 newly installed, 0 to remove and 0 not upgraded.
Need to get 62.9 MB of archives.
After this operation, 371 MB of additional disk space will be used.
Get:1 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-sys-dev all 0.0~git20210124.22da62e-1 [308 kB]
Get:2 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-sirupsen-logrus-dev all 1.7.0-2 [45.3 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main amd64 bash-completion all 1:2.11-2 [234 kB]
Get:4 http://repo.pureos.net/pureos byzantium/main amd64 dh-golang all 1.51 [24.6 kB]
Get:5 http://repo.pureos.net/pureos byzantium/main amd64 go-md2man amd64 2.0.0+ds-5+b2 [688 kB]
Get:6 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-src amd64 1.15.15-1~deb11u4 [13.9 MB]
Get:7 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-go amd64 1.15.15-1~deb11u4 [47.4 MB]
Get:8 http://repo.pureos.net/pureos byzantium/main amd64 golang-src amd64 2:1.15~1 [4856 B]
Get:9 http://repo.pureos.net/pureos byzantium/main amd64 golang-go amd64 2:1.15~1 [23.9 kB]
Get:10 http://repo.pureos.net/pureos byzantium/main amd64 golang-any amd64 2:1.15~1 [4976 B]
Get:11 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-pmezard-go-difflib-dev all 1.0.0-3 [12.3 kB]
Get:12 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-shurcool-sanitized-anchor-name-dev all 1.0.0-1 [3872 B]
Get:13 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-russross-blackfriday-v2-dev all 2.0.1-3 [67.8 kB]
Get:14 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-cpuguy83-go-md2man-v2-dev all 2.0.0+ds-5 [9932 B]
Get:15 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-opencontainers-go-digest-dev all 1.0.0-1 [12.1 kB]
Get:16 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-pkg-errors-dev all 0.9.1-1 [13.0 kB]
Get:17 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-xeipuuv-gojsonpointer-dev all 0.0~git20190905.02993c4-1 [5736 B]
Get:18 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-xeipuuv-gojsonreference-dev all 0.0~git20180127.bd5ef7b-2 [5132 B]
Get:19 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-xeipuuv-gojsonschema-dev all 1.2.0-1 [44.8 kB]
Get:20 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-opencontainers-image-spec-dev all 1.0.1-5 [34.2 kB]
Get:21 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-opencontainers-specs-dev all 1.0.2.41.g7413a7f-1+deb11u1 [26.9 kB]
Get:22 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-urfave-cli-dev all 1.22.4-2 [46.7 kB]
Fetched 62.9 MB in 2s (30.2 MB/s)
Selecting previously unselected package golang-golang-x-sys-dev.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 102989 files and directories currently installed.)
Preparing to unpack .../00-golang-golang-x-sys-dev_0.0~git20210124.22da62e-1_all.deb ...
Unpacking golang-golang-x-sys-dev (0.0~git20210124.22da62e-1) ...
Selecting previously unselected package golang-github-sirupsen-logrus-dev.
Preparing to unpack .../01-golang-github-sirupsen-logrus-dev_1.7.0-2_all.deb ...
Unpacking golang-github-sirupsen-logrus-dev (1.7.0-2) ...
Selecting previously unselected package bash-completion.
Preparing to unpack .../02-bash-completion_1%3a2.11-2_all.deb ...
Unpacking bash-completion (1:2.11-2) ...
Selecting previously unselected package dh-golang.
Preparing to unpack .../03-dh-golang_1.51_all.deb ...
Unpacking dh-golang (1.51) ...
Selecting previously unselected package go-md2man.
Preparing to unpack .../04-go-md2man_2.0.0+ds-5+b2_amd64.deb ...
Unpacking go-md2man (2.0.0+ds-5+b2) ...
Selecting previously unselected package golang-1.15-src.
Preparing to unpack .../05-golang-1.15-src_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-src (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-1.15-go.
Preparing to unpack .../06-golang-1.15-go_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-go (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-src:amd64.
Preparing to unpack .../07-golang-src_2%3a1.15~1_amd64.deb ...
Unpacking golang-src:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-go.
Preparing to unpack .../08-golang-go_2%3a1.15~1_amd64.deb ...
Unpacking golang-go (2:1.15~1) ...
Selecting previously unselected package golang-any:amd64.
Preparing to unpack .../09-golang-any_2%3a1.15~1_amd64.deb ...
Unpacking golang-any:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-github-pmezard-go-difflib-dev.
Preparing to unpack .../10-golang-github-pmezard-go-difflib-dev_1.0.0-3_all.deb ...
Unpacking golang-github-pmezard-go-difflib-dev (1.0.0-3) ...
Selecting previously unselected package golang-github-shurcool-sanitized-anchor-name-dev.
Preparing to unpack .../11-golang-github-shurcool-sanitized-anchor-name-dev_1.0.0-1_all.deb ...
Unpacking golang-github-shurcool-sanitized-anchor-name-dev (1.0.0-1) ...
Selecting previously unselected package golang-github-russross-blackfriday-v2-dev.
Preparing to unpack .../12-golang-github-russross-blackfriday-v2-dev_2.0.1-3_all.deb ...
Unpacking golang-github-russross-blackfriday-v2-dev (2.0.1-3) ...
Selecting previously unselected package golang-github-cpuguy83-go-md2man-v2-dev.
Preparing to unpack .../13-golang-github-cpuguy83-go-md2man-v2-dev_2.0.0+ds-5_all.deb ...
Unpacking golang-github-cpuguy83-go-md2man-v2-dev (2.0.0+ds-5) ...
Selecting previously unselected package golang-github-opencontainers-go-digest-dev.
Preparing to unpack .../14-golang-github-opencontainers-go-digest-dev_1.0.0-1_all.deb ...
Unpacking golang-github-opencontainers-go-digest-dev (1.0.0-1) ...
Selecting previously unselected package golang-github-pkg-errors-dev.
Preparing to unpack .../15-golang-github-pkg-errors-dev_0.9.1-1_all.deb ...
Unpacking golang-github-pkg-errors-dev (0.9.1-1) ...
Selecting previously unselected package golang-github-xeipuuv-gojsonpointer-dev.
Preparing to unpack .../16-golang-github-xeipuuv-gojsonpointer-dev_0.0~git20190905.02993c4-1_all.deb ...
Unpacking golang-github-xeipuuv-gojsonpointer-dev (0.0~git20190905.02993c4-1) ...
Selecting previously unselected package golang-github-xeipuuv-gojsonreference-dev.
Preparing to unpack .../17-golang-github-xeipuuv-gojsonreference-dev_0.0~git20180127.bd5ef7b-2_all.deb ...
Unpacking golang-github-xeipuuv-gojsonreference-dev (0.0~git20180127.bd5ef7b-2) ...
Selecting previously unselected package golang-github-xeipuuv-gojsonschema-dev.
Preparing to unpack .../18-golang-github-xeipuuv-gojsonschema-dev_1.2.0-1_all.deb ...
Unpacking golang-github-xeipuuv-gojsonschema-dev (1.2.0-1) ...
Selecting previously unselected package golang-github-opencontainers-image-spec-dev.
Preparing to unpack .../19-golang-github-opencontainers-image-spec-dev_1.0.1-5_all.deb ...
Unpacking golang-github-opencontainers-image-spec-dev (1.0.1-5) ...
Selecting previously unselected package golang-github-opencontainers-specs-dev.
Preparing to unpack .../20-golang-github-opencontainers-specs-dev_1.0.2.41.g7413a7f-1+deb11u1_all.deb ...
Unpacking golang-github-opencontainers-specs-dev (1.0.2.41.g7413a7f-1+deb11u1) ...
Selecting previously unselected package golang-github-urfave-cli-dev.
Preparing to unpack .../21-golang-github-urfave-cli-dev_1.22.4-2_all.deb ...
Unpacking golang-github-urfave-cli-dev (1.22.4-2) ...
Setting up golang-github-xeipuuv-gojsonpointer-dev (0.0~git20190905.02993c4-1) ...
Setting up dh-golang (1.51) ...
Setting up golang-github-opencontainers-specs-dev (1.0.2.41.g7413a7f-1+deb11u1) ...
Setting up golang-github-opencontainers-go-digest-dev (1.0.0-1) ...
Setting up golang-github-shurcool-sanitized-anchor-name-dev (1.0.0-1) ...
Setting up golang-github-pkg-errors-dev (0.9.1-1) ...
Setting up golang-1.15-src (1.15.15-1~deb11u4) ...
Setting up golang-golang-x-sys-dev (0.0~git20210124.22da62e-1) ...
Setting up golang-github-pmezard-go-difflib-dev (1.0.0-3) ...
Setting up bash-completion (1:2.11-2) ...
Setting up golang-github-xeipuuv-gojsonreference-dev (0.0~git20180127.bd5ef7b-2) ...
Setting up go-md2man (2.0.0+ds-5+b2) ...
Setting up golang-src:amd64 (2:1.15~1) ...
Setting up golang-1.15-go (1.15.15-1~deb11u4) ...
Setting up golang-github-sirupsen-logrus-dev (1.7.0-2) ...
Setting up golang-github-russross-blackfriday-v2-dev (2.0.1-3) ...
Setting up golang-go (2:1.15~1) ...
Setting up golang-github-xeipuuv-gojsonschema-dev (1.2.0-1) ...
Setting up golang-any:amd64 (2:1.15~1) ...
Setting up golang-github-opencontainers-image-spec-dev (1.0.1-5) ...
Setting up golang-github-cpuguy83-go-md2man-v2-dev (2.0.0+ds-5) ...
Setting up golang-github-urfave-cli-dev (1.22.4-2) ...
Processing triggers for man-db (2.9.4-2) ...
+ find . -maxdepth 1 -name oci-image-tools* -type d
+ cd ./oci-image-tools-1.0.0~rc2+really.rc1+dfsg
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package oci-image-tools
dpkg-buildpackage: info: source version 1.0.0~rc2+really.rc1+dfsg-2
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Reinhard Tartler <siretart@tauware.de>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --buildsystem=golang --with=golang,bash-completion
   dh_auto_clean -O--buildsystem=golang
   dh_autoreconf_clean -O--buildsystem=golang
   dh_clean -O--buildsystem=golang
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building oci-image-tools using existing ./oci-image-tools_1.0.0~rc2+really.rc1+dfsg.orig.tar.xz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: warning: ignoring deletion of file image/manifest_test.go, use --include-removal to override
dpkg-source: info: building oci-image-tools in oci-image-tools_1.0.0~rc2+really.rc1+dfsg-2.debian.tar.xz
dpkg-source: info: building oci-image-tools in oci-image-tools_1.0.0~rc2+really.rc1+dfsg-2.dsc
 debian/rules build
dh build --buildsystem=golang --with=golang,bash-completion
   dh_update_autotools_config -O--buildsystem=golang
   dh_autoreconf -O--buildsystem=golang
   dh_auto_configure -O--buildsystem=golang
   debian/rules override_dh_auto_build
make[1]: Entering directory '/build/oci-image-tools/oci-image-tools-1.0.0~rc2+really.rc1+dfsg'
dh_auto_build
	cd obj-x86_64-linux-gnu && go install -trimpath -v -p 40 github.com/opencontainers/image-tools/cmd/oci-image-tool github.com/opencontainers/image-tools/image github.com/opencontainers/image-tools/version
internal/unsafeheader
unicode/utf8
internal/race
math/bits
runtime/internal/sys
unicode
unicode/utf16
sync/atomic
runtime/internal/atomic
encoding
vendor/golang.org/x/crypto/cryptobyte/asn1
crypto/internal/subtle
golang.org/x/sys/internal/unsafeheader
vendor/golang.org/x/crypto/internal/subtle
internal/nettrace
crypto/subtle
container/list
runtime/cgo
internal/cpu
runtime/internal/math
internal/testlog
internal/bytealg
math
github.com/shurcooL/sanitized_anchor_name
runtime
internal/reflectlite
sync
internal/singleflight
math/rand
errors
sort
internal/oserror
io
vendor/golang.org/x/net/dns/dnsmessage
strconv
syscall
hash
crypto/internal/randutil
text/tabwriter
bytes
strings
crypto/hmac
hash/crc32
vendor/golang.org/x/crypto/hkdf
crypto
crypto/rc4
reflect
vendor/golang.org/x/text/transform
path
bufio
regexp/syntax
html
compress/bzip2
internal/syscall/execenv
time
internal/syscall/unix
regexp
context
internal/poll
internal/fmtsort
encoding/binary
os
encoding/base64
crypto/sha512
crypto/cipher
crypto/md5
crypto/ed25519/internal/edwards25519
crypto/sha1
crypto/sha256
vendor/golang.org/x/crypto/poly1305
golang.org/x/sys/unix
encoding/pem
crypto/des
vendor/golang.org/x/crypto/chacha20
crypto/aes
path/filepath
fmt
net
io/ioutil
vendor/golang.org/x/sys/cpu
github.com/opencontainers/image-spec/specs-go
github.com/xeipuuv/gojsonpointer
compress/flate
github.com/opencontainers/go-digest
log
encoding/hex
net/url
github.com/pkg/errors
encoding/json
vendor/golang.org/x/net/http2/hpack
mime
vendor/golang.org/x/crypto/curve25519
mime/quotedprintable
net/http/internal
text/template/parse
os/user
vendor/golang.org/x/crypto/chacha20poly1305
github.com/opencontainers/runtime-spec/specs-go
github.com/opencontainers/image-tools/version
math/big
vendor/golang.org/x/text/unicode/norm
flag
github.com/russross/blackfriday/v2
github.com/opencontainers/image-spec/specs-go/v1
vendor/golang.org/x/text/unicode/bidi
github.com/xeipuuv/gojsonreference
compress/gzip
archive/zip
text/template
vendor/golang.org/x/text/secure/bidirule
github.com/cpuguy83/go-md2man/v2/md2man
vendor/golang.org/x/net/idna
github.com/sirupsen/logrus
crypto/dsa
crypto/elliptic
encoding/asn1
crypto/rand
github.com/urfave/cli
crypto/ed25519
crypto/rsa
crypto/x509/pkix
vendor/golang.org/x/crypto/cryptobyte
crypto/ecdsa
archive/tar
vendor/golang.org/x/net/http/httpproxy
crypto/x509
net/textproto
vendor/golang.org/x/net/http/httpguts
net/mail
mime/multipart
crypto/tls
net/http/httptrace
net/http
github.com/xeipuuv/gojsonschema
github.com/opencontainers/image-spec/schema
github.com/opencontainers/image-tools/image
github.com/opencontainers/image-tools/cmd/oci-image-tool
/usr/bin/make man
make[2]: Entering directory '/build/oci-image-tools/oci-image-tools-1.0.0~rc2+really.rc1+dfsg'
go-md2man -in "man/oci-image-tool.1.md" -out "oci-image-tool.1"
go-md2man -in "man/oci-image-tool-create.1.md" -out "oci-image-tool-create.1"
go-md2man -in "man/oci-image-tool-unpack.1.md" -out "oci-image-tool-unpack.1"
go-md2man -in "man/oci-image-tool-validate.1.md" -out "oci-image-tool-validate.1"
make[2]: Leaving directory '/build/oci-image-tools/oci-image-tools-1.0.0~rc2+really.rc1+dfsg'
make[1]: Leaving directory '/build/oci-image-tools/oci-image-tools-1.0.0~rc2+really.rc1+dfsg'
   dh_auto_test -O--buildsystem=golang
	cd obj-x86_64-linux-gnu && go test -vet=off -v -p 40 github.com/opencontainers/image-tools/cmd/oci-image-tool github.com/opencontainers/image-tools/image github.com/opencontainers/image-tools/version
?   	github.com/opencontainers/image-tools/cmd/oci-image-tool	[no test files]
=== RUN   TestImageLayout
warning: manifest sha256:6c6a52c90398d50cd8e08ffe4f49fa4e830cde92265cd9eadf514c4c2999b780 has an unknown media type: application/vnd.oci.image.index.v1+json
warning: manifest sha256:6c6a52c90398d50cd8e08ffe4f49fa4e830cde92265cd9eadf514c4c2999b780 has an unknown media type: application/vnd.oci.image.index.v1+json
warning: manifest sha256:6c6a52c90398d50cd8e08ffe4f49fa4e830cde92265cd9eadf514c4c2999b780 has an unknown media type: application/vnd.oci.image.index.v1+json
warning: manifest sha256:6c6a52c90398d50cd8e08ffe4f49fa4e830cde92265cd9eadf514c4c2999b780 has an unknown media type: application/vnd.oci.image.index.v1+json
warning: manifest sha256:6c6a52c90398d50cd8e08ffe4f49fa4e830cde92265cd9eadf514c4c2999b780 has an unknown media type: application/vnd.oci.image.index.v1+json
--- PASS: TestImageLayout (0.06s)
PASS
ok  	github.com/opencontainers/image-tools/image	0.072s
?   	github.com/opencontainers/image-tools/version	[no test files]
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary --buildsystem=golang --with=golang,bash-completion
   dh_testroot -O--buildsystem=golang
   dh_prep -O--buildsystem=golang
   debian/rules override_dh_auto_install
make[1]: Entering directory '/build/oci-image-tools/oci-image-tools-1.0.0~rc2+really.rc1+dfsg'
dh_auto_install -- --no-source
	cd obj-x86_64-linux-gnu && mkdir -p /build/oci-image-tools/oci-image-tools-1.0.0\~rc2\+really.rc1\+dfsg/debian/oci-image-tool/usr
	cd obj-x86_64-linux-gnu && cp -r bin /build/oci-image-tools/oci-image-tools-1.0.0\~rc2\+really.rc1\+dfsg/debian/oci-image-tool/usr
make[1]: Leaving directory '/build/oci-image-tools/oci-image-tools-1.0.0~rc2+really.rc1+dfsg'
   dh_installdocs -O--buildsystem=golang
   dh_installchangelogs -O--buildsystem=golang
   dh_installman -O--buildsystem=golang
   dh_bash-completion -O--buildsystem=golang
   dh_installinit -O--buildsystem=golang
   dh_installsystemduser -O--buildsystem=golang
   dh_perl -O--buildsystem=golang
   dh_link -O--buildsystem=golang
   dh_strip_nondeterminism -O--buildsystem=golang
   dh_compress -O--buildsystem=golang
   dh_fixperms -O--buildsystem=golang
   dh_missing -O--buildsystem=golang
   dh_dwz -O--buildsystem=golang
dwz: debian/oci-image-tool/usr/bin/oci-image-tool: .debug_info section not present
   dh_strip -O--buildsystem=golang
dh_strip: warning: Could not find the BuildID in debian/oci-image-tool/usr/bin/oci-image-tool
   dh_makeshlibs -O--buildsystem=golang
   dh_shlibdeps -O--buildsystem=golang
   dh_installdeb -O--buildsystem=golang
   dh_golang -O--buildsystem=golang
   dh_gencontrol -O--buildsystem=golang
   dh_md5sums -O--buildsystem=golang
   dh_builddeb -O--buildsystem=golang
dpkg-deb: building package 'oci-image-tool' in '../oci-image-tool_1.0.0~rc2+really.rc1+dfsg-2_amd64.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../oci-image-tools_1.0.0~rc2+really.rc1+dfsg-2_amd64.changes
dpkg-genchanges: info: not including original source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: binary and diff upload (original source NOT included)
+ date
Wed Apr 19 11:36:06 UTC 2023
+ cd ..
+ ls -la
total 2444
drwxr-xr-x  3 root root    4096 Apr 19 11:36 .
drwxr-xr-x  3 root root    4096 Apr 19 11:34 ..
-rw-r--r--  1 root root   20263 Apr 19 11:36 buildlog.txt
-rw-r--r--  1 root root 2414884 Apr 19 11:36 oci-image-tool_1.0.0~rc2+really.rc1+dfsg-2_amd64.deb
drwxr-xr-x 12 root root    4096 Apr 19 11:35 oci-image-tools-1.0.0~rc2+really.rc1+dfsg
-rw-r--r--  1 root root    3368 Apr 19 11:35 oci-image-tools_1.0.0~rc2+really.rc1+dfsg-2.debian.tar.xz
-rw-r--r--  1 root root    1624 Apr 19 11:35 oci-image-tools_1.0.0~rc2+really.rc1+dfsg-2.dsc
-rw-r--r--  1 root root    6457 Apr 19 11:36 oci-image-tools_1.0.0~rc2+really.rc1+dfsg-2_amd64.buildinfo
-rw-r--r--  1 root root    2136 Apr 19 11:36 oci-image-tools_1.0.0~rc2+really.rc1+dfsg-2_amd64.changes
-rw-r--r--  1 root root   30844 Dec 18  2020 oci-image-tools_1.0.0~rc2+really.rc1+dfsg.orig.tar.xz
+ find . -maxdepth 1 -type f
+ sha256sum ./oci-image-tools_1.0.0~rc2+really.rc1+dfsg-2_amd64.changes ./buildlog.txt ./oci-image-tools_1.0.0~rc2+really.rc1+dfsg.orig.tar.xz ./oci-image-tool_1.0.0~rc2+really.rc1+dfsg-2_amd64.deb ./oci-image-tools_1.0.0~rc2+really.rc1+dfsg-2.debian.tar.xz ./oci-image-tools_1.0.0~rc2+really.rc1+dfsg-2.dsc ./oci-image-tools_1.0.0~rc2+really.rc1+dfsg-2_amd64.buildinfo
6fe04a0d215c1ebee9b5685b7dc3eb62af9b3f2bb927ac7d1fcfc049877003f4  ./oci-image-tools_1.0.0~rc2+really.rc1+dfsg-2_amd64.changes
8fb7429cb8380c31222fed8e1e4c930da4d980668865ae1b9b538f15978d1bf0  ./buildlog.txt
69b9be07bf3bbdfc0e97fafc705badbe98f486d568c0a05419b9ed47f74a42c3  ./oci-image-tools_1.0.0~rc2+really.rc1+dfsg.orig.tar.xz
456a98a46b89ccd8233f463651a4ce00f4b910add308122366b562f0199f81b8  ./oci-image-tool_1.0.0~rc2+really.rc1+dfsg-2_amd64.deb
b8033874d63a2434b6fd1254c9295b5c6363a6d0eacf772e52d20c03e9fa4c74  ./oci-image-tools_1.0.0~rc2+really.rc1+dfsg-2.debian.tar.xz
6d3861dd48aa0b1bb16cce130937f87cde19c636c248dfd03c3165bf1c15d525  ./oci-image-tools_1.0.0~rc2+really.rc1+dfsg-2.dsc
aff23dcf7fec6ed308a03f546ac657945754f0af23000aa628cc1c381fa791a0  ./oci-image-tools_1.0.0~rc2+really.rc1+dfsg-2_amd64.buildinfo
+ mkdir published
+ cd published
+ cd ../
+ ls oci-image-tool_1.0.0~rc2+really.rc1+dfsg-2_amd64.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/o/oci-image-tools/oci-image-tool_1.0.0~rc2+really.rc1+dfsg-2_amd64.deb
--2023-04-19 11:36:06--  http://repo.pureos.net/pureos/pool/main/o/oci-image-tools/oci-image-tool_1.0.0~rc2+really.rc1+dfsg-2_amd64.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 404 Not Found
2023-04-19 11:36:06 ERROR 404: Not Found.

