+ date
Wed Apr 19 08:19:02 UTC 2023
+ apt-get source --only-source moodle-debian-edu-theme=1.0.4
Reading package lists...
Need to get 245 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main moodle-debian-edu-theme 1.0.4 (dsc) [840 B]
Get:2 http://repo.pureos.net/pureos byzantium/main moodle-debian-edu-theme 1.0.4 (tar) [244 kB]
dpkg-source: info: extracting moodle-debian-edu-theme in moodle-debian-edu-theme-1.0.4
dpkg-source: info: unpacking moodle-debian-edu-theme_1.0.4.tar.gz
Fetched 245 kB in 0s (988 kB/s)
W: Download is performed unsandboxed as root as file 'moodle-debian-edu-theme_1.0.4.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source moodle-debian-edu-theme=1.0.4
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name moodle-debian-edu-theme* -type d
+ cd ./moodle-debian-edu-theme-1.0.4
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package moodle-debian-edu-theme
dpkg-buildpackage: info: source version 1.0.4
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Emanuele Rocca <ema@debian.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh_testdir
dh_testroot
rm -f build-stamp configure-stamp
dh_clean 
dh_clean: warning: Compatibility levels before 10 are deprecated (level 7 in use)
 dpkg-source -b .
dpkg-source: warning: no source format specified in debian/source/format, see dpkg-source(1)
dpkg-source: info: using source format '1.0'
dpkg-source: info: building moodle-debian-edu-theme in moodle-debian-edu-theme_1.0.4.tar.gz
dpkg-source: info: building moodle-debian-edu-theme in moodle-debian-edu-theme_1.0.4.dsc
 debian/rules build
dh_testdir
touch configure-stamp
dh_testdir
touch build-stamp
 debian/rules binary
dh_testdir
dh_testroot
dh_prep 
dh_installdirs
dh_installdirs: warning: Compatibility levels before 10 are deprecated (level 7 in use)
cp -R theme/* debian/moodle-debian-edu-theme/usr/share/moodle/theme/ 
chown -R root.root debian/moodle-debian-edu-theme/usr/share/moodle/theme/
find debian/moodle-debian-edu-theme/usr/share/moodle/theme/ -type f -exec chmod 640 {} \;
dh_testdir
dh_testroot
dh_installchangelogs 
dh_installdocs
dh_installdocs: warning: Compatibility levels before 10 are deprecated (level 7 in use)
dh_link
dh_compress 
dh_compress: warning: Compatibility levels before 10 are deprecated (level 7 in use)
dh_fixperms
dh_installdeb
dh_installdeb: warning: Compatibility levels before 10 are deprecated (level 7 in use)
dh_gencontrol
dh_md5sums
dh_builddeb
dpkg-deb: building package 'moodle-debian-edu-theme' in '../moodle-debian-edu-theme_1.0.4_all.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../moodle-debian-edu-theme_1.0.4_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Wed Apr 19 08:19:07 UTC 2023
+ cd ..
+ ls -la
total 512
drwxr-xr-x 3 root root   4096 Apr 19 08:19 .
drwxr-xr-x 3 root root   4096 Apr 19 08:19 ..
-rw-r--r-- 1 root root   3262 Apr 19 08:19 buildlog.txt
drwxr-xr-x 4 root root   4096 Apr 19 08:19 moodle-debian-edu-theme-1.0.4
-rw-r--r-- 1 root root    599 Apr 19 08:19 moodle-debian-edu-theme_1.0.4.dsc
-rw-r--r-- 1 root root 243253 Apr 19 08:19 moodle-debian-edu-theme_1.0.4.tar.gz
-rw-r--r-- 1 root root 244276 Apr 19 08:19 moodle-debian-edu-theme_1.0.4_all.deb
-rw-r--r-- 1 root root   5310 Apr 19 08:19 moodle-debian-edu-theme_1.0.4_amd64.buildinfo
-rw-r--r-- 1 root root   1796 Apr 19 08:19 moodle-debian-edu-theme_1.0.4_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./buildlog.txt ./moodle-debian-edu-theme_1.0.4_amd64.buildinfo ./moodle-debian-edu-theme_1.0.4_amd64.changes ./moodle-debian-edu-theme_1.0.4_all.deb ./moodle-debian-edu-theme_1.0.4.dsc ./moodle-debian-edu-theme_1.0.4.tar.gz
92440d0716b17b97ccc005566706ac7543029f59dc435a8d31dcafc59cfd4af3  ./buildlog.txt
c458df1e1de1360eb4e065deee7f089bd8bd6697742697fb40ccc049041d22c6  ./moodle-debian-edu-theme_1.0.4_amd64.buildinfo
02f4c709bb5023ccb38eb49428401f825316effb2c5090edb0ed8943df54c4cf  ./moodle-debian-edu-theme_1.0.4_amd64.changes
97c0021b9f8ee485d01a9c285420e7e24f1e05ab306513a483d1b8557949cf9b  ./moodle-debian-edu-theme_1.0.4_all.deb
2b4101308b6a3f5f7c92826f7a698f0c491af2afc00f63c4f523e623724e8e9e  ./moodle-debian-edu-theme_1.0.4.dsc
9902e6c821567b2825095a5697cd2b8f2c85dce91ee5253e009a98e01211480a  ./moodle-debian-edu-theme_1.0.4.tar.gz
+ mkdir published
+ cd published
+ cd ../
+ ls moodle-debian-edu-theme_1.0.4_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/m/moodle-debian-edu-theme/moodle-debian-edu-theme_1.0.4_all.deb
--2023-04-19 08:19:07--  http://repo.pureos.net/pureos/pool/main/m/moodle-debian-edu-theme/moodle-debian-edu-theme_1.0.4_all.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 244302 (239K) [application/octet-stream]
Saving to: 'moodle-debian-edu-theme_1.0.4_all.deb'

     0K .......... .......... .......... .......... .......... 20%  930K 0s
    50K .......... .......... .......... .......... .......... 41% 1.76M 0s
   100K .......... .......... .......... .......... .......... 62% 43.5M 0s
   150K .......... .......... .......... .......... .......... 83% 34.9M 0s
   200K .......... .......... .......... ........             100% 1.44M=0.1s

2023-04-19 08:19:07 (2.11 MB/s) - 'moodle-debian-edu-theme_1.0.4_all.deb' saved [244302/244302]

+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./moodle-debian-edu-theme_1.0.4_all.deb
03f9ffc0a1e8bb9c01b25e7c901ac828f8682eb60647026f724860de694339eb  ./moodle-debian-edu-theme_1.0.4_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./moodle-debian-edu-theme_1.0.4_all.deb: FAILED
sha256sum: WARNING: 1 computed checksum did NOT match
