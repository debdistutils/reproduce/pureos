+ date
Mon Apr 17 13:02:40 UTC 2023
+ apt-get source --only-source auto-apt=0.3.24
Reading package lists...
Need to get 41.4 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main auto-apt 0.3.24 (dsc) [1419 B]
Get:2 http://repo.pureos.net/pureos byzantium/main auto-apt 0.3.24 (tar) [40.0 kB]
dpkg-source: info: extracting auto-apt in auto-apt-0.3.24
dpkg-source: info: unpacking auto-apt_0.3.24.tar.xz
Fetched 41.4 kB in 0s (118 kB/s)
W: Download is performed unsandboxed as root as file 'auto-apt_0.3.24.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source auto-apt=0.3.24
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  ed quilt
0 upgraded, 2 newly installed, 0 to remove and 0 not upgraded.
Need to get 379 kB of archives.
After this operation, 924 kB of additional disk space will be used.
Get:1 http://repo.pureos.net/pureos byzantium/main amd64 ed amd64 1.17-1 [60.7 kB]
Get:2 http://repo.pureos.net/pureos byzantium/main amd64 quilt all 0.66-2.1 [319 kB]
Fetched 379 kB in 0s (1626 kB/s)
Selecting previously unselected package ed.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 102989 files and directories currently installed.)
Preparing to unpack .../archives/ed_1.17-1_amd64.deb ...
Unpacking ed (1.17-1) ...
Selecting previously unselected package quilt.
Preparing to unpack .../quilt_0.66-2.1_all.deb ...
Unpacking quilt (0.66-2.1) ...
Setting up ed (1.17-1) ...
Setting up quilt (0.66-2.1) ...
Processing triggers for man-db (2.9.4-2) ...
+ find . -maxdepth 1 -name auto-apt* -type d
+ cd ./auto-apt-0.3.24
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package auto-apt
dpkg-buildpackage: info: source version 0.3.24
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Iain Lane <iain@orangesquash.org.uk>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
dh: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_auto_clean
dh_auto_clean: warning: Compatibility levels before 10 are deprecated (level 7 in use)
	make -j1 distclean
make[1]: Entering directory '/build/auto-apt/auto-apt-0.3.24'
(cd pkgcdb && make clean)
make[2]: Entering directory '/build/auto-apt/auto-apt-0.3.24/pkgcdb'
rm -f pkgcdb2.o pathnode.o pkgtab.o strtab.o mempool.o pkgcdb2.a
make[2]: Leaving directory '/build/auto-apt/auto-apt-0.3.24/pkgcdb'
rm -f auto-apt.so auto-apt.o
rm -f auto-apt-pkgcdb auto-apt-pkgcdb.o
rm -rf cache
(cd pkgcdb && make distclean)
make[2]: Entering directory '/build/auto-apt/auto-apt-0.3.24/pkgcdb'
rm -f pkgcdb2.o pathnode.o pkgtab.o strtab.o mempool.o pkgcdb2.a
rm -f *~ *.bak *.orig *.o
make[2]: Leaving directory '/build/auto-apt/auto-apt-0.3.24/pkgcdb'
rm -f *~ *.bak *.orig *.db *.o *.so
make[1]: Leaving directory '/build/auto-apt/auto-apt-0.3.24'
   dh_clean
dh_clean: warning: Compatibility levels before 10 are deprecated (level 7 in use)
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building auto-apt in auto-apt_0.3.24.tar.xz
dpkg-source: info: building auto-apt in auto-apt_0.3.24.dsc
 debian/rules build
dh build
dh: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_update_autotools_config
   dh_auto_configure
dh_auto_configure: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_auto_build
dh_auto_build: warning: Compatibility levels before 10 are deprecated (level 7 in use)
	make -j1
make[1]: Entering directory '/build/auto-apt/auto-apt-0.3.24'
gcc -g -Wall -finline-functions -Ipkgcdb -DUSE_DETECT -DDEBUG -fPIC -o auto-apt.o -c auto-apt.c
In file included from auto-apt.c:29:
pkgcdb/mempool.c: In function 'mempool_restore':
pkgcdb/mempool.c:246:13: warning: format '%d' expects argument of type 'int', but argument 2 has type 'size_t' {aka 'long unsigned int'} [-Wformat=]
  246 |     DPRINT(("siz %d, ", mp->siz));
      |             ^~~~~~~~~~  ~~~~~~~
      |                           |
      |                           size_t {aka long unsigned int}
pkgcdb/debug.h:28:37: note: in definition of macro 'DPRINT'
   28 | #define DPRINT(x) if (debug) printf x
      |                                     ^
In file included from auto-apt.c:31:
pkgcdb/mempool.c:246:19: note: format string is defined here
  246 |     DPRINT(("siz %d, ", mp->siz));
      |                  ~^
      |                   |
      |                   int
      |                  %ld
In file included from auto-apt.c:34:
pkgcdb/pathnode.c: In function 'pathnode_unserialize':
pkgcdb/pathnode.c:413:45: warning: cast from pointer to integer of different size [-Wpointer-to-int-cast]
  413 |  if (pn->left) pn->left = mempool_fetch(mp, (int)pn->left);
      |                                             ^
pkgcdb/pathnode.c:414:47: warning: cast from pointer to integer of different size [-Wpointer-to-int-cast]
  414 |  if (pn->right) pn->right = mempool_fetch(mp, (int)pn->right);
      |                                               ^
pkgcdb/pathnode.c:415:45: warning: cast from pointer to integer of different size [-Wpointer-to-int-cast]
  415 |  if (pn->down) pn->down = mempool_fetch(mp, (int)pn->down);
      |                                             ^
pkgcdb/pathnode.c:416:45: warning: cast from pointer to integer of different size [-Wpointer-to-int-cast]
  416 |  if (pn->dups) pn->dups = mempool_fetch(mp, (int)pn->dups);
      |                                             ^
In file included from auto-apt.c:29:
pkgcdb/pkgcdb2.c: In function 'pkgcdb_get':
pkgcdb/pkgcdb2.c:181:13: warning: format '%d' expects argument of type 'int', but argument 3 has type 'long int' [-Wformat=]
  181 |     DPRINT(("last?%s (%d)\n", file, p - file));
      |             ^~~~~~~~~~~~~~~~        ~~~~~~~~
      |                                       |
      |                                       long int
pkgcdb/debug.h:28:37: note: in definition of macro 'DPRINT'
   28 | #define DPRINT(x) if (debug) printf x
      |                                     ^
In file included from auto-apt.c:35:
pkgcdb/pkgcdb2.c:181:24: note: format string is defined here
  181 |     DPRINT(("last?%s (%d)\n", file, p - file));
      |                       ~^
      |                        |
      |                        int
      |                       %ld
auto-apt.c: In function 'detectdb_lock':
auto-apt.c:170:23: warning: the comparison will always evaluate as 'true' for the address of 'detectdb_lock' will never be NULL [-Waddress]
  170 |     if (detectdb_lock != NULL) {
      |                       ^~
auto-apt.c: In function 'detectdb_unlock':
auto-apt.c:199:23: warning: the comparison will always evaluate as 'true' for the address of 'detectdb_lock' will never be NULL [-Waddress]
  199 |     if (detectdb_lock != NULL) {
      |                       ^~
auto-apt.c: In function 'execve':
auto-apt.c:863:59: warning: passing argument 2 of 'detect_package' discards 'const' qualifier from pointer target type [-Wdiscarded-qualifiers]
  863 |     if (!apt && detectdb_file) { detect_package(filename, __FUNCTION__); }
      |                                                           ^~~~~~~~~~~~
auto-apt.c:215:44: note: expected 'char *' but argument is of type 'const char *'
  215 | detect_package(const char *filename, char *func)
      |                                      ~~~~~~^~~~
auto-apt.c: In function 'execv':
auto-apt.c:898:59: warning: passing argument 2 of 'detect_package' discards 'const' qualifier from pointer target type [-Wdiscarded-qualifiers]
  898 |     if (!apt && detectdb_file) { detect_package(filename, __FUNCTION__); }
      |                                                           ^~~~~~~~~~~~
auto-apt.c:215:44: note: expected 'char *' but argument is of type 'const char *'
  215 | detect_package(const char *filename, char *func)
      |                                      ~~~~~~^~~~
auto-apt.c: In function 'open':
auto-apt.c:939:34: warning: passing argument 2 of 'detect_package' discards 'const' qualifier from pointer target type [-Wdiscarded-qualifiers]
  939 |  o = 1; detect_package(filename, __FUNCTION__); o = 0;
      |                                  ^~~~~~~~~~~~
auto-apt.c:215:44: note: expected 'char *' but argument is of type 'const char *'
  215 | detect_package(const char *filename, char *func)
      |                                      ~~~~~~^~~~
auto-apt.c: In function '__libc_open':
auto-apt.c:985:34: warning: passing argument 2 of 'detect_package' discards 'const' qualifier from pointer target type [-Wdiscarded-qualifiers]
  985 |  o = 1; detect_package(filename, __FUNCTION__); o = 0;
      |                                  ^~~~~~~~~~~~
auto-apt.c:215:44: note: expected 'char *' but argument is of type 'const char *'
  215 | detect_package(const char *filename, char *func)
      |                                      ~~~~~~^~~~
auto-apt.c: In function 'open64':
auto-apt.c:1032:34: warning: passing argument 2 of 'detect_package' discards 'const' qualifier from pointer target type [-Wdiscarded-qualifiers]
 1032 |  o = 1; detect_package(filename, __FUNCTION__); o = 0;
      |                                  ^~~~~~~~~~~~
auto-apt.c:215:44: note: expected 'char *' but argument is of type 'const char *'
  215 | detect_package(const char *filename, char *func)
      |                                      ~~~~~~^~~~
auto-apt.c: In function '__libc_open64':
auto-apt.c:1078:34: warning: passing argument 2 of 'detect_package' discards 'const' qualifier from pointer target type [-Wdiscarded-qualifiers]
 1078 |  o = 1; detect_package(filename, __FUNCTION__); o = 0;
      |                                  ^~~~~~~~~~~~
auto-apt.c:215:44: note: expected 'char *' but argument is of type 'const char *'
  215 | detect_package(const char *filename, char *func)
      |                                      ~~~~~~^~~~
auto-apt.c: In function 'access':
auto-apt.c:1120:57: warning: passing argument 2 of 'detect_package' discards 'const' qualifier from pointer target type [-Wdiscarded-qualifiers]
 1120 |     if (!apt && detectdb_file) detect_package(filename, __FUNCTION__);
      |                                                         ^~~~~~~~~~~~
auto-apt.c:215:44: note: expected 'char *' but argument is of type 'const char *'
  215 | detect_package(const char *filename, char *func)
      |                                      ~~~~~~^~~~
auto-apt.c: In function 'euidaccess':
auto-apt.c:1156:57: warning: passing argument 2 of 'detect_package' discards 'const' qualifier from pointer target type [-Wdiscarded-qualifiers]
 1156 |     if (!apt && detectdb_file) detect_package(filename, __FUNCTION__);
      |                                                         ^~~~~~~~~~~~
auto-apt.c:215:44: note: expected 'char *' but argument is of type 'const char *'
  215 | detect_package(const char *filename, char *func)
      |                                      ~~~~~~^~~~
auto-apt.c: In function '__xstat':
auto-apt.c:1194:57: warning: passing argument 2 of 'detect_package' discards 'const' qualifier from pointer target type [-Wdiscarded-qualifiers]
 1194 |     if (!apt && detectdb_file) detect_package(filename, __FUNCTION__);
      |                                                         ^~~~~~~~~~~~
auto-apt.c:215:44: note: expected 'char *' but argument is of type 'const char *'
  215 | detect_package(const char *filename, char *func)
      |                                      ~~~~~~^~~~
auto-apt.c: In function '__xstat64':
auto-apt.c:1232:57: warning: passing argument 2 of 'detect_package' discards 'const' qualifier from pointer target type [-Wdiscarded-qualifiers]
 1232 |     if (!apt && detectdb_file) detect_package(filename, __FUNCTION__);
      |                                                         ^~~~~~~~~~~~
auto-apt.c:215:44: note: expected 'char *' but argument is of type 'const char *'
  215 | detect_package(const char *filename, char *func)
      |                                      ~~~~~~^~~~
auto-apt.c: In function '__lxstat':
auto-apt.c:1270:57: warning: passing argument 2 of 'detect_package' discards 'const' qualifier from pointer target type [-Wdiscarded-qualifiers]
 1270 |     if (!apt && detectdb_file) detect_package(filename, __FUNCTION__);
      |                                                         ^~~~~~~~~~~~
auto-apt.c:215:44: note: expected 'char *' but argument is of type 'const char *'
  215 | detect_package(const char *filename, char *func)
      |                                      ~~~~~~^~~~
auto-apt.c: In function '__lxstat64':
auto-apt.c:1307:57: warning: passing argument 2 of 'detect_package' discards 'const' qualifier from pointer target type [-Wdiscarded-qualifiers]
 1307 |     if (!apt && detectdb_file) detect_package(filename, __FUNCTION__);
      |                                                         ^~~~~~~~~~~~
auto-apt.c:215:44: note: expected 'char *' but argument is of type 'const char *'
  215 | detect_package(const char *filename, char *func)
      |                                      ~~~~~~^~~~
In file included from auto-apt.c:34:
At top level:
pkgcdb/pathnode.c:291:1: warning: 'pathnode_traverse' defined but not used [-Wunused-function]
  291 | pathnode_traverse(PathNodeTree pnt,
      | ^~~~~~~~~~~~~~~~~
(cd pkgcdb && \
 make pkgcdb2.a CC="gcc" DEFS="-DUSE_DETECT -DDEBUG" CFLAGS="-g -Wall -finline-functions -Ipkgcdb -DUSE_DETECT -DDEBUG")
make[2]: Entering directory '/build/auto-apt/auto-apt-0.3.24/pkgcdb'
gcc -g -Wall -finline-functions -Ipkgcdb -DUSE_DETECT -DDEBUG   -c -o pkgcdb2.o pkgcdb2.c
In file included from pathnode.h:12,
                 from pkgcdb2.h:12,
                 from pkgcdb2.c:10:
pkgcdb2.c: In function 'pkgcdb_get':
pkgcdb2.c:181:13: warning: format '%d' expects argument of type 'int', but argument 3 has type 'long int' [-Wformat=]
  181 |     DPRINT(("last?%s (%d)\n", file, p - file));
      |             ^~~~~~~~~~~~~~~~        ~~~~~~~~
      |                                       |
      |                                       long int
debug.h:28:37: note: in definition of macro 'DPRINT'
   28 | #define DPRINT(x) if (debug) printf x
      |                                     ^
pkgcdb2.c:181:24: note: format string is defined here
  181 |     DPRINT(("last?%s (%d)\n", file, p - file));
      |                       ~^
      |                        |
      |                        int
      |                       %ld
pkgcdb2.c: In function 'pkgcdb_save':
pkgcdb2.c:116:5: warning: 'strncpy' output truncated before terminating nul copying 8 bytes from a string of the same length [-Wstringop-truncation]
  116 |     strncpy(buf, PKGCDB_VERSION_TAG, sizeof(buf));
      |     ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
gcc -g -Wall -finline-functions -Ipkgcdb -DUSE_DETECT -DDEBUG   -c -o pathnode.o pathnode.c
pathnode.c: In function 'pathnode_serialize':
pathnode.c:395:27: warning: cast to pointer from integer of different size [-Wint-to-pointer-cast]
  395 |  if (pn->left) pn->left = (void *)mempool_index(pnt->p_st, pn0->left);
      |                           ^
pathnode.c:396:29: warning: cast to pointer from integer of different size [-Wint-to-pointer-cast]
  396 |  if (pn->right) pn->right = (void *)mempool_index(pnt->p_st, pn0->right);
      |                             ^
pathnode.c:397:27: warning: cast to pointer from integer of different size [-Wint-to-pointer-cast]
  397 |  if (pn->down) pn->down = (void *)mempool_index(pnt->p_st, pn0->down);
      |                           ^
pathnode.c:398:27: warning: cast to pointer from integer of different size [-Wint-to-pointer-cast]
  398 |  if (pn->dups) pn->dups = (void *)mempool_index(pnt->p_st, pn0->dups);
      |                           ^
pathnode.c: In function 'pathnode_unserialize':
pathnode.c:413:45: warning: cast from pointer to integer of different size [-Wpointer-to-int-cast]
  413 |  if (pn->left) pn->left = mempool_fetch(mp, (int)pn->left);
      |                                             ^
pathnode.c:414:47: warning: cast from pointer to integer of different size [-Wpointer-to-int-cast]
  414 |  if (pn->right) pn->right = mempool_fetch(mp, (int)pn->right);
      |                                               ^
pathnode.c:415:45: warning: cast from pointer to integer of different size [-Wpointer-to-int-cast]
  415 |  if (pn->down) pn->down = mempool_fetch(mp, (int)pn->down);
      |                                             ^
pathnode.c:416:45: warning: cast from pointer to integer of different size [-Wpointer-to-int-cast]
  416 |  if (pn->dups) pn->dups = mempool_fetch(mp, (int)pn->dups);
      |                                             ^
gcc -g -Wall -finline-functions -Ipkgcdb -DUSE_DETECT -DDEBUG   -c -o pkgtab.o pkgtab.c
pkgtab.c: In function 'pkg_intern':
pkgtab.c:61:16: warning: implicit declaration of function 'malloc' [-Wimplicit-function-declaration]
   61 |  buf = (char *)malloc(p - pkg + 1);
      |                ^~~~~~
pkgtab.c:61:16: warning: incompatible implicit declaration of built-in function 'malloc'
pkgtab.c:13:1: note: include '<stdlib.h>' or provide a declaration of 'malloc'
   12 | #include <assert.h>
  +++ |+#include <stdlib.h>
   13 | 
pkgtab.c:63:6: warning: implicit declaration of function 'abort' [-Wimplicit-function-declaration]
   63 |      abort();
      |      ^~~~~
pkgtab.c:63:6: warning: incompatible implicit declaration of built-in function 'abort'
pkgtab.c:63:6: note: include '<stdlib.h>' or provide a declaration of 'abort'
pkgtab.c:68:2: warning: implicit declaration of function 'free' [-Wimplicit-function-declaration]
   68 |  free(buf);
      |  ^~~~
pkgtab.c:68:2: warning: incompatible implicit declaration of built-in function 'free'
pkgtab.c:68:2: note: include '<stdlib.h>' or provide a declaration of 'free'
pkgtab.c: In function 'pkg_symbol':
pkgtab.c:97:2: warning: incompatible implicit declaration of built-in function 'free'
   97 |  free(buf);
      |  ^~~~
pkgtab.c:97:2: note: include '<stdlib.h>' or provide a declaration of 'free'
pkgtab.c:99:19: warning: incompatible implicit declaration of built-in function 'malloc'
   99 |     buf = (char *)malloc(len + 1);
      |                   ^~~~~~
pkgtab.c:99:19: note: include '<stdlib.h>' or provide a declaration of 'malloc'
pkgtab.c:101:2: warning: incompatible implicit declaration of built-in function 'abort'
  101 |  abort();
      |  ^~~~~
pkgtab.c:101:2: note: include '<stdlib.h>' or provide a declaration of 'abort'
gcc -g -Wall -finline-functions -Ipkgcdb -DUSE_DETECT -DDEBUG   -c -o strtab.o strtab.c
gcc -g -Wall -finline-functions -Ipkgcdb -DUSE_DETECT -DDEBUG   -c -o mempool.o mempool.c
In file included from mempool.h:13,
                 from mempool.c:10:
mempool.c: In function 'mempool_dump_rec':
mempool.c:187:13: warning: format '%d' expects argument of type 'int', but argument 3 has type 'size_t' {aka 'long unsigned int'} [-Wformat=]
  187 |     DPRINT(("-data: %d * %d => (%d)\n", mp->count, mp->siz, len));
      |             ^~~~~~~~~~~~~~~~~~~~~~~~~~             ~~~~~~~
      |                                                      |
      |                                                      size_t {aka long unsigned int}
debug.h:28:37: note: in definition of macro 'DPRINT'
   28 | #define DPRINT(x) if (debug) printf x
      |                                     ^
mempool.c:187:27: note: format string is defined here
  187 |     DPRINT(("-data: %d * %d => (%d)\n", mp->count, mp->siz, len));
      |                          ~^
      |                           |
      |                           int
      |                          %ld
In file included from mempool.h:13,
                 from mempool.c:10:
mempool.c: In function 'mempool_restore':
mempool.c:246:13: warning: format '%d' expects argument of type 'int', but argument 2 has type 'size_t' {aka 'long unsigned int'} [-Wformat=]
  246 |     DPRINT(("siz %d, ", mp->siz));
      |             ^~~~~~~~~~  ~~~~~~~
      |                           |
      |                           size_t {aka long unsigned int}
debug.h:28:37: note: in definition of macro 'DPRINT'
   28 | #define DPRINT(x) if (debug) printf x
      |                                     ^
mempool.c:246:19: note: format string is defined here
  246 |     DPRINT(("siz %d, ", mp->siz));
      |                  ~^
      |                   |
      |                   int
      |                  %ld
ar rc pkgcdb2.a pkgcdb2.o pathnode.o pkgtab.o strtab.o mempool.o
make[2]: Leaving directory '/build/auto-apt/auto-apt-0.3.24/pkgcdb'
gcc -shared -o auto-apt.so auto-apt.o -lc -ldl 
gcc -g -Wall -finline-functions -Ipkgcdb -DUSE_DETECT -DDEBUG   -c -o auto-apt-pkgcdb.o auto-apt-pkgcdb.c
auto-apt-pkgcdb.c: In function 'get_line':
auto-apt-pkgcdb.c:136:6: warning: pointer targets in assignment from 'char *' to 'unsigned char *' differ in signedness [-Wpointer-sign]
  136 |  buf = new_buf;
      |      ^
auto-apt-pkgcdb.c:137:16: warning: pointer targets in passing argument 1 of 'fgets' differ in signedness [-Wpointer-sign]
  137 |  if (fgets(buf + last, buf_size - last, f) == NULL) {
      |            ~~~~^~~~~~
      |                |
      |                unsigned char *
In file included from pkgcdb/debug.h:12,
                 from auto-apt-pkgcdb.c:8:
/usr/include/stdio.h:564:38: note: expected 'char * restrict' but argument is of type 'unsigned char *'
  564 | extern char *fgets (char *__restrict __s, int __n, FILE *__restrict __stream)
      |                     ~~~~~~~~~~~~~~~~~^~~
auto-apt-pkgcdb.c:141:16: warning: pointer targets in passing argument 1 of 'strlen' differ in signedness [-Wpointer-sign]
  141 |  last = strlen(buf);
      |                ^~~
      |                |
      |                unsigned char *
In file included from auto-apt-pkgcdb.c:16:
/usr/include/string.h:385:35: note: expected 'const char *' but argument is of type 'unsigned char *'
  385 | extern size_t strlen (const char *__s)
      |                       ~~~~~~~~~~~~^~~
auto-apt-pkgcdb.c: In function 'main':
auto-apt-pkgcdb.c:230:21: warning: pointer targets in passing argument 1 of 'strlen' differ in signedness [-Wpointer-sign]
  230 |      if (buf[strlen(buf)-1] == '\n') {
      |                     ^~~
      |                     |
      |                     unsigned char *
In file included from auto-apt-pkgcdb.c:16:
/usr/include/string.h:385:35: note: expected 'const char *' but argument is of type 'unsigned char *'
  385 | extern size_t strlen (const char *__s)
      |                       ~~~~~~~~~~~~^~~
auto-apt-pkgcdb.c:231:14: warning: pointer targets in passing argument 1 of 'strlen' differ in signedness [-Wpointer-sign]
  231 |   buf[strlen(buf)-1] = '\0';
      |              ^~~
      |              |
      |              unsigned char *
In file included from auto-apt-pkgcdb.c:16:
/usr/include/string.h:385:35: note: expected 'const char *' but argument is of type 'unsigned char *'
  385 | extern size_t strlen (const char *__s)
      |                       ~~~~~~~~~~~~^~~
auto-apt-pkgcdb.c:241:18: warning: pointer targets in passing argument 1 of 'strncmp' differ in signedness [-Wpointer-sign]
  241 |      if (strncmp(fname, "./", 2) == 0) {
      |                  ^~~~~
      |                  |
      |                  unsigned char *
In file included from auto-apt-pkgcdb.c:16:
/usr/include/string.h:140:33: note: expected 'const char *' but argument is of type 'unsigned char *'
  140 | extern int strncmp (const char *__s1, const char *__s2, size_t __n)
      |                     ~~~~~~~~~~~~^~~~
auto-apt-pkgcdb.c:245:32: warning: pointer targets in passing argument 1 of 'strlen' differ in signedness [-Wpointer-sign]
  245 |      for (pkg = fname + strlen(fname)-1;
      |                                ^~~~~
      |                                |
      |                                unsigned char *
In file included from auto-apt-pkgcdb.c:16:
/usr/include/string.h:385:35: note: expected 'const char *' but argument is of type 'unsigned char *'
  385 | extern size_t strlen (const char *__s)
      |                       ~~~~~~~~~~~~^~~
auto-apt-pkgcdb.c:278:19: warning: pointer targets in passing argument 2 of 'pkgcdb_put' differ in signedness [-Wpointer-sign]
  278 |   pkgcdb_put(pnt, fname, pkg, &nent);
      |                   ^~~~~
      |                   |
      |                   unsigned char *
In file included from auto-apt-pkgcdb.c:13:
pkgcdb/pkgcdb2.h:44:12: note: expected 'char *' but argument is of type 'unsigned char *'
   44 |      char *file, char *pkg,
      |      ~~~~~~^~~~
auto-apt-pkgcdb.c:278:26: warning: pointer targets in passing argument 3 of 'pkgcdb_put' differ in signedness [-Wpointer-sign]
  278 |   pkgcdb_put(pnt, fname, pkg, &nent);
      |                          ^~~
      |                          |
      |                          unsigned char *
In file included from auto-apt-pkgcdb.c:13:
pkgcdb/pkgcdb2.h:44:24: note: expected 'char *' but argument is of type 'unsigned char *'
   44 |      char *file, char *pkg,
      |                  ~~~~~~^~~
auto-apt-pkgcdb.c:287:19: warning: pointer targets in passing argument 2 of 'pkgcdb_del' differ in signedness [-Wpointer-sign]
  287 |   pkgcdb_del(pnt, fname, pkg, &nent);
      |                   ^~~~~
      |                   |
      |                   unsigned char *
In file included from auto-apt-pkgcdb.c:13:
pkgcdb/pkgcdb2.h:49:13: note: expected 'char *' but argument is of type 'unsigned char *'
   49 |       char *file, char *pkg, int *nent);
      |       ~~~~~~^~~~
auto-apt-pkgcdb.c:287:26: warning: pointer targets in passing argument 3 of 'pkgcdb_del' differ in signedness [-Wpointer-sign]
  287 |   pkgcdb_del(pnt, fname, pkg, &nent);
      |                          ^~~
      |                          |
      |                          unsigned char *
In file included from auto-apt-pkgcdb.c:13:
pkgcdb/pkgcdb2.h:49:25: note: expected 'char *' but argument is of type 'unsigned char *'
   49 |       char *file, char *pkg, int *nent);
      |                   ~~~~~~^~~
auto-apt-pkgcdb.c:305:25: warning: pointer targets in passing argument 1 of 'strdup' differ in signedness [-Wpointer-sign]
  305 |       max_file = strdup(fname);
      |                         ^~~~~
      |                         |
      |                         unsigned char *
In file included from auto-apt-pkgcdb.c:16:
/usr/include/string.h:167:34: note: expected 'const char *' but argument is of type 'unsigned char *'
  167 | extern char *strdup (const char *__s)
      |                      ~~~~~~~~~~~~^~~
gcc -g -Wall -finline-functions -Ipkgcdb -DUSE_DETECT -DDEBUG -o auto-apt-pkgcdb auto-apt-pkgcdb.o pkgcdb/pkgcdb2.a
make[1]: Leaving directory '/build/auto-apt/auto-apt-0.3.24'
   dh_auto_test
dh_auto_test: warning: Compatibility levels before 10 are deprecated (level 7 in use)
 debian/rules binary
dh binary
dh: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_testroot
   dh_prep
   dh_installdirs
dh_installdirs: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_auto_install
dh_auto_install: warning: Compatibility levels before 10 are deprecated (level 7 in use)
	make -j1 install DESTDIR=/build/auto-apt/auto-apt-0.3.24/debian/auto-apt AM_UPDATE_INFO_DIR=no
make[1]: Entering directory '/build/auto-apt/auto-apt-0.3.24'
install -m 755 auto-apt.sh /build/auto-apt/auto-apt-0.3.24/debian/auto-apt/usr/bin/auto-apt
install -m 755 auto-apt-pkgcdb /build/auto-apt/auto-apt-0.3.24/debian/auto-apt/usr/lib/auto-apt/
install -m 755 auto-apt-installer.pl /build/auto-apt/auto-apt-0.3.24/debian/auto-apt/usr/lib/auto-apt/auto-apt-installer
install -m 644 auto-apt.so /build/auto-apt/auto-apt-0.3.24/debian/auto-apt/lib/
install -m 644 paths.list /build/auto-apt/auto-apt-0.3.24/debian/auto-apt/etc/auto-apt/
make[1]: Leaving directory '/build/auto-apt/auto-apt-0.3.24'
   dh_installdocs
dh_installdocs: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_installchangelogs
   dh_installexamples
dh_installexamples: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_installman
dh_installman: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
dh_compress: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_fixperms
   dh_missing
dh_missing: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_strip
dh_strip: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_makeshlibs
dh_makeshlibs: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_shlibdeps
dh_shlibdeps: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_installdeb
dh_installdeb: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_gencontrol
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'auto-apt' in '../auto-apt_0.3.24_amd64.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../auto-apt_0.3.24_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Mon Apr 17 13:02:52 UTC 2023
+ cd ..
+ ls -la
total 148
drwxr-xr-x 3 root root  4096 Apr 17 13:02 .
drwxr-xr-x 3 root root  4096 Apr 17 13:02 ..
drwxr-xr-x 5 root root  4096 Apr 17 13:02 auto-apt-0.3.24
-rw-r--r-- 1 root root   546 Apr 17 13:02 auto-apt_0.3.24.dsc
-rw-r--r-- 1 root root 39956 Apr 17 13:02 auto-apt_0.3.24.tar.xz
-rw-r--r-- 1 root root  5255 Apr 17 13:02 auto-apt_0.3.24_amd64.buildinfo
-rw-r--r-- 1 root root  1846 Apr 17 13:02 auto-apt_0.3.24_amd64.changes
-rw-r--r-- 1 root root 45332 Apr 17 13:02 auto-apt_0.3.24_amd64.deb
-rw-r--r-- 1 root root 30205 Apr 17 13:02 buildlog.txt
+ find . -maxdepth 1 -type f
+ sha256sum ./buildlog.txt ./auto-apt_0.3.24_amd64.deb ./auto-apt_0.3.24.tar.xz ./auto-apt_0.3.24.dsc ./auto-apt_0.3.24_amd64.buildinfo ./auto-apt_0.3.24_amd64.changes
a3bdb49eca4521a9fc4ad8ccd3392f34a75bccb1cdefa58e915b1add083f8b1f  ./buildlog.txt
add7243538a4368e1eb1bbee90e2ee5650a743538ac3343fc57d21bc3af81e24  ./auto-apt_0.3.24_amd64.deb
e24db92fb9266badd9e5259325542cd45e4cd9004e725375a771df3adaf7da7f  ./auto-apt_0.3.24.tar.xz
d10d28e3f764b6151489daad89ebdf98ac2c9a5b68495b6f5dbb18a442682765  ./auto-apt_0.3.24.dsc
83ce196487c5d396596d7745ead2c97340842d718fc11eb1ccf7cc6c581e3286  ./auto-apt_0.3.24_amd64.buildinfo
52e7478cb7baf6f0a2abb9fcef7edefeec50d1accd4d2d26d092e4b49c988568  ./auto-apt_0.3.24_amd64.changes
+ mkdir published
+ cd published
+ cd ../
+ ls auto-apt_0.3.24_amd64.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget -q http://repo.pureos.net/pureos/pool/main/a/auto-apt/auto-apt_0.3.24_amd64.deb
+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./auto-apt_0.3.24_amd64.deb
394e0aaa1d0528047a95f3ad2b369d5a9a2066ce046a2ba59c03c940db147f3f  ./auto-apt_0.3.24_amd64.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./auto-apt_0.3.24_amd64.deb: FAILED
sha256sum: WARNING: 1 computed checksum did NOT match
