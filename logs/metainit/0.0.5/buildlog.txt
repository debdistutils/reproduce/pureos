+ date
Wed Apr 19 07:57:21 UTC 2023
+ apt-get source --only-source metainit=0.0.5
Reading package lists...
NOTICE: 'metainit' packaging is maintained in the 'Darcs' version control system at:
http://darcs.nomeata.de/metainit
Need to get 0 B of source archives.
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source metainit=0.0.5
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name metainit* -type d
+ cd
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: error: cannot open file debian/changelog: No such file or directory
