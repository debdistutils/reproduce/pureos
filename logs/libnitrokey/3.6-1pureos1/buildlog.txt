+ date
Tue Apr 18 18:24:34 UTC 2023
+ apt-get source --only-source libnitrokey=3.6-1pureos1
Reading package lists...
NOTICE: 'libnitrokey' packaging is maintained in the 'Git' version control system at:
https://source.puri.sm/pureos/packages/libnitrokey.git
Please use:
git clone https://source.puri.sm/pureos/packages/libnitrokey.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 492 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main libnitrokey 3.6-1pureos1 (dsc) [2038 B]
Get:2 http://repo.pureos.net/pureos byzantium/main libnitrokey 3.6-1pureos1 (tar) [479 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main libnitrokey 3.6-1pureos1 (diff) [10.6 kB]
dpkg-source: info: extracting libnitrokey in libnitrokey-3.6
dpkg-source: info: unpacking libnitrokey_3.6.orig.tar.xz
dpkg-source: info: unpacking libnitrokey_3.6-1pureos1.debian.tar.xz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: applying 001-librem-key-updated-udev-rules.patch
Fetched 492 kB in 0s (1794 kB/s)
W: Download is performed unsandboxed as root as file 'libnitrokey_3.6-1pureos1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source libnitrokey=3.6-1pureos1
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  cmake cmake-data libhidapi-dev libhidapi-hidraw0 libhidapi-libusb0
  libjsoncpp24 librhash0 libusb-1.0-0-dev pkg-kde-tools
0 upgraded, 9 newly installed, 0 to remove and 0 not upgraded.
Need to get 7756 kB of archives.
After this operation, 34.0 MB of additional disk space will be used.
Get:1 http://repo.pureos.net/pureos byzantium/main amd64 cmake-data all 3.18.4-2+deb11u1 [1725 kB]
Get:2 http://repo.pureos.net/pureos byzantium/main amd64 libjsoncpp24 amd64 1.9.4-4 [78.9 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main amd64 librhash0 amd64 1.4.1-2 [129 kB]
Get:4 http://repo.pureos.net/pureos byzantium/main amd64 cmake amd64 3.18.4-2+deb11u1 [5593 kB]
Get:5 http://repo.pureos.net/pureos byzantium/main amd64 libhidapi-libusb0 amd64 0.10.1+dfsg-1 [14.8 kB]
Get:6 http://repo.pureos.net/pureos byzantium/main amd64 libhidapi-hidraw0 amd64 0.10.1+dfsg-1 [12.3 kB]
Get:7 http://repo.pureos.net/pureos byzantium/main amd64 libhidapi-dev amd64 0.10.1+dfsg-1 [26.6 kB]
Get:8 http://repo.pureos.net/pureos byzantium/main amd64 libusb-1.0-0-dev amd64 2:1.0.24-3 [80.4 kB]
Get:9 http://repo.pureos.net/pureos byzantium/main amd64 pkg-kde-tools all 0.15.32 [96.7 kB]
Fetched 7756 kB in 1s (13.0 MB/s)
Selecting previously unselected package cmake-data.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 102989 files and directories currently installed.)
Preparing to unpack .../0-cmake-data_3.18.4-2+deb11u1_all.deb ...
Unpacking cmake-data (3.18.4-2+deb11u1) ...
Selecting previously unselected package libjsoncpp24:amd64.
Preparing to unpack .../1-libjsoncpp24_1.9.4-4_amd64.deb ...
Unpacking libjsoncpp24:amd64 (1.9.4-4) ...
Selecting previously unselected package librhash0:amd64.
Preparing to unpack .../2-librhash0_1.4.1-2_amd64.deb ...
Unpacking librhash0:amd64 (1.4.1-2) ...
Selecting previously unselected package cmake.
Preparing to unpack .../3-cmake_3.18.4-2+deb11u1_amd64.deb ...
Unpacking cmake (3.18.4-2+deb11u1) ...
Selecting previously unselected package libhidapi-libusb0:amd64.
Preparing to unpack .../4-libhidapi-libusb0_0.10.1+dfsg-1_amd64.deb ...
Unpacking libhidapi-libusb0:amd64 (0.10.1+dfsg-1) ...
Selecting previously unselected package libhidapi-hidraw0:amd64.
Preparing to unpack .../5-libhidapi-hidraw0_0.10.1+dfsg-1_amd64.deb ...
Unpacking libhidapi-hidraw0:amd64 (0.10.1+dfsg-1) ...
Selecting previously unselected package libhidapi-dev:amd64.
Preparing to unpack .../6-libhidapi-dev_0.10.1+dfsg-1_amd64.deb ...
Unpacking libhidapi-dev:amd64 (0.10.1+dfsg-1) ...
Selecting previously unselected package libusb-1.0-0-dev:amd64.
Preparing to unpack .../7-libusb-1.0-0-dev_2%3a1.0.24-3_amd64.deb ...
Unpacking libusb-1.0-0-dev:amd64 (2:1.0.24-3) ...
Selecting previously unselected package pkg-kde-tools.
Preparing to unpack .../8-pkg-kde-tools_0.15.32_all.deb ...
Unpacking pkg-kde-tools (0.15.32) ...
Setting up libusb-1.0-0-dev:amd64 (2:1.0.24-3) ...
Setting up libhidapi-hidraw0:amd64 (0.10.1+dfsg-1) ...
Setting up libhidapi-libusb0:amd64 (0.10.1+dfsg-1) ...
Setting up pkg-kde-tools (0.15.32) ...
Setting up libjsoncpp24:amd64 (1.9.4-4) ...
Setting up librhash0:amd64 (1.4.1-2) ...
Setting up cmake-data (3.18.4-2+deb11u1) ...
Setting up libhidapi-dev:amd64 (0.10.1+dfsg-1) ...
Setting up cmake (3.18.4-2+deb11u1) ...
Processing triggers for man-db (2.9.4-2) ...
Processing triggers for libc-bin (2.31-13+deb11u5) ...
+ find . -maxdepth 1 -name libnitrokey* -type d
+ cd ./libnitrokey-3.6
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package libnitrokey
dpkg-buildpackage: info: source version 3.6-1pureos1
dpkg-buildpackage: info: source distribution byzantium
dpkg-buildpackage: info: source changed by Jeremiah C. Foster <jeremiah.foster@puri.sm>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --with pkgkde_symbolshelper
   dh_auto_clean
   dh_clean
	rm -f debian/debhelper-build-stamp
	rm -rf debian/.debhelper/
	rm -f -- debian/libnitrokey-dev.substvars debian/libnitrokey3.substvars debian/libnitrokey-common.substvars debian/files
	rm -fr -- debian/libnitrokey-dev/ debian/tmp/ debian/libnitrokey3/ debian/libnitrokey-common/
	find .  \( \( \
		\( -path .\*/.git -o -path .\*/.svn -o -path .\*/.bzr -o -path .\*/.hg -o -path .\*/CVS -o -path .\*/.pc -o -path .\*/_darcs \) -prune -o -type f -a \
	        \( -name '#*#' -o -name '.*~' -o -name '*~' -o -name DEADJOE \
		 -o -name '*.orig' -o -name '*.rej' -o -name '*.bak' \
		 -o -name '.*.orig' -o -name .*.rej -o -name '.SUMS' \
		 -o -name TAGS -o \( -path '*/.deps/*' -a -name '*.P' \) \
		\) -exec rm -f {} + \) -o \
		\( -type d -a -name autom4te.cache -prune -exec rm -rf {} + \) \)
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building libnitrokey using existing ./libnitrokey_3.6.orig.tar.xz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: building libnitrokey in libnitrokey_3.6-1pureos1.debian.tar.xz
dpkg-source: info: building libnitrokey in libnitrokey_3.6-1pureos1.dsc
 debian/rules build
dh build --with pkgkde_symbolshelper
   dh_update_autotools_config
   dh_autoreconf
   dh_auto_configure
	install -d obj-x86_64-linux-gnu
	cd obj-x86_64-linux-gnu && cmake -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=None -DCMAKE_INSTALL_SYSCONFDIR=/etc -DCMAKE_INSTALL_LOCALSTATEDIR=/var -DCMAKE_EXPORT_NO_PACKAGE_REGISTRY=ON -DCMAKE_FIND_PACKAGE_NO_PACKAGE_REGISTRY=ON -DCMAKE_INSTALL_RUNSTATEDIR=/run "-GUnix Makefiles" -DCMAKE_VERBOSE_MAKEFILE=ON -DCMAKE_INSTALL_LIBDIR=lib/x86_64-linux-gnu ..
-- The C compiler identification is GNU 10.2.1
-- The CXX compiler identification is GNU 10.2.1
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/bin/cc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/c++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
libnitrokey: Build type: None
-- Found PkgConfig: /usr/bin/pkg-config (found version "0.29.2") 
-- Checking for one of the modules 'hidapi-libusb'
-- Setting fallback Git library version
-- Setting Git library version to: v3.6.0
-- Setting udev rules dir to lib/udev/rules.d
-- Could NOT find Doxygen (missing: DOXYGEN_EXECUTABLE) 
-- Configuring done
-- Generating done
CMake Warning:
  Manually-specified variables were not used by the project:

    CMAKE_EXPORT_NO_PACKAGE_REGISTRY


-- Build files have been written to: /build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu
   dh_auto_build
	cd obj-x86_64-linux-gnu && make -j50 "INSTALL=install --strip-program=true" VERBOSE=1
make[1]: Entering directory '/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu'
/usr/bin/cmake -S/build/libnitrokey/libnitrokey-3.6 -B/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu --check-build-system CMakeFiles/Makefile.cmake 0
/usr/bin/cmake -E cmake_progress_start /build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu/CMakeFiles /build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu//CMakeFiles/progress.marks
make  -f CMakeFiles/Makefile2 all
make[2]: Entering directory '/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu'
make  -f CMakeFiles/nitrokey.dir/build.make CMakeFiles/nitrokey.dir/depend
make[3]: Entering directory '/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu'
cd /build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /build/libnitrokey/libnitrokey-3.6 /build/libnitrokey/libnitrokey-3.6 /build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu /build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu /build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu/CMakeFiles/nitrokey.dir/DependInfo.cmake --color=
Dependee "/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu/CMakeFiles/nitrokey.dir/DependInfo.cmake" is newer than depender "/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu/CMakeFiles/nitrokey.dir/depend.internal".
Dependee "/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu/CMakeFiles/CMakeDirectoryInformation.cmake" is newer than depender "/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu/CMakeFiles/nitrokey.dir/depend.internal".
Scanning dependencies of target nitrokey
make[3]: Leaving directory '/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu'
make  -f CMakeFiles/nitrokey.dir/build.make CMakeFiles/nitrokey.dir/build
make[3]: Entering directory '/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu'
[ 22%] Building CXX object CMakeFiles/nitrokey.dir/command_id.cc.o
[ 22%] Building CXX object CMakeFiles/nitrokey.dir/misc.cc.o
/usr/bin/c++ -Dnitrokey_EXPORTS -I/build/libnitrokey/libnitrokey-3.6/hidapi -I/build/libnitrokey/libnitrokey-3.6/libnitrokey -g -O2 -ffile-prefix-map=/build/libnitrokey/libnitrokey-3.6=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -fPIC -Wall -Wno-unused-function -Wcast-qual -Woverloaded-virtual -Wsign-compare -Wformat -Wformat-security -I/usr/include/hidapi -std=c++14 -o CMakeFiles/nitrokey.dir/command_id.cc.o -c /build/libnitrokey/libnitrokey-3.6/command_id.cc
[ 33%] Building CXX object CMakeFiles/nitrokey.dir/DeviceCommunicationExceptions.cpp.o
/usr/bin/c++ -Dnitrokey_EXPORTS -I/build/libnitrokey/libnitrokey-3.6/hidapi -I/build/libnitrokey/libnitrokey-3.6/libnitrokey -g -O2 -ffile-prefix-map=/build/libnitrokey/libnitrokey-3.6=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -fPIC -Wall -Wno-unused-function -Wcast-qual -Woverloaded-virtual -Wsign-compare -Wformat -Wformat-security -I/usr/include/hidapi -std=c++14 -o CMakeFiles/nitrokey.dir/misc.cc.o -c /build/libnitrokey/libnitrokey-3.6/misc.cc
[ 44%] Building CXX object CMakeFiles/nitrokey.dir/version.cc.o
/usr/bin/c++ -Dnitrokey_EXPORTS -I/build/libnitrokey/libnitrokey-3.6/hidapi -I/build/libnitrokey/libnitrokey-3.6/libnitrokey -g -O2 -ffile-prefix-map=/build/libnitrokey/libnitrokey-3.6=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -fPIC -Wall -Wno-unused-function -Wcast-qual -Woverloaded-virtual -Wsign-compare -Wformat -Wformat-security -I/usr/include/hidapi -std=c++14 -o CMakeFiles/nitrokey.dir/DeviceCommunicationExceptions.cpp.o -c /build/libnitrokey/libnitrokey-3.6/DeviceCommunicationExceptions.cpp
/usr/bin/c++ -Dnitrokey_EXPORTS -I/build/libnitrokey/libnitrokey-3.6/hidapi -I/build/libnitrokey/libnitrokey-3.6/libnitrokey -g -O2 -ffile-prefix-map=/build/libnitrokey/libnitrokey-3.6=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -fPIC -Wall -Wno-unused-function -Wcast-qual -Woverloaded-virtual -Wsign-compare -Wformat -Wformat-security -I/usr/include/hidapi -std=c++14 -o CMakeFiles/nitrokey.dir/version.cc.o -c /build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu/version.cc
[ 55%] Building CXX object CMakeFiles/nitrokey.dir/device.cc.o
[ 66%] Building CXX object CMakeFiles/nitrokey.dir/log.cc.o
/usr/bin/c++ -Dnitrokey_EXPORTS -I/build/libnitrokey/libnitrokey-3.6/hidapi -I/build/libnitrokey/libnitrokey-3.6/libnitrokey -g -O2 -ffile-prefix-map=/build/libnitrokey/libnitrokey-3.6=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -fPIC -Wall -Wno-unused-function -Wcast-qual -Woverloaded-virtual -Wsign-compare -Wformat -Wformat-security -I/usr/include/hidapi -std=c++14 -o CMakeFiles/nitrokey.dir/device.cc.o -c /build/libnitrokey/libnitrokey-3.6/device.cc
[ 77%] Building CXX object CMakeFiles/nitrokey.dir/NitrokeyManager.cc.o
/usr/bin/c++ -Dnitrokey_EXPORTS -I/build/libnitrokey/libnitrokey-3.6/hidapi -I/build/libnitrokey/libnitrokey-3.6/libnitrokey -g -O2 -ffile-prefix-map=/build/libnitrokey/libnitrokey-3.6=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -fPIC -Wall -Wno-unused-function -Wcast-qual -Woverloaded-virtual -Wsign-compare -Wformat -Wformat-security -I/usr/include/hidapi -std=c++14 -o CMakeFiles/nitrokey.dir/log.cc.o -c /build/libnitrokey/libnitrokey-3.6/log.cc
[ 88%] Building CXX object CMakeFiles/nitrokey.dir/NK_C_API.cc.o
/usr/bin/c++ -Dnitrokey_EXPORTS -I/build/libnitrokey/libnitrokey-3.6/hidapi -I/build/libnitrokey/libnitrokey-3.6/libnitrokey -g -O2 -ffile-prefix-map=/build/libnitrokey/libnitrokey-3.6=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -fPIC -Wall -Wno-unused-function -Wcast-qual -Woverloaded-virtual -Wsign-compare -Wformat -Wformat-security -I/usr/include/hidapi -std=c++14 -o CMakeFiles/nitrokey.dir/NitrokeyManager.cc.o -c /build/libnitrokey/libnitrokey-3.6/NitrokeyManager.cc
/usr/bin/c++ -Dnitrokey_EXPORTS -I/build/libnitrokey/libnitrokey-3.6/hidapi -I/build/libnitrokey/libnitrokey-3.6/libnitrokey -g -O2 -ffile-prefix-map=/build/libnitrokey/libnitrokey-3.6=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -fPIC -Wall -Wno-unused-function -Wcast-qual -Woverloaded-virtual -Wsign-compare -Wformat -Wformat-security -I/usr/include/hidapi -std=c++14 -o CMakeFiles/nitrokey.dir/NK_C_API.cc.o -c /build/libnitrokey/libnitrokey-3.6/NK_C_API.cc
[100%] Linking CXX shared library libnitrokey.so
/usr/bin/cmake -E cmake_link_script CMakeFiles/nitrokey.dir/link.txt --verbose=1
/usr/bin/c++ -fPIC -g -O2 -ffile-prefix-map=/build/libnitrokey/libnitrokey-3.6=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -Wl,-z,relro -Wl,-z,now -shared -Wl,-soname,libnitrokey.so.3 -o libnitrokey.so.3.6.0 CMakeFiles/nitrokey.dir/command_id.cc.o CMakeFiles/nitrokey.dir/device.cc.o CMakeFiles/nitrokey.dir/log.cc.o CMakeFiles/nitrokey.dir/misc.cc.o CMakeFiles/nitrokey.dir/NitrokeyManager.cc.o CMakeFiles/nitrokey.dir/NK_C_API.cc.o CMakeFiles/nitrokey.dir/DeviceCommunicationExceptions.cpp.o CMakeFiles/nitrokey.dir/version.cc.o  -lhidapi-libusb 
/usr/bin/cmake -E cmake_symlink_library libnitrokey.so.3.6.0 libnitrokey.so.3 libnitrokey.so
make[3]: Leaving directory '/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu'
[100%] Built target nitrokey
make[2]: Leaving directory '/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu'
/usr/bin/cmake -E cmake_progress_start /build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu/CMakeFiles 0
make[1]: Leaving directory '/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu'
   dh_auto_test
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary --with pkgkde_symbolshelper
   dh_testroot
   dh_prep
	rm -f -- debian/libnitrokey-dev.substvars debian/libnitrokey3.substvars debian/libnitrokey-common.substvars
	rm -fr -- debian/.debhelper/generated/libnitrokey-dev/ debian/libnitrokey-dev/ debian/tmp/ debian/.debhelper/generated/libnitrokey3/ debian/libnitrokey3/ debian/.debhelper/generated/libnitrokey-common/ debian/libnitrokey-common/
   dh_auto_install
	install -d /build/libnitrokey/libnitrokey-3.6/debian/tmp
	cd obj-x86_64-linux-gnu && make -j50 install DESTDIR=/build/libnitrokey/libnitrokey-3.6/debian/tmp AM_UPDATE_INFO_DIR=no "INSTALL=install --strip-program=true"
make[1]: Entering directory '/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu'
/usr/bin/cmake -S/build/libnitrokey/libnitrokey-3.6 -B/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu --check-build-system CMakeFiles/Makefile.cmake 0
/usr/bin/cmake -E cmake_progress_start /build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu/CMakeFiles /build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu//CMakeFiles/progress.marks
make  -f CMakeFiles/Makefile2 all
make[2]: Entering directory '/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu'
make  -f CMakeFiles/nitrokey.dir/build.make CMakeFiles/nitrokey.dir/depend
make[3]: Entering directory '/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu'
cd /build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /build/libnitrokey/libnitrokey-3.6 /build/libnitrokey/libnitrokey-3.6 /build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu /build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu /build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu/CMakeFiles/nitrokey.dir/DependInfo.cmake --color=
make[3]: Leaving directory '/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu'
make  -f CMakeFiles/nitrokey.dir/build.make CMakeFiles/nitrokey.dir/build
make[3]: Entering directory '/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu'
make[3]: Nothing to be done for 'CMakeFiles/nitrokey.dir/build'.
make[3]: Leaving directory '/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu'
[100%] Built target nitrokey
make[2]: Leaving directory '/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu'
/usr/bin/cmake -E cmake_progress_start /build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu/CMakeFiles 0
make  -f CMakeFiles/Makefile2 preinstall
make[2]: Entering directory '/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu'
make[2]: Nothing to be done for 'preinstall'.
make[2]: Leaving directory '/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu'
Install the project...
/usr/bin/cmake -P cmake_install.cmake
-- Install configuration: "None"
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/include/libnitrokey/NK_C_API.h
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/include/libnitrokey/CommandFailedException.h
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/include/libnitrokey/DeviceCommunicationExceptions.h
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/include/libnitrokey/LibraryException.h
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/include/libnitrokey/LongOperationInProgressException.h
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/include/libnitrokey/NitrokeyManager.h
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/include/libnitrokey/command.h
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/include/libnitrokey/command_id.h
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/include/libnitrokey/cxx_semantics.h
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/include/libnitrokey/deprecated.h
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/include/libnitrokey/device.h
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/include/libnitrokey/device_proto.h
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/include/libnitrokey/dissect.h
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/include/libnitrokey/log.h
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/include/libnitrokey/misc.h
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/include/libnitrokey/stick10_commands.h
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/include/libnitrokey/stick10_commands_0.8.h
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/include/libnitrokey/stick20_commands.h
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/include/libnitrokey/version.h
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/lib/x86_64-linux-gnu/libnitrokey.so.3.6.0
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/lib/x86_64-linux-gnu/libnitrokey.so.3
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/lib/x86_64-linux-gnu/libnitrokey.so
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/lib/udev/rules.d/41-nitrokey.rules
-- Installing: /build/libnitrokey/libnitrokey-3.6/debian/tmp/usr/lib/x86_64-linux-gnu/pkgconfig/libnitrokey-1.pc
make[1]: Leaving directory '/build/libnitrokey/libnitrokey-3.6/obj-x86_64-linux-gnu'
   dh_install
	install -d debian/libnitrokey-dev//usr/include
	cp --reflink=auto -a debian/tmp/usr/include/libnitrokey debian/libnitrokey-dev//usr/include/
	install -d debian/libnitrokey-dev//usr/lib/x86_64-linux-gnu
	cp --reflink=auto -a debian/tmp/usr/lib/x86_64-linux-gnu/libnitrokey.so debian/libnitrokey-dev//usr/lib/x86_64-linux-gnu/
	install -d debian/libnitrokey-dev//usr/lib/x86_64-linux-gnu/pkgconfig
	cp --reflink=auto -a debian/tmp/usr/lib/x86_64-linux-gnu/pkgconfig/libnitrokey-1.pc debian/libnitrokey-dev//usr/lib/x86_64-linux-gnu/pkgconfig/
	install -d debian/.debhelper/generated/libnitrokey-dev
	install -d debian/libnitrokey3//usr/lib/x86_64-linux-gnu
	cp --reflink=auto -a debian/tmp/usr/lib/x86_64-linux-gnu/libnitrokey.so.3 debian/tmp/usr/lib/x86_64-linux-gnu/libnitrokey.so.3.6.0 debian/libnitrokey3//usr/lib/x86_64-linux-gnu/
	install -d debian/.debhelper/generated/libnitrokey3
	install -d debian/libnitrokey-common//usr/lib/udev/rules.d
	cp --reflink=auto -a debian/tmp/usr/lib/udev/rules.d/41-nitrokey.rules debian/libnitrokey-common//usr/lib/udev/rules.d/
	install -d debian/libnitrokey-common/usr/lib/udev/rules.d/60-librem-key.rules
	cp --reflink=auto -a ./data/60-librem-key.rules debian/libnitrokey-common/usr/lib/udev/rules.d/60-librem-key.rules/
	install -d debian/.debhelper/generated/libnitrokey-common
   dh_installdocs
	install -d debian/libnitrokey-dev/usr/share/doc/libnitrokey-dev
	cp --reflink=auto -a ./README.md debian/libnitrokey-dev/usr/share/doc/libnitrokey-dev
	cp --reflink=auto -a ./TODO debian/libnitrokey-dev/usr/share/doc/libnitrokey-dev
	cp --reflink=auto -a ./python_bindings_example.py debian/libnitrokey-dev/usr/share/doc/libnitrokey-dev
	chown -R 0:0 debian/libnitrokey-dev/usr/share/doc
	chmod -R u\+rw,go=rX debian/libnitrokey-dev/usr/share/doc
	install -p -m0644 debian/copyright debian/libnitrokey-dev/usr/share/doc/libnitrokey-dev/copyright
	install -d debian/libnitrokey3/usr/share/doc/libnitrokey3
	install -p -m0644 debian/copyright debian/libnitrokey3/usr/share/doc/libnitrokey3/copyright
	install -d debian/libnitrokey-common/usr/share/doc/libnitrokey-common
	install -p -m0644 debian/copyright debian/libnitrokey-common/usr/share/doc/libnitrokey-common/copyright
   dh_installchangelogs
	install -p -m0644 debian/changelog debian/libnitrokey-dev/usr/share/doc/libnitrokey-dev/changelog.Debian
	install -p -m0644 ./CHANGELOG.md debian/libnitrokey-dev/usr/share/doc/libnitrokey-dev/changelog
	install -p -m0644 debian/changelog debian/libnitrokey3/usr/share/doc/libnitrokey3/changelog.Debian
	install -p -m0644 ./CHANGELOG.md debian/libnitrokey3/usr/share/doc/libnitrokey3/changelog
	install -p -m0644 debian/changelog debian/libnitrokey-common/usr/share/doc/libnitrokey-common/changelog.Debian
	install -p -m0644 ./CHANGELOG.md debian/libnitrokey-common/usr/share/doc/libnitrokey-common/changelog
   dh_lintian
	install -d debian/libnitrokey-common/usr/share/lintian/overrides
	install -p -m0644 debian/libnitrokey-common.lintian-overrides debian/libnitrokey-common/usr/share/lintian/overrides/libnitrokey-common
   dh_perl
   dh_link
	rm -f debian/libnitrokey-dev/usr/lib/x86_64-linux-gnu/libnitrokey.so
	ln -s libnitrokey.so.3 debian/libnitrokey-dev/usr/lib/x86_64-linux-gnu/libnitrokey.so
	rm -f debian/libnitrokey3/usr/lib/x86_64-linux-gnu/libnitrokey.so.3
	ln -s libnitrokey.so.3.6.0 debian/libnitrokey3/usr/lib/x86_64-linux-gnu/libnitrokey.so.3
   dh_strip_nondeterminism
   dh_compress
	cd debian/libnitrokey-dev
	cd debian/libnitrokey3
	cd debian/libnitrokey-common
	chmod a-x usr/share/doc/libnitrokey-dev/README.md usr/share/doc/libnitrokey-dev/changelog usr/share/doc/libnitrokey-dev/changelog.Debian usr/share/doc/libnitrokey-dev/python_bindings_example.py
	chmod a-x usr/share/doc/libnitrokey3/changelog usr/share/doc/libnitrokey3/changelog.Debian
	chmod a-x usr/share/doc/libnitrokey-common/changelog usr/share/doc/libnitrokey-common/changelog.Debian
	gzip -9nf usr/share/doc/libnitrokey-dev/README.md usr/share/doc/libnitrokey-dev/changelog usr/share/doc/libnitrokey-dev/changelog.Debian usr/share/doc/libnitrokey-dev/python_bindings_example.py
	gzip -9nf usr/share/doc/libnitrokey3/changelog usr/share/doc/libnitrokey3/changelog.Debian
	gzip -9nf usr/share/doc/libnitrokey-common/changelog usr/share/doc/libnitrokey-common/changelog.Debian
	cd '/build/libnitrokey/libnitrokey-3.6'
	cd '/build/libnitrokey/libnitrokey-3.6'
	cd '/build/libnitrokey/libnitrokey-3.6'
   dh_fixperms
	find debian/libnitrokey-dev -true -print0 2>/dev/null | xargs -0r chown --no-dereference 0:0
	find debian/libnitrokey-common -true -print0 2>/dev/null | xargs -0r chown --no-dereference 0:0
	find debian/libnitrokey3 -true -print0 2>/dev/null | xargs -0r chown --no-dereference 0:0
	find debian/libnitrokey-common ! -type l -a -true -a -true -print0 2>/dev/null | xargs -0r chmod go=rX,u+rw,a-s
	find debian/libnitrokey3 ! -type l -a -true -a -true -print0 2>/dev/null | xargs -0r chmod go=rX,u+rw,a-s
	find debian/libnitrokey-dev ! -type l -a -true -a -true -print0 2>/dev/null | xargs -0r chmod go=rX,u+rw,a-s
	find debian/libnitrokey-common/usr/share/doc -type f -a -true -a ! -regex 'debian/libnitrokey-common/usr/share/doc/[^/]*/examples/.*' -print0 2>/dev/null | xargs -0r chmod 0644
	find debian/libnitrokey3/usr/share/doc -type f -a -true -a ! -regex 'debian/libnitrokey3/usr/share/doc/[^/]*/examples/.*' -print0 2>/dev/null | xargs -0r chmod 0644
	find debian/libnitrokey-dev/usr/share/doc -type f -a -true -a ! -regex 'debian/libnitrokey-dev/usr/share/doc/[^/]*/examples/.*' -print0 2>/dev/null | xargs -0r chmod 0644
	find debian/libnitrokey-common/usr/share/doc -type d -a -true -a -true -print0 2>/dev/null | xargs -0r chmod 0755
	find debian/libnitrokey3/usr/share/doc -type d -a -true -a -true -print0 2>/dev/null | xargs -0r chmod 0755
	find debian/libnitrokey-dev/usr/share/doc -type d -a -true -a -true -print0 2>/dev/null | xargs -0r chmod 0755
	find debian/libnitrokey-common/usr/share/lintian/overrides -type f -a -true -a -true -print0 2>/dev/null | xargs -0r chmod 0644
	find debian/libnitrokey3 -type f \( -name '*.so.*' -o -name '*.so' -o -name '*.la' -o -name '*.a' -o -name '*.js' -o -name '*.css' -o -name '*.scss' -o -name '*.sass' -o -name '*.jpeg' -o -name '*.jpg' -o -name '*.png' -o -name '*.gif' -o -name '*.cmxs' -o -name '*.node' \) -a -true -a -true -print0 2>/dev/null | xargs -0r chmod 0644
	find debian/libnitrokey-dev/usr/include -type f -a -true -a -true -print0 2>/dev/null | xargs -0r chmod 0644
	find debian/libnitrokey-common -type f \( -name '*.so.*' -o -name '*.so' -o -name '*.la' -o -name '*.a' -o -name '*.js' -o -name '*.css' -o -name '*.scss' -o -name '*.sass' -o -name '*.jpeg' -o -name '*.jpg' -o -name '*.png' -o -name '*.gif' -o -name '*.cmxs' -o -name '*.node' \) -a -true -a -true -print0 2>/dev/null | xargs -0r chmod 0644
	find debian/libnitrokey3/usr/lib -type f -name '*.ali' -a -true -a -true -print0 2>/dev/null | xargs -0r chmod uga-w
	find debian/libnitrokey-dev -type f \( -name '*.so.*' -o -name '*.so' -o -name '*.la' -o -name '*.a' -o -name '*.js' -o -name '*.css' -o -name '*.scss' -o -name '*.sass' -o -name '*.jpeg' -o -name '*.jpg' -o -name '*.png' -o -name '*.gif' -o -name '*.cmxs' -o -name '*.node' \) -a -true -a -true -print0 2>/dev/null | xargs -0r chmod 0644
	find debian/libnitrokey-common/usr/lib -type f -name '*.ali' -a -true -a -true -print0 2>/dev/null | xargs -0r chmod uga-w
	find debian/libnitrokey-dev/usr/lib -type f -name '*.ali' -a -true -a -true -print0 2>/dev/null | xargs -0r chmod uga-w
   dh_missing
   dh_dwz
	dwz -- debian/libnitrokey3/usr/lib/x86_64-linux-gnu/libnitrokey.so.3.6.0
   dh_strip
	rm -fr debian/.debhelper/libnitrokey-dev/dbgsym-root
	install -d debian/.debhelper/libnitrokey3/dbgsym-root/usr/lib/debug/.build-id/29
	objcopy --only-keep-debug --compress-debug-sections debian/libnitrokey3/usr/lib/x86_64-linux-gnu/libnitrokey.so.3.6.0 debian/.debhelper/libnitrokey3/dbgsym-root/usr/lib/debug/.build-id/29/b1c75de4ce919d9c569c93fab40ada0849cf2f.debug
	chmod 0644 -- debian/.debhelper/libnitrokey3/dbgsym-root/usr/lib/debug/.build-id/29/b1c75de4ce919d9c569c93fab40ada0849cf2f.debug
	chown 0:0 -- debian/.debhelper/libnitrokey3/dbgsym-root/usr/lib/debug/.build-id/29/b1c75de4ce919d9c569c93fab40ada0849cf2f.debug
	strip --remove-section=.comment --remove-section=.note --strip-unneeded debian/libnitrokey3/usr/lib/x86_64-linux-gnu/libnitrokey.so.3.6.0
	objcopy --add-gnu-debuglink debian/.debhelper/libnitrokey3/dbgsym-root/usr/lib/debug/.build-id/29/b1c75de4ce919d9c569c93fab40ada0849cf2f.debug debian/libnitrokey3/usr/lib/x86_64-linux-gnu/libnitrokey.so.3.6.0
	rm -fr debian/.debhelper/libnitrokey3/dbgsym-root
   dh_makeshlibs
	rm -f debian/libnitrokey-dev/DEBIAN/shlibs
	rm -f debian/libnitrokey3/DEBIAN/shlibs
	install -d debian/libnitrokey3/DEBIAN
	echo "libnitrokey 3 libnitrokey3 (>= 3.6)" >> debian/libnitrokey3/DEBIAN/shlibs
	chmod 0644 -- debian/libnitrokey3/DEBIAN/shlibs
	chown 0:0 -- debian/libnitrokey3/DEBIAN/shlibs
	dpkg-gensymbols -plibnitrokey3 -Idebian/libnitrokey3.symbols -Pdebian/libnitrokey3 -edebian/libnitrokey3/usr/lib/x86_64-linux-gnu/libnitrokey.so.3.6.0
	mv debian/.debhelper/generated/libnitrokey3/triggers.new debian/.debhelper/generated/libnitrokey3/triggers
	rm -f debian/libnitrokey-common/DEBIAN/shlibs
   dh_shlibdeps
	dpkg-shlibdeps -Tdebian/libnitrokey3.substvars debian/libnitrokey3/usr/lib/x86_64-linux-gnu/libnitrokey.so.3.6.0
   dh_installdeb
	install -d debian/libnitrokey-dev/DEBIAN
	install -p -m0644 debian/.debhelper/generated/libnitrokey3/triggers debian/libnitrokey3/DEBIAN/triggers
	install -d debian/libnitrokey-common/DEBIAN
   dh_gencontrol
	echo misc:Depends= >> debian/libnitrokey-dev.substvars
	echo misc:Pre-Depends= >> debian/libnitrokey-dev.substvars
	dpkg-gencontrol -plibnitrokey-dev -ldebian/changelog -Tdebian/libnitrokey-dev.substvars -Pdebian/libnitrokey-dev
	echo misc:Depends= >> debian/libnitrokey-common.substvars
	echo misc:Pre-Depends= >> debian/libnitrokey-common.substvars
	dpkg-gencontrol -plibnitrokey-common -ldebian/changelog -Tdebian/libnitrokey-common.substvars -Pdebian/libnitrokey-common
	echo misc:Depends= >> debian/libnitrokey3.substvars
	echo misc:Pre-Depends= >> debian/libnitrokey3.substvars
	dpkg-gencontrol -plibnitrokey3 -ldebian/changelog -Tdebian/libnitrokey3.substvars -Pdebian/libnitrokey3
	chmod 0644 -- debian/libnitrokey-common/DEBIAN/control
	chown 0:0 -- debian/libnitrokey-common/DEBIAN/control
	chmod 0644 -- debian/libnitrokey3/DEBIAN/control
	chown 0:0 -- debian/libnitrokey3/DEBIAN/control
	chmod 0644 -- debian/libnitrokey-dev/DEBIAN/control
	chown 0:0 -- debian/libnitrokey-dev/DEBIAN/control
   dh_md5sums
	cd debian/libnitrokey-dev >/dev/null && xargs -r0 md5sum | perl -pe 'if (s@^\\@@) { s/\\\\/\\/g; }' > DEBIAN/md5sums
	cd debian/libnitrokey3 >/dev/null && xargs -r0 md5sum | perl -pe 'if (s@^\\@@) { s/\\\\/\\/g; }' > DEBIAN/md5sums
	cd debian/libnitrokey-common >/dev/null && xargs -r0 md5sum | perl -pe 'if (s@^\\@@) { s/\\\\/\\/g; }' > DEBIAN/md5sums
	chmod 0644 -- debian/libnitrokey-common/DEBIAN/md5sums
	chown 0:0 -- debian/libnitrokey-common/DEBIAN/md5sums
	chmod 0644 -- debian/libnitrokey-dev/DEBIAN/md5sums
	chown 0:0 -- debian/libnitrokey-dev/DEBIAN/md5sums
	chmod 0644 -- debian/libnitrokey3/DEBIAN/md5sums
	chown 0:0 -- debian/libnitrokey3/DEBIAN/md5sums
   dh_builddeb
	dpkg-deb --build debian/libnitrokey-dev ..
	dpkg-deb --build debian/libnitrokey3 ..
	dpkg-deb --build debian/libnitrokey-common ..
dpkg-deb: building package 'libnitrokey-dev' in '../libnitrokey-dev_3.6-1pureos1_amd64.deb'.
dpkg-deb: building package 'libnitrokey-common' in '../libnitrokey-common_3.6-1pureos1_all.deb'.
dpkg-deb: building package 'libnitrokey3' in '../libnitrokey3_3.6-1pureos1_amd64.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../libnitrokey_3.6-1pureos1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload (original source is included)
+ date
Tue Apr 18 18:26:00 UTC 2023
+ cd ..
+ ls -la
total 820
drwxr-xr-x  3 root root   4096 Apr 18 18:26 .
drwxr-xr-x  3 root root   4096 Apr 18 18:24 ..
-rw-r--r--  1 root root  33982 Apr 18 18:26 buildlog.txt
drwxr-xr-x 10 root root   4096 Apr 18 18:25 libnitrokey-3.6
-rw-r--r--  1 root root   6636 Apr 18 18:25 libnitrokey-common_3.6-1pureos1_all.deb
-rw-r--r--  1 root root  39484 Apr 18 18:25 libnitrokey-dev_3.6-1pureos1_amd64.deb
-rw-r--r--  1 root root 227672 Apr 18 18:26 libnitrokey3_3.6-1pureos1_amd64.deb
-rw-r--r--  1 root root  10584 Apr 18 18:24 libnitrokey_3.6-1pureos1.debian.tar.xz
-rw-r--r--  1 root root   1293 Apr 18 18:24 libnitrokey_3.6-1pureos1.dsc
-rw-r--r--  1 root root   7804 Apr 18 18:26 libnitrokey_3.6-1pureos1_amd64.buildinfo
-rw-r--r--  1 root root   2738 Apr 18 18:26 libnitrokey_3.6-1pureos1_amd64.changes
-rw-r--r--  1 root root 479284 Jun  3  2021 libnitrokey_3.6.orig.tar.xz
+ find . -maxdepth 1 -type f
+ sha256sum ./buildlog.txt ./libnitrokey-common_3.6-1pureos1_all.deb ./libnitrokey_3.6.orig.tar.xz ./libnitrokey_3.6-1pureos1.dsc ./libnitrokey_3.6-1pureos1_amd64.buildinfo ./libnitrokey3_3.6-1pureos1_amd64.deb ./libnitrokey_3.6-1pureos1_amd64.changes ./libnitrokey-dev_3.6-1pureos1_amd64.deb ./libnitrokey_3.6-1pureos1.debian.tar.xz
a7fdd3dfb65b3a353f2ca5cf88b524177f50844a425b1482025d1049d7cad13e  ./buildlog.txt
a4f4130694744a403cf89b4e61c50475a556c59884cd87c1549dfe53da8d8b19  ./libnitrokey-common_3.6-1pureos1_all.deb
09b480cb87e9ecea6ed06dd8b5e4c56f80e86688fca63a3cc4041e6067091c27  ./libnitrokey_3.6.orig.tar.xz
6d7e56b768b37e688b308e9c20bcf962359afa3f005d1108b5f230820fe4f870  ./libnitrokey_3.6-1pureos1.dsc
3c3791f5cb052955ce1b35766bd004aa2fb9926e1add1d95af7f13ea36d7a802  ./libnitrokey_3.6-1pureos1_amd64.buildinfo
2e8622802a3203ee1ae5b002737d488e0d3b30b0fb92b95047b31e962a7f4a27  ./libnitrokey3_3.6-1pureos1_amd64.deb
54841e490bc8a5e4d122358d938fb6ae6189b0fcbd4bb62e9d7068d792b9cb8c  ./libnitrokey_3.6-1pureos1_amd64.changes
eaec7ca4073076f8901845765992b34c401dfe42dc4c05e659a46d73f82f9494  ./libnitrokey-dev_3.6-1pureos1_amd64.deb
df1edef52b2514a5037e2beffd61e1c654ba7097cd6b508b3996aebef53b08a2  ./libnitrokey_3.6-1pureos1.debian.tar.xz
+ mkdir published
+ cd published
+ cd ../
+ ls libnitrokey-common_3.6-1pureos1_all.deb libnitrokey-dev_3.6-1pureos1_amd64.deb libnitrokey3_3.6-1pureos1_amd64.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/libn/libnitrokey/libnitrokey-common_3.6-1pureos1_all.deb
--2023-04-18 18:26:00--  http://repo.pureos.net/pureos/pool/main/libn/libnitrokey/libnitrokey-common_3.6-1pureos1_all.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 6636 (6.5K) [application/octet-stream]
Saving to: 'libnitrokey-common_3.6-1pureos1_all.deb'

     0K ......                                                100%  184M=0s

2023-04-18 18:26:01 (184 MB/s) - 'libnitrokey-common_3.6-1pureos1_all.deb' saved [6636/6636]

+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/libn/libnitrokey/libnitrokey-dev_3.6-1pureos1_amd64.deb
--2023-04-18 18:26:01--  http://repo.pureos.net/pureos/pool/main/libn/libnitrokey/libnitrokey-dev_3.6-1pureos1_amd64.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 39484 (39K) [application/octet-stream]
Saving to: 'libnitrokey-dev_3.6-1pureos1_amd64.deb'

     0K .......... .......... .......... ........             100% 1.41M=0.03s

2023-04-18 18:26:01 (1.41 MB/s) - 'libnitrokey-dev_3.6-1pureos1_amd64.deb' saved [39484/39484]

+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/libn/libnitrokey/libnitrokey3_3.6-1pureos1_amd64.deb
--2023-04-18 18:26:01--  http://repo.pureos.net/pureos/pool/main/libn/libnitrokey/libnitrokey3_3.6-1pureos1_amd64.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 229596 (224K) [application/octet-stream]
Saving to: 'libnitrokey3_3.6-1pureos1_amd64.deb'

     0K .......... .......... .......... .......... .......... 22%  952K 0s
    50K .......... .......... .......... .......... .......... 44% 1.77M 0s
   100K .......... .......... .......... .......... .......... 66% 18.6M 0s
   150K .......... .......... .......... .......... .......... 89% 25.2M 0s
   200K .......... .......... ....                            100% 1.04M=0.1s

2023-04-18 18:26:01 (2.04 MB/s) - 'libnitrokey3_3.6-1pureos1_amd64.deb' saved [229596/229596]

+ tee+  ../SHA256SUMS
find . -maxdepth 1 -type f
+ sha256sum ./libnitrokey-common_3.6-1pureos1_all.deb ./libnitrokey3_3.6-1pureos1_amd64.deb ./libnitrokey-dev_3.6-1pureos1_amd64.deb
a4f4130694744a403cf89b4e61c50475a556c59884cd87c1549dfe53da8d8b19  ./libnitrokey-common_3.6-1pureos1_all.deb
d21e079c6710329e8191b87a5849e4a03f626194aa245f30ef1d49f56c771960  ./libnitrokey3_3.6-1pureos1_amd64.deb
eaec7ca4073076f8901845765992b34c401dfe42dc4c05e659a46d73f82f9494  ./libnitrokey-dev_3.6-1pureos1_amd64.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./libnitrokey-common_3.6-1pureos1_all.deb: OK
./libnitrokey3_3.6-1pureos1_amd64.deb: FAILED
./libnitrokey-dev_3.6-1pureos1_amd64.deb: OK
sha256sum: WARNING: 1 computed checksum did NOT match
