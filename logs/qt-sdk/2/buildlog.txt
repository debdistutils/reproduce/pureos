+ date
Wed Apr 19 20:29:20 UTC 2023
+ apt-get source --only-source qt-sdk=2
Reading package lists...
Need to get 2267 B of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main qt-sdk 2 (dsc) [854 B]
Get:2 http://repo.pureos.net/pureos byzantium/main qt-sdk 2 (tar) [1413 B]
dpkg-source: info: extracting qt-sdk in qt-sdk-2
dpkg-source: info: unpacking qt-sdk_2.tar.gz
Fetched 2267 B in 0s (16.9 kB/s)
W: Download is performed unsandboxed as root as file 'qt-sdk_2.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source qt-sdk=2
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name qt-sdk* -type d
+ cd ./qt-sdk-2
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package qt-sdk
dpkg-buildpackage: info: source version 2
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Fathi Boudra <fabo@debian.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh  clean
dh: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_clean
dh_clean: warning: Compatibility levels before 10 are deprecated (level 7 in use)
 dpkg-source -b .
dpkg-source: warning: no source format specified in debian/source/format, see dpkg-source(1)
dpkg-source: info: using source format '1.0'
dpkg-source: info: building qt-sdk in qt-sdk_2.tar.gz
dpkg-source: info: building qt-sdk in qt-sdk_2.dsc
 debian/rules build
dh  build
dh: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_update_autotools_config
 debian/rules binary
dh  binary
dh: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_testroot
   dh_prep
   dh_installdocs
dh_installdocs: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_installchangelogs
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
dh_compress: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_fixperms
   dh_missing
dh_missing: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_installdeb
dh_installdeb: warning: Compatibility levels before 10 are deprecated (level 7 in use)
   dh_gencontrol
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'qt-sdk' in '../qt-sdk_2_all.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../qt-sdk_2_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Wed Apr 19 20:29:25 UTC 2023
+ cd ..
+ ls -la
total 40
drwxr-xr-x 3 root root 4096 Apr 19 20:29 .
drwxr-xr-x 3 root root 4096 Apr 19 20:29 ..
-rw-r--r-- 1 root root 2871 Apr 19 20:29 buildlog.txt
drwxr-xr-x 3 root root 4096 Apr 20  2010 qt-sdk-2
-rw-r--r-- 1 root root  537 Apr 19 20:29 qt-sdk_2.dsc
-rw-r--r-- 1 root root 1413 Apr 19 20:29 qt-sdk_2.tar.gz
-rw-r--r-- 1 root root 2036 Apr 19 20:29 qt-sdk_2_all.deb
-rw-r--r-- 1 root root 5102 Apr 19 20:29 qt-sdk_2_amd64.buildinfo
-rw-r--r-- 1 root root 1390 Apr 19 20:29 qt-sdk_2_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./qt-sdk_2_all.deb ./buildlog.txt ./qt-sdk_2.tar.gz ./qt-sdk_2.dsc ./qt-sdk_2_amd64.changes ./qt-sdk_2_amd64.buildinfo
46388eb640acb9e487e75d21beb998183ed59e3e980dbda0b9085f2bc4e08b59  ./qt-sdk_2_all.deb
f6146c62a189a5311dc0a5f9e1d05684e2e57e8659536d3671ae09b249185b3b  ./buildlog.txt
52e307ecbbbfed2e6c99015634405e6c402bbce4e4c73b32037bc7dc4c4b65da  ./qt-sdk_2.tar.gz
322801e4c18f4c968cb2fefa1d10096321bf287c3cc512720586dc71b4de36e3  ./qt-sdk_2.dsc
9d62a464c8f2375f314f60a07cc03bd373ffe906eec5660ab01e37969bc4459c  ./qt-sdk_2_amd64.changes
32704f2ac1a1c9da5de2684c1b402df3b54afcf9d972ad0b00cde9086089f75f  ./qt-sdk_2_amd64.buildinfo
+ mkdir published
+ cd published
+ cd ../
+ ls qt-sdk_2_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/q/qt-sdk/qt-sdk_2_all.deb
--2023-04-19 20:29:25--  http://repo.pureos.net/pureos/pool/main/q/qt-sdk/qt-sdk_2_all.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 1974 (1.9K) [application/octet-stream]
Saving to: 'qt-sdk_2_all.deb'

     0K .                                                     100%  102M=0s

2023-04-19 20:29:25 (102 MB/s) - 'qt-sdk_2_all.deb' saved [1974/1974]

+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./qt-sdk_2_all.deb
ecdbb304f3923219b1fa8ffad713b90b8ac8d754624b7ca2b933b6c4fb2e3fa2  ./qt-sdk_2_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./qt-sdk_2_all.deb: FAILED
sha256sum: WARNING: 1 computed checksum did NOT match
