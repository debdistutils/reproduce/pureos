+ date
Tue Apr 18 10:00:22 UTC 2023
+ apt-get source --only-source golang-github-dataence-porter2=0.0~git20150829.56e4718-3
Reading package lists...
NOTICE: 'golang-github-dataence-porter2' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/med-team/golang-github-dataence-porter2.git
Please use:
git clone https://salsa.debian.org/med-team/golang-github-dataence-porter2.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 122 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main golang-github-dataence-porter2 0.0~git20150829.56e4718-3 (dsc) [2643 B]
Get:2 http://repo.pureos.net/pureos byzantium/main golang-github-dataence-porter2 0.0~git20150829.56e4718-3 (tar) [115 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main golang-github-dataence-porter2 0.0~git20150829.56e4718-3 (diff) [4044 B]
dpkg-source: info: extracting golang-github-dataence-porter2 in golang-github-dataence-porter2-0.0~git20150829.56e4718
dpkg-source: info: unpacking golang-github-dataence-porter2_0.0~git20150829.56e4718.orig.tar.xz
dpkg-source: info: unpacking golang-github-dataence-porter2_0.0~git20150829.56e4718-3.debian.tar.xz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: applying 0001-porter2_test.go-Formally-port-from-surge-glog-to-gol.patch
dpkg-source: info: applying auto-gitignore
Fetched 122 kB in 0s (260 kB/s)
W: Download is performed unsandboxed as root as file 'golang-github-dataence-porter2_0.0~git20150829.56e4718-3.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source golang-github-dataence-porter2=0.0~git20150829.56e4718-3
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  dh-golang golang-1.15-go golang-1.15-src golang-any
  golang-github-davecgh-go-spew-dev golang-github-pmezard-go-difflib-dev
  golang-github-stretchr-objx-dev golang-github-stretchr-testify-dev
  golang-glog-dev golang-go golang-gopkg-yaml.v3-dev golang-src
0 upgraded, 12 newly installed, 0 to remove and 0 not upgraded.
Need to get 61.5 MB of archives.
After this operation, 361 MB of additional disk space will be used.
Get:1 http://repo.pureos.net/pureos byzantium/main amd64 dh-golang all 1.51 [24.6 kB]
Get:2 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-src amd64 1.15.15-1~deb11u4 [13.9 MB]
Get:3 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-go amd64 1.15.15-1~deb11u4 [47.4 MB]
Get:4 http://repo.pureos.net/pureos byzantium/main amd64 golang-src amd64 2:1.15~1 [4856 B]
Get:5 http://repo.pureos.net/pureos byzantium/main amd64 golang-go amd64 2:1.15~1 [23.9 kB]
Get:6 http://repo.pureos.net/pureos byzantium/main amd64 golang-any amd64 2:1.15~1 [4976 B]
Get:7 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-davecgh-go-spew-dev all 1.1.1-2 [29.7 kB]
Get:8 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-pmezard-go-difflib-dev all 1.0.0-3 [12.3 kB]
Get:9 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-stretchr-objx-dev all 0.3.0-1 [25.4 kB]
Get:10 http://repo.pureos.net/pureos byzantium/main amd64 golang-gopkg-yaml.v3-dev all 3.0.0~git20200121.a6ecf24-3 [70.5 kB]
Get:11 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-stretchr-testify-dev all 1.6.1-2 [60.4 kB]
Get:12 http://repo.pureos.net/pureos byzantium/main amd64 golang-glog-dev all 0.0~git20160126.23def4e-3 [17.3 kB]
Fetched 61.5 MB in 3s (21.7 MB/s)
Selecting previously unselected package dh-golang.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 102989 files and directories currently installed.)
Preparing to unpack .../00-dh-golang_1.51_all.deb ...
Unpacking dh-golang (1.51) ...
Selecting previously unselected package golang-1.15-src.
Preparing to unpack .../01-golang-1.15-src_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-src (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-1.15-go.
Preparing to unpack .../02-golang-1.15-go_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-go (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-src:amd64.
Preparing to unpack .../03-golang-src_2%3a1.15~1_amd64.deb ...
Unpacking golang-src:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-go.
Preparing to unpack .../04-golang-go_2%3a1.15~1_amd64.deb ...
Unpacking golang-go (2:1.15~1) ...
Selecting previously unselected package golang-any:amd64.
Preparing to unpack .../05-golang-any_2%3a1.15~1_amd64.deb ...
Unpacking golang-any:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-github-davecgh-go-spew-dev.
Preparing to unpack .../06-golang-github-davecgh-go-spew-dev_1.1.1-2_all.deb ...
Unpacking golang-github-davecgh-go-spew-dev (1.1.1-2) ...
Selecting previously unselected package golang-github-pmezard-go-difflib-dev.
Preparing to unpack .../07-golang-github-pmezard-go-difflib-dev_1.0.0-3_all.deb ...
Unpacking golang-github-pmezard-go-difflib-dev (1.0.0-3) ...
Selecting previously unselected package golang-github-stretchr-objx-dev.
Preparing to unpack .../08-golang-github-stretchr-objx-dev_0.3.0-1_all.deb ...
Unpacking golang-github-stretchr-objx-dev (0.3.0-1) ...
Selecting previously unselected package golang-gopkg-yaml.v3-dev.
Preparing to unpack .../09-golang-gopkg-yaml.v3-dev_3.0.0~git20200121.a6ecf24-3_all.deb ...
Unpacking golang-gopkg-yaml.v3-dev (3.0.0~git20200121.a6ecf24-3) ...
Selecting previously unselected package golang-github-stretchr-testify-dev.
Preparing to unpack .../10-golang-github-stretchr-testify-dev_1.6.1-2_all.deb ...
Unpacking golang-github-stretchr-testify-dev (1.6.1-2) ...
Selecting previously unselected package golang-glog-dev.
Preparing to unpack .../11-golang-glog-dev_0.0~git20160126.23def4e-3_all.deb ...
Unpacking golang-glog-dev (0.0~git20160126.23def4e-3) ...
Setting up dh-golang (1.51) ...
Setting up golang-github-stretchr-objx-dev (0.3.0-1) ...
Setting up golang-1.15-src (1.15.15-1~deb11u4) ...
Setting up golang-gopkg-yaml.v3-dev (3.0.0~git20200121.a6ecf24-3) ...
Setting up golang-github-pmezard-go-difflib-dev (1.0.0-3) ...
Setting up golang-github-davecgh-go-spew-dev (1.1.1-2) ...
Setting up golang-github-stretchr-testify-dev (1.6.1-2) ...
Setting up golang-glog-dev (0.0~git20160126.23def4e-3) ...
Setting up golang-src:amd64 (2:1.15~1) ...
Setting up golang-1.15-go (1.15.15-1~deb11u4) ...
Setting up golang-go (2:1.15~1) ...
Setting up golang-any:amd64 (2:1.15~1) ...
Processing triggers for man-db (2.9.4-2) ...
+ find . -maxdepth 1 -name golang-github-dataence-porter2* -type d
+ cd ./golang-github-dataence-porter2-0.0~git20150829.56e4718
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package golang-github-dataence-porter2
dpkg-buildpackage: info: source version 0.0~git20150829.56e4718-3
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Aaron M. Ucko <ucko@debian.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --buildsystem=golang --with=golang
   dh_auto_clean -O--buildsystem=golang
   dh_autoreconf_clean -O--buildsystem=golang
   dh_clean -O--buildsystem=golang
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building golang-github-dataence-porter2 using existing ./golang-github-dataence-porter2_0.0~git20150829.56e4718.orig.tar.xz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: building golang-github-dataence-porter2 in golang-github-dataence-porter2_0.0~git20150829.56e4718-3.debian.tar.xz
dpkg-source: info: building golang-github-dataence-porter2 in golang-github-dataence-porter2_0.0~git20150829.56e4718-3.dsc
 debian/rules build
dh build --buildsystem=golang --with=golang
   dh_update_autotools_config -O--buildsystem=golang
   dh_autoreconf -O--buildsystem=golang
   dh_auto_configure -O--buildsystem=golang
   dh_auto_build -O--buildsystem=golang
	cd obj-x86_64-linux-gnu && go install -trimpath -v -p 50 github.com/dataence/porter2 github.com/dataence/porter2/cmd/suffixfsm github.com/dataence/porter2/cmd/switchvsmap
internal/unsafeheader
internal/race
unicode
unicode/utf8
runtime/internal/sys
math/bits
runtime/internal/atomic
internal/cpu
sync/atomic
runtime/internal/math
internal/testlog
internal/bytealg
math
runtime
github.com/dataence/porter2
internal/reflectlite
sync
errors
sort
io
internal/oserror
strconv
syscall
hash
bytes
strings
hash/crc32
reflect
bufio
internal/syscall/execenv
internal/syscall/unix
time
internal/poll
internal/fmtsort
encoding/binary
os
fmt
github.com/dataence/porter2/cmd/switchvsmap
log
flag
compress/flate
compress/gzip
github.com/dataence/porter2/cmd/suffixfsm
   dh_auto_test -O--buildsystem=golang
	cd obj-x86_64-linux-gnu && go test -vet=off -v -p 50 github.com/dataence/porter2 github.com/dataence/porter2/cmd/suffixfsm github.com/dataence/porter2/cmd/switchvsmap
=== RUN   TestEnglishStep0
--- PASS: TestEnglishStep0 (0.00s)
=== RUN   TestEnglishStep1a
--- PASS: TestEnglishStep1a (0.00s)
=== RUN   TestEnglishStep1b
--- PASS: TestEnglishStep1b (0.00s)
=== RUN   TestEnglishStep1c
--- PASS: TestEnglishStep1c (0.00s)
=== RUN   TestEnglishStep2
--- PASS: TestEnglishStep2 (0.00s)
=== RUN   TestEnglishStep3
--- PASS: TestEnglishStep3 (0.00s)
=== RUN   TestEnglishStep4
--- PASS: TestEnglishStep4 (0.00s)
=== RUN   TestEnglishStep5
--- PASS: TestEnglishStep5 (0.00s)
=== RUN   TestEnglishMarkR1R2
--- PASS: TestEnglishMarkR1R2 (0.00s)
=== RUN   TestEnglishIsShortWord
--- PASS: TestEnglishIsShortWord (0.00s)
=== RUN   TestEnglishExceptions1
--- PASS: TestEnglishExceptions1 (0.00s)
=== RUN   TestEnglishExceptions2
--- PASS: TestEnglishExceptions2 (0.00s)
=== RUN   TestEnglishVocOutput
--- PASS: TestEnglishVocOutput (0.11s)
PASS
ok  	github.com/dataence/porter2	0.122s
?   	github.com/dataence/porter2/cmd/suffixfsm	[no test files]
?   	github.com/dataence/porter2/cmd/switchvsmap	[no test files]
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary --buildsystem=golang --with=golang
   dh_testroot -O--buildsystem=golang
   dh_prep -O--buildsystem=golang
   dh_installdirs -O--buildsystem=golang
   dh_auto_install -O--buildsystem=golang
	cd obj-x86_64-linux-gnu && mkdir -p /build/golang-github-dataence-porter2/golang-github-dataence-porter2-0.0\~git20150829.56e4718/debian/tmp/usr
	cd obj-x86_64-linux-gnu && cp -r bin /build/golang-github-dataence-porter2/golang-github-dataence-porter2-0.0\~git20150829.56e4718/debian/tmp/usr
   dh_install -O--buildsystem=golang
   dh_installdocs -O--buildsystem=golang
   dh_installchangelogs -O--buildsystem=golang
   dh_installexamples -O--buildsystem=golang
   dh_installman -O--buildsystem=golang
   dh_installinit -O--buildsystem=golang
   dh_installsystemduser -O--buildsystem=golang
   dh_perl -O--buildsystem=golang
   dh_link -O--buildsystem=golang
   dh_strip_nondeterminism -O--buildsystem=golang
   dh_compress -O--buildsystem=golang
   dh_fixperms -O--buildsystem=golang
   dh_missing -O--buildsystem=golang
dh_missing: warning: usr/bin/switchvsmap exists in debian/tmp but is not installed to anywhere 
	The following debhelper tools have reported what they installed (with files per package)
	 * dh_install: golang-github-dataence-porter2-dev (1), suffixfsm (1)
	 * dh_installdocs: golang-github-dataence-porter2-dev (1), suffixfsm (1)
	 * dh_installexamples: golang-github-dataence-porter2-dev (0), suffixfsm (6)
	 * dh_installman: golang-github-dataence-porter2-dev (0), suffixfsm (1)
	If the missing files are installed by another tool, please file a bug against it.
	When filing the report, if the tool is not part of debhelper itself, please reference the
	"Logging helpers and dh_missing" section from the "PROGRAMMING" guide for debhelper (10.6.3+).
	  (in the debhelper package: /usr/share/doc/debhelper/PROGRAMMING.gz)
	Be sure to test with dpkg-buildpackage -A/-B as the results may vary when only a subset is built
	If the omission is intentional or no other helper can take care of this consider adding the
	paths to debian/not-installed.
   dh_dwz -O--buildsystem=golang
dwz: debian/suffixfsm/usr/bin/suffixfsm: .debug_info section not present
   dh_strip -O--buildsystem=golang
dh_strip: warning: Could not find the BuildID in debian/suffixfsm/usr/bin/suffixfsm
   dh_makeshlibs -O--buildsystem=golang
   dh_shlibdeps -O--buildsystem=golang
   dh_installdeb -O--buildsystem=golang
   dh_golang -O--buildsystem=golang
   dh_gencontrol -O--buildsystem=golang
dpkg-gencontrol: warning: Depends field of package suffixfsm: substitution variable ${shlibs:Depends} used, but is not defined
   dh_md5sums -O--buildsystem=golang
   dh_builddeb -O--buildsystem=golang
dpkg-deb: building package 'suffixfsm' in '../suffixfsm_0.0~git20150829.56e4718-3_amd64.deb'.
dpkg-deb: building package 'golang-github-dataence-porter2-dev' in '../golang-github-dataence-porter2-dev_0.0~git20150829.56e4718-3_all.deb'.
 dpkg-genbuildinfo
 dpkg-genchanges  >../golang-github-dataence-porter2_0.0~git20150829.56e4718-3_amd64.changes
dpkg-genchanges: info: not including original source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: binary and diff upload (original source NOT included)
+ date
Tue Apr 18 10:01:26 UTC 2023
+ cd ..
+ ls -la
total 820
drwxr-xr-x 3 root root   4096 Apr 18 10:01 .
drwxr-xr-x 3 root root   4096 Apr 18 10:00 ..
-rw-r--r-- 1 root root  14068 Apr 18 10:01 buildlog.txt
drwxr-xr-x 6 root root   4096 Apr 18 10:01 golang-github-dataence-porter2-0.0~git20150829.56e4718
-rw-r--r-- 1 root root 111348 Apr 18 10:01 golang-github-dataence-porter2-dev_0.0~git20150829.56e4718-3_all.deb
-rw-r--r-- 1 root root   3996 Apr 18 10:01 golang-github-dataence-porter2_0.0~git20150829.56e4718-3.debian.tar.xz
-rw-r--r-- 1 root root   1606 Apr 18 10:01 golang-github-dataence-porter2_0.0~git20150829.56e4718-3.dsc
-rw-r--r-- 1 root root   6352 Apr 18 10:01 golang-github-dataence-porter2_0.0~git20150829.56e4718-3_amd64.buildinfo
-rw-r--r-- 1 root root   2768 Apr 18 10:01 golang-github-dataence-porter2_0.0~git20150829.56e4718-3_amd64.changes
-rw-r--r-- 1 root root 115204 May 14  2018 golang-github-dataence-porter2_0.0~git20150829.56e4718.orig.tar.xz
-rw-r--r-- 1 root root 555256 Apr 18 10:01 suffixfsm_0.0~git20150829.56e4718-3_amd64.deb
+ find . -maxdepth 1 -type f
+ sha256sum ./buildlog.txt ./suffixfsm_0.0~git20150829.56e4718-3_amd64.deb ./golang-github-dataence-porter2_0.0~git20150829.56e4718-3_amd64.changes ./golang-github-dataence-porter2-dev_0.0~git20150829.56e4718-3_all.deb ./golang-github-dataence-porter2_0.0~git20150829.56e4718-3.debian.tar.xz ./golang-github-dataence-porter2_0.0~git20150829.56e4718-3.dsc ./golang-github-dataence-porter2_0.0~git20150829.56e4718.orig.tar.xz ./golang-github-dataence-porter2_0.0~git20150829.56e4718-3_amd64.buildinfo
1096d52b73bb8dd308e6e8c3112e99dc478768c1a1668487758c680208fd0f92  ./buildlog.txt
f824c15da748e53fe2ddf7d1425cedfd1a9f9aab5e3cf2a90d257f23e933fc4c  ./suffixfsm_0.0~git20150829.56e4718-3_amd64.deb
f1039b22dd2447da3942a49114b289ac5fae7ca85e1daf4b8f60a023bfa54697  ./golang-github-dataence-porter2_0.0~git20150829.56e4718-3_amd64.changes
b47cb8745ee87458efd1d35d69525e0573372e195c3905faf3920e83ef3c5ae6  ./golang-github-dataence-porter2-dev_0.0~git20150829.56e4718-3_all.deb
4d5b92c373773217debba91bd5d5e8339523b94f7b1dfc13e82488707c49fd2c  ./golang-github-dataence-porter2_0.0~git20150829.56e4718-3.debian.tar.xz
0eeea36414388bf8b37ea6d937f03df0aebef4037df75bb8d961c0f7f0e47bd7  ./golang-github-dataence-porter2_0.0~git20150829.56e4718-3.dsc
338143a3dbdfb4d2d00ca60f54b14fa0cba5a262527cbb1488322852e041eb47  ./golang-github-dataence-porter2_0.0~git20150829.56e4718.orig.tar.xz
1a4edfe402e7e00135d2448eead617b0f514327b98fe255bf0572ed532618904  ./golang-github-dataence-porter2_0.0~git20150829.56e4718-3_amd64.buildinfo
+ mkdir published
+ cd published
+ cd ../
+ ls golang-github-dataence-porter2-dev_0.0~git20150829.56e4718-3_all.deb suffixfsm_0.0~git20150829.56e4718-3_amd64.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/g/golang-github-dataence-porter2/golang-github-dataence-porter2-dev_0.0~git20150829.56e4718-3_all.deb
--2023-04-18 10:01:26--  http://repo.pureos.net/pureos/pool/main/g/golang-github-dataence-porter2/golang-github-dataence-porter2-dev_0.0~git20150829.56e4718-3_all.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 111348 (109K) [application/octet-stream]
Saving to: 'golang-github-dataence-porter2-dev_0.0~git20150829.56e4718-3_all.deb'

     0K .......... .......... .......... .......... .......... 45%  954K 0s
    50K .......... .......... .......... .......... .......... 91% 1.83M 0s
   100K ........                                              100% 18.8M=0.08s

2023-04-18 10:01:26 (1.34 MB/s) - 'golang-github-dataence-porter2-dev_0.0~git20150829.56e4718-3_all.deb' saved [111348/111348]

+ wget --retry-connrefused --retry-on-host-error --tries=3 http://repo.pureos.net/pureos/pool/main/g/golang-github-dataence-porter2/suffixfsm_0.0~git20150829.56e4718-3_amd64.deb
--2023-04-18 10:01:26--  http://repo.pureos.net/pureos/pool/main/g/golang-github-dataence-porter2/suffixfsm_0.0~git20150829.56e4718-3_amd64.deb
Resolving repo.pureos.net (repo.pureos.net)... 138.201.228.45
Connecting to repo.pureos.net (repo.pureos.net)|138.201.228.45|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 554856 (542K) [application/octet-stream]
Saving to: 'suffixfsm_0.0~git20150829.56e4718-3_amd64.deb'

     0K .......... .......... .......... .......... ..........  9%  948K 1s
    50K .......... .......... .......... .......... .......... 18% 1.83M 0s
   100K .......... .......... .......... .......... .......... 27% 14.1M 0s
   150K .......... .......... .......... .......... .......... 36% 2.14M 0s
   200K .......... .......... .......... .......... .......... 46% 24.6M 0s
   250K .......... .......... .......... .......... .......... 55% 24.0M 0s
   300K .......... .......... .......... .......... .......... 64% 17.0M 0s
   350K .......... .......... .......... .......... .......... 73% 15.6M 0s
   400K .......... .......... .......... .......... .......... 83% 2.53M 0s
   450K .......... .......... .......... .......... .......... 92% 16.2M 0s
   500K .......... .......... .......... .......... .         100% 14.5M=0.1s

2023-04-18 10:01:26 (3.76 MB/s) - 'suffixfsm_0.0~git20150829.56e4718-3_amd64.deb' saved [554856/554856]

+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./suffixfsm_0.0~git20150829.56e4718-3_amd64.deb ./golang-github-dataence-porter2-dev_0.0~git20150829.56e4718-3_all.deb
51935ec21030c91f10cb48dae8ade60db19225b9b724a0729311ef72e5e54005  ./suffixfsm_0.0~git20150829.56e4718-3_amd64.deb
b47cb8745ee87458efd1d35d69525e0573372e195c3905faf3920e83ef3c5ae6  ./golang-github-dataence-porter2-dev_0.0~git20150829.56e4718-3_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./suffixfsm_0.0~git20150829.56e4718-3_amd64.deb: FAILED
./golang-github-dataence-porter2-dev_0.0~git20150829.56e4718-3_all.deb: OK
sha256sum: WARNING: 1 computed checksum did NOT match
