+ date
Tue Apr 18 15:11:40 UTC 2023
+ apt-get source --only-source hub=2.14.2~ds1-1
Reading package lists...
NOTICE: 'hub' packaging is maintained in the 'Git' version control system at:
https://salsa.debian.org/go-team/packages/hub.git
Please use:
git clone https://salsa.debian.org/go-team/packages/hub.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 146 kB of source archives.
Get:1 http://repo.pureos.net/pureos byzantium/main hub 2.14.2~ds1-1 (dsc) [2473 B]
Get:2 http://repo.pureos.net/pureos byzantium/main hub 2.14.2~ds1-1 (tar) [138 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main hub 2.14.2~ds1-1 (diff) [5252 B]
dpkg-source: info: extracting hub in hub-2.14.2~ds1
dpkg-source: info: unpacking hub_2.14.2~ds1.orig.tar.xz
dpkg-source: info: unpacking hub_2.14.2~ds1-1.debian.tar.xz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: applying 0001-blackfriday-v2.patch
Fetched 146 kB in 0s (452 kB/s)
W: Download is performed unsandboxed as root as file 'hub_2.14.2~ds1-1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source hub=2.14.2~ds1-1
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  dh-exec dh-golang golang-1.15-go golang-1.15-src golang-any
  golang-github-atotto-clipboard-dev golang-github-bmizerany-assert-dev
  golang-github-burntsushi-toml-dev golang-github-creack-pty-dev
  golang-github-google-go-cmp-dev golang-github-kballard-go-shellquote-dev
  golang-github-kr-pretty-dev golang-github-kr-text-dev
  golang-github-mattn-go-colorable-dev golang-github-mattn-go-isatty-dev
  golang-github-mitchellh-go-homedir-dev golang-github-pmezard-go-difflib-dev
  golang-github-russross-blackfriday-v2-dev
  golang-github-shurcool-sanitized-anchor-name-dev golang-go
  golang-golang-x-crypto-dev golang-golang-x-net-dev golang-golang-x-sys-dev
  golang-golang-x-term-dev golang-golang-x-text-dev
  golang-golang-x-xerrors-dev golang-gopkg-yaml.v2-dev golang-src
0 upgraded, 28 newly installed, 0 to remove and 0 not upgraded.
Need to get 68.1 MB of archives.
After this operation, 414 MB of additional disk space will be used.
Get:1 http://repo.pureos.net/pureos byzantium/main amd64 dh-exec amd64 0.23.2 [26.6 kB]
Get:2 http://repo.pureos.net/pureos byzantium/main amd64 dh-golang all 1.51 [24.6 kB]
Get:3 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-src amd64 1.15.15-1~deb11u4 [13.9 MB]
Get:4 http://repo.pureos.net/pureos byzantium/main amd64 golang-1.15-go amd64 1.15.15-1~deb11u4 [47.4 MB]
Get:5 http://repo.pureos.net/pureos byzantium/main amd64 golang-src amd64 2:1.15~1 [4856 B]
Get:6 http://repo.pureos.net/pureos byzantium/main amd64 golang-go amd64 2:1.15~1 [23.9 kB]
Get:7 http://repo.pureos.net/pureos byzantium/main amd64 golang-any amd64 2:1.15~1 [4976 B]
Get:8 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-atotto-clipboard-dev all 0.1.2-1 [5196 B]
Get:9 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-creack-pty-dev all 1.1.11-1 [8380 B]
Get:10 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-kr-text-dev all 0.2.0-1 [11.1 kB]
Get:11 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-kr-pretty-dev all 0.2.1+git20200831.59b4212-1 [14.6 kB]
Get:12 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-bmizerany-assert-dev all 0.0~git20120716-4 [4068 B]
Get:13 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-burntsushi-toml-dev all 0.3.1-1 [38.5 kB]
Get:14 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-xerrors-dev all 0.0~git20191204.9bdfabe-1 [14.2 kB]
Get:15 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-google-go-cmp-dev all 0.5.4-1 [83.7 kB]
Get:16 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-kballard-go-shellquote-dev all 0.0~git20180428.95032a8-1 [6472 B]
Get:17 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-sys-dev all 0.0~git20210124.22da62e-1 [308 kB]
Get:18 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-mattn-go-isatty-dev all 0.0.12-1 [6472 B]
Get:19 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-mattn-go-colorable-dev all 0.1.7-1 [9936 B]
Get:20 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-mitchellh-go-homedir-dev all 1.1.0-1 [5168 B]
Get:21 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-pmezard-go-difflib-dev all 1.0.0-3 [12.3 kB]
Get:22 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-shurcool-sanitized-anchor-name-dev all 1.0.0-1 [3872 B]
Get:23 http://repo.pureos.net/pureos byzantium/main amd64 golang-github-russross-blackfriday-v2-dev all 2.0.1-3 [67.8 kB]
Get:24 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-text-dev all 0.3.6-1 [3857 kB]
Get:25 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-net-dev all 1:0.0+git20210119.5f4716e+dfsg-4 [659 kB]
Get:26 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-term-dev all 0.0~git20201210.2321bbc-1 [14.5 kB]
Get:27 http://repo.pureos.net/pureos byzantium/main amd64 golang-golang-x-crypto-dev all 1:0.0~git20201221.eec23a3-1 [1538 kB]
Get:28 http://repo.pureos.net/pureos byzantium/main amd64 golang-gopkg-yaml.v2-dev all 2.4.0-1 [61.3 kB]
Fetched 68.1 MB in 11s (6399 kB/s)
Selecting previously unselected package dh-exec.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 102989 files and directories currently installed.)
Preparing to unpack .../00-dh-exec_0.23.2_amd64.deb ...
Unpacking dh-exec (0.23.2) ...
Selecting previously unselected package dh-golang.
Preparing to unpack .../01-dh-golang_1.51_all.deb ...
Unpacking dh-golang (1.51) ...
Selecting previously unselected package golang-1.15-src.
Preparing to unpack .../02-golang-1.15-src_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-src (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-1.15-go.
Preparing to unpack .../03-golang-1.15-go_1.15.15-1~deb11u4_amd64.deb ...
Unpacking golang-1.15-go (1.15.15-1~deb11u4) ...
Selecting previously unselected package golang-src:amd64.
Preparing to unpack .../04-golang-src_2%3a1.15~1_amd64.deb ...
Unpacking golang-src:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-go.
Preparing to unpack .../05-golang-go_2%3a1.15~1_amd64.deb ...
Unpacking golang-go (2:1.15~1) ...
Selecting previously unselected package golang-any:amd64.
Preparing to unpack .../06-golang-any_2%3a1.15~1_amd64.deb ...
Unpacking golang-any:amd64 (2:1.15~1) ...
Selecting previously unselected package golang-github-atotto-clipboard-dev.
Preparing to unpack .../07-golang-github-atotto-clipboard-dev_0.1.2-1_all.deb ...
Unpacking golang-github-atotto-clipboard-dev (0.1.2-1) ...
Selecting previously unselected package golang-github-creack-pty-dev.
Preparing to unpack .../08-golang-github-creack-pty-dev_1.1.11-1_all.deb ...
Unpacking golang-github-creack-pty-dev (1.1.11-1) ...
Selecting previously unselected package golang-github-kr-text-dev.
Preparing to unpack .../09-golang-github-kr-text-dev_0.2.0-1_all.deb ...
Unpacking golang-github-kr-text-dev (0.2.0-1) ...
Selecting previously unselected package golang-github-kr-pretty-dev.
Preparing to unpack .../10-golang-github-kr-pretty-dev_0.2.1+git20200831.59b4212-1_all.deb ...
Unpacking golang-github-kr-pretty-dev (0.2.1+git20200831.59b4212-1) ...
Selecting previously unselected package golang-github-bmizerany-assert-dev.
Preparing to unpack .../11-golang-github-bmizerany-assert-dev_0.0~git20120716-4_all.deb ...
Unpacking golang-github-bmizerany-assert-dev (0.0~git20120716-4) ...
Selecting previously unselected package golang-github-burntsushi-toml-dev.
Preparing to unpack .../12-golang-github-burntsushi-toml-dev_0.3.1-1_all.deb ...
Unpacking golang-github-burntsushi-toml-dev (0.3.1-1) ...
Selecting previously unselected package golang-golang-x-xerrors-dev.
Preparing to unpack .../13-golang-golang-x-xerrors-dev_0.0~git20191204.9bdfabe-1_all.deb ...
Unpacking golang-golang-x-xerrors-dev (0.0~git20191204.9bdfabe-1) ...
Selecting previously unselected package golang-github-google-go-cmp-dev.
Preparing to unpack .../14-golang-github-google-go-cmp-dev_0.5.4-1_all.deb ...
Unpacking golang-github-google-go-cmp-dev (0.5.4-1) ...
Selecting previously unselected package golang-github-kballard-go-shellquote-dev.
Preparing to unpack .../15-golang-github-kballard-go-shellquote-dev_0.0~git20180428.95032a8-1_all.deb ...
Unpacking golang-github-kballard-go-shellquote-dev (0.0~git20180428.95032a8-1) ...
Selecting previously unselected package golang-golang-x-sys-dev.
Preparing to unpack .../16-golang-golang-x-sys-dev_0.0~git20210124.22da62e-1_all.deb ...
Unpacking golang-golang-x-sys-dev (0.0~git20210124.22da62e-1) ...
Selecting previously unselected package golang-github-mattn-go-isatty-dev.
Preparing to unpack .../17-golang-github-mattn-go-isatty-dev_0.0.12-1_all.deb ...
Unpacking golang-github-mattn-go-isatty-dev (0.0.12-1) ...
Selecting previously unselected package golang-github-mattn-go-colorable-dev.
Preparing to unpack .../18-golang-github-mattn-go-colorable-dev_0.1.7-1_all.deb ...
Unpacking golang-github-mattn-go-colorable-dev (0.1.7-1) ...
Selecting previously unselected package golang-github-mitchellh-go-homedir-dev.
Preparing to unpack .../19-golang-github-mitchellh-go-homedir-dev_1.1.0-1_all.deb ...
Unpacking golang-github-mitchellh-go-homedir-dev (1.1.0-1) ...
Selecting previously unselected package golang-github-pmezard-go-difflib-dev.
Preparing to unpack .../20-golang-github-pmezard-go-difflib-dev_1.0.0-3_all.deb ...
Unpacking golang-github-pmezard-go-difflib-dev (1.0.0-3) ...
Selecting previously unselected package golang-github-shurcool-sanitized-anchor-name-dev.
Preparing to unpack .../21-golang-github-shurcool-sanitized-anchor-name-dev_1.0.0-1_all.deb ...
Unpacking golang-github-shurcool-sanitized-anchor-name-dev (1.0.0-1) ...
Selecting previously unselected package golang-github-russross-blackfriday-v2-dev.
Preparing to unpack .../22-golang-github-russross-blackfriday-v2-dev_2.0.1-3_all.deb ...
Unpacking golang-github-russross-blackfriday-v2-dev (2.0.1-3) ...
Selecting previously unselected package golang-golang-x-text-dev.
Preparing to unpack .../23-golang-golang-x-text-dev_0.3.6-1_all.deb ...
Unpacking golang-golang-x-text-dev (0.3.6-1) ...
Selecting previously unselected package golang-golang-x-net-dev.
Preparing to unpack .../24-golang-golang-x-net-dev_1%3a0.0+git20210119.5f4716e+dfsg-4_all.deb ...
Unpacking golang-golang-x-net-dev (1:0.0+git20210119.5f4716e+dfsg-4) ...
Selecting previously unselected package golang-golang-x-term-dev.
Preparing to unpack .../25-golang-golang-x-term-dev_0.0~git20201210.2321bbc-1_all.deb ...
Unpacking golang-golang-x-term-dev (0.0~git20201210.2321bbc-1) ...
Selecting previously unselected package golang-golang-x-crypto-dev.
Preparing to unpack .../26-golang-golang-x-crypto-dev_1%3a0.0~git20201221.eec23a3-1_all.deb ...
Unpacking golang-golang-x-crypto-dev (1:0.0~git20201221.eec23a3-1) ...
Selecting previously unselected package golang-gopkg-yaml.v2-dev.
Preparing to unpack .../27-golang-gopkg-yaml.v2-dev_2.4.0-1_all.deb ...
Unpacking golang-gopkg-yaml.v2-dev (2.4.0-1) ...
Setting up golang-github-atotto-clipboard-dev (0.1.2-1) ...
Setting up dh-golang (1.51) ...
Setting up golang-gopkg-yaml.v2-dev (2.4.0-1) ...
Setting up golang-github-mitchellh-go-homedir-dev (1.1.0-1) ...
Setting up golang-github-creack-pty-dev (1.1.11-1) ...
Setting up golang-github-shurcool-sanitized-anchor-name-dev (1.0.0-1) ...
Setting up golang-1.15-src (1.15.15-1~deb11u4) ...
Setting up golang-github-burntsushi-toml-dev (0.3.1-1) ...
Setting up golang-golang-x-sys-dev (0.0~git20210124.22da62e-1) ...
Setting up golang-github-pmezard-go-difflib-dev (1.0.0-3) ...
Setting up dh-exec (0.23.2) ...
Setting up golang-golang-x-term-dev (0.0~git20201210.2321bbc-1) ...
Setting up golang-github-kr-text-dev (0.2.0-1) ...
Setting up golang-golang-x-text-dev (0.3.6-1) ...
Setting up golang-golang-x-xerrors-dev (0.0~git20191204.9bdfabe-1) ...
Setting up golang-src:amd64 (2:1.15~1) ...
Setting up golang-1.15-go (1.15.15-1~deb11u4) ...
Setting up golang-github-kr-pretty-dev (0.2.1+git20200831.59b4212-1) ...
Setting up golang-github-google-go-cmp-dev (0.5.4-1) ...
Setting up golang-github-mattn-go-isatty-dev (0.0.12-1) ...
Setting up golang-github-russross-blackfriday-v2-dev (2.0.1-3) ...
Setting up golang-go (2:1.15~1) ...
Setting up golang-github-mattn-go-colorable-dev (0.1.7-1) ...
Setting up golang-any:amd64 (2:1.15~1) ...
Setting up golang-golang-x-net-dev (1:0.0+git20210119.5f4716e+dfsg-4) ...
Setting up golang-github-bmizerany-assert-dev (0.0~git20120716-4) ...
Setting up golang-golang-x-crypto-dev (1:0.0~git20201221.eec23a3-1) ...
Setting up golang-github-kballard-go-shellquote-dev (0.0~git20180428.95032a8-1) ...
Processing triggers for man-db (2.9.4-2) ...
+ find . -maxdepth 1 -name hub* -type d
+ cd ./hub-2.14.2~ds1
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package hub
dpkg-buildpackage: info: source version 2.14.2~ds1-1
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Anthony Fok <foka@debian.org>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean --builddirectory=_build --buildsystem=golang --with=golang
   dh_auto_clean -O--builddirectory=_build -O--buildsystem=golang
   dh_autoreconf_clean -O--builddirectory=_build -O--buildsystem=golang
   dh_clean -O--builddirectory=_build -O--buildsystem=golang
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building hub using existing ./hub_2.14.2~ds1.orig.tar.xz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: building hub in hub_2.14.2~ds1-1.debian.tar.xz
dpkg-source: info: building hub in hub_2.14.2~ds1-1.dsc
 debian/rules binary
dh binary --builddirectory=_build --buildsystem=golang --with=golang
   dh_update_autotools_config -O--builddirectory=_build -O--buildsystem=golang
   dh_autoreconf -O--builddirectory=_build -O--buildsystem=golang
   dh_auto_configure -O--builddirectory=_build -O--buildsystem=golang
   dh_auto_build -O--builddirectory=_build -O--buildsystem=golang
	cd _build && go install -trimpath -v -p 50 github.com/github/hub github.com/github/hub/cmd github.com/github/hub/commands github.com/github/hub/coverage github.com/github/hub/fixtures github.com/github/hub/git github.com/github/hub/github github.com/github/hub/md2roff github.com/github/hub/md2roff-bin github.com/github/hub/ui github.com/github/hub/utils github.com/github/hub/version
golang.org/x/sys/internal/unsafeheader
internal/unsafeheader
internal/nettrace
encoding
runtime/internal/atomic
runtime/internal/sys
unicode/utf16
sync/atomic
unicode
internal/cpu
github.com/github/hub/version
container/list
math/bits
internal/race
vendor/golang.org/x/crypto/internal/subtle
crypto/subtle
vendor/golang.org/x/crypto/cryptobyte/asn1
runtime/cgo
unicode/utf8
crypto/internal/subtle
runtime/internal/math
internal/testlog
internal/bytealg
math
github.com/shurcooL/sanitized_anchor_name
runtime
internal/reflectlite
sync
internal/singleflight
math/rand
errors
sort
internal/oserror
io
strconv
vendor/golang.org/x/net/dns/dnsmessage
syscall
hash
crypto/internal/randutil
bytes
strings
hash/crc32
crypto/hmac
vendor/golang.org/x/crypto/hkdf
crypto
crypto/rc4
reflect
vendor/golang.org/x/text/transform
golang.org/x/text/transform
bufio
path
github.com/kballard/go-shellquote
html
regexp/syntax
internal/syscall/execenv
internal/syscall/unix
time
regexp
context
internal/poll
internal/fmtsort
encoding/binary
os
encoding/base64
crypto/md5
crypto/cipher
crypto/sha512
crypto/ed25519/internal/edwards25519
crypto/sha1
crypto/sha256
vendor/golang.org/x/crypto/poly1305
golang.org/x/sys/unix
encoding/pem
crypto/aes
vendor/golang.org/x/crypto/chacha20
crypto/des
path/filepath
os/signal
fmt
net
os/exec
io/ioutil
vendor/golang.org/x/sys/cpu
vendor/golang.org/x/crypto/chacha20poly1305
log
github.com/BurntSushi/toml
net/url
encoding/json
encoding/hex
golang.org/x/text/unicode/norm
vendor/golang.org/x/crypto/curve25519
compress/flate
mime
vendor/golang.org/x/net/http2/hpack
text/template/parse
github.com/github/hub/coverage
net/http/internal
mime/quotedprintable
github.com/russross/blackfriday/v2
gopkg.in/yaml.v2
math/big
vendor/golang.org/x/text/unicode/norm
github.com/atotto/clipboard
github.com/mitchellh/go-homedir
golang.org/x/text/unicode/bidi
vendor/golang.org/x/text/unicode/bidi
github.com/mattn/go-isatty
golang.org/x/term
compress/gzip
github.com/mattn/go-colorable
vendor/golang.org/x/text/secure/bidirule
github.com/github/hub/ui
golang.org/x/text/secure/bidirule
golang.org/x/crypto/ssh/terminal
golang.org/x/net/idna
github.com/github/hub/md2roff
text/template
github.com/github/hub/cmd
github.com/github/hub/utils
vendor/golang.org/x/net/idna
github.com/github/hub/git
github.com/github/hub/fixtures
crypto/rand
encoding/asn1
crypto/dsa
crypto/elliptic
crypto/ed25519
crypto/rsa
github.com/github/hub/md2roff-bin
vendor/golang.org/x/crypto/cryptobyte
crypto/x509/pkix
crypto/ecdsa
golang.org/x/net/http/httpproxy
vendor/golang.org/x/net/http/httpproxy
net/textproto
crypto/x509
vendor/golang.org/x/net/http/httpguts
mime/multipart
crypto/tls
net/http/httptrace
net/http
github.com/github/hub/github
github.com/github/hub/commands
github.com/github/hub
   debian/rules execute_after_dh_auto_build
make[1]: Entering directory '/build/hub/hub-2.14.2~ds1'
mkdir -p bin
cp -av _build/bin/hub bin/hub
'_build/bin/hub' -> 'bin/hub'
mv -v _build/bin/md2roff-bin bin/md2roff
renamed '_build/bin/md2roff-bin' -> 'bin/md2roff'
/usr/bin/make man-pages
make[2]: Entering directory '/build/hub/hub-2.14.2~ds1'
go: downloading github.com/atotto/clipboard v0.0.0-20171229224153-bc5958e1c833
go: downloading github.com/kballard/go-shellquote v0.0.0-20170619183022-cd60e84ee657
go: downloading golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
go: downloading golang.org/x/net v0.0.0-20191002035440-2ec189313ef0
go: downloading github.com/mattn/go-isatty v0.0.3
go: downloading github.com/mitchellh/go-homedir v0.0.0-20161203194507-b8bc1bf76747
go: downloading github.com/mattn/go-colorable v0.0.9
go: downloading github.com/russross/blackfriday v0.0.0-20180526075726-670777b536d3
go: downloading gopkg.in/yaml.v2 v2.0.0-20190319135612-7b8349ac747c
go: downloading github.com/BurntSushi/toml v0.3.0
go: finding module for package github.com/russross/blackfriday/v2
go: downloading github.com/russross/blackfriday v1.6.0
go: downloading github.com/russross/blackfriday/v2 v2.1.0
go: downloading golang.org/x/sys v0.0.0-20190531175056-4c3a928424d2
go: downloading golang.org/x/text v0.3.0
go: found github.com/russross/blackfriday/v2 in github.com/russross/blackfriday/v2 v2.1.0
bin/hub help hub-alias --plain-text >share/man/man1/hub-alias.1.md
bin/hub help hub-api --plain-text >share/man/man1/hub-api.1.md
bin/hub help hub-browse --plain-text >share/man/man1/hub-browse.1.md
bin/hub help hub-ci-status --plain-text >share/man/man1/hub-ci-status.1.md
bin/hub help hub-compare --plain-text >share/man/man1/hub-compare.1.md
bin/hub help hub-create --plain-text >share/man/man1/hub-create.1.md
bin/hub help hub-delete --plain-text >share/man/man1/hub-delete.1.md
bin/hub help hub-fork --plain-text >share/man/man1/hub-fork.1.md
bin/hub help hub-gist --plain-text >share/man/man1/hub-gist.1.md
bin/hub help hub-pr --plain-text >share/man/man1/hub-pr.1.md
bin/hub help hub-pull-request --plain-text >share/man/man1/hub-pull-request.1.md
bin/hub help hub-release --plain-text >share/man/man1/hub-release.1.md
bin/hub help hub-issue --plain-text >share/man/man1/hub-issue.1.md
bin/hub help hub-sync --plain-text >share/man/man1/hub-sync.1.md
bin/hub help hub-am --plain-text >share/man/man1/hub-am.1.md
bin/hub help hub-apply --plain-text >share/man/man1/hub-apply.1.md
bin/hub help hub-checkout --plain-text >share/man/man1/hub-checkout.1.md
bin/hub help hub-cherry-pick --plain-text >share/man/man1/hub-cherry-pick.1.md
bin/hub help hub-clone --plain-text >share/man/man1/hub-clone.1.md
bin/hub help hub-fetch --plain-text >share/man/man1/hub-fetch.1.md
bin/hub help hub-help --plain-text >share/man/man1/hub-help.1.md
bin/hub help hub-init --plain-text >share/man/man1/hub-init.1.md
bin/hub help hub-merge --plain-text >share/man/man1/hub-merge.1.md
bin/hub help hub-push --plain-text >share/man/man1/hub-push.1.md
bin/hub help hub-remote --plain-text >share/man/man1/hub-remote.1.md
bin/hub help hub-submodule --plain-text >share/man/man1/hub-submodule.1.md
bin/md2roff --manual="hub manual" \
	--date="18 Feb 2021" --version="hub version 2.14.2" \
	--template=./man-template.html \
	share/man/man1/*.md
mkdir -p share/doc/hub-doc
mv share/man/*/*.html share/doc/hub-doc/
touch share/man/.man-pages.stamp
groff -Wall -mtty-char -mandoc -Tutf8 -rLL=87n share/man/man1/hub.1 | col -b >share/man/man1/hub.1.txt
col: failed on line 213: Invalid or incomplete multibyte or wide character
make[2]: *** [Makefile:76: share/man/man1/hub.1.txt] Error 1
make[2]: Leaving directory '/build/hub/hub-2.14.2~ds1'
make[1]: *** [debian/rules:12: execute_after_dh_auto_build] Error 2
make[1]: Leaving directory '/build/hub/hub-2.14.2~ds1'
make: *** [debian/rules:6: binary] Error 2
dpkg-buildpackage: error: debian/rules binary subprocess returned exit status 2
