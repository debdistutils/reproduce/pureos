# Copyright (C) 2023 Simon Josefsson <simon@josefsson.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

FROM pureos/byzantium:latest

MAINTAINER Simon Josefsson <simon@josefsson.org>

RUN true
RUN grep security /etc/apt/sources.list | sed s,security,backports, > /etc/apt/sources.list.d/backports.list && \
    grep -h '^deb ' /etc/apt/sources.list /etc/apt/sources.list.d/* | sed 's/^deb /deb-src /' > /etc/apt/sources.list.d/src.list && \
    apt-get update && \
    apt-get install -q -yy fakeroot strace && \
    id && \
    mkdir -p /foo && \
    mount -t tmpfs none /foo && \
    umount /foo && \
    rmdir /foo && \
    mknod /foo c 1 2 && \
    fakeroot strace cp -a /foo /bar && \
    ls -l /foo /bar && \
    rm -f /foo /bar && \
    apt-get dist-upgrade -q -y && \
    sha256sum /var/lib/apt/lists/*InRelease && \
    grep ^Date /var/lib/apt/lists/*InRelease && \
    env DEBIAN_FRONTEND=noninteractive apt-get install -q -y \
	sbuild schroot debootstrap eatmydata zstd git && \
    useradd debdistreproduce && \
    dir=/var/cache/ccache-sbuild && \
    install --group=sbuild --mode=2775 -d $dir && \
    echo $dir $dir none rw,bind 0 0 >> /etc/schroot/sbuild/fstab && \
    printf '%s\n' '#!/bin/sh' "export CCACHE_DIR=$dir" 'export CCACHE_UMASK=002' 'export CCACHE_COMPRESS=1' 'unset CCACHE_HARDLINK' 'export PATH="/usr/lib/ccache:$PATH"' 'exec "\$@"' > $dir/sbuild-setup && \
    chmod a+rx $dir/sbuild-setup && \
    addgroup debdistreproduce sbuild && \
    newgrp sbuild && \
    git clone https://gitlab.trisquel.org/trisquel/trisquel-builder.git && \
    sed -i 's:cp -a:fakeroot cp -a:' trisquel-builder/sbuild-create.sh && \
    sed -i 's:sbuild-update:fakeroot sbuild-update:' trisquel-builder/sbuild-create.sh && \
    bash -x trisquel-builder/sbuild-create.sh aramo amd64 && \
    mount && \
    sbuild-update -udcar aramo-amd64 && \
    fakeroot schroot -c aramo-amd64 -- /usr/bin/ls / && \
    apt-get source libidn && \
    fakeroot sbuild --no-run-lintian -v -A --dist aramo --arch amd64 libidn*.dsc
